/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at
 
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */
 
 
//
//  AppDelegate.m
//  Mi Claro
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//  Author Ing. Samuel Lozada
 
 
#import "AppDelegate.h"
#import "MainViewController.h"
 
 
 
 
@implementation AppDelegate
 
 
 
 
- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    self.viewController = [[MainViewController alloc] init];
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}
 
 
-(BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler{
 
 
    if ([userActivity.activityType isEqualToString: NSUserActivityTypeBrowsingWeb]) {
 
 
        NSURL *url = userActivity.webpageURL;
        NSString *path2 = url.path;
       
        NSMutableString *rediercURL = [NSMutableString stringWithString:@"miclaropr:/"];
            [rediercURL appendString:path2];
        NSLog(@"%@", rediercURL);
         
        path = rediercURL;
         
        UIApplicationState *state = [[UIApplication sharedApplication] applicationState];
                if (state == UIApplicationStateInactive) {
                    NSLog(@" la  app estaba cerrada ");
                     
                    [NSTimer scheduledTimerWithTimeInterval:1.0
                                                         target:self
                                                   selector:@selector (theAction)
                                                       userInfo:nil
                                                        repeats:NO];
                } else {
                     
                    [NSTimer scheduledTimerWithTimeInterval:5.0
                                                         target:self
                                                   selector:@selector (theAction)
                                                       userInfo:nil
                                                        repeats:NO];
                     
                }
     
    }
         
     
    return YES;
 
 
}
 
 
 
 
-(void) theAction{
     
  NSLog(@"Will appear after a X second delay.");
  NSLog( @" ------ redirect -----*" );
       NSURL *url = [NSURL URLWithString:path];
       // Get "UIApplication" class name through ASCII Character codes.
       NSString *className = [[NSString alloc] initWithData:[NSData dataWithBytes:(unsigned char []){0x55, 0x49, 0x41, 0x70, 0x70, 0x6C, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6F, 0x6E} length:13] encoding:NSASCIIStringEncoding];
       if (NSClassFromString(className)) {
           id object = [NSClassFromString(className) performSelector:@selector(sharedApplication)];
           [object performSelector:@selector(openURL:) withObject:url];
       }
}
 
 
 
 
@end

