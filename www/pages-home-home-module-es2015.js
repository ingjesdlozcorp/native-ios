(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/dashboard/dashboard.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/dashboard/dashboard.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-popup-gift-received\n        *ngIf=\"showPopupGiftReceived\"\n        [product]=\"product\"\n        (close)=\"closePopup1GB()\">\n</app-popup-gift-received>\n\n<ion-content #content class=\"allcont logsize\" style=\"overflow-y: auto; overflow-x: hidden\">\n\n    <app-header></app-header>\n\n    <app-menu></app-menu>\n\n    <div class=\"basicrow m-top-i lessm\">\n        <div *ngIf=\"typeBan.postpaid || typeBan.telephony\" class=\"container\">\n            <div class=\"autowcont dashapp m-top\">\n                <div class=\"basicrow msidespad\">\n                    <div class=\"b-title\">\n                        <div class=\"autocont f-black roboto-m f-bmed pull-left vcenter\">\n                            <div class=\"tabcell\">\n                                Balance\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow msidespad m-top-u-i\">\n\n                    <div class=\"balancesptr-i\">\n                        <label class=\"label-r\">N&uacute;mero de Cuenta</label>\n\n                        <div class=\"basicrow\">\n                            <app-account-select></app-account-select>\n                        </div>\n                    </div>\n\n                    <div class=\"balancesptr-iicont refdef\">\n                        <div class=\"balancesptr-ii text-center\">\n                      <span class=\"roboto-b\">\n                          Balance Pendiente\n                      </span>\n                            <br/>\n                            <span class=\"f-red f-lsemi roboto-b\">\n                          &#36;{{billBalance}}\n                      </span>\n                        </div>\n\n                        <div class=\"balancesptr-ii no-brd-i text-center\">\n                      <span class=\"roboto-b\">\n                          Su Factura Vence\n                      </span>\n                            <br/>\n                            <span class=\"f-black f-big roboto-b\">\n                          {{billDueDate}}\n                      </span>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-bott visible-md visible-sm\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow m-bott visible-xs\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"balancesptr-iv refdef f-reg roboto-r text-center\">\n                        <div class=\"basicrow m-top-u\">\n                            Fecha de Factura<br/>\n                            <span class=\"roboto-b\">\n                          {{billDate}}\n                      </span>\n                        </div>\n                    </div>\n\n                    <div class=\"balancesptr-iv refdef no-brd f-reg roboto-r text-center\">\n                        <div class=\"basicrow m-top-u\">\n                            &Uacute;ltimo Pago<br/>\n                            <span class=\"roboto-b\">\n                          &#36;{{lastPayment}}\n                      </span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div *ngIf=\"typeBan.prepaid\" class=\"container\">\n            <div class=\"autowcont dashapp m-top\">\n                <div class=\"basicrow msidespad\">\n                    <div class=\"b-title\">\n                        <div class=\"autocont f-black roboto-m f-bmed pull-left vcenter\">\n                            <div class=\"tabcell\">\n                                Balance en Prepago\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div *ngIf=\"typeBan.prepaid\" class=\"basicrow msidespad m-top-u-i\">\n        <div class=\"balancesptr-ii fortimes rfpad text-left\">\n            <label class=\"label-f\">N&uacute;mero de Tel&eacute;fono</label>\n\n            <div class=\"basicrow\">\n                <select class=\"sel-f\" [(ngModel)]=\"selectedSubscriber\" (change)=\"changePrepaidSubscriber()\">\n                    <option *ngFor=\"let subscriber of subscribers\" [ngValue]=\"subscriber\">\n                        {{subscriber.subscriberNumberField}}\n                    </option>\n                </select>\n            </div>\n        </div>\n\n        <div class=\"balancesptr-ii fortimes h-nobrd-i text-center\">\n\t\t\t<span class=\"roboto-b\">\n\t\t\t\tBalance en Prepago\n\t\t\t</span>\n            <br/>\n            <span class=\"f-red f-lsemi roboto-b\">\n\t\t\t\t&#36;{{getPrepaidBalance()}}\n\t\t\t</span>\n        </div>\n\n        <div class=\"basicrow visible-sm m-bott\">\n            <div class=\"logline full\"></div>\n        </div>\n\n        <div class=\"balancesptr-ii text-center\">\n\t\t\t<span class=\"roboto-b\">\n\t\t\t\tPr&oacute;xima Recarga\n\t\t\t</span>\n            <br/>\n            <span class=\"f-black f-big roboto-b\">\n\t\t\t\t{{getNextRecharge()}}\n\t\t\t</span>\n        </div>\n    </div>\n\n    <div *ngIf=\"typeBan.prepaid\" class=\"basicrow m-top-i lessm bg-gray hidden\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n                    <div class=\"autowcont dashapp m-top\">\n                        <div class=\"basicrow msidespad\">\n                            <div class=\"b-title\">\n                                <div class=\"autocont f-black roboto-m f-bmed pull-left vcenter\">\n                                    <div class=\"tabcell\">\n                                        Balance en Mi Claro <span class=\"f-red\">Wallet</span>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n\n                        <div class=\"basicrow msidespad m-top-u-i\">\n                            <div class=\"balancesptr-ii fullon text-center\">\n                                <span class=\"roboto-b\">\n                                    Cr&eacute;ditos Disponibles\n                                </span>\n                                <br/>\n                                <span class=\"f-red f-lsemi roboto-b\">\n                                    &#36;{{ getWalletBalance() }}\n                                </span>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n<!--    <div class=\"basicrow\" [ngClass]=\"{'bg-gray': !typeBan.prepaid}\">-->\n    <div class=\"basicrow bg-gray\">\n        <div class=\"container\">\n            <div class=\"autowcont dashapp m-top-i\">\n                <div class=\"basicrow msidespad\">\n                    <div class=\"b-title\">\n                        <div class=\"autocont f-black roboto-m f-bmed pull-left vcenter\">\n                            <div class=\"tabcell\">\n                                Opciones de Pago\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow msidespad m-top-u-i\">\n\n                    <div *ngIf=\"typeBan.postpaid\">\n\n                        <div class=\"balancesptr-i refdef text-center\">\n                            <span class=\"roboto-b\">\n                                Descuentos Disponibles\n                            </span>\n                            <br/>\n                            <span class=\"f-blue f-lsemi roboto-b\">\n                                {{discount}}\n                            </span>\n                        </div>\n\n                        <div class=\"basicrow m-bott visible-xs\">\n                            <div class=\"logline full\"></div>\n                        </div>\n\n                        <div class=\"balancesptr-iii refdef\">\n                            <label class=\"label-r roboto-b\">Monto de Descuento a Aplicar</label>\n\n                            <div class=\"basicrow rel\">\n                                <img alt=\"\" [popper]=\"'Deberá tener este monto disponible en sus creditos.'\"\n                                     [popperTrigger]=\"'click'\"\n                                     [popperTimeoutAfterShow]=\"3000\"\n                                     class=\"q-t-pay\" src=\"assets/images/tooltip1.png\">\n\n                                <input value=\"{{creditAmount}}\" class=\"inp-f payup qt\" readonly>\n\n\n                                <ion-button [disabled]=\"!canApplyCredits\"\n                                            class=\"btns payup vcenter\"\n                                            color=\"secondary\" expand=\"full\" (click)=\"applyCredits()\">\n                                    Aplicar\n                                </ion-button>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div *ngIf=\"typeBan.telephony\">\n\n                        <div class=\"balancesptr-iii refdef text-center\">\n                            <label class=\"label-r roboto-b\">Descuento Disponibles</label>\n\n                            <div class=\"basicrow rel\">\n                                <img alt=\"\" [popper]=\"'El Monto aplicable de credito es el 50% de su Balance Pendiente.'\"\n                                     [popperTrigger]=\"'click'\"\n                                     [popperTimeoutAfterShow]=\"3000\"\n                                     class=\"q-t-credits-telephony\" src=\"assets/images/tooltip1.png\">\n                            </div>\n\n                            <span class=\"f-blue f-lsemi roboto-b\">\n\t\t\t\t\t\t\t    {{discount}}\n\t\t\t\t\t\t    </span>\n                        </div>\n\n                        <div class=\"balancesptr-iii refdef\">\n\n                            <div class=\"basicrow\">\n                                <ion-button [disabled]=\"!canApplyCredits\"\n                                            class=\"btns payup vcenter\"\n                                            color=\"secondary\" expand=\"full\" (click)=\"applyCredits()\">\n                                    Aplicar a Renta Mensual\n                                </ion-button>\n                            </div>\n                        </div>\n\n                    </div>\n\n                    <div class=\"basicrow m-bott visible-xs\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"balancesptr-iii refdef no-brd\">\n                        <label *ngIf=\"!typeBan.prepaid\" class=\"label-r roboto-b\">\n                            Monto a Pagar (&#36;)\n                        </label>\n                        <label *ngIf=\"typeBan.prepaid\" class=\"label-r roboto-b\">\n                            Monto a Recargar (&#36;)\n                        </label>\n\n                        <div class=\"basicrow rel\">\n                            <img alt=\"\" *ngIf=\"!typeBan.prepaid\" [popper]=\"'Ahora puedes abonar hasta $800.00. Si la cantidad indica CR, significa que usted tiene cr&eacute;dito en su cuenta.'\"\n                                 [popperTrigger]=\"'click'\"\n                                 [popperTimeoutAfterShow]=\"5000\"\n                                 class=\"q-t-pay\" src=\"assets/images/tooltip1.png\">\n                            <img alt=\"\" *ngIf=\"typeBan.prepaid\" [popper]=\"'Podras recargar entre $5.00 y $150.00 a tu n&uacute;mero prepago.'\"\n                                 [popperTrigger]=\"'click'\"\n                                 [popperTimeoutAfterShow]=\"3000\"\n                                 class=\"q-t-pay\" src=\"assets/images/tooltip1.png\">\n\n                            <input  [(ngModel)]=\"amountPayable\" value=\"{{amountPayable}}\" class=\"inp-f payup qt\" (keyup.enter)=\"typeBan.prepaid? recharge() : billPayment()\">\n\n                            <ion-button *ngIf=\"!typeBan.prepaid\"\n                                        class=\"btns payup vcenter\"\n                                        color=\"primary\" expand=\"full\" (click)=\"billPayment()\">\n                                Pagar\n                            </ion-button>\n                            <ion-button *ngIf=\"typeBan.prepaid\"\n                                        class=\"btns payup vcenter\"\n                                        color=\"primary\" expand=\"full\" (click)=\"recharge()\">\n                                Recargar\n                            </ion-button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-6\" *ngIf=\"!isGuest\">\n                    <div class=\"dashdirect-digf\">\n\n                        <img alt=\"\" class=\"digf-def-i\" width=\"100%\" src=\"assets/images/fact-dig.png\">\n\n                        <!--[INIT ESTADO SUBSCRITO]-->\n                        <div class=\"autocont nofloat alrd-mleft\" *ngIf=\"paperless\">\n                            <div class=\"autocont f-green-i vcenter\">\n                                <div class=\"tabcell\">\n                                    <span class=\"f-newfdig\">&nbsp; Suscrito a Factura Electr&oacute;nica&nbsp;</span>\n                                </div>\n                            </div>\n                        </div>\n\n                        <div class=\"autocont nofloat\" *ngIf=\"paperless\">\n                            <div class=\"autocont vcenter\">\n                                <div class=\"tabcell\">\n                                    <img alt=\"\" class=\"fdig-check\" width=\"100%\" src=\"assets/images/fact-dig-check.png\">\n                                </div>\n                            </div>\n                        </div>\n                        <!---[END ESTADO SUBSCRITO]-->\n\n\n                        <!--[INIT ESTADO NO SUBSCRITO]-->\n                        <div class=\"autocont nofloat m-septrt\" *ngIf=\"!paperless\">\n                            <div class=\"autocont vcenter\">\n                                <div class=\"tabcell\">\n                                    Factura Electr&oacute;nica:\n                                </div>\n                            </div>\n                        </div>\n\n                        <div class=\"autocont nofloat m-septrt\" *ngIf=\"!paperless\">\n                            <div class=\"autocont vcenter\">\n                                <div class=\"tabcell\">\n                                    <label class=\"switch m-left-i\">\n                                        <input type=\"checkbox\"\n                                               [value]=\"check\"\n                                               [(ngModel)]=\"check\"\n                                               (click)=\"changePaperless(!check)\">\n                                        <span class=\"slider\"></span>\n                                    </label>\n                                </div>\n                            </div>\n                        </div>\n                        <!--[INIT ESTADO NO SUBSCRITO]-->\n\n                    </div>\n                </div>\n\n                <div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-6\">\n                    <a class=\"dashdirect-chat vcenter\" (click)=\"openChat()\">\n                        <img alt=\"\" class=\"direct-chat-i\" width=\"100%\" src=\"assets/images/chatnow-icon.png\">\n\n                        <span class=\"tabcell\">\n                          Chatea con Nosotros\n                      </span>\n                    </a>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow m-top-i\">\n\n        <div class=\"row dashboard\">\n\n            <div class=\"dashconts {{getExtraClass(26)}}\" (click)=\"going(26)\" *ngIf=\"checkAccessPermission(26)\">\n                <div class=\"dashbox gray vcenter\">\n                      <span class=\"tabcell\">\n                          <span class=\"icon-dash-24\"></span>\n                          Recarga\n                      </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(9)}}\" (click)=\"going(9)\" *ngIf=\"checkAccessPermission(9)\">\n                <div class=\"dashbox gray vcenter\">\n                      <span class=\"tabcell\">\n                          <span class=\"icon-dash-01\"></span>\n                          Factura y Pago\n                      </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(14)}}\" (click)=\"going(14)\" *ngIf=\"checkAccessPermission(14) && typeBan.postpaid\">\n                <div class=\"dashbox blue vcenter\">\n                      <span class=\"tabcell\">\n                          <span class=\"icon-dash-02\"></span>\n                          Consumo de Voz, Mensajer&iacute;a y Roaming\n                      </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(14)}}\" (click)=\"going(14)\" *ngIf=\"checkAccessPermission(14) && typeBan.postpaid\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-03\"></span>\n                        Consumo de Datos\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(28)}}\" (click)=\"going(28)\" *ngIf=\"checkAccessPermission(28)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-02\"></span>\n                        Mi Consumo\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(16)}}\" (click)=\"going(16)\" *ngIf=\"checkAccessPermission(16)\">\n                <div class=\"dashbox white vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-25\"></span>\n                        Mis Servicios\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(34)}}\" (click)=\"going(34)\" *ngIf=\"checkAccessPermission(34)\">\n                <div class=\"dashbox white vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-25\"></span>\n                        Mis Servicios\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(21)}}\" (click)=\"going(21)\" *ngIf=\"checkAccessPermission(21)\">\n                <div class=\"dashbox blue vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-08\"></span>\n                        Compra de Data Adicional\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(17)}}\" (click)=\"going(17)\" *ngIf=\"checkAccessPermission(17)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-05\"></span>\n                        Cambio de Plan\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(35)}}\" (click)=\"going(35)\" *ngIf=\"checkAccessPermission(35)\">\n                <div class=\"dashbox blue vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-05\"></span>\n                        Cambio de Plan\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(33)}}\" (click)=\"going(33)\" *ngIf=\"checkAccessPermission(33)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-05\"></span>\n                        Reporta Interrupci&oacute;n\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(20)}}\" (click)=\"going(20)\" *ngIf=\"checkAccessPermission(20)\">\n                <div class=\"dashbox white vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-07\"></span>\n                        Tienda\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(24)}}\" (click)=\"going(24)\" *ngIf=\"checkAccessPermission(24)\">\n                <div class=\"dashbox gray vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-23\"></span>\n                        Regala 1 GB a un Amigo/a o Familiar Postpago​\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(25)}}\" (click)=\"going(25)\" *ngIf=\"checkAccessPermission(25)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-10\"></span>\n                        Regala una Recarga Prepago\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(13)}}\" (click)=\"going(13)\" *ngIf=\"checkAccessPermission(13)\">\n                <div class=\"dashbox gray vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-11\"></span>\n                        Factura Digital\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(2)}}\" (click)=\"going(2)\" *ngIf=\"checkAccessPermission(2)\">\n                <div class=\"dashbox blue vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-14\"></span>\n                        Configuraci&oacute;n de Cuenta\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(18)}}\" (click)=\"going(18)\" *ngIf=\"checkAccessPermission(18)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-16\"></span>\n                        Netflix\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(36)}}\" (click)=\"going(36)\" *ngIf=\"checkAccessPermission(36)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-16\"></span>\n                        Netflix\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(19)}}\" (click)=\"going(19)\" *ngIf=\"checkAccessPermission(19)\">\n                <div class=\"dashbox blue vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-21\"></span>\n                        Programa de Referidos\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(31)}}\" (click)=\"going(31)\" *ngIf=\"checkAccessPermission(31)\">\n                <div class=\"dashbox blue vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-21\"></span>\n                        Programa de Referidos\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(37)}}\" (click)=\"going(37)\" *ngIf=\"checkAccessPermission(37)\">\n                <div class=\"dashbox blue vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-21\"></span>\n                        Programa de Referidos\n                    </span>\n                </div>\n            </div>\n\n\n            <div class=\"dashconts {{getExtraClass(321)}}\" (click)=\"going(321)\" *ngIf=\"checkAccessPermission(321)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n\t\t\t\t\t\t<span><img src=\"assets/images/icon_club.png\" style=\"height: 70px;display: block; margin: 0 auto 10px;\"></span>\n\t\t\t\t\t</span>\n                </div>\n            </div>\n\n        </div>\n    </div>\n\n    <div class=\"basicrow m-top-i m-bott\" id=\"banner-home\" (click)=\"going(321)\"  *ngIf=\"!typeBan.prepaid\">\n\t\t<div class=\"container\">\n            <img *ngIf=\"platform.android\" width=\"100%\" src=\"assets/images/claro_club_banner_android.jpg\">\n            <img *ngIf=\"platform.ios\" width=\"100%\" src=\"assets/images/claro_club_banner_ios.jpg\">\n\t\t</div>\n    </div>\n\n    <div class=\"basicrow m-top-i m-bott padding-bottom-footer\" *ngIf=\"typeBan.prepaid\">\n        <img alt=\"\" width=\"100%\" src=\"assets/images/banner-prepago.jpg\">\n    </div>\n\n</ion-content>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n    <app-header></app-header>\n    <app-menu></app-menu>\n    <router-outlet></router-outlet>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/no-associated/no-associated.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/no-associated/no-associated.component.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header></app-header>\n\n    <app-menu></app-menu>\n\n    <div class=\"basicrow mbott\">\n        <div class=\"container\">\n            <div class=\"basicrow m-top-i roboto-r text-center\">\n                <div class=\"shortcont\">\n                    <div class=\"basicrow f-black text-center noacc-icon\">\n                        <i class=\"fa fa-frown-o\" aria-hidden=\"true\"></i>\n                    </div>\n\n                    <div class=\"basicrow f-reg f-black m-top-ii roboto-b text-center\">\n                        No tienes productos Claro en este segmento.\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"btns red rippleR ascbtn-i centr vcenter\" (click)=\"openAccountsManage()\">\n                            <span class=\"tabcell\">\n                                Agrega tus Productos Claro Aquí\n                            </span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>");

/***/ }),

/***/ "./src/app/pages/home/dashboard/dashboard.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/home/dashboard/dashboard.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".fdig-check {\n  width: 27px;\n  margin-top: 11px;\n}\n\n.q-t-credits-telephony {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  margin: auto;\n  cursor: pointer;\n  z-index: 80;\n  width: 25px;\n  height: 25px;\n  right: 40%;\n}\n\n@media (max-width: 768px) {\n  .q-t-credits-telephony {\n    top: 5px;\n    right: 8px;\n    margin: inherit;\n  }\n}\n\nion-button.btns.payup {\n  width: 100%;\n  margin-top: 20px;\n  border-radius: 3px;\n  -webkit-border-radius: 3px;\n  -moz-border-radius: 3px;\n}\n\n.balancesptr-iii.refdef.no-brd {\n  width: 100%;\n}\n\n.inp-f.payup.qt {\n  width: 100%;\n  border: 1px solid #cccccc;\n  font-size: 14px;\n  padding: 10px 12px;\n}\n\nimg.q-t-pay {\n  top: 5px;\n  right: 8px;\n  margin: inherit;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZG1pbi9Eb2N1bWVudHMvRSRHUy9SRUJSTkFESU5HL1Jlc3BhbGRvcy9pb25pYy9taWNsYXJvMy1pb25pYy12ZXJzaW9uL3NyYy9hcHAvcGFnZXMvaG9tZS9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy9ob21lL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUNDSjs7QURFQTtFQUNJO0lBQ0ksUUFBQTtJQUNBLFVBQUE7SUFDQSxlQUFBO0VDQ047QUFDRjs7QURFQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsMEJBQUE7RUFDQSx1QkFBQTtBQ0FKOztBREdBO0VBQ0ksV0FBQTtBQ0FKOztBREdBO0VBQ0ksV0FBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDQUo7O0FER0E7RUFDSSxRQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mZGlnLWNoZWNrIHtcbiAgICB3aWR0aDogMjdweDtcbiAgICBtYXJnaW4tdG9wOiAxMXB4O1xufVxuXG4ucS10LWNyZWRpdHMtdGVsZXBob255IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGJvdHRvbTogMDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHotaW5kZXg6IDgwO1xuICAgIHdpZHRoOiAyNXB4O1xuICAgIGhlaWdodDogMjVweDtcbiAgICByaWdodDogNDAlO1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgICAucS10LWNyZWRpdHMtdGVsZXBob255IHtcbiAgICAgICAgdG9wOiA1cHg7XG4gICAgICAgIHJpZ2h0OiA4cHg7XG4gICAgICAgIG1hcmdpbjogaW5oZXJpdDtcbiAgICB9XG59XG5cbmlvbi1idXR0b24uYnRucy5wYXl1cCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgLW1vei1ib3JkZXItcmFkaXVzOiAzcHg7XG59XG5cbi5iYWxhbmNlc3B0ci1paWkucmVmZGVmLm5vLWJyZCB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5pbnAtZi5wYXl1cC5xdCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjY2NjYztcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgcGFkZGluZzogMTBweCAxMnB4O1xufVxuXG5pbWcucS10LXBheSB7XG4gICAgdG9wOiA1cHg7XG4gICAgcmlnaHQ6IDhweDtcbiAgICBtYXJnaW46IGluaGVyaXQ7XG59IiwiLmZkaWctY2hlY2sge1xuICB3aWR0aDogMjdweDtcbiAgbWFyZ2luLXRvcDogMTFweDtcbn1cblxuLnEtdC1jcmVkaXRzLXRlbGVwaG9ueSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIG1hcmdpbjogYXV0bztcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB6LWluZGV4OiA4MDtcbiAgd2lkdGg6IDI1cHg7XG4gIGhlaWdodDogMjVweDtcbiAgcmlnaHQ6IDQwJTtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5xLXQtY3JlZGl0cy10ZWxlcGhvbnkge1xuICAgIHRvcDogNXB4O1xuICAgIHJpZ2h0OiA4cHg7XG4gICAgbWFyZ2luOiBpbmhlcml0O1xuICB9XG59XG5pb24tYnV0dG9uLmJ0bnMucGF5dXAge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDNweDtcbiAgLW1vei1ib3JkZXItcmFkaXVzOiAzcHg7XG59XG5cbi5iYWxhbmNlc3B0ci1paWkucmVmZGVmLm5vLWJyZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uaW5wLWYucGF5dXAucXQge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjY2NjYztcbiAgZm9udC1zaXplOiAxNHB4O1xuICBwYWRkaW5nOiAxMHB4IDEycHg7XG59XG5cbmltZy5xLXQtcGF5IHtcbiAgdG9wOiA1cHg7XG4gIHJpZ2h0OiA4cHg7XG4gIG1hcmdpbjogaW5oZXJpdDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/home/dashboard/dashboard.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/home/dashboard/dashboard.component.ts ***!
  \*************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var _models_access_filter__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../models/access.filter */ "./src/app/models/access.filter.ts");
/* harmony import */ var src_app_utils_const_keys__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/app/utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _utils_const_appConstants__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../utils/const/appConstants */ "./src/app/utils/const/appConstants.ts");
/* harmony import */ var _services_redirect_provider__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../services/redirect.provider */ "./src/app/services/redirect.provider.ts");
/* harmony import */ var _services_browser_provider__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../services/browser.provider */ "./src/app/services/browser.provider.ts");















let DashboardComponent = class DashboardComponent extends _base_page__WEBPACK_IMPORTED_MODULE_8__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage, redirectProvider, browserProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.redirectProvider = redirectProvider;
        this.browserProvider = browserProvider;
        this.accounts = [];
        this.subscribers = [];
        this.billBalance = '';
        this.lastPayment = '';
        this.billDueDate = '';
        this.billDate = '';
        this.amountPayable = '';
        this.check = false;
        this.paperless = false;
        this.typeBan = {
            postpaid: false,
            prepaid: false,
            telephony: false,
            byop: false
        };
        this.access = [];
        this.showPopupGiftReceived = false;
        this.isGuest = false;
        this.platform = _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].getPlatformInfo();
        this.prepaidAddress = [];
        this.prepaidPayments = [];
        this.prepaidProducts = [];
        this.totalAvailable = 0;
        this.creditsToApply = 0;
        this.tempAvailableCredits = 0;
        this.checkAvailableCredits = false;
        this.canApplyCredits = false;
        this.discount = '$0.00';
        this.creditAmount = '0.00';
        this.productId = _utils_const_appConstants__WEBPACK_IMPORTED_MODULE_12__["APP"].PRODUCT_ID_INVOICE_PAYMENTS;
        this.merchantId = '';
        this.utils.registerScreen('dashboard');
    }
    ngOnInit() {
        const data = this.cacheStorage();
        this.isGuest = data.isGuest;
        this.typeBan = {
            postpaid: data.isPostpaidAccount,
            prepaid: data.isPrepaidAccount,
            telephony: data.isTelephonyAccount,
            byop: data.isByop
        };
        this.access = data.access;
        this.billBalance = String(data.accountInfo.billBalanceField);
        this.lastPayment = _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(String(data.accountInfo.lastPaymentAmountField));
        this.billDueDate = data.accountInfo.billDueDateField;
        this.billDate = data.accountInfo.billDateField;
        const amountDue = data.accountInfo.billBalanceField;
        let amountPayable = amountDue.includes('CR') ? '0' : amountDue;
        amountPayable = parseFloat(String(amountPayable
            .replace(',', '')))
            .toFixed(2);
        this.amountPayable = amountPayable;
        this.paperless = data.accountInfo.paperlessField;
        this.check = this.paperless;
        this.subscribers = data.subscriberList;
    }
    ngAfterViewInit() {
        this.showProgress();
        this.loadData().then(() => this.dismissProgress());
    }
    checkIfBanIsSuspend() {
        const accountInfo = this.cacheStorage().accountInfo;
        if (accountInfo.banStatusField === 'S' && !this.cacheStorage().hasConfirmDue) {
            this.showConfirmCustom('Aviso', 'Su cuenta esta suspendida. Para activar la misma, favor realice su pago.', 'Pagar', 'Cancelar', () => {
                this.getCreditCardPaymentOptions(String(accountInfo.bANField), accountInfo.defaultSubscriberField, this.amountPayable);
            }, () => {
                this.cacheStorage().hasConfirmDue = true;
            });
        }
    }
    loadData() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.typeBan.postpaid) {
                yield this.get1GBReceived();
            }
            if (this.typeBan.prepaid) {
                this.selectedSubscriber = this.cacheStorage().getFullCurrentSelectedSubscriber();
            }
            else {
                yield this.getUserCredits();
            }
            this.checkIfBanIsSuspend();
            this.redirectProvider.checkIfRedirectIsWaiting();
        });
    }
    getUserCredits() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.services.getCredits(String(this.cacheStorage().accountInfo.bANField), this.cacheStorage().tokenSession).then((response) => {
                this.totalAvailable = 0;
                if (response.CreditAsReferer !== null && response.CreditAsReferer !== undefined) {
                    if (response.CreditAsReferer.discount !== null && response.CreditAsReferer.discount !== undefined) {
                        this.totalAvailable = response.CreditAsReferer.discount;
                    }
                }
                if (response.CreditItems !== null && response.CreditItems !== undefined) {
                    if (response.CreditItems.length > 0) {
                        if (response.CreditItems[0].TotalAvailable > 0) {
                            if (response.CreditItems[0].TotalAvailable > this.totalAvailable) {
                                this.totalAvailable = response.CreditItems[0].TotalAvailable;
                            }
                        }
                    }
                }
                let mountToCompare = 50;
                if (this.typeBan.prepaid || this.typeBan.byop) {
                    mountToCompare = 25;
                }
                if (this.totalAvailable >= mountToCompare) {
                    this.creditsToApply = mountToCompare;
                }
                else {
                    this.creditsToApply = 0;
                }
                let discountType = '$';
                if (this.typeBan.telephony) {
                    discountType = '%';
                    this.discount = this.creditsToApply + ' ' + discountType;
                }
                else {
                    this.discount = discountType + '' + _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(this.totalAvailable);
                    this.creditAmount = discountType + '' + _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(this.creditsToApply);
                }
                this.canApplyCredits = this.totalAvailable > 0;
                if (this.checkAvailableCredits) {
                    this.checkAvailableCredits = false;
                    if (this.tempAvailableCredits === this.totalAvailable) {
                        this.showAlert('Aviso', 'Gracias por su interés en nuestro Programa Refiere y Gana! Su ' +
                            'balance se estará actualizando próximamente.');
                    }
                    else {
                        this.showProgress();
                        this.reloadCurrentAccount().then(() => {
                            this.dismissProgress();
                            this.goHomePage();
                        }, error => {
                            this.dismissProgress();
                            this.showError(error.message);
                        });
                    }
                }
                this.tempAvailableCredits = this.totalAvailable;
            }, error => {
                this.dismissProgress();
            });
        });
    }
    applyCredits() {
        const accountInfo = this.cacheStorage().accountInfo;
        const paperless = accountInfo.paperlessField;
        let amount = 50;
        if (!this.typeBan.telephony) {
            amount = parseFloat(String(this.creditAmount.replace('$', '')));
        }
        const billBalance = accountInfo.billBalanceField;
        let debt = billBalance.includes('CR') ? 0 : billBalance;
        let creditsAvailable = this.totalAvailable;
        debt = parseFloat(String(debt));
        creditsAvailable = parseFloat(String(creditsAvailable));
        if (!paperless) {
            this.showError('Para participar en nuestro Programa Refiere y Gana, debe suscribirse a facturación ' +
                'electrónica. Para detalles, verifique los términos y condiciones de este Programa.');
        }
        else if ((this.typeBan.postpaid) && (billBalance.includes('CR') || parseFloat(billBalance) === 0)) {
            this.showError('Para aplicar su cupón de descuento deberá tener un balance pendiente.');
        }
        else if (creditsAvailable === 0) {
            this.showError('En este momento no cuenta con cupones disponibles.');
        }
        else if (this.typeBan.telephony) {
            this.showConfirmCustom('Confirmación', 'Al aplicar su cupón, el mismo se verá reflejado en su próximo ciclo de facturación.', 'Aplicar', 'Regresar', () => {
                this.applyCreditsToAccount(amount);
            }, () => { });
        }
        else if (creditsAvailable < amount) {
            this.showError('En este momento no cuenta con cupones disponibles.');
        }
        else if (this.typeBan.prepaid || amount === debt) {
            this.showConfirmCustom('Confirmación', 'Una vez acepte redimir su cupón el mismo perderá su validez, este no podrá ser\n' +
                'cancelado, reversado, reembolsado o transferido.', 'Aplicar', 'Regresar', () => {
                this.applyCreditsToAccount(amount);
            }, () => { });
        }
        else if (amount > debt) {
            this.showConfirmCustom('Confirmación', 'El valor del cupón (descuento) redimido es mayor al balance pendiente de su factura, al ' +
                'redimir este cupón usted perderá el valor del descuento restante.', 'Aplicar', 'Regresar', () => {
                this.applyCreditsToAccount(amount);
            }, () => { });
        }
        else if (amount < debt) {
            this.showConfirmCustom('Confirmación', 'El valor del cupón (descuento) redimido es menor al balance pendiente de su factura. Favor ' +
                'de realizar el pago remanente de su factura en o antes de la fecha de vencimiento.', 'Aplicar', 'Regresar', () => {
                this.applyCreditsToAccount(amount);
            }, () => { });
        }
    }
    applyCreditsToAccount(amount) {
        const accountInfo = this.cacheStorage().accountInfo;
        this.showProgress();
        this.services.applyCredits(String(accountInfo.bANField), accountInfo.defaultSubscriberField, amount, this.cacheStorage().tokenSession).then((response) => {
            this.showAlert(response.errorDisplay, () => {
                this.checkAvailableCredits = true;
                this.getUserCredits();
            }, 'Continuar');
        }, error => {
            this.dismissProgress();
            this.showError(error.message);
        });
    }
    changePaperless(check) {
        if (check) {
            const data = this.cacheStorage();
            setTimeout(() => {
                this.showProgress();
                this.services.updateBillParameters(String(data.accountInfo.bANField), data.tokenSession).then(() => {
                    this.reloadCurrentAccount().then((response) => {
                        this.dismissProgress();
                        this.paperless = true;
                        this.check = true;
                    }, error => {
                        this.dismissProgress();
                        this.showError(error.message);
                    });
                }, error => {
                    this.dismissProgress();
                    this.showError(error.message);
                    this.paperless = false;
                    this.check = false;
                });
            }, 500);
        }
    }
    checkAccessPermission(id) {
        let permitted = false;
        this.access.forEach((section) => {
            section.Pages.forEach((page) => {
                if (page.accessID === id) {
                    permitted = true;
                    return permitted;
                }
            });
        });
        return permitted;
    }
    getExtraClass(id) {
        let classE = '';
        this.access.forEach((section) => {
            section.Pages.forEach((page) => {
                if (page.accessID === id) {
                    classE = page.extraClass;
                    return classE;
                }
            });
        });
        return classE;
    }
    going(id) {
        const page = this.cacheStorage().getAccessPageByID(id);
        if (this.cacheStorage().isGuest && !page.allowAsGuest) {
            this.showAlertAccessLimited();
        }
        else {
            if (id === 20) {
                this.openStore();
            }
            else {
                if (!this.isGuest && !this.services.isAccountDetailsUpdated() && this.pageIsNeedingSubscribers(page)) {
                    const account = this.cacheStorage().accountInfo.bANField;
                    const subscriber = this.cacheStorage().accountInfo.defaultSubscriberField;
                    this.showProgress();
                    this.services.loadAccount(String(account), subscriber, this.cacheStorage().tokenSession, this.cacheStorage().isGuest)
                        .then(() => {
                        this.dismissProgress();
                        this.services.setAccountDetailsUpdate();
                        this.filterAndGo(page);
                    });
                }
                else {
                    this.filterAndGo(page);
                }
            }
        }
    }
    pageIsNeedingSubscribers(page) {
        for (const route of _models_access_filter__WEBPACK_IMPORTED_MODULE_10__["AccessFilter"].routesToGo) {
            if (page.accessID === route.accessID && route.path !== '') {
                if (route.needSubscribers) {
                    return true;
                }
            }
        }
        return false;
    }
    filterAndGo(page) {
        _models_access_filter__WEBPACK_IMPORTED_MODULE_10__["AccessFilter"].routesToGo.forEach(route => {
            if (page.accessID === route.accessID && route.path !== '') {
                this.goPage('module/' + route.path);
                return;
            }
        });
    }
    billPayment() {
        const accountInfo = this.cacheStorage().accountInfo;
        let creditAmountDue = accountInfo.billBalanceField.includes('CR') ? accountInfo.billBalanceField.replace('CR', '') : 0;
        let billBalance = accountInfo.billBalanceField.includes('CR') ? 0 : accountInfo.billBalanceField;
        creditAmountDue = parseFloat(String(creditAmountDue).replace(',', ''));
        billBalance = parseFloat(String(billBalance).replace(',', ''));
        this.amountPayable = parseFloat(String(this.amountPayable)).toFixed(2);
        if (Number(this.amountPayable) < 5) {
            this.showError('El monto no puede ser menor a $5.00');
        }
        else if (Number(this.amountPayable) > 800) {
            this.showError('El monto no puede ser mayor a $800.00');
        }
        else if (creditAmountDue > 0 && (creditAmountDue + Number(this.amountPayable)) > 800) {
            this.showError('El monto total abonado en su cuenta no puede ser mayor a $800.00');
        }
        else if (Number(this.amountPayable) > billBalance) {
            this.showConfirm('Confirmación', 'La cantidad ingresada es mayor al balance de su factura, la diferencia será acreditada a su cuenta.', () => {
                this.getCreditCardPaymentOptions(String(accountInfo.bANField), accountInfo.defaultSubscriberField, this.amountPayable);
            }, () => {
                // Nothing to do
            });
        }
        else {
            this.getCreditCardPaymentOptions(String(accountInfo.bANField), accountInfo.defaultSubscriberField, this.amountPayable);
        }
    }
    getCreditCardPaymentOptions(account, subscriber, amount) {
        this.showProgress('Está siendo redirigido a la página de el Banco Popular para realizar el pago.');
        this.services.getPaymentOptions(this.productId, this.merchantId)
            .then((response) => {
            if (response.paymentOptions === undefined ||
                response.paymentOptions == null ||
                response.paymentOptions.length === 0) {
                this.dismissProgress();
                this.showError('Lo sentimos, actualmente no contamos con metodos de pagos disponibles.');
            }
            else {
                const options = response.paymentOptions[0];
                this.initPayment(account, subscriber, amount, options);
            }
        }, error => {
            this.dismissProgress();
            this.showError(error.message);
        });
    }
    initPayment(account, subscriber, amount, options) {
        const self = this;
        const data = this.cacheStorage();
        this.services.initiatePaymentProcess(this.productId, options.paymentOptionId, amount, data.accountInfo.emailField, subscriber, account, data.accountInfo.firstNameField + ' ' + data.accountInfo.lastNameField, options.paymentOptionName, this.merchantId).then((response) => {
            this.store(_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_12__["APP"].PAYMENT_STATUS_STORED, response.paymentToken);
            this.browserProvider.openPaymentBrowser(response.redirectURL).then(done => {
                // if browser desktop, we wait 1 minute for payment
                if (_utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].getPlatformInfo().desktop) {
                    setTimeout(() => {
                        self.verifyPayment();
                    }, 60 * 1000);
                }
                else { // if mobile, we wait 15 seconds after payment done
                    if (done) {
                        this.showProgress('Verificando pago...');
                        setTimeout(() => {
                            self.verifyPayment();
                        }, 30 * 1000); // 30 seconds after finish payment for verify
                    }
                    else {
                        this.dismissProgress();
                    }
                }
            }).catch(err => {
                this.dismissProgress();
                this.showError(err);
            });
        }, error => {
            this.dismissProgress();
            this.showError(error.message);
        });
    }
    verifyPayment() {
        this.fetch(_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_12__["APP"].PAYMENT_STATUS_STORED).then(result => {
            this.services.verifyPayment(result).then((response) => {
                if (response.processEndState) {
                    this.showProgress('Actualizando su balance...');
                    this.reloadCurrentAccount().then(() => {
                        this.dismissProgress();
                        this.goHomePage();
                    });
                }
                else {
                    this.dismissProgress();
                    this.showAlert('Su pago aun no ha sido procesado, si ya realizo el pago espere mientras lo procesamos.', () => {
                        this.goHomePage();
                    });
                }
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
        });
    }
    recharge() {
        const rechargeAmount = parseFloat(this.amountPayable);
        this.amountPayable = parseFloat(String(rechargeAmount)).toFixed(2);
        if (Number.isNaN(rechargeAmount)) {
            this.amountPayable = '0.00';
            this.showError('El monto de recarga no es un número válido.');
            return;
        }
        else if (rechargeAmount < 5) {
            this.showError('El monto de recarga no puede ser menor a $5.00');
        }
        else if (rechargeAmount > 150) {
            this.showError('El monto de recarga no puede ser mayor a $150.00');
        }
        else {
            this.showConfirm('Confirmación', '¿Desea proceder a recargar $' + _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(rechargeAmount) + '?', () => {
                this.getPrepaidProducts(rechargeAmount);
            }, () => {
                // Nothing to do
            });
        }
    }
    getPrepaidProducts(rechargeAmount) {
        this.showProgress();
        const data = this.cacheStorage();
        this.services.listProductService(this.selectedSubscriber.subscriberNumberField, 2, data.tokenSession)
            .then((response) => {
            this.dismissProgress();
            this.prepaidProducts = response.formProducts;
            this.goToRechargeConfirm(rechargeAmount);
        }, error => {
            this.dismissProgress();
            this.showError(error.message);
        });
    }
    getOtherProduct() {
        let item;
        this.prepaidProducts.forEach(product => {
            if (product.idProduct === 24) {
                item = product;
            }
        });
        return item;
    }
    goToRechargeConfirm(rechargeAmount) {
        const product = this.getOtherProduct();
        product.amountRecharge = rechargeAmount;
        const amount = _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(product.amountRecharge);
        const ivu = _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(product.amountRecharge * product.ivuState);
        const totalAmount = _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(Number(amount) + Number(ivu));
        this.cacheStorage().PaymentProcess = {
            subscriber: this.selectedSubscriber.subscriberNumberField,
            payments: this.prepaidPayments,
            products: this.prepaidProducts,
            address: this.prepaidAddress,
            selectedProduct: product,
            amount,
            ivu,
            totalAmount,
        };
        this.goPage('/module/recharge/confirm');
    }
    getWalletBalance() {
        if (this.selectedSubscriber !== undefined) {
            return _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(this.selectedSubscriber.eWalletBalanceField);
        }
        return '0.00';
    }
    getPrepaidBalance() {
        if (this.selectedSubscriber !== undefined) {
            return _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(this.selectedSubscriber.prepaidBalanceField);
        }
        return '0.00';
    }
    getNextRecharge() {
        let nextRecharge = 'N/A';
        if (this.selectedSubscriber !== undefined && this.selectedSubscriber.planInfoField !== undefined) {
            nextRecharge = this.selectedSubscriber.planInfoField.endDateField;
            if (nextRecharge === null || nextRecharge === undefined || nextRecharge === '') {
                nextRecharge = 'N/A';
            }
        }
        return nextRecharge;
    }
    get1GBReceived() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const data = this.cacheStorage();
            yield this.services.getGift1GBSend(String(data.accountInfo.bANField), data.tokenSession).then((response) => {
                if (response.Gift1GBsents.length > 0) {
                    this.product = response.Gift1GBsents[0];
                    this.store(src_app_utils_const_keys__WEBPACK_IMPORTED_MODULE_11__["keys"].GIFT.DATA_GIFT, this.product);
                    this.showPopupGiftReceived = true;
                }
            });
        });
    }
    closePopup1GB() {
        this.showPopupGiftReceived = false;
        this.showProgress();
        const data = this.cacheStorage();
        this.services.getGift1GBByGUI(this.product.BANReceiver, this.product.GUI, data.tokenSession).then((response) => {
            this.dismissProgress();
            this.showAlert('El Regalo ha sido aceptado con Éxito', () => {
                this.goHomePage();
            });
        }, error => {
            this.dismissProgress();
            this.showError(error.message);
        });
    }
    changePrepaidSubscriber() {
        this.showProgress();
        this.dismissProgress();
        this.cacheStorage().prepaidSelectedSubscriber = this.selectedSubscriber.subscriberNumberField;
        this.continueSelectAccount(this.cacheStorage().loginData, false);
    }
    openChat() {
        this.redirectProvider.openChat();
    }
};
DashboardComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__["IntentProvider"] },
    { type: _services_redirect_provider__WEBPACK_IMPORTED_MODULE_13__["RedirectProvider"] },
    { type: _services_browser_provider__WEBPACK_IMPORTED_MODULE_14__["BrowserProvider"] }
];
DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-dashboard',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dashboard.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/dashboard/dashboard.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/pages/home/dashboard/dashboard.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__["IntentProvider"], _services_redirect_provider__WEBPACK_IMPORTED_MODULE_13__["RedirectProvider"], _services_browser_provider__WEBPACK_IMPORTED_MODULE_14__["BrowserProvider"]])
], DashboardComponent);



/***/ }),

/***/ "./src/app/pages/home/home.component.scss":
/*!************************************************!*\
  !*** ./src/app/pages/home/home.component.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/home/home.component.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/home/home.component.ts ***!
  \**********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HomeComponent = class HomeComponent {
    constructor() {
    }
    ngOnInit() {
    }
};
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.component.scss */ "./src/app/pages/home/home.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], HomeComponent);



/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: routes, HomeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.component */ "./src/app/pages/home/home.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_shared/shared.module */ "./src/app/pages/_shared/shared.module.ts");
/* harmony import */ var _no_associated_no_associated_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./no-associated/no-associated.component */ "./src/app/pages/home/no-associated/no-associated.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/pages/home/dashboard/dashboard.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var ngx_popper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-popper */ "./node_modules/ngx-popper/fesm2015/ngx-popper.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");












const routes = [
    { path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_8__["DashboardComponent"] },
    { path: 'no-associated', component: _no_associated_no_associated_component__WEBPACK_IMPORTED_MODULE_5__["NoAssociatedComponent"] }
];
let HomeModule = class HomeModule {
};
HomeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"],
            _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_8__["DashboardComponent"],
            _no_associated_no_associated_component__WEBPACK_IMPORTED_MODULE_5__["NoAssociatedComponent"]
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"]],
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forChild(routes),
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_11__["IonicModule"],
            ngx_popper__WEBPACK_IMPORTED_MODULE_10__["NgxPopperModule"].forRoot({ placement: 'top', styles: { 'background-color': 'white' } })
        ]
    })
], HomeModule);



/***/ }),

/***/ "./src/app/pages/home/no-associated/no-associated.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/home/no-associated/no-associated.component.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvbm8tYXNzb2NpYXRlZC9uby1hc3NvY2lhdGVkLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/home/no-associated/no-associated.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/home/no-associated/no-associated.component.ts ***!
  \*********************************************************************/
/*! exports provided: NoAssociatedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoAssociatedComponent", function() { return NoAssociatedComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");









let NoAssociatedComponent = class NoAssociatedComponent extends _base_page__WEBPACK_IMPORTED_MODULE_8__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.accounts = [];
    }
    ngOnInit() {
    }
    openAccountsManage() {
        this.goPage('module/add-account');
    }
};
NoAssociatedComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__["IntentProvider"] }
];
NoAssociatedComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-no-associated',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./no-associated.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/no-associated/no-associated.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./no-associated.component.scss */ "./src/app/pages/home/no-associated/no-associated.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__["IntentProvider"]])
], NoAssociatedComponent);



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module-es2015.js.map