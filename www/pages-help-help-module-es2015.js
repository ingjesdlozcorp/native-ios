(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-help-help-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/about/about.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/about/about.component.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-help></app-header-help>\n\n    <div class=\"basicrow m-top\">\n        <div class=\"container\">\n            <div class=\"basicr-line-i\">\n                <div class=\"fulltitle m-top-i f-btitle\">\n                    <span><img src=\"assets/images/ico_terminosCondiciones.png\" style=\"height: 50px; width: 50px; margin: 10px\" alt=\"\"></span>\n                    &nbsp;Acerca de\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow f-reg text-justify f-black diffr m-top-i\">\n\n        <div class=\"container\">\n\n            En Claro Puerto Rico te damos soluciones digitales seguras, fáciles y rápidas para que tengas el control de tu cuenta.  Desde Mi Claro puedes:<br/><br/>\n\n            <ul>\n                <li>Ver y descargar tu factura.</li>\n                <li>Realiza tus pagos con ATH, Tarjeta de Credito o Cuenta de banco.</li>\n                <li>Revisar tus consumos.</li>\n                <li>Cambiar tu plan.</li>\n                <li>Revisar tus consumos.</li>\n                <li>Suscríbirte para que tus pagos se debiten automáticamente.</li>\n                <li>Entra a nuestra tienda y activa, renueva o porta tu línea a Claro.</li>\n                <li>Tu Chat esta disponible todos los días.</li>\n                <li>Utiliza el programa de lealtad..</li>\n            </ul>\n\n            <br>\n            <br>\n            Mantenla actualizada para que disfrutes de los Updates que constantemente le hacemos a nuestra aplicación.\n            Si necesitas ayuda puedes escribirnos a <a href=\"mailto:miclaro@claropr.com\">miclaro@claropr.com</a>.  Te asistiremos rápidamente.\n        </div>\n    </div>\n\n    <div class=\"basicrow m-top-i graybg\">\n        <div class=\"container\">\n            <div class=\"basicrow m-bott\">\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"row refdef\">\n                        <div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4\">\n                            <div class=\"basicrow text-center\">\n                                <img width=\"100%\" class=\"ref-icons\" src=\"assets/images/netflixicon3.png\" alt=\"\">\n                            </div>\n\n                            <div class=\"basicrow f-black roboto-r f-reg text-center m-top\">\n                                <div class=\"basicrow f-bmed roboto-b\">\n                                    Versi&oacute;n {{version}}\n                                </div>\n                            </div>\n                        </div>\n\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow m-top m-bott\">\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"row refdef\">\n                        <div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4\">\n                            <div class=\"basicrow text-center\">\n                                <img width=\"100%\" class=\"ref-icons\" src=\"assets/images/ref-gmail-icon.png\" alt=\"\">\n                            </div>\n\n                            <div class=\"basicrow f-black roboto-r f-reg text-center m-top\">\n                                <div class=\"basicrow f-bmed roboto-b\">\n                                    <a href=\"mailto:miclaro@claropr.com\" class=\"cdef\">miclaro@claropr.com&nbsp;&nbsp;<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i></a>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/chat/chat.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/chat/chat.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-help></app-header-help>\n\n\n    <div class=\"basicrow m-top\">\n        <div class=\"container\">\n            <div class=\"basicr-line-i\">\n                <div class=\"fulltitle m-top-i f-btitle\">\n                    <span><img src=\"assets/images/chatnow-icon.png\" style=\"height: 50px; width: 50px; margin: 10px\" alt=\"\"></span>\n                    &nbsp;Soporte en Linea\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow mbott-i\">\n        <div class=\"container\">\n            <div class=\"basicrow m-top-i\">\n                <div class=\"basicrow f-med f-black\">\n                    <strong>Bienvenido a nuestro Chat en Linea</strong>\n                </div>\n\n                <div class=\"basicrow f-med roboto-l f-black m-top\">\n                    Por favor complete la siguiente información para servirle mejor.\n                </div>\n\n                <div class=\"basicrow text-center\">\n                    <div class=\"basicrow\">\n                        <div class=\"text-left roboto-b\">\n                            <div class=\"basicrow\">\n                                <div class=\"basicrow m-top-i\">\n                                    <label><input [(ngModel)]=\"selectedChatCategory\" value=\"2\" type=\"radio\" name=\"radio\">&nbsp;&nbsp;Servicio al Cliente Móvil</label>\n                                </div>\n\n                                <div class=\"basicrow m-top-u\">\n                                    <label><input [(ngModel)]=\"selectedChatCategory\" value=\"3\" type=\"radio\" name=\"radio\">&nbsp;&nbsp;Servicio al Cliente Fijo</label>\n                                </div>\n\n                                <div class=\"basicrow m-top-u\">\n                                    <label><input [(ngModel)]=\"selectedChatCategory\" value=\"11\" type=\"radio\" name=\"radio\">&nbsp;&nbsp;Asistencia Mi Claro</label>\n                                </div>\n\n                                <div class=\"basicrow m-top-u\">\n                                    <label><input [(ngModel)]=\"selectedChatCategory\" value=\"5\" type=\"radio\" name=\"radio\">&nbsp;&nbsp;Servicio al viajero - Roaming</label>\n                                </div>\n\n                                <div class=\"basicrow m-top-u\">\n                                    <label><input [(ngModel)]=\"selectedChatCategory\" value=\"15\" type=\"radio\" name=\"radio\">&nbsp;&nbsp;Claro Empresas PR</label>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <label class=\"label-f\">\n                        <strong>&#42; Nombre y Apellido</strong>\n                    </label>\n\n                    <input type=\"text\"\n                           [(ngModel)]=\"name\"\n                           [ngClass]=\"isLogged ? 'off':''\"\n                           placeholder=\"Nombre y apellido\"\n                           class=\"inp-f\">\n                </div>\n\n                <div class=\"basicrow m-top\">\n                    <label class=\"label-f\">\n                        <strong>&#42; Email</strong>\n                    </label>\n\n                    <input type=\"email\"\n                           [(ngModel)]=\"email\"\n                           placeholder=\"Correo Electronico\"\n                           class=\"inp-f\">\n                </div>\n\n                <div class=\"basicrow m-top\">\n                    <label class=\"label-f\">\n                        <strong>Pregunta</strong>\n                    </label>\n\n                    <textarea\n                            [(ngModel)]=\"question\"\n                            class=\"t-areadef\"\n                            placeholder=\"(Opcional)\"\n                            maxlength=\"500\">\n                </textarea>\n\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow m-top-i text-center\">\n\n                    <div class=\"btns def-iv red centr vcenter rippleR\" (click)='send()'>\n                      <span class=\"tabcell\">\n                          Continuar&nbsp;&nbsp;<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>\n                      </span>\n                    </div>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/contact-us/contact-us.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/contact-us/contact-us.component.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-help></app-header-help>\n\n    <div class=\"basicrow m-top\">\n        <div class=\"container\">\n            <div class=\"basicr-line-i\">\n                <div class=\"fulltitle m-top-i f-btitle\">\n                    <span><img src=\"assets/images/netflixicon4.png\" style=\"height: 50px; width: 50px; margin: 10px\" alt=\"\"></span>\n                    &nbsp;Cont&aacute;ctanos\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow f-black roboto-r f-reg m-top-i m-bott\">\n\n                <div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4\">\n                    <div class=\"basicrow f-bmed roboto-b\">\n                        Horario de Servicio al Cliente\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <span class=\"roboto-b\">Lunes a Viernes:</span> 8:00 a.m - 7:00 p.m<br/><br/>\n\n                        <span class=\"roboto-b\">S&aacute;bados y Domingos:</span> 8:00 a.m - 5:00 p.m<br/><br/>\n\n                        <span class=\"roboto-b\">D&iacute;as Feriados:</span> 8:00 a.m - 5:00 p.m\n                    </div>\n                </div>\n\n                <div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 h-mtop-i\">\n                    <div class=\"basicrow f-bmed roboto-b\">\n                        Tel&eacute;fonos de Contacto\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <span class=\"roboto-b\">Servicio al Cliente:</span> <a href=\"tel:7877750000\">787-775-0000</a><br/><br/>\n\n                        &oacute; marcando <span class=\"roboto-b\"> <a href=\"tel:*611\">&#42;611</a></span> desde tu tel&eacute;fono celular.\n                    </div>\n                </div>\n\n                <div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 h-mtop-i\">\n                    <div class=\"basicrow f-bmed roboto-b\">\n                        Escr&iacute;benos\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <span class=\"roboto-b\">Correo Electr&oacute;nico:</span> <a href=\"mailto:miclaro@claropr.com\">miclaro@claropr.com</a><br/><br/>\n\n                        <span class=\"roboto-b\">Siguenos y enterate primero:</span>\n                    </div>\n\n                    <div class=\"basicrow m-top-u-i\">\n                        <a href=\"https://www.facebook.com/Claropr/\" target=\"_blank\" class=\"supp-social\">\n                            <img alt=\"\" width=\"100%\" src=\"assets/images/claroface.png\">\n                        </a>\n\n                        <a href=\"https://twitter.com/clarotodo?lang=es\" target=\"_blank\" class=\"supp-social\">\n                            <img alt=\"\" width=\"100%\" src=\"assets/images/clarotwitter.png\">\n                        </a>\n\n                        <a href=\"https://www.instagram.com/claropr/\" target=\"_blank\" class=\"supp-social\">\n                            <img alt=\"\" width=\"100%\" src=\"assets/images/instaclaro.png\">\n                        </a>\n\n                        <a href=\"https://www.youtube.com/user/clarotodo/videos\" target=\"_blank\" class=\"supp-social\">\n                            <img alt=\"\" width=\"100%\" src=\"assets/images/youtubeclaro.png\">\n                        </a>\n                    </div>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/failure-report/failure-report.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/failure-report/failure-report.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-help></app-header-help>\n\n    <div class=\"basicrow m-top\">\n        <div class=\"container\">\n            <div class=\"basicrow\">\n                <div class=\"fulltitle m-top-i f-btitle autodef\">\n                    <span class=\"icon-dash-13\"></span> Reportar Falla\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow mbott m-top-ii\">\n        <div class=\"container\">\n            <div class=\"autowcont allpads\">\n\n                <div class=\"row allpads f-reg roboto-r f-black\">\n                    <div class=\"basicrow text-center m-top-u\">\n                        Por favor, elige la categor&iacute;a mas adecuada.\n                    </div>\n\n                    <div class=\"basicrow text-center m-top\">\n                        <div class=\"basicrow\">\n                            <div class=\"text-left roboto-b\">\n                                <div *ngFor=\"let category of failureCategory\">\n                                    <div class=\"basicrow m-top\" style=\"display: flex\">\n                                        <input type=\"radio\"\n                                               name=\"failure_category\"\n                                               (change)=\"radioChangeHandler($event)\"\n                                               value=\"{{category}}\">\n                                        <label for=\"radio-choice-v-2a\" class=\"roboto-b\" style=\"margin-left: 5px; margin-top: 3px; text-align: left\" >\n                                            {{category}}\n                                        </label>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i text-left\">\n                        <div class=\"basicrow f-bmed roboto-b\">\n                            Detalla tu Comentario\n                        </div>\n\n                        <!--   <% if (!isLogged) { %> -->\n\n                        <div class=\"basicrow\">\n                            <div class=\"row\">\n                                <div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6 m-top\">\n                                    <input id=\"email\"\n                                           name=\"email\"\n                                           type=\"email\"\n                                           [(ngModel)]=\"email\"\n                                           placeholder=\"Correo Electrónico\"\n                                           class=\"inp-f\">\n                                </div>\n                            </div>\n                        </div>\n\n                        <!--  <% } %> -->\n\n                        <div class=\"basicrow m-top-i\">\n                            <textarea\n                                id=\"failure-comment\"\n                                name=\"failure_comment\"\n                                [(ngModel)]=\"failureComment\"\n                                class=\"t-areadef\"\n                                placeholder=\"Ingrese aqu&iacute; una breve explicaci&oacute;n de lo que desea reportar\"\n                                maxlength=\"500\"></textarea>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <button class=\"basicrow m-top\">\n                        <div\n                            class=\"btns def-i red pull-right vcenter rippleR\"\n                            id=\"send-failure\"\n                            (click)='send()'\n                            [ngClass]=\"enabled ? 'btns def-i red pull-right vcenter rippleR' : 'btns def-i red pull-right vcenter rippleR ui-disabled'\"\n                        >\n                            <span class=\"tabcell\">\n                                Enviar&nbsp;&nbsp;<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>\n                            </span>\n                        </div>\n                    </button>\n                </div>\n\n            </div>\n        </div>\n    </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/faq/faq.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/faq/faq.component.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-help></app-header-help>\n\n    <div class=\"basicrow m-top\">\n        <div class=\"container\">\n            <div class=\"basicr-line-i\">\n                <div class=\"fulltitle m-top-i f-btitle\">\n                    <span><img src=\"assets/images/ico_preguntasFrecuentes.png\" style=\"height: 50px; width: 50px; margin: 10px\" alt=\"\"></span>\n                    &nbsp;Soporte\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow f-black roboto-r f-reg m-top-i m-bott\">\n                <div class=\"basicrow f-mildt roboto-m\">\n                    Preguntas Frecuentes\n                </div>\n\n                <div class=\"basicrow f-mildt roboto-m m-top-i\" style=\"color: darkgray\">\n                    Mi Claro PR\n                </div>\n\n                <div class=\"basicrow m-top\" (click)='showingMenu(0)'>\n                    <div data-toggle=\"collapse\" data-target=\"#preg1\" class=\"preg-f\">\n                        &#191;Cómo me registro&#63;\n                        <i  aria-hidden=\"true\" [ngClass]=\"contentShow[0] ?  'fa fa-minus':'fa fa-plus'\"></i>\n                    </div>\n\n                    <div id=\"preg1\"  [ngClass]=\" contentShow[0]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            Para registrar tu cuenta en Mi Claro Solo necesitas Número de teléfono de Claro PR, PIN que llegara vía mensaje de texto a tu numero celular o vía correo electrónico. los últimos 4 del seguro social del titular de la cuenta, correo electrónico y contraseña a utilizar.\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)='showingMenu(1)'>\n                    <div data-toggle=\"collapse\" data-target=\"#preg2\" class=\"preg-f\">\n                        Cuando me registro, el sistema no me pregunto nombre de usuario.\n\n                        <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"contentShow[1] ?  'fa fa-minus':'fa fa-plus'\"></i>\n\n                    </div>\n\n                    <div id=\"preg2\" class=\"collapse\" [ngClass]=\" contentShow[1]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            El nombre de usuario es tu número de teléfono móvil o correo electrónico utilizado para registrarse.\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)='showingMenu(2)'>\n                    <div data-toggle=\"collapse\" data-target=\"#preg3\" class=\"preg-f\">\n                        &#191;Cómo añadir una cuenta adicional en mi usuario ya existente de Mi Claro&#63;\n\n                        <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"contentShow[2] ?  'fa fa-minus':'fa fa-plus'\"></i>\n\n                    </div>\n\n                    <div id=\"preg3\" class=\"collapse\" [ngClass]=\" contentShow[2]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            Puedes añadir una cuenta adicional bajo el mismo usuario existente a través de la misma aplicación.  Dentro de Mi Claro oprime Mis Cuentas, en la parte superior de tu pantalla oprime Agregar Cuentas.  El sistema te solicitara número de cuenta a incluir y últimos 4 del seguro social del titular de la cuenta. Para finalizar oprime, guardar cuenta.  Para ver el cambio en efecto, debes de salir y volver a entrar a la aplicación.\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)='showingMenu(3)'>\n                    <div data-toggle=\"collapse\" data-target=\"#preg4\" class=\"preg-f\">\n                        &#191;Cómo desbloquear mi cuenta&#63;\n                        <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"contentShow[3] ?  'fa fa-minus':'fa fa-plus'\"></i>\n                    </div>\n\n                    <div id=\"preg4\" class=\"collapse\" [ngClass]=\" contentShow[3]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            Si bloqueas tu usuario de Mi Claro, podrás recuperar el acceso de una manera simple. Luego de 3 intentos fallidos para entrar a Mi Claro, se levantará un mensaje donde te indicará lo siguiente:\n                            Hemos detectado una actividad inusual en el uso de tus credenciales de acceso. Espera unos segundos que un representante de servicio estará habilitando una sesión de Chat para darte soporte con tu problema de acceso.\n                            De solicitar asistencia adicional, puedes escribirnos a miclaro@claropr.com.\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)='showingMenu(4)'>\n                    <div data-toggle=\"collapse\" data-target=\"#preg5\" class=\"preg-f\">\n                        &#191;Tiene problemas de conexión al acceder a Mi Claro&#63;\n\n                        <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"contentShow[4] ?  'fa fa-minus':'fa fa-plus'\"></i>\n                    </div>\n\n                    <div id=\"preg5\" class=\"collapse\" [ngClass]=\" contentShow[4]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            Si tienes problemas de conexión, verifica que tengas actualizada la aplicación con la última versión. Asegúrate que tu conexión de data es una estable y se encuentre activa o que tenga conexión Wi-Fi disponible o Intenta acceder a través de los navegadores CHROME o FIRE FOX. También puedes revisar las opciones de tu navegador y eliminar los cookies o archivos temporeros.\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow f-mildt roboto-m m-top-i\" style=\"color: darkgray\">\n                    Factura\n                </div>\n\n                <div class=\"basicrow m-top\" (click)='showingMenu(5)' >\n                    <div data-toggle=\"collapse\" data-target=\"#preg6\" class=\"preg-f\">\n                        &#191;No he recibido mi factura&#63;\n\n                        <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"contentShow[5] ?  'fa fa-minus':'fa fa-plus'\"></i>\n                    </div>\n\n                    <div id=\"preg6\" class=\"collapse\" [ngClass]=\" contentShow[5]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            Si es un nuevo cliente su factura debe estar en formato digital.  Para ver su factura, información sobre su cuenta y hacer pagos, regístrate en Mi Claro PR.  Tienes opciones de App y Web disponibles. Dentro de Mi Claro PR en el módulo de Factura y Pago tendrás acceso al Historial de Facturas y las mismas se pueden Descargar e Imprimir.\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)='showingMenu(6)'>\n                    <div data-toggle=\"collapse\" data-target=\"#preg7\" class=\"preg-f\">\n                        Recibí factura y no tengo servicio\n\n                        <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"contentShow[6] ?  'fa fa-minus':'fa fa-plus'\"></i>\n\n                        <i class=\"fa fa-minus\" aria-hidden=\"true\"></i>\n                    </div>\n\n                    <div id=\"preg7\" class=\"collapse\" [ngClass]=\" contentShow[6]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            Tenemos varias opciones para ti.  Puedes contactarnos en las redes sociales @claroservicio en Facebook o twitter o en ayuda.claropr.com.\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow f-mildt roboto-m m-top-i\" style=\"color: darkgray\">\n                    Pagos\n                </div>\n\n                <div class=\"basicrow m-top\" (click)='showingMenu(7)'>\n                    <div data-toggle=\"collapse\" data-target=\"#preg8\" class=\"preg-f\">\n                        &#191;Puedo realizar pagos desde Mi Claro&#63;\n\n                        <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"contentShow[7] ?  'fa fa-minus':'fa fa-plus'\"></i>\n                    </div>\n\n                    <div id=\"preg8\" class=\"collapse\" [ngClass]=\" contentShow[7]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            Si, a través de nuestra aplicación puedes realizar pagos totales, parciales y/o abonos a tu cuenta.  Puedes realizar sus pagos con ATH, VISA, MasterCard, American Express, cuenta de cheque o ahorro.\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\"  (click)='showingMenu(8)'>\n                    <div data-toggle=\"collapse\" data-target=\"#preg9\" class=\"preg-f\">\n                        &#191;En qué momento se reflejará el pago en mi cuenta&#63;\n\n                        <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"contentShow[8] ?  'fa fa-minus':'fa fa-plus'\"></i>\n\n                    </div>\n\n                    <div id=\"preg9\" class=\"collapse\" [ngClass]=\" contentShow[8]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            Si el pago realizado fue con Tarjeta de Crédito o ATH el mismo se reflejará en la próxima hora.\n\n                            Si el pago realizado fue con cuenta de Cheque o Ahorro el mismo se reflejará en 3 días laborables.\n\n                            Si su cuenta es Fija (Residencial) y se encuentra suspendido, puede demorar hasta 8 horas en reestablecer el servicio.\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)='showingMenu(9)'>\n                    <div data-toggle=\"collapse\" data-target=\"#preg10\" class=\"preg-f\">\n                        &#191;Tengo teléfono prepagado, puedo registrarme en Mi Claro&#63;\n\n                        <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"contentShow[9] ?  'fa fa-minus':'fa fa-plus'\"></i>\n                    </div>\n\n                    <div id=\"preg10\" class=\"collapse\" [ngClass]=\" contentShow[9]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            Para registrar tu servicio prepagado debes acceder a nuestro Portal Único y seguir las instrucciones en pantalla.\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow f-mildt roboto-m m-top-i\" style=\"color: darkgray\">\n                    Compra Planes y Paquetes\n                </div>\n\n                <div class=\"basicrow m-top\" (click)='showingMenu(10)'>\n                    <div data-toggle=\"collapse\" data-target=\"#preg11\" class=\"preg-f\">\n                        &#191;Cómo puedo conocer las ofertas y productos&#63;\n\n                        <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"contentShow[10] ?  'fa fa-minus':'fa fa-plus'\"></i>\n\n                        <i class=\"fa fa-minus\" aria-hidden=\"true\"></i>\n                    </div>\n\n                    <div id=\"preg11\" class=\"collapse\" [ngClass]=\" contentShow[10]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            Puedes entrar a https://tienda.claropr.com  y adquirir nuevos equipos y las mejores ofertas en la Red Más Poderosa.\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)='showingMenu(11)'>\n                    <div data-toggle=\"collapse\" data-target=\"#preg12\" class=\"preg-f\">\n                        &#191;En cuánto tiempo recibiré mi equipo&#63;\n\n                        <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"contentShow[11] ?  'fa fa-minus':'fa fa-plus'\"></i>\n                    </div>\n\n                    <div id=\"preg12\" class=\"collapse\" [ngClass]=\" contentShow[11]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            Recibirás el equipo dentro de los próximos 3-5 días laborables sin ningún cargo por envío.\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)='showingMenu(12)'>\n                    <div data-toggle=\"collapse\" data-target=\"#preg13\" class=\"preg-f\">\n                        Se me agotó la data, &#191;Qué puedo hacer&#63;\n\n                        <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"contentShow[12] ?  'fa fa-minus':'fa fa-plus'\"></i>\n                    </div>\n\n                    <div id=\"preg13\" class=\"collapse\" [ngClass]=\" contentShow[12]? 'in':'collapse'\">\n                        <div class=\"basicrow allpads bordsdef text-justify\">\n                            En Mi Claro PR app o Web, puedes adquirir paquetes de data adicional y hasta aumentar la data de tu plan actual.\n                        </div>\n                    </div>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/header-help/header-help.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/header-help/header-help.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"menubar\">\n    <div class=\"header container rel\">\n        <div class=\"mc-logo mcenter\" (click)=\"goHome()\">\n            <span class=\"autocont vcenter\">\n                <span class=\"tabcell\">\n                    <img width=\"100%\" src=\"assets/images/miclaro-logo.png\" alt=\"\">\n                </span>\n            </span>\n        </div>\n\n        <div class=\"item pull-left\" (click)='goBack()'>\n            <img class=\"dispicon\" src=\"assets/images/back.svg\" alt=\"\">\n        </div>\n    </div>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/help.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/help.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n    <app-header-help></app-header-help>\n    <router-outlet></router-outlet>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/home/home.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/home/home.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\" style=\"overflow-y: auto\">\n    <app-header-help></app-header-help>\n\n    <app-popup-general-terms\n            *ngIf='open'\n            (close)=\"closeTerms()\">\n    </app-popup-general-terms>\n\n    <div class=\"basicrow m-top\">\n        <div class=\"container\">\n            <div class=\"basicr-line-i\">\n                <div class=\"fulltitle m-top-i f-btitle autodef\">\n                    <span><img alt=\"\" src=\"assets/images/help-icon.png\" style=\"height: 50px; width: 50px; margin: 10px\"></span>\n                    &nbsp;Ayuda\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow f-black roboto-r f-reg m-top-i m-bott-ii\">\n                <div class=\"basicrow f-mildt roboto-m\">\n                    Sección de Ayuda\n                </div>\n\n                <div class=\"basicrow m-top\" (click)=\"goPage('/help/faq')\">\n                    <div class=\"preg-f\">\n                        Preguntas Frecuentes\n\n                        <img alt=\"\" class=\"sectarrow\" width=\"100%\" src=\"assets/images/arrow-iii.png\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)=\"openChat()\">\n                    <div class=\"preg-f\">\n                        Soporte en L&iacute;nea por Chat\n\n                        <img alt=\"\" class=\"sectarrow\" width=\"100%\" src=\"assets/images/arrow-iii.png\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)=\"goPage('/help/contact-us')\">\n                    <div class=\"preg-f\">\n                        Cont&aacute;ctanos\n\n                        <img alt=\"\" class=\"sectarrow\" width=\"100%\" src=\"assets/images/arrow-iii.png\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)=\"goPage('/help/failure-report')\">\n                    <div class=\"preg-f\">\n                        Reportar Fallas<br/>\n                        <span class=\"f-small f-gray\">Informar sobre un problema.</span>\n\n                        <img alt=\"\" class=\"sectarrow\" width=\"100%\" src=\"assets/images/arrow-iii.png\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)=\"goPage('/help/improvement')\">\n                    <div class=\"preg-f\">\n                        Sugerencias<br/>\n                        <span class=\"f-small f-gray\">Tu opini&oacute;n es valiosa para nosotros.</span>\n\n                        <img alt=\"\" class=\"sectarrow\" width=\"100%\" src=\"assets/images/arrow-iii.png\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)=\"goPage('/help/about')\">\n                    <div class=\"preg-f\">\n                        Acerca de\n\n                        <img alt=\"\" class=\"sectarrow\" width=\"100%\" src=\"assets/images/arrow-iii.png\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top\" (click)=\"goPage('/help/locations')\">\n                    <div class=\"preg-f\">\n                        Localidades\n\n                        <img alt=\"\" class=\"sectarrow\" width=\"100%\" src=\"assets/images/arrow-iii.png\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow text-center\" style=\"margin-top: 50px\">\n                    <a id=\"link-terms\" class=\"linkdefs\" (click)=\"openTerms()\" >T&eacute;rminos y Condiciones</a><br/>\n                </div>\n\n            </div>\n        </div>\n    </div>\n</div>\n\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/improvement/improvement.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/improvement/improvement.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-help></app-header-help>\n\n    <div class=\"basicrow m-top\">\n        <div class=\"container\">\n            <div class=\"basicrow\">\n                <div class=\"fulltitle m-top-i f-btitle autodef\">\n                    <span class=\"icon-dash-13\"></span> Sugerencias\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow mbott m-top-ii\">\n        <div class=\"container\">\n            <div class=\"autowcont allpads\">\n                <div class=\"row allpads f-reg roboto-r f-black\">\n\n                    <div class=\"basicrow text-left\">\n                        <div class=\"basicrow f-bmed roboto-b\">\n                            Detalla tu Comentario\n                        </div>\n                        <div class=\"basicrow\">\n                            <div class=\"row\">\n                                <div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6 m-top\">\n                                    <input \n                                        type=\"email\"\n                                        name=\"email\" \n                                        [(ngModel)]=\"email\"\n                                        class=\"form-control\"\n                                        required\n                                        pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$\"\n                                        placeholder=\"Correo Electrónico\" class=\"inp-f\"\n                                    >\n                                </div>\n                            </div>\n                        </div>\n\n                        <div class=\"basicrow m-top-i\">\n                                <textarea\n                                        id=\"comment\"\n                                        name=\"comment\"\n                                        [(ngModel)]=\"comment\"\n                                        class=\"t-areadef\"\n                                        placeholder=\"Ingrese aqu&iacute; una breve explicaci&oacute;n de lo que desea reportar\" maxlength=\"500\"\n                                ></textarea>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow m-top\">\n                        <div\n                                class=\"btns def-i red pull-right vcenter rippleR\"\n                                id=\"send-improvement\"\n                                (click)='send()'\n                                [ngClass]=\"enabled ? 'btns def-i red pull-right vcenter rippleR' : 'btns def-i red pull-right vcenter rippleR ui-disabled'\"\n                        >\n                                <span class=\"tabcell\">\n                                Enviar&nbsp;&nbsp;<i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>\n                                </span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/locations-details/locations-details.component.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/locations-details/locations-details.component.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-help></app-header-help>\n\n    <div class=\"basicrow m-top\">\n        <div class=\"container\">\n            <div class=\"basicrow\">\n                <div class=\"fulltitle m-top-i f-btitle autodef\">\n                    <span><img src=\"assets/images/map-icon.png\" style=\"height: 50px; width: 50px; margin: 10px\" alt=\"\"></span>\n                    &nbsp;Localidades\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow mbott m-top-i\">\n        <div class=\"container\">\n            <div class=\"autowcont allpads m-top-ii\">\n                <div class=\"basicrow f-reg f-black roboto-r\">\n                    <div class=\"basicrow f-big\">\n                        <!--<%= store.name %>-->\n                        {{store.name}}\n                    </div>\n\n\n                    <!--<% if(store.schedule!=null && store.schedule != '' ) { %>-->\n                    <div\n                            class=\"dts-rcrg m-top-ii text-center\"\n                            *ngIf=\"store.schedule!=null && store.schedule != ''\"\n                    >\n                        <div class=\"row\">\n                            <div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\">\n                                Horario:<br/>\n                                <!--<%= store.schedule %>-->\n                                {{store.schedule}}\n                                <!--<% if(store.distance != 0 && store.longitude != 0 && store.latitude != 0) { %>-->\n                                <div\n                                        *ngIf=\"store.distance != 0 && store.longitude != 0 && store.latitude != 0\"\n                                        class=\"tab-arrow less-r m-w\"\n                                >\n                                    <div class=\"basicrow f-red roboto-b f-mini\">\n                    <span class=\"pull-right\">\n                     {{store.distance}} mil</span>\n                                    </div>\n                                </div>\n                                <!--<% } %>-->\n                            </div>\n                        </div>\n                    </div>\n                    <!--<% } %>-->\n\n                    <!--<% if (store.address != null && store.address != '' ) { %>-->\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"autobar f-reg text-justify f-bold f-black diffr\">\n                            <div class=\"container\">\n                                <!--<%= store.address %><br/>\n                                <%= store.town %>-->\n                                {{store.address}} <br>\n                                {{store.town}}\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow\">\n                        <div class=\"logline full\"></div>\n                    </div>\n                    <!--<% } %>-->\n\n                    <div class=\"basicrow m-top-i\">\n                        <!-- [INIT MAP] -->\n                        <div class=\"autobar diffr nopads\">\n                            <div class=\"map-container\" id=\"store-map\" style=\"height: 200px\"></div>\n                        </div>\n                        <!-- [END MAP] -->\n                    </div>\n\n                    <!--<% if(store.latitude!='' && store.longitude!=''){ %>-->\n                    <div class=\"basicrow m-top-i m-bott-ii\">\n                        <div class=\"lineb store-route\" (click)=showStoreRoute()>\n                            <div class=\"sectbar\">\n                                <div class=\"container rel\">\n                                    <img class=\"sectarrow\" width=\"100%\" src=\"assets/images/arrow-iii.png\" alt=\"\">\n\n                                    <div class=\"sect-a vcenter\">\n                                        <div class=\"tabcell\">\n                                            <img class=\"sect-icon address\" width=\"100%\" src=\"assets/images/address-icon.png\" alt=\"\">\n                                        </div>\n                                    </div>\n\n                                    <div class=\"sect-b f-lmed f-bold f-black vcenter\">\n                                        <div class=\"tabcell\">\n                                            Direcci&oacute;n\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <!--<% } %>-->\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/locations/locations.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/locations/locations.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-help></app-header-help>\n\n    <div class=\"basicrow m-top\">\n        <div class=\"container\">\n            <div class=\"basicrow\">\n                <div class=\"fulltitle m-top-i f-btitle autodef\">\n                    <span><img src=\"assets/images/map-icon.png\" style=\"height: 50px; width: 50px; margin: 10px\" alt=\"\"></span>\n                    &nbsp;Localidades\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow m-top-i\">\n\n        <div class=\"acctypescont text-center\">\n\n            <div (click)=\"tabSelected(type.value)\"\n                 *ngFor =\"let type of storeType\"\n                 [ngClass]=\"type.selected ? 'btn-store acctype on':'btn-store acctype'\">\n                <div class=\"autocont full vcenter\">\n                    <div class=\"tabcell\">\n                        {{type.name}}\n                    </div>\n                </div>\n            </div>\n        </div>\n\n    </div>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow f-black roboto-r f-reg m-top-i m-bott\">\n                <div class=\"basicrow f-mildt roboto-m\">\n                    Localidades Cercanas\n                </div>\n\n\n                <div (click)=\"storeDetail(store)\"\n                     class=\"basicrow m-top btn-store-detail\"\n                     data-store-id=\"store.id\"\n                     *ngFor =\"let store of storeList\">\n                    <div class=\"preg-f\" style=\"padding-right: 0\">\n\n                        <div class=\"rel\">\n                            <img class=\"sectarrow\" width=\"100%\" src=\"assets/images/arrow-iii.png\" alt=\"\">\n\n                            <div class=\"sect-full modrt f-reg f-bold f-black vcenter\">\n                                <div class=\"tabcell\">\n                                    <span class=\"f-red\">{{store.name}}</span>\n                                    <br/>\n                                    <span class=\"f-dgray f-little\">\n                                        {{store.address}} <br>\n                                        {{store.town}}\n                                    </span>\n                                </div>\n                            </div>\n\n                            <div *ngIf=\"store.longitude != 0 && store.latitude != 0\"\n                                 [ngClass]=\"store.distance == 0 ? 'basicrow f-red roboto-b f-mini hidden' : ''\"\n                                 class=\"basicrow f-red roboto-b f-mini\"\n                                 style=\"margin-top:10px; width:auto;\">\n                                <span class=\"pull-right\" *ngIf=\"store.distance != 0\">\n                                    {{store.distance}} mil\n                                </span>\n                            </div>\n\n                        </div>\n                    </div>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/success-report/success-report.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/success-report/success-report.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-help></app-header-help>\n\n    <div class=\"basicrow m-top\">\n        <div class=\"container\">\n            <div class=\"basicrow\">\n                <div class=\"fulltitle m-top-i f-btitle autodef\">\n                    <span class=\"icon-dash-13\"></span> {{title}}\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow mbott m-top-ii\">\n        <div class=\"container\">\n            <div class=\"autowcont allpads\">\n                <div class=\"row allpads f-reg roboto-r f-black\">\n\n                    <div class=\"basicrow allpads f-reg roboto-r f-black\">\n                        <div class=\"basicrow text-center m-top-u\">\n                            <div class=\"basicrow\">\n                                Reporte Enviado\n                            </div>\n\n                            <div class=\"basicrow roboto-b m-top\">\n                                Gracias. Tu mensaje ha sido enviado.\n                            </div>\n                        </div>\n\n                        <div class=\"basicrow m-top-i\">\n                            <div class=\"logline full\"></div>\n                        </div>\n\n                        <div class=\"basicrow m-top\">\n                            <div id=\"btn-continue\" class=\"btns def-i red centr vcenter rippleR\" (click)=\"goHelp()\">\n                                      <span class=\"tabcell\">\n                                        Ok\n                                      </span>\n                            </div>\n                        </div>\n                    </div>\n\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>");

/***/ }),

/***/ "./src/app/pages/help/about/about.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/help/about/about.component.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvYWJvdXQvYWJvdXQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/help/about/about.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/help/about/about.component.ts ***!
  \*****************************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/device/ngx */ "./node_modules/@ionic-native/device/ngx/index.js");
/* harmony import */ var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/app-version/ngx */ "./node_modules/@ionic-native/app-version/ngx/index.js");




let AboutComponent = class AboutComponent {
    constructor(device, appVersion) {
        this.device = device;
        this.appVersion = appVersion;
    }
    ngOnInit() {
        this.getVersion();
    }
    getVersion() {
        this.appVersion.getVersionNumber().then((result) => {
            console.log(result);
            this.version = result;
        }).catch(error => {
            console.log(error);
        });
    }
};
AboutComponent.ctorParameters = () => [
    { type: _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_2__["Device"] },
    { type: _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_3__["AppVersion"] }
];
AboutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-help-about',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./about.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/about/about.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./about.component.scss */ "./src/app/pages/help/about/about.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_2__["Device"], _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_3__["AppVersion"]])
], AboutComponent);



/***/ }),

/***/ "./src/app/pages/help/chat/chat.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/help/chat/chat.component.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvY2hhdC9jaGF0LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/help/chat/chat.component.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/help/chat/chat.component.ts ***!
  \***************************************************/
/*! exports provided: ChatComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatComponent", function() { return ChatComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../utils/utils */ "./src/app/utils/utils.ts");










let ChatComponent = class ChatComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_6__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.name = '';
        this.email = '';
        this.question = '';
        this.enabled = true;
        this.isLogged = false;
    }
    ngOnInit() {
        if (this.cacheStorage().logged) {
            const data = this.cacheStorage().accountInfo;
            this.name = data.firstNameField + ' ' + data.lastNameField;
            this.email = data.emailField;
            this.isLogged = true;
        }
    }
    send() {
        if (this.name === '') {
            alert('Debe ingresar su nombre y apellido.');
        }
        else if (!_utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].validateEmail(this.email)) {
            alert('Debe ingresar un correo electrónico válido.');
        }
        else if (this.selectedChatCategory === undefined) {
            alert('Debe seleccionar un departamento.');
        }
        else {
            this.openChat();
        }
    }
    openChat() {
        const question = this.question;
        let name = this.name;
        const email = this.email;
        const dept = this.selectedChatCategory;
        if (this.isLogged) {
            const data = this.cacheStorage().accountInfo;
            name = `${data.firstNameField} ${data.lastNameField}`;
        }
        /*  let url = this.utils.chat;
          url += '?Email=' + email;
          url += '&Department=' + dept;
          url += '&firstname=' + name;
          url += '&lastname=' + lastname;
          url += '&Question=' + question;
          url = url.replace(/\s+/g, '%20');*/
        /*       this.utils.registerScreen('chat');
               this.showProgress();
               this.services.chat(
                   this.isLogged ? this.cacheStorage().tokenSession : undefined,
                   name,
                   email,
                   dept)
                   .then(response => {
                       console.log(response);
                       this.openIABrowser(response.url).then(() => {
                           this.dismissProgress();
                       });
                   }, error => {
                       this.dismissProgress();
                       this.showAlert(error.message);
                   });*/
    }
};
ChatComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_3__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] }
];
ChatComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-help-chat',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./chat.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/chat/chat.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./chat.component.scss */ "./src/app/pages/help/chat/chat.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_3__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"]])
], ChatComponent);



/***/ }),

/***/ "./src/app/pages/help/contact-us/contact-us.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/help/contact-us/contact-us.component.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvY29udGFjdC11cy9jb250YWN0LXVzLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/help/contact-us/contact-us.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/help/contact-us/contact-us.component.ts ***!
  \***************************************************************/
/*! exports provided: ContactUsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsComponent", function() { return ContactUsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ContactUsComponent = class ContactUsComponent {
    constructor() { }
    ngOnInit() { }
};
ContactUsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-help-contact-us',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./contact-us.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/contact-us/contact-us.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./contact-us.component.scss */ "./src/app/pages/help/contact-us/contact-us.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ContactUsComponent);



/***/ }),

/***/ "./src/app/pages/help/failure-report/failure-report.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/help/failure-report/failure-report.component.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvZmFpbHVyZS1yZXBvcnQvZmFpbHVyZS1yZXBvcnQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/help/failure-report/failure-report.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/help/failure-report/failure-report.component.ts ***!
  \***********************************************************************/
/*! exports provided: FailureReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FailureReportComponent", function() { return FailureReportComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var src_app_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/utils/const/appConstants */ "./src/app/utils/const/appConstants.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var src_app_utils_utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/utils/utils */ "./src/app/utils/utils.ts");











let FailureReportComponent = class FailureReportComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_6__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.isLogged = false;
        this.enabled = true;
        this.failureCategory = [
            'La aplicación no permite ingresar',
            'La aplicación no actualiza correctamente',
            'La aplicación no funciona en el teléfono',
            'La aplicación se traba',
            'La aplicación cierra inesperadamente'
        ];
        this.selectedFailureCategory = '';
    }
    radioChangeHandler(event) {
        this.selectedFailureCategory = event.target.value;
    }
    ngOnInit() {
        if (this.services.data !== undefined) {
            console.log(this.services.data);
            this.loged = true;
            this.accountSelected = this.cacheStorage().accountInfo.bANField;
            this.email = this.cacheStorage().accountInfo.emailField;
        }
        else {
            this.loged = false;
            this.accountSelected = null;
            this.email = '';
        }
    }
    send() {
        for (let i = 0; i < this.failureCategory.length; i++) {
            if (this.selectedFailureCategory === this.failureCategory[i]) {
                this.selectedFailureCategory = 'C0' + (i + 1);
            }
        }
        if (this.failureComment === '') {
            this.showError('Campo de "Detalle tu comentario" es requerido');
            return;
        }
        else if (this.email === '') {
            this.showError('Campo "Correo electrónico" es requerido');
            return;
        }
        else if (this.selectedFailureCategory === '') {
            this.showError('Debe seleccionar una categoría para su reporte');
            return;
        }
        this.sendFailure();
    }
    sendFailure() {
        let failureCategory, userEmail, failureComment, message, userId;
        // set user message
        failureCategory = this.selectedFailureCategory;
        failureComment = this.failureComment;
        message = failureCategory + '*' + failureComment;
        let isLogged = false;
        if (this.loged != null && this.accountSelected != null) {
            isLogged = true;
        }
        if (isLogged) {
            userId = this.cacheStorage().loginData.userID;
        }
        else {
            userId = this.services.UUID;
            userEmail = this.email;
        }
        /*
         Validate network
        var networkState = navigator.connection.type;

          if (networkState == Connection.NONE) {
           showAlert('Error', 'Revisa que tengas acceso a Internet.', 'Aceptar');
          } else if (networkState == Connection.UNKNOWN) {
           showAlert('Error', 'Por el momento el servicio no está disponible, intenta más tarde. Gracias.', 'Aceptar');
          } else {

         */
        if (src_app_utils_utils__WEBPACK_IMPORTED_MODULE_10__["Utils"].validateEmail(userEmail)) {
            this.showProgress();
            this.enabled = false;
            const proxyurl = 'https://cors-anywhere.herokuapp.com/';
            const url = this.utils.help + '/failure/' + src_app_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_8__["APP"].country + '/' + src_app_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_8__["APP"].id;
            const newUrl = proxyurl + url;
            this.services.sendHelpMessage(userEmail, message, newUrl).then((success) => {
                this.dismissProgress();
                this.services.temporaryStorage.success_report_title = 'Reportar Falla';
                // Navegar a: success_report
                console.log('cambiando de vista');
                this.goPage('help/success-report');
                this.enabled = true;
            }, error => {
                this.dismissProgress();
                alert('No se puede enviar su mensaje en este momento, intente más tarde');
            });
        }
        else {
            alert('Por favor introduzca un correo electrónico válido');
        }
        // } Este es el else de la validación del estado de conexión
    }
};
FailureReportComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_3__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
FailureReportComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-help-failure-report',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./failure-report.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/failure-report/failure-report.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./failure-report.component.scss */ "./src/app/pages/help/failure-report/failure-report.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_3__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], FailureReportComponent);



/***/ }),

/***/ "./src/app/pages/help/faq/faq.component.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/help/faq/faq.component.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvZmFxL2ZhcS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/help/faq/faq.component.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/help/faq/faq.component.ts ***!
  \*************************************************/
/*! exports provided: FaqComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaqComponent", function() { return FaqComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FaqComponent = class FaqComponent {
    constructor() {
        this.open = true;
        this.touched = false;
        this.contentShow = [
            false, false, false, false,
            false, false, false, false,
            false, false, false, false,
            false,
        ];
        this.theOpenGroup = [];
    }
    ngOnInit() { }
    searchDelete(param) {
        for (let i = 0; i < this.theOpenGroup.length; i++) {
            if (param === this.theOpenGroup[i]) {
                this.theOpenGroup.splice(i, 1);
            }
        }
    }
    showingMenu(param) {
        let close = false;
        if (this.theOpenGroup.length > 0) {
            this.theOpenGroup.forEach(group => {
                if (group === param) {
                    close = true;
                }
            });
        }
        // to show content
        if (!close) {
            for (let i = 0; i < this.contentShow.length; i++) {
                if (param === i) {
                    this.contentShow[i] = true;
                    this.theOpenGroup.push(param);
                    console.log(this.contentShow[i]);
                }
            }
        }
        else {
            // para cerrar el contenido;
            console.log('quieres ocultar contenido');
            this.searchDelete(param);
            for (let i = 0; i < this.contentShow.length; i++) {
                if (param === i) {
                    this.contentShow[i] = false;
                    console.log(this.contentShow[i]);
                }
            }
        }
        console.log(this.theOpenGroup);
    }
};
FaqComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-help-faq',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./faq.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/faq/faq.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./faq.component.scss */ "./src/app/pages/help/faq/faq.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], FaqComponent);



/***/ }),

/***/ "./src/app/pages/help/header-help/header-help.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/help/header-help/header-help.component.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mySideNav {\n  height: auto;\n  /* 100% Full-height */\n  width: 100%;\n  /* 0 width - change this with JavaScript */\n  position: absolute;\n  /* Stay in place */\n  z-index: 10000;\n  /* Stay on top */\n  top: 84px;\n  right: 100%;\n  overflow-x: auto;\n  /* Disable horizontal scroll */\n  transition: 0.4s;\n  /* 0.5 second transition effect to slide in the sidenav */\n  -webkit-transition: 0.4s;\n  -moz-transition: 0.4s;\n  -o-transition: 0.4s;\n  overflow-y: hidden;\n  border-bottom: 1px solid #dddddd;\n}\n\n.mySideNav {\n  margin-top: -1px;\n}\n\n.mySideNav {\n  top: 50px;\n}\n\n@media only screen and (orientation: landscape) {\n  .mySideNav {\n    top: 49px;\n    /* fixed header for ios version */\n  }\n}\n\n/* iphone X and iphone Xs*/\n\n@media only screen and (min-device-width: 375px) and (max-device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone Xr TEST*/\n\n@media only screen and (min-device-width: 375px) and (max-device-height: 812px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone Xr */\n\n@media only screen and (min-device-width: 414px) and (max-device-height: 896px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone Xs Max */\n\n@media only screen and (min-device-width: 414px) and (max-device-height: 896px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n.and {\n  margin-top: -35px !important;\n}\n\n.theRed {\n  color: #ef3829 !important;\n}\n\n.theWhite {\n  color: #fcf7f7 !important;\n}\n\n.menuIsOpen {\n  right: 0;\n}\n\n.menuIsClosed {\n  right: 100%;\n}\n\n.m-menuin,\n.m-menubar {\n  height: 50px;\n  width: 100%;\n  border-top: 1px solid #ffffff;\n  float: left;\n  position: relative;\n  font-size: 19.2px;\n  color: #ffffff;\n  padding: 0 20px;\n  font-family: \"Roboto\", sans-serif;\n  font-weight: 400;\n  text-decoration: none;\n  background-color: #ef3829;\n}\n\n.m-menuin {\n  color: #222222;\n  background-color: #ffffff;\n  font-size: 14px;\n  border-top: 1px solid #ffffff;\n  border-bottom: 2px solid #dddddd;\n}\n\n.m-menuin.lastlink {\n  border: 0;\n}\n\na.m-menuin {\n  color: #222222;\n  text-decoration: none;\n}\n\na.m-menubar {\n  color: #ffffff;\n  text-decoration: none;\n}\n\n.m-menuin,\n.m-menubar {\n  padding-right: 40px;\n}\n\n.m-menubar i {\n  font-size: 20px;\n  line-height: 20px;\n  position: absolute;\n  height: 20px;\n  width: 20px;\n  top: 0;\n  bottom: 0;\n  right: 7px;\n  color: #ffffff;\n  margin: auto;\n}\n\n.m-menuin i {\n  font-size: 16px;\n  line-height: 16px;\n  position: absolute;\n  height: 16px;\n  width: 16px;\n  top: 0;\n  bottom: 0;\n  right: 7px;\n  color: #222222;\n  margin: auto;\n}\n\n.m-menubar i.fa-minus,\n.m-menubar i.fa-plus {\n  right: 14px;\n}\n\n.m-menuin i.fa-times-circle {\n  font-size: 18px;\n  line-height: 18px;\n  position: absolute;\n  height: 18px;\n  width: 18px;\n  right: 11px;\n  color: #b7202e;\n}\n\n.m-menubar.open-ins i.fa-plus,\n.m-menubar i.minus {\n  display: none;\n}\n\n.m-menubar.open-ins i.fa-minus {\n  display: inline;\n}\n\n.m-menubar i.fa-times-circle {\n  right: 13px;\n}\n\n.m-menubar.important {\n  background-color: #b7202e;\n}\n\n.mclosex {\n  font-size: 22px;\n  line-height: 22px;\n  height: 22px;\n  width: 22px;\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  right: 7px;\n  margin: auto;\n  color: #abaead;\n  cursor: pointer;\n}\n\n.mc-logo.mcenter {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  margin: auto;\n  width: 123px;\n}\n\n.hidden {\n  display: none !important;\n}\n\n.notifc {\n  margin-left: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZG1pbi9Eb2N1bWVudHMvRSRHUy9SRUJSTkFESU5HL1Jlc3BhbGRvcy9pb25pYy9taWNsYXJvMy1pb25pYy12ZXJzaW9uL3NyYy9hcHAvcGFnZXMvaGVscC9oZWFkZXItaGVscC9oZWFkZXItaGVscC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcGFnZXMvaGVscC9oZWFkZXItaGVscC9oZWFkZXItaGVscC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFBYyxxQkFBQTtFQUNkLFdBQUE7RUFBYSwwQ0FBQTtFQUNiLGtCQUFBO0VBQW9CLGtCQUFBO0VBQ3BCLGNBQUE7RUFBZ0IsZ0JBQUE7RUFDaEIsU0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUFrQiw4QkFBQTtFQUNsQixnQkFBQTtFQUFrQix5REFBQTtFQUNsQix3QkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdDQUFBO0FDT0o7O0FESkE7RUFDSSxnQkFBQTtBQ09KOztBREpBO0VBQ0ksU0FBQTtBQ09KOztBREpBO0VBQ0k7SUFDSSxTQUFBO0lBQ0EsaUNBQUE7RUNPTjtBQUNGOztBREpBLDBCQUFBOztBQUNBO0VBQ0k7SUFDSSxTQUFBO0VDTU47QUFDRjs7QURIQSxrQkFBQTs7QUFDQTtFQUNJO0lBQ0ksU0FBQTtFQ0tOO0FBQ0Y7O0FERkEsY0FBQTs7QUFDQTtFQUNJO0lBQ0ksU0FBQTtFQ0lOO0FBQ0Y7O0FEREEsa0JBQUE7O0FBQ0E7RUFDSTtJQUNJLFNBQUE7RUNHTjtBQUNGOztBREFBO0VBQ0ksNEJBQUE7QUNFSjs7QURDQTtFQUNJLHlCQUFBO0FDRUo7O0FEQUE7RUFDSSx5QkFBQTtBQ0dKOztBREFBO0VBQ0ksUUFBQTtBQ0dKOztBRERBO0VBQ0ksV0FBQTtBQ0lKOztBREZBOztFQUVJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUNBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7QUNLSjs7QURGQTtFQUNJLGNBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0FDS0o7O0FERkE7RUFDSSxTQUFBO0FDS0o7O0FERkE7RUFDSSxjQUFBO0VBQ0EscUJBQUE7QUNLSjs7QURGQTtFQUNJLGNBQUE7RUFDQSxxQkFBQTtBQ0tKOztBREZBOztFQUVJLG1CQUFBO0FDS0o7O0FERkE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7QUNLSjs7QURGQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxNQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ0tKOztBREZBOztFQUVJLFdBQUE7QUNLSjs7QURGQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtBQ0tKOztBREZBOztFQUVJLGFBQUE7QUNLSjs7QURGQTtFQUNJLGVBQUE7QUNLSjs7QURGQTtFQUNJLFdBQUE7QUNLSjs7QURGQTtFQUNJLHlCQUFBO0FDS0o7O0FERkE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDS0o7O0FERkE7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQ0tKOztBREZBO0VBQ0ksd0JBQUE7QUNLSjs7QURGQTtFQUNJLGlCQUFBO0FDS0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9oZWxwL2hlYWRlci1oZWxwL2hlYWRlci1oZWxwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm15U2lkZU5hdiB7XG4gICAgaGVpZ2h0OiBhdXRvOyAvKiAxMDAlIEZ1bGwtaGVpZ2h0ICovXG4gICAgd2lkdGg6IDEwMCU7IC8qIDAgd2lkdGggLSBjaGFuZ2UgdGhpcyB3aXRoIEphdmFTY3JpcHQgKi9cbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7IC8qIFN0YXkgaW4gcGxhY2UgKi9cbiAgICB6LWluZGV4OiAxMDAwMDsgLyogU3RheSBvbiB0b3AgKi9cbiAgICB0b3A6IDg0cHg7XG4gICAgcmlnaHQ6IDEwMCU7XG4gICAgb3ZlcmZsb3cteDogYXV0bzsgLyogRGlzYWJsZSBob3Jpem9udGFsIHNjcm9sbCAqL1xuICAgIHRyYW5zaXRpb246IDAuNHM7IC8qIDAuNSBzZWNvbmQgdHJhbnNpdGlvbiBlZmZlY3QgdG8gc2xpZGUgaW4gdGhlIHNpZGVuYXYgKi9cbiAgICAtd2Via2l0LXRyYW5zaXRpb246IDAuNHM7XG4gICAgLW1vei10cmFuc2l0aW9uOiAwLjRzO1xuICAgIC1vLXRyYW5zaXRpb246IDAuNHM7XG4gICAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGRkZGRkO1xufVxuXG4ubXlTaWRlTmF2IHtcbiAgICBtYXJnaW4tdG9wOiAtMXB4O1xufVxuXG4ubXlTaWRlTmF2IHtcbiAgICB0b3A6IDUwcHg7XG59XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG9yaWVudGF0aW9uIDogbGFuZHNjYXBlKXtcbiAgICAubXlTaWRlTmF2IHtcbiAgICAgICAgdG9wOiA0OXB4O1xuICAgICAgICAvKiBmaXhlZCBoZWFkZXIgZm9yIGlvcyB2ZXJzaW9uICovXG4gICAgfVxufVxuXG4vKiBpcGhvbmUgWCBhbmQgaXBob25lIFhzKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDM3NXB4KSBhbmQgKG1heC1kZXZpY2UtaGVpZ2h0OiA4MTJweCkgYW5kICgtd2Via2l0LWRldmljZS1waXhlbC1yYXRpbzogMykgIGFuZCAob3JpZW50YXRpb24gOiBwb3J0cmFpdCkge1xuICAgIC5teVNpZGVOYXYge1xuICAgICAgICB0b3A6IDg0cHg7XG4gICAgfVxufVxuXG4vKiBpcGhvbmUgWHIgVEVTVCovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiAzNzVweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogODEycHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpICBhbmQgKG9yaWVudGF0aW9uIDogcG9ydHJhaXQpIHtcbiAgICAubXlTaWRlTmF2IHtcbiAgICAgICAgdG9wOiA4NHB4O1xuICAgIH1cbn1cblxuLyogaXBob25lIFhyICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiA0MTRweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogODk2cHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpICBhbmQgKG9yaWVudGF0aW9uIDogcG9ydHJhaXQpIHtcbiAgICAubXlTaWRlTmF2IHtcbiAgICAgICAgdG9wOiA4NHB4O1xuICAgIH1cbn1cblxuLyogaXBob25lIFhzIE1heCAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogNDE0cHgpIGFuZCAobWF4LWRldmljZS1oZWlnaHQ6IDg5NnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAzKSAgYW5kIChvcmllbnRhdGlvbiA6IHBvcnRyYWl0KSB7XG4gICAgLm15U2lkZU5hdiB7XG4gICAgICAgIHRvcDogODRweDtcbiAgICB9XG59XG5cbi5hbmR7XG4gICAgbWFyZ2luLXRvcDogLTM1cHggIWltcG9ydGFudDtcbn1cblxuLnRoZVJlZHtcbiAgICBjb2xvcjogI2VmMzgyOSAhaW1wb3J0YW50O1xufVxuLnRoZVdoaXRle1xuICAgIGNvbG9yOiBoc2woMCwgNTAlLCA5OCUpICFpbXBvcnRhbnQ7XG59XG5cbi5tZW51SXNPcGVue1xuICAgIHJpZ2h0OiAwO1xufVxuLm1lbnVJc0Nsb3NlZHtcbiAgICByaWdodDogMTAwJTtcbn1cbi5tLW1lbnVpbixcbi5tLW1lbnViYXIge1xuICAgIGhlaWdodDogNTBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2ZmZmZmZjtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZm9udC1zaXplOiAxOS4ycHg7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgcGFkZGluZzogMCAyMHB4O1xuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvJywgc2Fucy1zZXJpZjtcbiAgICBmb250LXdlaWdodDogNDAwO1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWYzODI5O1xufVxuXG4ubS1tZW51aW4ge1xuICAgIGNvbG9yOiAjMjIyMjIyO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZmZmZmZmO1xuICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjZGRkZGRkO1xufVxuXG4ubS1tZW51aW4ubGFzdGxpbmsge1xuICAgIGJvcmRlcjogMDtcbn1cblxuYS5tLW1lbnVpbiB7XG4gICAgY29sb3I6ICMyMjIyMjI7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG5hLm0tbWVudWJhciB7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG4ubS1tZW51aW4sXG4ubS1tZW51YmFyIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiA0MHB4O1xufVxuXG4ubS1tZW51YmFyIGkge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIHdpZHRoOiAyMHB4O1xuICAgIHRvcDogMDtcbiAgICBib3R0b206IDA7XG4gICAgcmlnaHQ6IDdweDtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBtYXJnaW46IGF1dG87XG59XG5cbi5tLW1lbnVpbiBpIHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogMTZweDtcbiAgICB3aWR0aDogMTZweDtcbiAgICB0b3A6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIHJpZ2h0OiA3cHg7XG4gICAgY29sb3I6ICMyMjIyMjI7XG4gICAgbWFyZ2luOiBhdXRvO1xufVxuXG4ubS1tZW51YmFyIGkuZmEtbWludXMsXG4ubS1tZW51YmFyIGkuZmEtcGx1cyB7XG4gICAgcmlnaHQ6IDE0cHg7XG59XG5cbi5tLW1lbnVpbiBpLmZhLXRpbWVzLWNpcmNsZSB7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDE4cHg7XG4gICAgd2lkdGg6IDE4cHg7XG4gICAgcmlnaHQ6IDExcHg7XG4gICAgY29sb3I6ICNiNzIwMmU7XG59XG5cbi5tLW1lbnViYXIub3Blbi1pbnMgaS5mYS1wbHVzLFxuLm0tbWVudWJhciBpLm1pbnVzIHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuXG4ubS1tZW51YmFyLm9wZW4taW5zIGkuZmEtbWludXMge1xuICAgIGRpc3BsYXk6IGlubGluZTtcbn1cblxuLm0tbWVudWJhciBpLmZhLXRpbWVzLWNpcmNsZSB7XG4gICAgcmlnaHQ6IDEzcHg7XG59XG5cbi5tLW1lbnViYXIuaW1wb3J0YW50IHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjcyMDJlO1xufVxuXG4ubWNsb3NleCB7XG4gICAgZm9udC1zaXplOiAyMnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICAgIGhlaWdodDogMjJweDtcbiAgICB3aWR0aDogMjJweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGJvdHRvbTogMDtcbiAgICByaWdodDogN3B4O1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBjb2xvcjogI2FiYWVhZDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5tYy1sb2dvLm1jZW50ZXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIHdpZHRoOiAxMjNweDtcbn1cblxuLmhpZGRlbiB7XG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4ubm90aWZjIHtcbiAgICBtYXJnaW4tbGVmdDogMTJweFxufVxuIiwiLm15U2lkZU5hdiB7XG4gIGhlaWdodDogYXV0bztcbiAgLyogMTAwJSBGdWxsLWhlaWdodCAqL1xuICB3aWR0aDogMTAwJTtcbiAgLyogMCB3aWR0aCAtIGNoYW5nZSB0aGlzIHdpdGggSmF2YVNjcmlwdCAqL1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8qIFN0YXkgaW4gcGxhY2UgKi9cbiAgei1pbmRleDogMTAwMDA7XG4gIC8qIFN0YXkgb24gdG9wICovXG4gIHRvcDogODRweDtcbiAgcmlnaHQ6IDEwMCU7XG4gIG92ZXJmbG93LXg6IGF1dG87XG4gIC8qIERpc2FibGUgaG9yaXpvbnRhbCBzY3JvbGwgKi9cbiAgdHJhbnNpdGlvbjogMC40cztcbiAgLyogMC41IHNlY29uZCB0cmFuc2l0aW9uIGVmZmVjdCB0byBzbGlkZSBpbiB0aGUgc2lkZW5hdiAqL1xuICAtd2Via2l0LXRyYW5zaXRpb246IDAuNHM7XG4gIC1tb3otdHJhbnNpdGlvbjogMC40cztcbiAgLW8tdHJhbnNpdGlvbjogMC40cztcbiAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RkZGRkZDtcbn1cblxuLm15U2lkZU5hdiB7XG4gIG1hcmdpbi10b3A6IC0xcHg7XG59XG5cbi5teVNpZGVOYXYge1xuICB0b3A6IDUwcHg7XG59XG5cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpIHtcbiAgLm15U2lkZU5hdiB7XG4gICAgdG9wOiA0OXB4O1xuICAgIC8qIGZpeGVkIGhlYWRlciBmb3IgaW9zIHZlcnNpb24gKi9cbiAgfVxufVxuLyogaXBob25lIFggYW5kIGlwaG9uZSBYcyovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiAzNzVweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogODEycHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDMpIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KSB7XG4gIC5teVNpZGVOYXYge1xuICAgIHRvcDogODRweDtcbiAgfVxufVxuLyogaXBob25lIFhyIFRFU1QqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogMzc1cHgpIGFuZCAobWF4LWRldmljZS1oZWlnaHQ6IDgxMnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAyKSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkge1xuICAubXlTaWRlTmF2IHtcbiAgICB0b3A6IDg0cHg7XG4gIH1cbn1cbi8qIGlwaG9uZSBYciAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogNDE0cHgpIGFuZCAobWF4LWRldmljZS1oZWlnaHQ6IDg5NnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAyKSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkge1xuICAubXlTaWRlTmF2IHtcbiAgICB0b3A6IDg0cHg7XG4gIH1cbn1cbi8qIGlwaG9uZSBYcyBNYXggKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDQxNHB4KSBhbmQgKG1heC1kZXZpY2UtaGVpZ2h0OiA4OTZweCkgYW5kICgtd2Via2l0LWRldmljZS1waXhlbC1yYXRpbzogMykgYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpIHtcbiAgLm15U2lkZU5hdiB7XG4gICAgdG9wOiA4NHB4O1xuICB9XG59XG4uYW5kIHtcbiAgbWFyZ2luLXRvcDogLTM1cHggIWltcG9ydGFudDtcbn1cblxuLnRoZVJlZCB7XG4gIGNvbG9yOiAjZWYzODI5ICFpbXBvcnRhbnQ7XG59XG5cbi50aGVXaGl0ZSB7XG4gIGNvbG9yOiAjZmNmN2Y3ICFpbXBvcnRhbnQ7XG59XG5cbi5tZW51SXNPcGVuIHtcbiAgcmlnaHQ6IDA7XG59XG5cbi5tZW51SXNDbG9zZWQge1xuICByaWdodDogMTAwJTtcbn1cblxuLm0tbWVudWluLFxuLm0tbWVudWJhciB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZmZmZmZmO1xuICBmbG9hdDogbGVmdDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXNpemU6IDE5LjJweDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDAgMjBweDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VmMzgyOTtcbn1cblxuLm0tbWVudWluIHtcbiAgY29sb3I6ICMyMjIyMjI7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNmZmZmZmY7XG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjZGRkZGRkO1xufVxuXG4ubS1tZW51aW4ubGFzdGxpbmsge1xuICBib3JkZXI6IDA7XG59XG5cbmEubS1tZW51aW4ge1xuICBjb2xvcjogIzIyMjIyMjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG5hLm0tbWVudWJhciB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi5tLW1lbnVpbixcbi5tLW1lbnViYXIge1xuICBwYWRkaW5nLXJpZ2h0OiA0MHB4O1xufVxuXG4ubS1tZW51YmFyIGkge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMjBweDtcbiAgd2lkdGg6IDIwcHg7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICByaWdodDogN3B4O1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4ubS1tZW51aW4gaSB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAxNnB4O1xuICB3aWR0aDogMTZweDtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiA3cHg7XG4gIGNvbG9yOiAjMjIyMjIyO1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5tLW1lbnViYXIgaS5mYS1taW51cyxcbi5tLW1lbnViYXIgaS5mYS1wbHVzIHtcbiAgcmlnaHQ6IDE0cHg7XG59XG5cbi5tLW1lbnVpbiBpLmZhLXRpbWVzLWNpcmNsZSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAxOHB4O1xuICB3aWR0aDogMThweDtcbiAgcmlnaHQ6IDExcHg7XG4gIGNvbG9yOiAjYjcyMDJlO1xufVxuXG4ubS1tZW51YmFyLm9wZW4taW5zIGkuZmEtcGx1cyxcbi5tLW1lbnViYXIgaS5taW51cyB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5tLW1lbnViYXIub3Blbi1pbnMgaS5mYS1taW51cyB7XG4gIGRpc3BsYXk6IGlubGluZTtcbn1cblxuLm0tbWVudWJhciBpLmZhLXRpbWVzLWNpcmNsZSB7XG4gIHJpZ2h0OiAxM3B4O1xufVxuXG4ubS1tZW51YmFyLmltcG9ydGFudCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNiNzIwMmU7XG59XG5cbi5tY2xvc2V4IHtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBsaW5lLWhlaWdodDogMjJweDtcbiAgaGVpZ2h0OiAyMnB4O1xuICB3aWR0aDogMjJweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDdweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBjb2xvcjogI2FiYWVhZDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4ubWMtbG9nby5tY2VudGVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIG1hcmdpbjogYXV0bztcbiAgd2lkdGg6IDEyM3B4O1xufVxuXG4uaGlkZGVuIHtcbiAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4ubm90aWZjIHtcbiAgbWFyZ2luLWxlZnQ6IDEycHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/help/header-help/header-help.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/help/header-help/header-help.component.ts ***!
  \*****************************************************************/
/*! exports provided: HeaderHelpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderHelpComponent", function() { return HeaderHelpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");









let HeaderHelpComponent = class HeaderHelpComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.router = router;
    }
    ngOnInit() { }
    goBack() {
        super.goBack();
    }
    goHome() {
        if (this.cacheStorage().logged) {
            super.goHomePage();
        }
        else {
            super.goLoginPage();
        }
    }
};
HeaderHelpComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] }
];
HeaderHelpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header-help',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header-help.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/header-help/header-help.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./header-help.component.scss */ "./src/app/pages/help/header-help/header-help.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"]])
], HeaderHelpComponent);



/***/ }),

/***/ "./src/app/pages/help/help.component.scss":
/*!************************************************!*\
  !*** ./src/app/pages/help/help.component.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvaGVscC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/help/help.component.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/help/help.component.ts ***!
  \**********************************************/
/*! exports provided: HelpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpComponent", function() { return HelpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HelpComponent = class HelpComponent {
    constructor() { }
};
HelpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-help',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./help.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/help.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./help.component.scss */ "./src/app/pages/help/help.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], HelpComponent);



/***/ }),

/***/ "./src/app/pages/help/help.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/help/help.module.ts ***!
  \*******************************************/
/*! exports provided: routes, HelpModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpModule", function() { return HelpModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _help_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./help.component */ "./src/app/pages/help/help.component.ts");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./about/about.component */ "./src/app/pages/help/about/about.component.ts");
/* harmony import */ var _chat_chat_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chat/chat.component */ "./src/app/pages/help/chat/chat.component.ts");
/* harmony import */ var _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contact-us/contact-us.component */ "./src/app/pages/help/contact-us/contact-us.component.ts");
/* harmony import */ var _failure_report_failure_report_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./failure-report/failure-report.component */ "./src/app/pages/help/failure-report/failure-report.component.ts");
/* harmony import */ var _faq_faq_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./faq/faq.component */ "./src/app/pages/help/faq/faq.component.ts");
/* harmony import */ var _header_help_header_help_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./header-help/header-help.component */ "./src/app/pages/help/header-help/header-help.component.ts");
/* harmony import */ var _improvement_improvement_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./improvement/improvement.component */ "./src/app/pages/help/improvement/improvement.component.ts");
/* harmony import */ var _locations_locations_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./locations/locations.component */ "./src/app/pages/help/locations/locations.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _success_report_success_report_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./success-report/success-report.component */ "./src/app/pages/help/success-report/success-report.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../_shared/shared.module */ "./src/app/pages/_shared/shared.module.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./home/home.component */ "./src/app/pages/help/home/home.component.ts");
/* harmony import */ var _locations_details_locations_details_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./locations-details/locations-details.component */ "./src/app/pages/help/locations-details/locations-details.component.ts");



















const routes = [
    { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_17__["HomeComponent"] },
    { path: 'about', component: _about_about_component__WEBPACK_IMPORTED_MODULE_4__["AboutComponent"] },
    { path: 'chat', component: _chat_chat_component__WEBPACK_IMPORTED_MODULE_5__["ChatComponent"] },
    { path: 'contact-us', component: _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_6__["ContactUsComponent"] },
    { path: 'faq', component: _faq_faq_component__WEBPACK_IMPORTED_MODULE_8__["FaqComponent"] },
    { path: 'failure-report', component: _failure_report_failure_report_component__WEBPACK_IMPORTED_MODULE_7__["FailureReportComponent"] },
    { path: 'improvement', component: _improvement_improvement_component__WEBPACK_IMPORTED_MODULE_10__["ImprovementComponent"] },
    { path: 'locations', component: _locations_locations_component__WEBPACK_IMPORTED_MODULE_11__["LocationsComponent"] },
    { path: 'locations-detail', component: _locations_details_locations_details_component__WEBPACK_IMPORTED_MODULE_18__["LocationsDetailsComponent"] },
    { path: 'success-report', component: _success_report_success_report_component__WEBPACK_IMPORTED_MODULE_13__["SuccessReportComponent"] },
    { path: '', redirectTo: '/help/home', pathMatch: 'full' },
];
let HelpModule = class HelpModule {
};
HelpModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _help_component__WEBPACK_IMPORTED_MODULE_3__["HelpComponent"],
            _home_home_component__WEBPACK_IMPORTED_MODULE_17__["HomeComponent"],
            _about_about_component__WEBPACK_IMPORTED_MODULE_4__["AboutComponent"],
            _chat_chat_component__WEBPACK_IMPORTED_MODULE_5__["ChatComponent"],
            _contact_us_contact_us_component__WEBPACK_IMPORTED_MODULE_6__["ContactUsComponent"],
            _failure_report_failure_report_component__WEBPACK_IMPORTED_MODULE_7__["FailureReportComponent"],
            _faq_faq_component__WEBPACK_IMPORTED_MODULE_8__["FaqComponent"],
            _improvement_improvement_component__WEBPACK_IMPORTED_MODULE_10__["ImprovementComponent"],
            _header_help_header_help_component__WEBPACK_IMPORTED_MODULE_9__["HeaderHelpComponent"],
            _locations_locations_component__WEBPACK_IMPORTED_MODULE_11__["LocationsComponent"],
            _locations_details_locations_details_component__WEBPACK_IMPORTED_MODULE_18__["LocationsDetailsComponent"],
            _success_report_success_report_component__WEBPACK_IMPORTED_MODULE_13__["SuccessReportComponent"]
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_14__["RouterModule"]],
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_14__["RouterModule"].forChild(routes),
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_12__["FormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_15__["HttpClientModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_16__["SharedModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ReactiveFormsModule"]
        ]
    })
], HelpModule);



/***/ }),

/***/ "./src/app/pages/help/home/home.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/pages/help/home/home.component.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/help/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/help/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _services_redirect_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/redirect.provider */ "./src/app/services/redirect.provider.ts");










let HomeComponent = class HomeComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage, redirectProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.redirectProvider = redirectProvider;
        this.open = false;
        this.tokenSession = '';
        this.tokenSSO = '';
        this.isLogged = false;
    }
    ngOnInit() {
        this.services.temporaryStorage.storeList = [];
        if (this.cacheStorage().logged) {
            this.tokenSession = this.cacheStorage().loginData.token;
            this.tokenSSO = this.cacheStorage().loginData.SSOAccessToken;
            this.isLogged = true;
        }
    }
    openTerms() {
        this.open = true;
    }
    closeTerms() {
        this.open = false;
    }
    openChat() {
        this.redirectProvider.openChat();
    }
};
HomeComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_3__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] },
    { type: _services_redirect_provider__WEBPACK_IMPORTED_MODULE_9__["RedirectProvider"] }
];
HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-help-home',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/home/home.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.component.scss */ "./src/app/pages/help/home/home.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_3__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"], _services_redirect_provider__WEBPACK_IMPORTED_MODULE_9__["RedirectProvider"]])
], HomeComponent);



/***/ }),

/***/ "./src/app/pages/help/improvement/improvement.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/help/improvement/improvement.component.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvaW1wcm92ZW1lbnQvaW1wcm92ZW1lbnQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/help/improvement/improvement.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/help/improvement/improvement.component.ts ***!
  \*****************************************************************/
/*! exports provided: ImprovementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImprovementComponent", function() { return ImprovementComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var src_app_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/utils/const/appConstants */ "./src/app/utils/const/appConstants.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var src_app_utils_utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/utils/utils */ "./src/app/utils/utils.ts");











let ImprovementComponent = class ImprovementComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_6__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.isLoged = false;
        this.logged = false;
        this.enabled = true;
    }
    ngOnInit() {
        if (this.services.data !== undefined) {
            console.log(this.services.data);
            this.logged = true;
            this.accountSelected = this.cacheStorage().accountInfo.bANField;
            this.email = this.cacheStorage().accountInfo.emailField;
        }
        else {
            this.logged = false;
            this.accountSelected = null;
            this.email = '';
        }
    }
    send() {
        if (this.comment === '') {
            this.showError('Campo de "Detalle tu comentario" es requerido');
            return;
        }
        else if (this.email === '') {
            this.showError('Campo "Correo electrónico" es requerido');
            return;
        }
        this.sendImprovement();
    }
    sendImprovement() {
        let userEmail, message, userId;
        message = this.comment;
        let isLogged = false;
        if (this.logged !== null && this.accountSelected !== null) {
            isLogged = true;
        }
        if (isLogged) {
            userId = this.cacheStorage().loginData.userID;
        }
        else {
            userId = this.services.UUID;
            userEmail = this.email;
        }
        /*
        // Validate network
        var networkState = navigator.connection.type;

        if (networkState == Connection.NONE) {
          showAlert('Error', 'Revisa que tengas acceso a Internet.', 'Aceptar');
        } else if (networkState == Connection.UNKNOWN) {
          showAlert('Error', 'Por el momento el servicio no está disponible, intenta más tarde. Gracias.', 'Aceptar');
        } else {
          */
        if (src_app_utils_utils__WEBPACK_IMPORTED_MODULE_10__["Utils"].validateEmail(userEmail)) {
            this.showProgress();
            this.enabled = false;
            const proxyurl = 'https://cors-anywhere.herokuapp.com/';
            const url = this.utils.help + '/improvement/' + src_app_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_8__["APP"].country + '/' + src_app_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_8__["APP"].id;
            const newUrl = proxyurl + url;
            this.services.sendHelpMessage(userEmail, message, newUrl).then((success) => {
                this.dismissProgress();
                this.services.temporaryStorage.success_report_title = 'Sugerencias';
                // Navegar a: success_report
                console.log('cambiando de vista');
                this.goPage('help/success-report');
                this.enabled = true;
            }, error => {
                this.dismissProgress();
                this.showError('No se puede enviar su mensaje en este momento, intente más tarde');
            });
        }
        else {
            this.showError('Por favor introduzca un correo electrónico válido');
        }
        // } Ests es el else de la validación del estado de conexión
    }
};
ImprovementComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_3__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
ImprovementComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-help-improvement',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./improvement.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/improvement/improvement.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./improvement.component.scss */ "./src/app/pages/help/improvement/improvement.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_3__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_5__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], ImprovementComponent);



/***/ }),

/***/ "./src/app/pages/help/locations-details/locations-details.component.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/help/locations-details/locations-details.component.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvbG9jYXRpb25zLWRldGFpbHMvbG9jYXRpb25zLWRldGFpbHMuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/help/locations-details/locations-details.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/help/locations-details/locations-details.component.ts ***!
  \*****************************************************************************/
/*! exports provided: LocationsDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocationsDetailsComponent", function() { return LocationsDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var src_app_services_intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/launch-navigator/ngx */ "./node_modules/@ionic-native/launch-navigator/ngx/index.js");













let LocationsDetailsComponent = class LocationsDetailsComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage, geolocation, launchNavigator, loadingCtrl) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.geolocation = geolocation;
        this.launchNavigator = launchNavigator;
        this.loadingCtrl = loadingCtrl;
        this.mapRef = null;
    }
    ngOnInit() {
        this.checkData();
    }
    checkData() {
        this.store = this.services.temporaryStorage.store;
        this.services.temporaryStorage.storeList = [];
        console.log(this.store);
        const acum = this.store.distance;
        this.store.distance = parseFloat(acum).toFixed(2);
        this.showMap();
        // this.loadMap();
    }
    showMap() {
        this.mapElement = document.getElementById('store-map');
        const mapOption = {
            camera: {
                target: {
                    lat: this.store.latitude,
                    lng: this.store.longitude
                },
                zoom: 18,
                tilt: 30
            }
        };
        this.map = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_9__["GoogleMaps"].create(this.mapElement, mapOption);
        this.map.one(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_9__["GoogleMapsEvent"].MAP_READY).then(() => {
            this.map.addMarker({
                title: this.store.name,
                icon: 'red',
                animation: 'DROP',
                position: {
                    lat: this.store.latitude,
                    lng: this.store.longitude
                }
            });
        });
    }
    showStoreRoute() {
        const launchnavigator = this.launchNavigator;
        launchnavigator.navigate([this.store.latitude, this.store.longitude,], {
            start: this.store.latitude + ',' + this.store.longitude
        });
    }
};
LocationsDetailsComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"] },
    { type: src_app_services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"] },
    { type: _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_11__["LaunchNavigator"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] }
];
LocationsDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-help-locations-details',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./locations-details.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/locations-details/locations-details.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./locations-details.component.scss */ "./src/app/pages/help/locations-details/locations-details.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"],
        src_app_services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"], _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_11__["LaunchNavigator"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"]])
], LocationsDetailsComponent);



/***/ }),

/***/ "./src/app/pages/help/locations/locations.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/help/locations/locations.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvbG9jYXRpb25zL2xvY2F0aW9ucy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/help/locations/locations.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/help/locations/locations.component.ts ***!
  \*************************************************************/
/*! exports provided: LocationsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocationsComponent", function() { return LocationsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var src_app_services_intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");










let LocationsComponent = class LocationsComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.storeType = [
            { name: 'CACs', value: '1', type: 'postpago', selected: true },
            { name: 'Kioscos', value: '2', type: 'prepago', selected: false },
            { name: 'Tiendas', value: '3', type: 'telefonia', selected: false }
        ];
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.showProgress();
            this.coords = yield this.services.myPosition();
            this.services.temporaryStorage.coords = this.coords;
            this.changeStoreList(1);
        });
    }
    tabSelected(param) {
        this.showProgress();
        this.storeType[param - 1].selected = true;
        for (let i = 0; i < this.storeType.length; i++) {
            if (i !== (param - 1)) {
                this.storeType[i].selected = false;
            }
        }
        this.changeStoreList(param);
    }
    changeStoreList(param) {
        const storeType = this.storeType[param - 1].value;
        const userLocation = {
            latitude: this.services.location.latitude,
            longitude: this.services.location.longitude
        };
        const distanceMeasure = _environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].DISTANCE_MEASURE;
        const url = _environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].API_URL + '/store/list';
        this.services.getStores(storeType, userLocation, distanceMeasure, url)
            .then((success) => {
            this.dismissProgress();
            this.storeList = success.object.stores;
            for (const storeItem of this.storeList) {
                const acum = storeItem.distance;
                storeItem.distance = parseFloat(acum).toFixed(2);
            }
            this.services.temporaryStorage.storeList = this.storeList;
            this.services.temporaryStorage.storeType = param;
        }).catch(error => {
            this.dismissProgress();
            console.log('Error getting location', error);
            alert('Disculpe, no fue posible establecer la comunicación');
            this.goBack();
        });
    }
    storeDetail(store) {
        this.services.temporaryStorage.store = store;
        this.goPage('help/locations-detail');
    }
};
LocationsComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"] },
    { type: src_app_services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] }
];
LocationsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-help-locations',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./locations.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/locations/locations.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./locations.component.scss */ "./src/app/pages/help/locations/locations.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"],
        src_app_services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"]])
], LocationsComponent);



/***/ }),

/***/ "./src/app/pages/help/success-report/success-report.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/help/success-report/success-report.component.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hlbHAvc3VjY2Vzcy1yZXBvcnQvc3VjY2Vzcy1yZXBvcnQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/help/success-report/success-report.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/help/success-report/success-report.component.ts ***!
  \***********************************************************************/
/*! exports provided: SuccessReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuccessReportComponent", function() { return SuccessReportComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");









let SuccessReportComponent = class SuccessReportComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }
    ngOnInit() {
        console.log('reporte exitoso');
        if (this.services.temporaryStorage.success_report_title !== null) {
            this.title = this.services.temporaryStorage.success_report_title;
        }
    }
    goHelp() {
        this.goPage('help/home');
    }
};
SuccessReportComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] }
];
SuccessReportComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-help-success-report',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./success-report.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/help/success-report/success-report.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./success-report.component.scss */ "./src/app/pages/help/success-report/success-report.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"]])
], SuccessReportComponent);



/***/ })

}]);
//# sourceMappingURL=pages-help-help-module-es2015.js.map