function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-recover-recover-module"], {
  /***/
  "./node_modules/ng-recaptcha/fesm2015/ng-recaptcha.js":
  /*!************************************************************!*\
    !*** ./node_modules/ng-recaptcha/fesm2015/ng-recaptcha.js ***!
    \************************************************************/

  /*! exports provided: RECAPTCHA_BASE_URL, RECAPTCHA_LANGUAGE, RECAPTCHA_NONCE, RECAPTCHA_SETTINGS, RECAPTCHA_V3_SITE_KEY, ReCaptchaV3Service, RecaptchaComponent, RecaptchaFormsModule, RecaptchaLoaderService, RecaptchaModule, RecaptchaV3Module, RecaptchaValueAccessorDirective, ɵa */

  /***/
  function node_modulesNgRecaptchaFesm2015NgRecaptchaJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RECAPTCHA_BASE_URL", function () {
      return RECAPTCHA_BASE_URL;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RECAPTCHA_LANGUAGE", function () {
      return RECAPTCHA_LANGUAGE;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RECAPTCHA_NONCE", function () {
      return RECAPTCHA_NONCE;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RECAPTCHA_SETTINGS", function () {
      return RECAPTCHA_SETTINGS;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RECAPTCHA_V3_SITE_KEY", function () {
      return RECAPTCHA_V3_SITE_KEY;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReCaptchaV3Service", function () {
      return ReCaptchaV3Service;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RecaptchaComponent", function () {
      return RecaptchaComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RecaptchaFormsModule", function () {
      return RecaptchaFormsModule;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RecaptchaLoaderService", function () {
      return RecaptchaLoaderService;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RecaptchaModule", function () {
      return RecaptchaModule;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RecaptchaV3Module", function () {
      return RecaptchaV3Module;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RecaptchaValueAccessorDirective", function () {
      return RecaptchaValueAccessorDirective;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵa", function () {
      return RecaptchaCommonModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");

    function loadScript(renderMode, onLoaded, urlParams, url, nonce) {
      window.ng2recaptchaloaded = function () {
        onLoaded(grecaptcha);
      };

      var script = document.createElement("script");
      script.innerHTML = "";
      var baseUrl = url || "https://www.google.com/recaptcha/api.js";
      script.src = "".concat(baseUrl, "?render=").concat(renderMode, "&onload=ng2recaptchaloaded").concat(urlParams);

      if (nonce) {
        script.nonce = nonce;
      }

      script.async = true;
      script.defer = true;
      document.head.appendChild(script);
    }

    var RECAPTCHA_LANGUAGE = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]("recaptcha-language");
    var RECAPTCHA_BASE_URL = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]("recaptcha-base-url");
    var RECAPTCHA_NONCE = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]("recaptcha-nonce-tag");
    var RECAPTCHA_SETTINGS = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]("recaptcha-settings");
    var RECAPTCHA_V3_SITE_KEY = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]("recaptcha-v3-site-key");

    var RecaptchaLoaderService = /*#__PURE__*/function () {
      function RecaptchaLoaderService( // eslint-disable-next-line @typescript-eslint/ban-types
      platformId, language, baseUrl, nonce, v3SiteKey) {
        _classCallCheck(this, RecaptchaLoaderService);

        this.platformId = platformId;
        this.language = language;
        this.baseUrl = baseUrl;
        this.nonce = nonce;
        this.v3SiteKey = v3SiteKey;
        this.init();
        this.ready = Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(this.platformId) ? RecaptchaLoaderService.ready.asObservable() : Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])();
      }
      /** @internal */


      _createClass(RecaptchaLoaderService, [{
        key: "init",
        value: function init() {
          if (RecaptchaLoaderService.ready) {
            return;
          }

          if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(this.platformId)) {
            var subject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
            RecaptchaLoaderService.ready = subject;
            var langParam = this.language ? "&hl=" + this.language : "";
            var renderMode = this.v3SiteKey || "explicit";
            loadScript(renderMode, function (grecaptcha) {
              return subject.next(grecaptcha);
            }, langParam, this.baseUrl, this.nonce);
          }
        }
      }]);

      return RecaptchaLoaderService;
    }();
    /**
     * @internal
     * @nocollapse
     */


    RecaptchaLoaderService.ready = null;
    RecaptchaLoaderService.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }];

    RecaptchaLoaderService.ctorParameters = function () {
      return [{
        type: Object,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"]]
        }]
      }, {
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [RECAPTCHA_LANGUAGE]
        }]
      }, {
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [RECAPTCHA_BASE_URL]
        }]
      }, {
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [RECAPTCHA_NONCE]
        }]
      }, {
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [RECAPTCHA_V3_SITE_KEY]
        }]
      }];
    };

    var nextId = 0;

    var RecaptchaComponent = /*#__PURE__*/function () {
      function RecaptchaComponent(elementRef, loader, zone, settings) {
        _classCallCheck(this, RecaptchaComponent);

        this.elementRef = elementRef;
        this.loader = loader;
        this.zone = zone;
        this.id = "ngrecaptcha-".concat(nextId++);
        this.errorMode = "default";
        this.resolved = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"](); // The rename will happen a bit later
        // eslint-disable-next-line @angular-eslint/no-output-native

        this.error = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();

        if (settings) {
          this.siteKey = settings.siteKey;
          this.theme = settings.theme;
          this.type = settings.type;
          this.size = settings.size;
          this.badge = settings.badge;
        }
      }

      _createClass(RecaptchaComponent, [{
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {
          var _this = this;

          this.subscription = this.loader.ready.subscribe(function (grecaptcha) {
            if (grecaptcha != null && grecaptcha.render instanceof Function) {
              _this.grecaptcha = grecaptcha;

              _this.renderRecaptcha();
            }
          });
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          // reset the captcha to ensure it does not leave anything behind
          // after the component is no longer needed
          this.grecaptchaReset();

          if (this.subscription) {
            this.subscription.unsubscribe();
          }
        }
        /**
         * Executes the invisible recaptcha.
         * Does nothing if component's size is not set to "invisible".
         */

      }, {
        key: "execute",
        value: function execute() {
          if (this.size !== "invisible") {
            return;
          }

          if (this.widget != null) {
            this.grecaptcha.execute(this.widget);
          } else {
            // delay execution of recaptcha until it actually renders
            this.executeRequested = true;
          }
        }
      }, {
        key: "reset",
        value: function reset() {
          if (this.widget != null) {
            if (this.grecaptcha.getResponse(this.widget)) {
              // Only emit an event in case if something would actually change.
              // That way we do not trigger "touching" of the control if someone does a "reset"
              // on a non-resolved captcha.
              this.resolved.emit(null);
            }

            this.grecaptchaReset();
          }
        }
        /** @internal */

      }, {
        key: "expired",
        value: function expired() {
          this.resolved.emit(null);
        }
        /** @internal */

      }, {
        key: "errored",
        value: function errored(args) {
          this.error.emit(args);
        }
        /** @internal */

      }, {
        key: "captchaResponseCallback",
        value: function captchaResponseCallback(response) {
          this.resolved.emit(response);
        }
        /** @internal */

      }, {
        key: "grecaptchaReset",
        value: function grecaptchaReset() {
          var _this2 = this;

          if (this.widget != null) {
            this.zone.runOutsideAngular(function () {
              return _this2.grecaptcha.reset(_this2.widget);
            });
          }
        }
        /** @internal */

      }, {
        key: "renderRecaptcha",
        value: function renderRecaptcha() {
          var _this3 = this;

          // This `any` can be removed after @types/grecaptcha get updated
          var renderOptions = {
            badge: this.badge,
            callback: function callback(response) {
              _this3.zone.run(function () {
                return _this3.captchaResponseCallback(response);
              });
            },
            "expired-callback": function expiredCallback() {
              _this3.zone.run(function () {
                return _this3.expired();
              });
            },
            sitekey: this.siteKey,
            size: this.size,
            tabindex: this.tabIndex,
            theme: this.theme,
            type: this.type
          };

          if (this.errorMode === "handled") {
            renderOptions["error-callback"] = function () {
              for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
                args[_key] = arguments[_key];
              }

              _this3.zone.run(function () {
                return _this3.errored(args);
              });
            };
          }

          this.widget = this.grecaptcha.render(this.elementRef.nativeElement, renderOptions);

          if (this.executeRequested === true) {
            this.executeRequested = false;
            this.execute();
          }
        }
      }]);

      return RecaptchaComponent;
    }();

    RecaptchaComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        exportAs: "reCaptcha",
        selector: "re-captcha",
        template: ""
      }]
    }];

    RecaptchaComponent.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
      }, {
        type: RecaptchaLoaderService
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]
      }, {
        type: undefined,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [RECAPTCHA_SETTINGS]
        }]
      }];
    };

    RecaptchaComponent.propDecorators = {
      id: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"],
        args: ["attr.id"]
      }],
      siteKey: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      theme: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      type: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      size: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      tabIndex: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      badge: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      errorMode: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      resolved: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      error: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }]
    };

    var RecaptchaCommonModule = function RecaptchaCommonModule() {
      _classCallCheck(this, RecaptchaCommonModule);
    };

    RecaptchaCommonModule.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
      args: [{
        declarations: [RecaptchaComponent],
        exports: [RecaptchaComponent]
      }]
    }];

    var RecaptchaModule = function RecaptchaModule() {
      _classCallCheck(this, RecaptchaModule);
    };

    RecaptchaModule.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
      args: [{
        exports: [RecaptchaComponent],
        imports: [RecaptchaCommonModule],
        providers: [RecaptchaLoaderService]
      }]
    }];
    /**
     * The main service for working with reCAPTCHA v3 APIs.
     *
     * Use the `execute` method for executing a single action, and
     * `onExecute` observable for listening to all actions at once.
     */

    var ReCaptchaV3Service = /*#__PURE__*/function () {
      function ReCaptchaV3Service(zone, siteKey, // eslint-disable-next-line @typescript-eslint/ban-types
      platformId, baseUrl, nonce, language) {
        var _this4 = this;

        _classCallCheck(this, ReCaptchaV3Service);

        /** @internal */
        this.onLoadComplete = function (grecaptcha) {
          _this4.grecaptcha = grecaptcha;

          if (_this4.actionBacklog && _this4.actionBacklog.length > 0) {
            _this4.actionBacklog.forEach(function (_ref) {
              var _ref2 = _slicedToArray(_ref, 2),
                  action = _ref2[0],
                  subject = _ref2[1];

              return _this4.executeActionWithSubject(action, subject);
            });

            _this4.actionBacklog = undefined;
          }
        };

        this.zone = zone;
        this.isBrowser = Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(platformId);
        this.siteKey = siteKey;
        this.nonce = nonce;
        this.language = language;
        this.baseUrl = baseUrl;
        this.init();
      }

      _createClass(ReCaptchaV3Service, [{
        key: "execute",

        /**
         * Executes the provided `action` with reCAPTCHA v3 API.
         * Use the emitted token value for verification purposes on the backend.
         *
         * For more information about reCAPTCHA v3 actions and tokens refer to the official documentation at
         * https://developers.google.com/recaptcha/docs/v3.
         *
         * @param {string} action the action to execute
         * @returns {Observable<string>} an `Observable` that will emit the reCAPTCHA v3 string `token` value whenever ready.
         * The returned `Observable` completes immediately after emitting a value.
         */
        value: function execute(action) {
          var subject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();

          if (this.isBrowser) {
            if (!this.grecaptcha) {
              // todo: add to array of later executions
              if (!this.actionBacklog) {
                this.actionBacklog = [];
              }

              this.actionBacklog.push([action, subject]);
            } else {
              this.executeActionWithSubject(action, subject);
            }
          }

          return subject.asObservable();
        }
        /** @internal */

      }, {
        key: "executeActionWithSubject",
        value: function executeActionWithSubject(action, subject) {
          var _this5 = this;

          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          var onError = function onError(error) {
            _this5.zone.run(function () {
              subject.error(error);

              if (_this5.onExecuteErrorSubject) {
                // We don't know any better at this point, unfortunately, so have to resort to `any`
                // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                _this5.onExecuteErrorSubject.next({
                  action: action,
                  error: error
                });
              }
            });
          };

          this.zone.runOutsideAngular(function () {
            try {
              _this5.grecaptcha.execute(_this5.siteKey, {
                action: action
              }).then(function (token) {
                _this5.zone.run(function () {
                  subject.next(token);
                  subject.complete();

                  if (_this5.onExecuteSubject) {
                    _this5.onExecuteSubject.next({
                      action: action,
                      token: token
                    });
                  }
                });
              }, onError);
            } catch (e) {
              onError(e);
            }
          });
        }
        /** @internal */

      }, {
        key: "init",
        value: function init() {
          if (this.isBrowser) {
            if ("grecaptcha" in window) {
              this.grecaptcha = grecaptcha;
            } else {
              var langParam = this.language ? "&hl=" + this.language : "";
              loadScript(this.siteKey, this.onLoadComplete, langParam, this.baseUrl, this.nonce);
            }
          }
        }
      }, {
        key: "onExecute",
        get: function get() {
          if (!this.onExecuteSubject) {
            this.onExecuteSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
            this.onExecuteObservable = this.onExecuteSubject.asObservable();
          }

          return this.onExecuteObservable;
        }
      }, {
        key: "onExecuteError",
        get: function get() {
          if (!this.onExecuteErrorSubject) {
            this.onExecuteErrorSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
            this.onExecuteErrorObservable = this.onExecuteErrorSubject.asObservable();
          }

          return this.onExecuteErrorObservable;
        }
      }]);

      return ReCaptchaV3Service;
    }();

    ReCaptchaV3Service.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }];

    ReCaptchaV3Service.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]
      }, {
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [RECAPTCHA_V3_SITE_KEY]
        }]
      }, {
        type: Object,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"]]
        }]
      }, {
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [RECAPTCHA_BASE_URL]
        }]
      }, {
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [RECAPTCHA_NONCE]
        }]
      }, {
        type: String,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
          args: [RECAPTCHA_LANGUAGE]
        }]
      }];
    };

    var RecaptchaV3Module = function RecaptchaV3Module() {
      _classCallCheck(this, RecaptchaV3Module);
    };

    RecaptchaV3Module.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
      args: [{
        providers: [ReCaptchaV3Service]
      }]
    }];

    var RecaptchaValueAccessorDirective = /*#__PURE__*/function () {
      function RecaptchaValueAccessorDirective(host) {
        _classCallCheck(this, RecaptchaValueAccessorDirective);

        this.host = host;
      }

      _createClass(RecaptchaValueAccessorDirective, [{
        key: "writeValue",
        value: function writeValue(value) {
          if (!value) {
            this.host.reset();
          }
        }
      }, {
        key: "registerOnChange",
        value: function registerOnChange(fn) {
          this.onChange = fn;
        }
      }, {
        key: "registerOnTouched",
        value: function registerOnTouched(fn) {
          this.onTouched = fn;
        }
      }, {
        key: "onResolve",
        value: function onResolve($event) {
          if (this.onChange) {
            this.onChange($event);
          }

          if (this.onTouched) {
            this.onTouched();
          }
        }
      }]);

      return RecaptchaValueAccessorDirective;
    }();

    RecaptchaValueAccessorDirective.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
      args: [{
        providers: [{
          multi: true,
          provide: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"],
          // tslint:disable-next-line:no-forward-ref
          useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () {
            return RecaptchaValueAccessorDirective;
          })
        }],
        // tslint:disable-next-line:directive-selector
        selector: "re-captcha[formControlName],re-captcha[formControl],re-captcha[ngModel]"
      }]
    }];

    RecaptchaValueAccessorDirective.ctorParameters = function () {
      return [{
        type: RecaptchaComponent
      }];
    };

    RecaptchaValueAccessorDirective.propDecorators = {
      onResolve: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
        args: ["resolved", ["$event"]]
      }]
    };

    var RecaptchaFormsModule = function RecaptchaFormsModule() {
      _classCallCheck(this, RecaptchaFormsModule);
    };

    RecaptchaFormsModule.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
      args: [{
        declarations: [RecaptchaValueAccessorDirective],
        exports: [RecaptchaValueAccessorDirective],
        imports: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], RecaptchaCommonModule]
      }]
    }];
    /**
     * Generated bundle index. Do not edit.
     */
    //# sourceMappingURL=ng-recaptcha.js.map

    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/recover.component.html":
  /*!********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/recover.component.html ***!
    \********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRecoverRecoverComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <router-outlet></router-outlet>\n\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step1/step1.component.html":
  /*!************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step1/step1.component.html ***!
    \************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRecoverStep1Step1ComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                <span>\n                                    &#191;Olvidaste tu contrase&ntilde;a&#63;\n                                </span>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"ok\">\n                        <div class=\"basicrow m-top-i\">\n                            <div class=\"basicrow\">\n                                <input type=\"text\"\n                                    id=\"username\"\n                                    name=\"username\"\n                                    [(ngModel)]=\"username\"\n                                    placeholder=\"Correo o N&uacute;mero de Tel&eacute;fono\"\n                                    class=\"inp-f\"\n                                    autocapitalize=\"off\">\n                            </div>\n                        </div>\n\n                        <div class=\"basicrow m-top-i\">\n\n                            <form #captchaProtectedForm=\"ngForm\" class=\"captcha\">\n                                <re-captcha\n                                    #captchaRef=\"reCaptcha\"\n                                    [(ngModel)]=\"formModel.captcha\"\n                                    name=\"captcha\"\n                                    required\n                                    [siteKey]=\"siteKey\"\n                                    #captchaControl=\"ngModel\"\n                                ></re-captcha>\n                              </form>\n\n                        </div>\n\n\n                        <div class=\"basicrow m-top-i\">\n                            <div [className]=\"(formModel && formModel.captcha) ? 'btns vcenter red rippleR' : 'btns vcenter gray'\" (click)=\"nextStep()\">\n                                <div class=\"tabcell\">\n                                    Continuar\n                                </div>\n                            </div>\n                        </div>\n\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step2/step2.component.html":
  /*!************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step2/step2.component.html ***!
    \************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRecoverStep2Step2ComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                &#191;Olvidaste tu contrase&ntilde;a&#63;\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow f-reg f-black roboto-r m-top-i text-justify\">\n                    Escoge entre las opciones para recuperación de acceso a tu cuenta.\n                </div>\n\n                <div class=\"basicrow\">\n                    <div class=\"basicrow m-top-i text-left\">\n                        <label>\n                            <input [(ngModel)]=\"selected\" type=\"radio\" name=\"group\" [value]=\"1\" checked/>\n                            <span *ngIf=\"isTelephony\">&nbsp;V&iacute;a correo electr&oacute;nico</span>\n                            <span *ngIf=\"!isTelephony\">&nbsp;V&iacute;a SMS o correo electr&oacute;nico</span>\n                        </label>\n                    </div>\n\n                    <div class=\"basicrow m-top text-left\">\n                        <label>\n                            <input [(ngModel)]=\"selected\" type=\"radio\" name=\"group\" [value]=\"2\"/>\n                            <span>&nbsp;Contestar preguntas de Seguridad</span>\n                        </label>\n                    </div>\n                </div>\n\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"btns red vcenter rippleR\" (click)=\"nextStep()\">\n                        <div class=\"tabcell\">\n                            Continuar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step3/step3.component.html":
  /*!************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step3/step3.component.html ***!
    \************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRecoverStep3Step3ComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                &#191;Olvidaste tu contrase&ntilde;a&#63;\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                    &nbsp;&nbsp;&nbsp;Por favor responde las preguntas de seguridad previamente configuradas en tu cuenta de acceso.\n                </div>\n\n                <div class=\"basicrow m-top-i text-left\">\n                    <label id=\"question1\" class=\"label-f\">{{firstQuestion}}</label>\n                    <div class=\"basicrow rel\">\n                        <input type=\"password\" placeholder=\"Respuesta\" class=\"inp-f\" autocapitalize=\"off\" [(ngModel)]=\"firstAnswer\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top text-left\">\n                    <label id=\"question2\" class=\"label-f\">{{secondQuestion}}</label>\n                    <div class=\"basicrow\">\n                        <input type=\"password\" placeholder=\"Respuesta\" class=\"inp-f\" autocapitalize=\"off\" [(ngModel)]=\"secondAnswer\">\n                    </div>\n                </div>\n\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"btns red vcenter rippleR\" (click)=\"nextStep()\">\n                        <div class=\"tabcell\">\n                            Recuperar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step4/step4.component.html":
  /*!************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step4/step4.component.html ***!
    \************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRecoverStep4Step4ComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                &#191;Olvidaste tu contrase&ntilde;a&#63;\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow f-reg f-black roboto-r m-top-i text-justify\">\n                    Escoja d&oacute;nde deseas recibir la contrase&ntilde;a temporal.\n                </div>\n\n                <div class=\"basicrow f-reg f-black roboto-r\">\n                    <div class=\"row\">\n\n                        <div *ngIf=\"!isTelephony\" class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n                            <div [ngClass]=\"{'on': bySubscriber}\" class=\"optchoice roboto-r text-left\" (click)=\"byEmail = false; bySubscriber = true; $event.stopPropagation()\">\n                                <div class=\"checkdef\">\n                                    <input type=\"checkbox\" class=\"css-radio\" [(ngModel)]=\"bySubscriber\"/><label class=\"css-label3 radGroup1\"></label>\n                                </div>\n\n                                <div class=\"basicrow opttext\">\n                                    N&uacute;mero Registrado<br/>\n                                    <span class=\"roboto-b\">{{subscriberHidden}}</span>\n                                </div>\n                            </div>\n                        </div>\n\n                        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n                            <div [ngClass]=\"{'on': byEmail}\" class=\"optchoice roboto-r text-left\" (click)=\"byEmail = true; bySubscriber = false; $event.stopPropagation()\">\n                                <div class=\"checkdef\">\n                                    <input type=\"checkbox\" class=\"css-radio\" [(ngModel)]=\"byEmail\"/><label class=\"css-label3 radGroup1\"></label>\n                                </div>\n\n                                <div class=\"basicrow opttext\">\n                                    Email Registrado<br/>\n                                    <span class=\"roboto-b\">{{emailHidden}}</span>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div (click)=\"nextStep()\" [className]=\"byEmail || bySubscriber ? 'btns vcenter red rippleR' : 'btns vcenter gray'\">\n                        <div class=\"tabcell\">\n                            Recuperar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step5/step5.component.html":
  /*!************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step5/step5.component.html ***!
    \************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRecoverStep5Step5ComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                &#191;Olvidaste tu contrase&ntilde;a&#63;\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                        Debe modificar la clave de acceso para poder ingresar a su cuenta Claro.\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"oldPassword\" placeholder=\"Contrase&ntilde;a temporal\" class=\"inp-f\" maxlength=\"15\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"password\" placeholder=\"Nueva Contrase&ntilde;a\" class=\"inp-f\" maxlength=\"15\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"passwordRepeat\" placeholder=\"Confirmar Contrase&ntilde;a\" class=\"inp-f\" maxlength=\"15\" (keyup.enter)=\"nextStep()\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow f-black m-top roboto-r\">\n                        <div class=\"basicrow f-lmini roboto-b\">\n                            Requerimientos de contrase&ntilde;a:\n                        </div>\n\n                        <div class=\"basicrow\">\n                            <ul class=\"redb\">\n                                <li [ngClass]=\"{'done': (password.length >= 8 && password.length <= 16)}\">\n                                    Debe tener entre 8 y 15 caracteres\n                                </li>\n\n                                <li [ngClass]=\"{'done': lowercaseValidator(password)}\">\n                                    Al menos una letra min&uacute;scula\n                                </li>\n\n                                <li [ngClass]=\"{'done': uppercaseValidator(password)}\">\n                                    Al menos una letra may&uacute;scula\n                                </li>\n\n                                <li [ngClass]=\"{'done': twoNumberValidator(password)}\">\n                                    Al menos 2 n&uacute;meros\n                                </li>\n\n                                <li [ngClass]=\"{'done': specialCharacterValidator(password)}\">\n                                    No est&aacute; permitido ning&uacute;n car&aacute;cter especial o no alfanum&eacute;rico\n                                </li>\n\n                                <li [ngClass]=\"{'done': password === passwordRepeat && password.length > 0}\">\n                                    Las contrase&ntilde;as deben coincidir\n                                </li>\n                            </ul>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div (click)=\"nextStep()\" [className]=\"\n                        (password.length >= 8 && password.length <= 16)\n                         && lowercaseValidator(password)\n                         && uppercaseValidator(password)\n                         && twoNumberValidator(password)\n                         && specialCharacterValidator(password)\n                         && (password === passwordRepeat && password.length > 0)\n                         ? 'btns vcenter red rippleR' : 'btns vcenter gray'\">\n                            <div class=\"tabcell\">\n                                Continuar\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow text-center m-top-i\">\n                        <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step6/step6.component.html":
  /*!************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step6/step6.component.html ***!
    \************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRecoverStep6Step6ComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                &#191;Olvidaste tu contrase&ntilde;a&#63;\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                        La contraseña de acceso es la clave con la que podrás acceder a manejar tu cuenta de servicios en Claro.\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"password\" placeholder=\"Nueva Contrase&ntilde;a\" class=\"inp-f\" maxlength=\"15\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"passwordRepeat\" placeholder=\"Confirmar Contrase&ntilde;a\" class=\"inp-f\" maxlength=\"15\" (keyup.enter)=\"nextStep()\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow f-black m-top roboto-r\">\n                        <div class=\"basicrow f-lmini roboto-b\">\n                            Requerimientos de contrase&ntilde;a:\n                        </div>\n\n                        <div class=\"basicrow\">\n                            <ul class=\"redb\">\n                                <li [ngClass]=\"{'done': (password.length >= 8 && password.length <= 16)}\">\n                                    Debe tener entre 8 y 15 caracteres\n                                </li>\n\n                                <li [ngClass]=\"{'done': lowercaseValidator(password)}\">\n                                    Al menos una letra min&uacute;scula\n                                </li>\n\n                                <li [ngClass]=\"{'done': uppercaseValidator(password)}\">\n                                    Al menos una letra may&uacute;scula\n                                </li>\n\n                                <li [ngClass]=\"{'done': twoNumberValidator(password)}\">\n                                    Al menos 2 n&uacute;meros\n                                </li>\n\n                                <li [ngClass]=\"{'done': specialCharacterValidator(password)}\">\n                                    No est&aacute; permitido ning&uacute;n car&aacute;cter especial o no alfanum&eacute;rico\n                                </li>\n\n                                <li [ngClass]=\"{'done': password === passwordRepeat && password.length > 0}\">\n                                    Las contrase&ntilde;as deben coincidir\n                                </li>\n                            </ul>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div (click)=\"nextStep()\" [className]=\"\n                        (password.length >= 8 && password.length <= 16)\n                         && lowercaseValidator(password)\n                         && uppercaseValidator(password)\n                         && twoNumberValidator(password)\n                         && specialCharacterValidator(password)\n                         && (password === passwordRepeat && password.length > 0)\n                         ? 'btns vcenter red rippleR' : 'btns vcenter gray'\">\n                            <div class=\"tabcell\">\n                                Continuar\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow text-center m-top-i\">\n                        <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n";
    /***/
  },

  /***/
  "./src/app/pages/recover/recover.component.ts":
  /*!****************************************************!*\
    !*** ./src/app/pages/recover/recover.component.ts ***!
    \****************************************************/

  /*! exports provided: RecoverComponent */

  /***/
  function srcAppPagesRecoverRecoverComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RecoverComponent", function () {
      return RecoverComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var RecoverComponent = /*#__PURE__*/function () {
      function RecoverComponent() {
        _classCallCheck(this, RecoverComponent);
      }

      _createClass(RecoverComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return RecoverComponent;
    }();

    RecoverComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-recover',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./recover.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/recover.component.html"))["default"]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], RecoverComponent);
    /***/
  },

  /***/
  "./src/app/pages/recover/recover.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/pages/recover/recover.module.ts ***!
    \*************************************************/

  /*! exports provided: routes, RecoverModule */

  /***/
  function srcAppPagesRecoverRecoverModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "routes", function () {
      return routes;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RecoverModule", function () {
      return RecoverModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../_shared/shared.module */
    "./src/app/pages/_shared/shared.module.ts");
    /* harmony import */


    var _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./step1/step1.component */
    "./src/app/pages/recover/step1/step1.component.ts");
    /* harmony import */


    var _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./step2/step2.component */
    "./src/app/pages/recover/step2/step2.component.ts");
    /* harmony import */


    var _step3_step3_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./step3/step3.component */
    "./src/app/pages/recover/step3/step3.component.ts");
    /* harmony import */


    var _step4_step4_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./step4/step4.component */
    "./src/app/pages/recover/step4/step4.component.ts");
    /* harmony import */


    var _step5_step5_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./step5/step5.component */
    "./src/app/pages/recover/step5/step5.component.ts");
    /* harmony import */


    var _step6_step6_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./step6/step6.component */
    "./src/app/pages/recover/step6/step6.component.ts");
    /* harmony import */


    var _recover_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./recover.component */
    "./src/app/pages/recover/recover.component.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var ngx_popper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ngx-popper */
    "./node_modules/ngx-popper/fesm2015/ngx-popper.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var ng_recaptcha__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ng-recaptcha */
    "./node_modules/ng-recaptcha/fesm2015/ng-recaptcha.js");

    var routes = [{
      path: 'step1',
      component: _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__["Step1Component"]
    }, {
      path: 'step2',
      component: _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__["Step2Component"]
    }, {
      path: 'step3',
      component: _step3_step3_component__WEBPACK_IMPORTED_MODULE_6__["Step3Component"]
    }, {
      path: 'step4',
      component: _step4_step4_component__WEBPACK_IMPORTED_MODULE_7__["Step4Component"]
    }, {
      path: 'step5',
      component: _step5_step5_component__WEBPACK_IMPORTED_MODULE_8__["Step5Component"]
    }, {
      path: 'step6',
      component: _step6_step6_component__WEBPACK_IMPORTED_MODULE_9__["Step6Component"]
    }];

    var RecoverModule = function RecoverModule() {
      _classCallCheck(this, RecoverModule);
    };

    RecoverModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      providers: [{
        provide: ng_recaptcha__WEBPACK_IMPORTED_MODULE_14__["RECAPTCHA_LANGUAGE"],
        useValue: 'es'
      }],
      declarations: [_recover_component__WEBPACK_IMPORTED_MODULE_10__["RecoverComponent"], _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__["Step1Component"], _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__["Step2Component"], _step3_step3_component__WEBPACK_IMPORTED_MODULE_6__["Step3Component"], _step4_step4_component__WEBPACK_IMPORTED_MODULE_7__["Step4Component"], _step5_step5_component__WEBPACK_IMPORTED_MODULE_8__["Step5Component"], _step6_step6_component__WEBPACK_IMPORTED_MODULE_9__["Step6Component"]],
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_13__["RouterModule"].forChild(routes), _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ReactiveFormsModule"], ng_recaptcha__WEBPACK_IMPORTED_MODULE_14__["RecaptchaFormsModule"], ng_recaptcha__WEBPACK_IMPORTED_MODULE_14__["RecaptchaModule"], ngx_popper__WEBPACK_IMPORTED_MODULE_12__["NgxPopperModule"].forRoot({
        placement: 'top',
        styles: {
          'background-color': 'white'
        }
      })]
    })], RecoverModule);
    /***/
  },

  /***/
  "./src/app/pages/recover/step1/step1.component.scss":
  /*!**********************************************************!*\
    !*** ./src/app/pages/recover/step1/step1.component.scss ***!
    \**********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesRecoverStep1Step1ComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".error {\n  color: crimson;\n}\n\n.success {\n  color: green;\n}\n\n.captcha {\n  display: flex;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZG1pbi9Eb2N1bWVudHMvRSRHUy9SRUJSTkFESU5HL1Jlc3BhbGRvcy9pb25pYy9taWNsYXJvMy1pb25pYy12ZXJzaW9uL3NyYy9hcHAvcGFnZXMvcmVjb3Zlci9zdGVwMS9zdGVwMS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcGFnZXMvcmVjb3Zlci9zdGVwMS9zdGVwMS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7QUNDSjs7QURDQTtFQUNJLFlBQUE7QUNFSjs7QURDQTtFQUNJLGFBQUE7RUFBZSx1QkFBQTtBQ0duQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY292ZXIvc3RlcDEvc3RlcDEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXJyb3Ige1xuICAgIGNvbG9yOiBjcmltc29uO1xufVxuLnN1Y2Nlc3Mge1xuICAgIGNvbG9yOiBncmVlbjtcbn1cblxuLmNhcHRjaGEge1xuICAgIGRpc3BsYXk6IGZsZXg7IGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuIiwiLmVycm9yIHtcbiAgY29sb3I6IGNyaW1zb247XG59XG5cbi5zdWNjZXNzIHtcbiAgY29sb3I6IGdyZWVuO1xufVxuXG4uY2FwdGNoYSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/recover/step1/step1.component.ts":
  /*!********************************************************!*\
    !*** ./src/app/pages/recover/step1/step1.component.ts ***!
    \********************************************************/

  /*! exports provided: Step1Component */

  /***/
  function srcAppPagesRecoverStep1Step1ComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Step1Component", function () {
      return Step1Component;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../base.page */
    "./src/app/pages/base.page.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/services.provider */
    "./src/app/services/services.provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../utils/const/keys */
    "./src/app/utils/const/keys.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../services/utils.service */
    "./src/app/services/utils.service.ts");
    /* harmony import */


    var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../services/intent.provider */
    "./src/app/services/intent.provider.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");

    var Step1Component = /*#__PURE__*/function (_base_page__WEBPACK_I) {
      _inherits(Step1Component, _base_page__WEBPACK_I);

      var _super = _createSuper(Step1Component);

      function Step1Component(router, storage, modelsServices, alertController, utilsService, userStorage, fb) {
        var _this6;

        _classCallCheck(this, Step1Component);

        _this6 = _super.call(this, router, storage, modelsServices, alertController, utilsService, userStorage);
        _this6.fb = fb;
        _this6.formModel = {};

        _this6.utils.registerScreen('forgot_password');

        return _this6;
      }

      _createClass(Step1Component, [{
        key: "nextStep",
        value: function nextStep() {
          if (this.formModel.captcha) {
            this.goNext();
          } else {
            this.showAlert('Debe completar el captcha para continuar');
          }
        }
      }, {
        key: "goNext",
        value: function goNext() {
          var _this7 = this;

          if (!this.username) {
            this.showError('El número de teléfono o correo ingresado no se encuentra registrado en nuestros sistemas, ' + 'su formato es incorrecto o no pertenece a nuestra red. Por favor intente nuevamente.');
            this.formModel.captcha = '';
          } else {
            this.showProgress();
            this.services.getChallengeQuestions(this.username).then(function (success) {
              _this7.dismissProgress();

              _this7.processResponse(success);
            }, function (error) {
              _this7.dismissProgress();

              _this7.showError(error.message);

              _this7.formModel.captcha = '';
            });
          }
        }
      }, {
        key: "processResponse",
        value: function processResponse(response) {
          this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.SUBSCRIBER, response.subscriber);
          this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.EMAIL, response.email);

          if (response.accountType === 'I' && response.accountSubType === 'W' && response.productType === 'O') {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.IS_TELEPHONY, true);
          } else {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.IS_TELEPHONY, false);
          }

          if (response.ResponseList == null) {
            this.goPage('/recover/step4');
          } else {
            var questions = [];

            var _iterator = _createForOfIteratorHelper(response.ResponseList),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var object = _step.value;
                questions.push({
                  question: object.question,
                  questionID: object.questionID,
                  response: object.response
                });
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }

            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.QUESTION_LIST, questions);
            this.goPage('/recover/step2');
          }

          this.formModel.captcha = '';
        }
      }, {
        key: "siteKey",
        get: function get() {
          return _environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].siteKey;
        }
      }]);

      return Step1Component;
    }(_base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"]);

    Step1Component.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"]
      }, {
        type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormBuilder"]
      }];
    };

    Step1Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'recover-step1',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./step1.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step1/step1.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./step1.component.scss */
      "./src/app/pages/recover/step1/step1.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"], _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"], _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormBuilder"]])], Step1Component);
    /***/
  },

  /***/
  "./src/app/pages/recover/step2/step2.component.scss":
  /*!**********************************************************!*\
    !*** ./src/app/pages/recover/step2/step2.component.scss ***!
    \**********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesRecoverStep2Step2ComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY292ZXIvc3RlcDIvc3RlcDIuY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/pages/recover/step2/step2.component.ts":
  /*!********************************************************!*\
    !*** ./src/app/pages/recover/step2/step2.component.ts ***!
    \********************************************************/

  /*! exports provided: Step2Component */

  /***/
  function srcAppPagesRecoverStep2Step2ComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Step2Component", function () {
      return Step2Component;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../base.page */
    "./src/app/pages/base.page.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/services.provider */
    "./src/app/services/services.provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../services/utils.service */
    "./src/app/services/utils.service.ts");
    /* harmony import */


    var _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../services/intent.provider */
    "./src/app/services/intent.provider.ts");

    var Step2Component = /*#__PURE__*/function (_base_page__WEBPACK_I2) {
      _inherits(Step2Component, _base_page__WEBPACK_I2);

      var _super2 = _createSuper(Step2Component);

      function Step2Component(router, storage, modelsServices, alertController, utilsService, userStorage) {
        var _this8;

        _classCallCheck(this, Step2Component);

        _this8 = _super2.call(this, router, storage, modelsServices, alertController, utilsService, userStorage);
        _this8.username = '';
        _this8.isTelephony = false;
        _this8.selected = 1;
        return _this8;
      }

      _createClass(Step2Component, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "nextStep",
        value: function nextStep() {
          console.log(this.selected);

          if (this.selected === 1) {
            this.goPage('/recover/step4');
          }

          if (this.selected === 2) {
            this.goPage('/recover/step3');
          }
        }
      }]);

      return Step2Component;
    }(_base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"]);

    Step2Component.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"]
      }, {
        type: _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"]
      }, {
        type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"]
      }];
    };

    Step2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'recover-step2',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./step2.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step2/step2.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./step2.component.scss */
      "./src/app/pages/recover/step2/step2.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"], _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"]])], Step2Component);
    /***/
  },

  /***/
  "./src/app/pages/recover/step3/step3.component.scss":
  /*!**********************************************************!*\
    !*** ./src/app/pages/recover/step3/step3.component.scss ***!
    \**********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesRecoverStep3Step3ComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY292ZXIvc3RlcDMvc3RlcDMuY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/pages/recover/step3/step3.component.ts":
  /*!********************************************************!*\
    !*** ./src/app/pages/recover/step3/step3.component.ts ***!
    \********************************************************/

  /*! exports provided: Step3Component */

  /***/
  function srcAppPagesRecoverStep3Step3ComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Step3Component", function () {
      return Step3Component;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../base.page */
    "./src/app/pages/base.page.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/services.provider */
    "./src/app/services/services.provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../services/utils.service */
    "./src/app/services/utils.service.ts");
    /* harmony import */


    var _utils_const_keys__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../utils/const/keys */
    "./src/app/utils/const/keys.ts");
    /* harmony import */


    var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../services/intent.provider */
    "./src/app/services/intent.provider.ts");

    var Step3Component = /*#__PURE__*/function (_base_page__WEBPACK_I3) {
      _inherits(Step3Component, _base_page__WEBPACK_I3);

      var _super3 = _createSuper(Step3Component);

      function Step3Component(router, storage, modelsServices, alertController, utilsService, userStorage) {
        var _this9;

        _classCallCheck(this, Step3Component);

        _this9 = _super3.call(this, router, storage, modelsServices, alertController, utilsService, userStorage);
        _this9.firstQuestion = '¿Primera Pregunta?';
        _this9.secondQuestion = '¿Segunda Pregunta?';
        _this9.firstAnswer = '';
        _this9.secondAnswer = '';
        _this9.questions = [];
        return _this9;
      }

      _createClass(Step3Component, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this10 = this;

          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].RECOVER_PASSWORD.QUESTION_LIST).then(function (questions) {
            _this10.questions = questions;

            if (_this10.questions.length > 0) {
              _this10.setQuestions();
            }
          });
          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].RECOVER_PASSWORD.SUBSCRIBER).then(function (success) {
            _this10.subscriber = success;
          });
        }
      }, {
        key: "setQuestions",
        value: function setQuestions() {
          var question1 = this.questions[0].question;
          var question2 = this.questions[1].question; // init validate if you have question marks

          if (!question1.includes('¿')) {
            question1 = '¿' + question1;
          }

          if (!question1.includes('?')) {
            question1 = question1 + '?';
          }

          if (!question2.includes('¿')) {
            question2 = '¿' + question2;
          }

          if (!question2.includes('?')) {
            question2 = question2 + '?';
          }

          this.firstQuestion = question1;
          this.secondQuestion = question2;
        }
      }, {
        key: "nextStep",
        value: function nextStep() {
          var _this11 = this;

          if (this.firstAnswer.length <= 0 || this.secondAnswer.length <= 0) {
            this.showError('Por favor, complete los campos para continuar.');
          } else {
            this.questions[0].response = this.firstAnswer;
            this.questions[1].response = this.secondAnswer;
            this.showProgress();
            this.services.answerSecurityQuestions(this.subscriber, this.questions).then(function (success) {
              _this11.dismissProgress();

              _this11.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].RECOVER_PASSWORD.TEMPORARY_PASSWORD, success.newpassword);

              _this11.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].RECOVER_PASSWORD.TOKEN, success.token);

              _this11.goPage('/recover/step6');
            }, function (error) {
              _this11.dismissProgress();

              _this11.showError(error.message);
            });
          }
        }
      }]);

      return Step3Component;
    }(_base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"]);

    Step3Component.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"]
      }, {
        type: _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"]
      }, {
        type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]
      }];
    };

    Step3Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'recover-step3',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./step3.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step3/step3.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./step3.component.scss */
      "./src/app/pages/recover/step3/step3.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"], _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])], Step3Component);
    /***/
  },

  /***/
  "./src/app/pages/recover/step4/step4.component.scss":
  /*!**********************************************************!*\
    !*** ./src/app/pages/recover/step4/step4.component.scss ***!
    \**********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesRecoverStep4Step4ComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY292ZXIvc3RlcDQvc3RlcDQuY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/pages/recover/step4/step4.component.ts":
  /*!********************************************************!*\
    !*** ./src/app/pages/recover/step4/step4.component.ts ***!
    \********************************************************/

  /*! exports provided: Step4Component */

  /***/
  function srcAppPagesRecoverStep4Step4ComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Step4Component", function () {
      return Step4Component;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/services.provider */
    "./src/app/services/services.provider.ts");
    /* harmony import */


    var _base_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../base.page */
    "./src/app/pages/base.page.ts");
    /* harmony import */


    var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../utils/const/keys */
    "./src/app/utils/const/keys.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../services/utils.service */
    "./src/app/services/utils.service.ts");
    /* harmony import */


    var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../services/intent.provider */
    "./src/app/services/intent.provider.ts");

    var Step4Component = /*#__PURE__*/function (_base_page__WEBPACK_I4) {
      _inherits(Step4Component, _base_page__WEBPACK_I4);

      var _super4 = _createSuper(Step4Component);

      function Step4Component(router, storage, modelsServices, alertController, utilsService, userStorage) {
        var _this12;

        _classCallCheck(this, Step4Component);

        _this12 = _super4.call(this, router, storage, modelsServices, alertController, utilsService, userStorage);
        _this12.isTelephony = true;
        _this12.subscriberHidden = '';
        _this12.subscriber = '';
        _this12.emailHidden = '';
        _this12.email = '';
        _this12.bySubscriber = false;
        _this12.byEmail = false;
        return _this12;
      }

      _createClass(Step4Component, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this13 = this;

          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.IS_TELEPHONY).then(function (success) {
            _this13.isTelephony = success;
          });
          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.SUBSCRIBER).then(function (success) {
            _this13.subscriber = success;
            _this13.subscriberHidden = success.substr(success.length - 4);
            _this13.subscriberHidden = '********' + _this13.subscriberHidden;
          });
          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.EMAIL).then(function (success) {
            _this13.email = success;
            var first2 = success.substring(0, 2);
            var emailCut = success.split('@');
            _this13.emailHidden = first2 + '******@' + emailCut[1];
          });
        }
      }, {
        key: "nextStep",
        value: function nextStep() {
          var _this14 = this;

          if (!this.bySubscriber && !this.byEmail) {
            return;
          } else {
            this.showProgress();
            this.services.recoveryPasswordBySubscriber(this.subscriber, this.bySubscriber ? this.subscriber : '', this.byEmail ? this.email : '').then(function (success) {
              _this14.showAlert(success.response, function () {
                _this14.goLoginPage();
              });

              _this14.dismissProgress();
            }, function (error) {
              _this14.dismissProgress();

              _this14.showError(error.message);
            });
          }
        }
      }]);

      return Step4Component;
    }(_base_page__WEBPACK_IMPORTED_MODULE_5__["BasePage"]);

    Step4Component.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"]
      }, {
        type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]
      }];
    };

    Step4Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'recover-step4',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./step4.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step4/step4.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./step4.component.scss */
      "./src/app/pages/recover/step4/step4.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"], _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])], Step4Component);
    /***/
  },

  /***/
  "./src/app/pages/recover/step5/step5.component.scss":
  /*!**********************************************************!*\
    !*** ./src/app/pages/recover/step5/step5.component.scss ***!
    \**********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesRecoverStep5Step5ComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY292ZXIvc3RlcDUvc3RlcDUuY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/pages/recover/step5/step5.component.ts":
  /*!********************************************************!*\
    !*** ./src/app/pages/recover/step5/step5.component.ts ***!
    \********************************************************/

  /*! exports provided: Step5Component */

  /***/
  function srcAppPagesRecoverStep5Step5ComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Step5Component", function () {
      return Step5Component;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/services.provider */
    "./src/app/services/services.provider.ts");
    /* harmony import */


    var _base_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../base.page */
    "./src/app/pages/base.page.ts");
    /* harmony import */


    var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../utils/const/keys */
    "./src/app/utils/const/keys.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../services/utils.service */
    "./src/app/services/utils.service.ts");
    /* harmony import */


    var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../services/intent.provider */
    "./src/app/services/intent.provider.ts");

    var Step5Component = /*#__PURE__*/function (_base_page__WEBPACK_I5) {
      _inherits(Step5Component, _base_page__WEBPACK_I5);

      var _super5 = _createSuper(Step5Component);

      function Step5Component(router, storage, modelsServices, alertController, utilsService, userStorage) {
        var _this15;

        _classCallCheck(this, Step5Component);

        _this15 = _super5.call(this, router, storage, modelsServices, alertController, utilsService, userStorage);
        _this15.oldPassword = '';
        _this15.password = '';
        _this15.passwordRepeat = '';
        _this15.token = '';
        return _this15;
      }

      _createClass(Step5Component, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this16 = this;

          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.TOKEN).then(function (success) {
            _this16.token = success;
          });
        }
      }, {
        key: "nextStep",
        value: function nextStep() {
          var _this17 = this;

          if (this.password.length < 8 || this.password.length > 15 || this.password !== this.passwordRepeat || !this.lowercaseValidator(this.password) || !this.uppercaseValidator(this.password) || !this.twoNumberValidator(this.password) || !this.specialCharacterValidator(this.password)) {
            return;
          } else if (this.oldPassword.length < 8) {
            this.showError('Hemos detectado que la clave temporal ingresada no es válida. Por favor intente nuevamente.');
          } else {
            this.showProgress();
            this.services.passwordUpdate(btoa(this.oldPassword), btoa(this.password), this.token).then(function (success) {
              _this17.dismissProgress();

              _this17.showAlert(success.response, function () {
                _this17.goLoginPage();
              });
            }, function (error) {
              _this17.dismissProgress();

              _this17.showError(error.message);
            });
          }
        }
      }, {
        key: "lowercaseValidator",
        value: function lowercaseValidator(c) {
          var regex = /[a-z]/g;
          return regex.test(c);
        }
      }, {
        key: "uppercaseValidator",
        value: function uppercaseValidator(c) {
          var regex = /[A-Z]/g;
          return regex.test(c);
        }
      }, {
        key: "twoNumberValidator",
        value: function twoNumberValidator(c) {
          var numbers = '0123456789';
          var count = 0;

          for (var i = 0; i < c.length; i++) {
            if (numbers.indexOf(c.charAt(i), 0) !== -1) {
              count++;
            }
          }

          return count >= 2;
        }
      }, {
        key: "specialCharacterValidator",
        value: function specialCharacterValidator(c) {
          return c.match('^[A-z0-9]+$');
        }
      }]);

      return Step5Component;
    }(_base_page__WEBPACK_IMPORTED_MODULE_5__["BasePage"]);

    Step5Component.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"]
      }, {
        type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]
      }];
    };

    Step5Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'recover-step5',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./step5.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step5/step5.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./step5.component.scss */
      "./src/app/pages/recover/step5/step5.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"], _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])], Step5Component);
    /***/
  },

  /***/
  "./src/app/pages/recover/step6/step6.component.scss":
  /*!**********************************************************!*\
    !*** ./src/app/pages/recover/step6/step6.component.scss ***!
    \**********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesRecoverStep6Step6ComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY292ZXIvc3RlcDYvc3RlcDYuY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/pages/recover/step6/step6.component.ts":
  /*!********************************************************!*\
    !*** ./src/app/pages/recover/step6/step6.component.ts ***!
    \********************************************************/

  /*! exports provided: Step6Component */

  /***/
  function srcAppPagesRecoverStep6Step6ComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Step6Component", function () {
      return Step6Component;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/services.provider */
    "./src/app/services/services.provider.ts");
    /* harmony import */


    var _base_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../base.page */
    "./src/app/pages/base.page.ts");
    /* harmony import */


    var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../utils/const/keys */
    "./src/app/utils/const/keys.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../services/utils.service */
    "./src/app/services/utils.service.ts");
    /* harmony import */


    var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../services/intent.provider */
    "./src/app/services/intent.provider.ts");

    var Step6Component = /*#__PURE__*/function (_base_page__WEBPACK_I6) {
      _inherits(Step6Component, _base_page__WEBPACK_I6);

      var _super6 = _createSuper(Step6Component);

      function Step6Component(router, storage, modelsServices, alertController, utilsService, userStorage) {
        var _this18;

        _classCallCheck(this, Step6Component);

        _this18 = _super6.call(this, router, storage, modelsServices, alertController, utilsService, userStorage);
        _this18.oldPassword = '';
        _this18.password = '';
        _this18.passwordRepeat = '';
        _this18.token = '';
        return _this18;
      }

      _createClass(Step6Component, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this19 = this;

          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.TEMPORARY_PASSWORD).then(function (success) {
            _this19.oldPassword = success;
          });
          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.TOKEN).then(function (success) {
            _this19.token = success;
          });
        }
      }, {
        key: "nextStep",
        value: function nextStep() {
          var _this20 = this;

          if (this.password.length < 8 || this.password.length > 15 || this.password !== this.passwordRepeat || !this.lowercaseValidator(this.password) || !this.uppercaseValidator(this.password) || !this.twoNumberValidator(this.password) || !this.specialCharacterValidator(this.password)) {
            return;
          } else {
            this.showProgress();
            this.services.passwordUpdate(this.oldPassword, btoa(this.password), this.token).then(function (success) {
              _this20.dismissProgress();

              _this20.showAlert(success.response, function () {
                _this20.goLoginPage();
              });
            }, function (error) {
              _this20.dismissProgress();

              _this20.showError(error.message);
            });
          }
        }
      }, {
        key: "lowercaseValidator",
        value: function lowercaseValidator(c) {
          var regex = /[a-z]/g;
          return regex.test(c);
        }
      }, {
        key: "uppercaseValidator",
        value: function uppercaseValidator(c) {
          var regex = /[A-Z]/g;
          return regex.test(c);
        }
      }, {
        key: "twoNumberValidator",
        value: function twoNumberValidator(c) {
          var numbers = '0123456789';
          var count = 0;

          for (var i = 0; i < c.length; i++) {
            if (numbers.indexOf(c.charAt(i), 0) !== -1) {
              count++;
            }
          }

          return count >= 2;
        }
      }, {
        key: "specialCharacterValidator",
        value: function specialCharacterValidator(c) {
          return c.match('^[A-z0-9]+$');
        }
      }]);

      return Step6Component;
    }(_base_page__WEBPACK_IMPORTED_MODULE_5__["BasePage"]);

    Step6Component.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"]
      }, {
        type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]
      }];
    };

    Step6Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'recover-step6',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./step6.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step6/step6.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./step6.component.scss */
      "./src/app/pages/recover/step6/step6.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"], _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])], Step6Component);
    /***/
  }
}]);
//# sourceMappingURL=pages-recover-recover-module-es5.js.map