function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-register-register-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/guest/guest.component.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/guest/guest.component.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRegisterGuestGuestComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-c-icon.png\">\n                                C&oacute;digo de seguridad\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                    Introduce el c&oacute;digo de seis d&iacute;gitos que hemos enviado a su n&uacute;mero de teléfono y/o correo electrónico asociado a su cuenta.\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"basicrow rel\">\n                        <input type=\"tel\"\n                               maxlength=\"6\"\n                               inputmode=\"numeric\"\n                               pattern=\"^[0-9]*$\"\n                               placeholder=\"Código de confirmación\"\n                               class=\"inp-f disc\"\n                               [(ngModel)]=\"code\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"btns red vcenter rippleR\" (click)=\"nextStep()\">\n                        <div class=\"tabcell\">\n                            Confirmar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top f-reg f-black roboto-r text-center\">\n                    &#191;No has recibido el c&oacute;digo de seguridad&#63; <a (click)=\"resend()\" class=\"linkdefs\">Reenviar</a>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.component.html":
  /*!**********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.component.html ***!
    \**********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRegisterRegisterComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <router-outlet></router-outlet>\n\n</div>\n\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step1/step1.component.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step1/step1.component.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRegisterStep1Step1ComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                Registro\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"basicrow\">\n                        <input type=\"number\"\n                               placeholder=\"N&uacute;mero de Tel&eacute;fono\"\n                               class=\"inp-f\"\n                               autocapitalize=\"off\"\n                               [(ngModel)]=\"number\"\n                               (keyup.enter)=\"nextStep()\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"btns red vcenter rippleR\" (click)=\"nextStep()\">\n                        <div class=\"tabcell\">\n                            Continuar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step2/step2.component.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step2/step2.component.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRegisterStep2Step2ComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\" style=\"overflow-y: auto\">\n\n    <app-header-static></app-header-static>\n\n    <app-popup-general-terms\n            *ngIf='showTerms'\n            (close)=\"showTerms = false\">\n    </app-popup-general-terms>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                Registro\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"basicrow rel\">\n                        <img alt=\"\" [popper]=\"'PIN a tu correo electrónico.'\"\n                             [popperTrigger]=\"'click'\"\n                             [popperTimeoutAfterShow]=\"3000\"\n                             class=\"q-t-pay\" src=\"assets/images/tooltip1.png\">\n                        <input type=\"tel\"\n                               maxlength=\"6\"\n                               inputmode=\"numeric\"\n                               pattern=\"^[0-9]*$\"\n                               placeholder=\"Código de confirmación\"\n                               class=\"inp-f disc\"\n                               [(ngModel)]=\"code\">\n                        <div class=\"basicrow m-top-u f-reg f-black roboto-r text-center\">\n                            &#191;No has recibido el c&oacute;digo de seguridad&#63; <a (click)=\"resend()\" class=\"linkdefs\">Reenviar</a>\n                        </div>\n                    </div>\n                </div>\n\n                <div *ngIf=\"!isPrepaid\" class=\"basicrow m-top-i\">\n                    <div class=\"basicrow\">\n                        <input type=\"tel\"\n                               maxlength=\"4\"\n                               inputmode=\"numeric\"\n                               pattern=\"^[0-9]*$\"\n                               placeholder=\"4 &uacute;ltimos de SSN\"\n                               class=\"inp-f disc\"\n                               [(ngModel)]=\"ssn\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"basicrow\">\n                        <input type=\"email\"\n                               placeholder=\"Correo Electr&oacute;nico\"\n                               class=\"inp-f\"\n                               [(ngModel)]=\"email\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a class=\"linkdefs\" (click)=\"showTerms = true\">T&eacute;rminos y Condiciones</a><br/>\n                    <div>\n                        <label class=\"label-terms\">\n                            <input type=\"checkbox\" [(ngModel)]=\"check\">\n                            &nbsp; He le&iacute;do, entiendo y acepto estos t&eacute;rminos y condiciones\n                        </label>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"btns red vcenter rippleR\" (click)=\"nextStep()\">\n                        <div class=\"tabcell\">\n                            Continuar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step3/step3.component.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step3/step3.component.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRegisterStep3Step3ComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon last\" src=\"assets/images/reg-c-icon.png\">\n                                Escoja su contrase&ntilde;a de acceso\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                        La contrase&ntilde;a de acceso es la clave con la que podr&aacute; acceder a manejar su cuenta de servicios en Claro\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"password\" placeholder=\"Contrase&ntilde;a\" class=\"inp-f\" maxlength=\"15\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"passwordRepeat\" placeholder=\"Confirmar Contrase&ntilde;a\" class=\"inp-f\" maxlength=\"15\" (keyup.enter)=\"nextStep()\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow f-black m-top roboto-r\">\n                        <div class=\"basicrow f-lmini roboto-b\">\n                            Requerimientos de contrase&ntilde;a:\n                        </div>\n\n                        <div class=\"basicrow\">\n                            <ul class=\"redb\">\n                                <li [ngClass]=\"{'done': (password.length >= 8 && password.length <= 16)}\">\n                                    Debe tener entre 8 y 15 caracteres\n                                </li>\n\n                                <li [ngClass]=\"{'done': lowercaseValidator(password)}\">\n                                    Al menos una letra min&uacute;scula\n                                </li>\n\n                                <li [ngClass]=\"{'done': uppercaseValidator(password)}\">\n                                    Al menos una letra may&uacute;scula\n                                </li>\n\n                                <li [ngClass]=\"{'done': twoNumberValidator(password)}\">\n                                    Al menos 2 n&uacute;meros\n                                </li>\n\n                                <li [ngClass]=\"{'done': specialCharacterValidator(password)}\">\n                                    No est&aacute; permitido ning&uacute;n car&aacute;cter especial o no alfanum&eacute;rico\n                                </li>\n\n                                <li [ngClass]=\"{'done': password === passwordRepeat && password.length > 0}\">\n                                    Las contrase&ntilde;as deben coincidir\n                                </li>\n                            </ul>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div (click)=\"nextStep()\" [className]=\"\n                        (password.length >= 8 && password.length <= 16)\n                         && lowercaseValidator(password)\n                         && uppercaseValidator(password)\n                         && twoNumberValidator(password)\n                         && specialCharacterValidator(password)\n                         && (password === passwordRepeat && password.length > 0)\n                         ? 'btns vcenter red rippleR' : 'btns vcenter gray'\">\n                            <div class=\"tabcell\">\n                                Continuar\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow text-center m-top-i\">\n                        <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n";
    /***/
  },

  /***/
  "./src/app/pages/register/guest/guest.component.scss":
  /*!***********************************************************!*\
    !*** ./src/app/pages/register/guest/guest.component.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesRegisterGuestGuestComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZ2lzdGVyL2d1ZXN0L2d1ZXN0LmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/pages/register/guest/guest.component.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/register/guest/guest.component.ts ***!
    \*********************************************************/

  /*! exports provided: GuestComponent */

  /***/
  function srcAppPagesRegisterGuestGuestComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GuestComponent", function () {
      return GuestComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../base.page */
    "./src/app/pages/base.page.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/services.provider */
    "./src/app/services/services.provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../utils/const/keys */
    "./src/app/utils/const/keys.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../services/utils.service */
    "./src/app/services/utils.service.ts");
    /* harmony import */


    var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../services/intent.provider */
    "./src/app/services/intent.provider.ts");

    var GuestComponent = /*#__PURE__*/function (_base_page__WEBPACK_I) {
      _inherits(GuestComponent, _base_page__WEBPACK_I);

      var _super = _createSuper(GuestComponent);

      function GuestComponent(router, storage, modelsServices, alertController, utilsService, userStorage) {
        _classCallCheck(this, GuestComponent);

        return _super.call(this, router, storage, modelsServices, alertController, utilsService, userStorage);
      }

      _createClass(GuestComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER).then(function (success) {
            _this.number = success;
          });
          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_GUEST_UPDATE).then(function (success) {
            _this.isUpdate = success;
          });
          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.TOKEN).then(function (success) {
            _this.token = success;
          });
        }
      }, {
        key: "nextStep",
        value: function nextStep() {
          if (this.code.length !== 6) {
            this.showError('Debe ingresar los datos solicitados.');
          } else {
            if (this.isUpdate) {
              this.updateGuest();
            } else {
              this.validateGuest();
            }
          }
        }
      }, {
        key: "validateGuest",
        value: function validateGuest() {
          var _this2 = this;

          this.showProgress();
          this.services.validateGuest(this.number, this.code, this.token).then(function (success) {
            _this2.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.SUBSCRIBER, String(_this2.number));

            _this2.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.IS_LOGGED, true);

            _this2.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.IS_GUEST, true);

            _this2.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.BIOMETRIC, false);

            _this2.dismissProgress();

            _this2.showAlert('Su registro como usuario invitado fue realizado exitosamente.', function () {
              _this2.goPage('/guest');
            });
          }, function (error) {
            _this2.dismissProgress();

            _this2.showError(error.message);
          });
        }
      }, {
        key: "updateGuest",
        value: function updateGuest() {
          var _this3 = this;

          this.showProgress();
          this.services.updateGuest(this.number, this.code).then(function (success) {
            _this3.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.SUBSCRIBER, String(_this3.number));

            _this3.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.IS_LOGGED, true);

            _this3.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.IS_GUEST, true);

            _this3.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.BIOMETRIC, false);

            _this3.dismissProgress();

            _this3.goPage('/guest');
          }, function (error) {
            _this3.dismissProgress();

            _this3.showError(error.message);
          });
        }
      }, {
        key: "resend",
        value: function resend() {
          var _this4 = this;

          this.showProgress();
          this.services.resendGuestCode(this.number).then(function (success) {
            _this4.dismissProgress();

            _this4.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER, String(_this4.number));

            _this4.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.TOKEN, success.token);

            _this4.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_GUEST_UPDATE, true);

            _this4.showAlert('Su código de verificación ha sido enviado nuevamente.');
          }, function (error) {
            _this4.dismissProgress();

            _this4.showError(error.message);
          });
        }
      }]);

      return GuestComponent;
    }(_base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"]);

    GuestComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"]
      }, {
        type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]
      }];
    };

    GuestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'register-guest',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./guest.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/guest/guest.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./guest.component.scss */
      "./src/app/pages/register/guest/guest.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"], _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])], GuestComponent);
    /***/
  },

  /***/
  "./src/app/pages/register/register.component.ts":
  /*!******************************************************!*\
    !*** ./src/app/pages/register/register.component.ts ***!
    \******************************************************/

  /*! exports provided: RegisterComponent */

  /***/
  function srcAppPagesRegisterRegisterComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RegisterComponent", function () {
      return RegisterComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var RegisterComponent = /*#__PURE__*/function () {
      function RegisterComponent() {
        _classCallCheck(this, RegisterComponent);
      }

      _createClass(RegisterComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return RegisterComponent;
    }();

    RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-register',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./register.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.component.html"))["default"]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], RegisterComponent);
    /***/
  },

  /***/
  "./src/app/pages/register/register.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/register/register.module.ts ***!
    \***************************************************/

  /*! exports provided: routes, RegisterModule */

  /***/
  function srcAppPagesRegisterRegisterModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "routes", function () {
      return routes;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RegisterModule", function () {
      return RegisterModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../_shared/shared.module */
    "./src/app/pages/_shared/shared.module.ts");
    /* harmony import */


    var _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./step1/step1.component */
    "./src/app/pages/register/step1/step1.component.ts");
    /* harmony import */


    var _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./step2/step2.component */
    "./src/app/pages/register/step2/step2.component.ts");
    /* harmony import */


    var _step3_step3_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./step3/step3.component */
    "./src/app/pages/register/step3/step3.component.ts");
    /* harmony import */


    var _register_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./register.component */
    "./src/app/pages/register/register.component.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var ngx_popper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ngx-popper */
    "./node_modules/ngx-popper/fesm2015/ngx-popper.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _guest_guest_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./guest/guest.component */
    "./src/app/pages/register/guest/guest.component.ts");

    var routes = [{
      path: 'guest',
      component: _guest_guest_component__WEBPACK_IMPORTED_MODULE_11__["GuestComponent"]
    }, {
      path: 'step1',
      component: _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__["Step1Component"]
    }, {
      path: 'step2',
      component: _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__["Step2Component"]
    }, {
      path: 'step3',
      component: _step3_step3_component__WEBPACK_IMPORTED_MODULE_6__["Step3Component"]
    }];

    var RegisterModule = function RegisterModule() {
      _classCallCheck(this, RegisterModule);
    };

    RegisterModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_register_component__WEBPACK_IMPORTED_MODULE_7__["RegisterComponent"], _guest_guest_component__WEBPACK_IMPORTED_MODULE_11__["GuestComponent"], _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__["Step1Component"], _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__["Step2Component"], _step3_step3_component__WEBPACK_IMPORTED_MODULE_6__["Step3Component"]],
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_10__["RouterModule"].forChild(routes), _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"], ngx_popper__WEBPACK_IMPORTED_MODULE_9__["NgxPopperModule"].forRoot({
        placement: 'top',
        styles: {
          'background-color': 'white'
        }
      })]
    })], RegisterModule);
    /***/
  },

  /***/
  "./src/app/pages/register/step1/step1.component.scss":
  /*!***********************************************************!*\
    !*** ./src/app/pages/register/step1/step1.component.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesRegisterStep1Step1ComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZ2lzdGVyL3N0ZXAxL3N0ZXAxLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/pages/register/step1/step1.component.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/register/step1/step1.component.ts ***!
    \*********************************************************/

  /*! exports provided: Step1Component */

  /***/
  function srcAppPagesRegisterStep1Step1ComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Step1Component", function () {
      return Step1Component;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../base.page */
    "./src/app/pages/base.page.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/services.provider */
    "./src/app/services/services.provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../utils/const/keys */
    "./src/app/utils/const/keys.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../services/utils.service */
    "./src/app/services/utils.service.ts");
    /* harmony import */


    var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../services/intent.provider */
    "./src/app/services/intent.provider.ts");

    var Step1Component = /*#__PURE__*/function (_base_page__WEBPACK_I2) {
      _inherits(Step1Component, _base_page__WEBPACK_I2);

      var _super2 = _createSuper(Step1Component);

      function Step1Component(router, storage, modelsServices, alertController, utilsService, userStorage) {
        var _this5;

        _classCallCheck(this, Step1Component);

        _this5 = _super2.call(this, router, storage, modelsServices, alertController, utilsService, userStorage);

        _this5.utils.registerScreen('register');

        return _this5;
      }

      _createClass(Step1Component, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "nextStep",
        value: function nextStep() {
          var _this6 = this;

          if (String(this.number).length !== 10) {
            this.showError('Debe ingresar un número de suscriptor válido.');
          } else {
            this.showProgress();
            this.services.validateSubscriber(String(this.number)).then(function (success) {
              _this6.dismissProgress();

              _this6.processResponse(success);
            }, function (error) {
              _this6.dismissProgress();

              if (error.message.includes('cuenta ya existente')) {
                error.message = 'Hemos detectado que está intentando registrar una cuenta ya existente en nuestro sistema. Por favor presione la opción ' + '<b>Olvido su Contraseña</b> para recuperar su acceso al sistema';

                _this6.showConfirmCustom('Aviso', error.message, 'Olvido su Contraseña', 'Cerrar', function () {
                  _this6.goPage('recover/step1');
                });
              } else {
                _this6.showError(error.message);
              }
            });
          }
        }
      }, {
        key: "processResponse",
        value: function processResponse(response) {
          this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER, String(this.number));

          if (response.accountType === 'I' && response.accountSubType === 'P' || response.accountType === 'I3' && response.accountSubType === 'P') {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_PREPAID, true);
          } else {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_PREPAID, false);
          }

          if (response.accountType === 'I2' && response.accountSubType === '4' || response.accountType === 'I' && response.accountSubType === 'R' || response.accountType === 'I' && response.accountSubType === '4' || response.accountType === 'I' && response.accountSubType === 'E') {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_POSTPAID, true);
          } else {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_POSTPAID, false);
          }

          this.goPage('/register/step2');
        }
      }]);

      return Step1Component;
    }(_base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"]);

    Step1Component.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"]
      }, {
        type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]
      }];
    };

    Step1Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'register-step1',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./step1.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step1/step1.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./step1.component.scss */
      "./src/app/pages/register/step1/step1.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"], _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])], Step1Component);
    /***/
  },

  /***/
  "./src/app/pages/register/step2/step2.component.scss":
  /*!***********************************************************!*\
    !*** ./src/app/pages/register/step2/step2.component.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesRegisterStep2Step2ComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZ2lzdGVyL3N0ZXAyL3N0ZXAyLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/pages/register/step2/step2.component.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/register/step2/step2.component.ts ***!
    \*********************************************************/

  /*! exports provided: Step2Component */

  /***/
  function srcAppPagesRegisterStep2Step2ComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Step2Component", function () {
      return Step2Component;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../base.page */
    "./src/app/pages/base.page.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/services.provider */
    "./src/app/services/services.provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../utils/const/keys */
    "./src/app/utils/const/keys.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../services/utils.service */
    "./src/app/services/utils.service.ts");
    /* harmony import */


    var _utils_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../utils/utils */
    "./src/app/utils/utils.ts");
    /* harmony import */


    var _services_intent_provider__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../services/intent.provider */
    "./src/app/services/intent.provider.ts");

    var Step2Component = /*#__PURE__*/function (_base_page__WEBPACK_I3) {
      _inherits(Step2Component, _base_page__WEBPACK_I3);

      var _super3 = _createSuper(Step2Component);

      function Step2Component(router, storage, modelsServices, alertController, utilsService, userStorage) {
        var _this7;

        _classCallCheck(this, Step2Component);

        _this7 = _super3.call(this, router, storage, modelsServices, alertController, utilsService, userStorage);
        _this7.code = '';
        _this7.ssn = '';
        _this7.email = '';
        _this7.subscriber = '';
        _this7.showTerms = false;
        return _this7;
      }

      _createClass(Step2Component, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this8 = this;

          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER).then(function (success) {
            _this8.subscriber = success;
          });
          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_PREPAID).then(function (success) {
            _this8.isPrepaid = success;
          });
          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_POSTPAID).then(function (success) {
            _this8.isPostpaid = success;
          });
        }
      }, {
        key: "nextStep",
        value: function nextStep() {
          var _this9 = this;

          if (this.code.length !== 6) {
            this.showError('Debe ingresar los datos solicitados.');
          } else if (!this.isPrepaid && this.ssn.length !== 4) {
            this.showError('Debe ingresar los datos solicitados.');
          } else if (this.email.length === 0) {
            this.showError('Debe ingresar los datos solicitados.');
          } else if (!_utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].validateEmail(this.email)) {
            this.showError('Debe ingresar un correo electrónico válido.');
          } else if (!this.check) {
            this.showError('Debe seleccionar los términos y condiciones para poder continuar.');
          } else {
            if (this.isPrepaid) {
              this.ssn = this.code;
            }

            this.showProgress();
            this.services.validateSSNAndEmail(this.subscriber, this.code, this.ssn, this.email).then(function (success) {
              _this9.dismissProgress();

              _this9.processResponse(success);
            }, function (error) {
              _this9.dismissProgress();

              _this9.showError(error.message);
            });
          }
        }
      }, {
        key: "processResponse",
        value: function processResponse(response) {
          this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER, this.subscriber);
          this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.EMAIL, this.email);
          this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SSN, this.ssn);
          this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.CODE, this.code);
          this.goPage('/register/step3');
        }
      }, {
        key: "resend",
        value: function resend() {
          var _this10 = this;

          this.showProgress();
          this.services.validateSubscriber(this.subscriber).then(function (success) {
            _this10.dismissProgress();

            _this10.showAlert(success.errorDisplay);
          }, function (error) {
            if (error.includes('href="http://miclaroreferals.claroinfo.com/forgotpassword')) {
              error = error.replace('id="forgotpassword" target="_blank" href="http://miclaroreferals.claroinfo.com/forgotpassword"', 'href="/recover/step1"');
            }

            if (error.includes('href="https://miclaro.claropr.com/forgotpassword')) {
              error = error.replace('id="forgotpassword" target="_blank" href="https://miclaro.claropr.com/forgotpassword"', 'href="/recover/step1"');
            }

            _this10.dismissProgress();

            _this10.showError(error.message);
          });
        }
      }]);

      return Step2Component;
    }(_base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"]);

    Step2Component.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"]
      }, {
        type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_10__["IntentProvider"]
      }];
    };

    Step2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'register-step2',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./step2.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step2/step2.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./step2.component.scss */
      "./src/app/pages/register/step2/step2.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"], _services_intent_provider__WEBPACK_IMPORTED_MODULE_10__["IntentProvider"]])], Step2Component);
    /***/
  },

  /***/
  "./src/app/pages/register/step3/step3.component.scss":
  /*!***********************************************************!*\
    !*** ./src/app/pages/register/step3/step3.component.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesRegisterStep3Step3ComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZ2lzdGVyL3N0ZXAzL3N0ZXAzLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/pages/register/step3/step3.component.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/register/step3/step3.component.ts ***!
    \*********************************************************/

  /*! exports provided: Step3Component */

  /***/
  function srcAppPagesRegisterStep3Step3ComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Step3Component", function () {
      return Step3Component;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../base.page */
    "./src/app/pages/base.page.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/services.provider */
    "./src/app/services/services.provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../utils/const/keys */
    "./src/app/utils/const/keys.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../services/utils.service */
    "./src/app/services/utils.service.ts");
    /* harmony import */


    var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../services/intent.provider */
    "./src/app/services/intent.provider.ts");

    var Step3Component = /*#__PURE__*/function (_base_page__WEBPACK_I4) {
      _inherits(Step3Component, _base_page__WEBPACK_I4);

      var _super4 = _createSuper(Step3Component);

      function Step3Component(router, storage, modelsServices, alertController, utilsService, userStorage) {
        var _this11;

        _classCallCheck(this, Step3Component);

        _this11 = _super4.call(this, router, storage, modelsServices, alertController, utilsService, userStorage);
        _this11.password = '';
        _this11.passwordRepeat = '';
        _this11.subscriber = '';
        _this11.email = '';
        _this11.code = '';
        _this11.ssn = '';
        return _this11;
      }

      _createClass(Step3Component, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this12 = this;

          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER).then(function (success) {
            _this12.subscriber = success;
          });
          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.EMAIL).then(function (success) {
            _this12.email = success;
          });
          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.CODE).then(function (success) {
            _this12.code = success;
          });
          this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SSN).then(function (success) {
            _this12.ssn = success;
          });
        }
      }, {
        key: "nextStep",
        value: function nextStep() {
          var _this13 = this;

          if (this.password.length < 8 || this.password.length > 15 || this.password !== this.passwordRepeat || !this.lowercaseValidator(this.password) || !this.uppercaseValidator(this.password) || !this.twoNumberValidator(this.password) || !this.specialCharacterValidator(this.password)) {
            return;
          } else {
            this.showProgress();
            this.services.validatePassword(this.subscriber, this.code, this.ssn, this.email, this.password).then(function (success) {
              _this13.dismissProgress();

              _this13.clearStore();

              _this13.showAlert('Has sido registrado con Exito!', function () {
                _this13.goLoginPage();
              });
            }, function (error) {
              _this13.dismissProgress();

              _this13.showError(error.message);
            });
          }
        }
      }, {
        key: "lowercaseValidator",
        value: function lowercaseValidator(c) {
          var regex = /[a-z]/g;
          return regex.test(c);
        }
      }, {
        key: "uppercaseValidator",
        value: function uppercaseValidator(c) {
          var regex = /[A-Z]/g;
          return regex.test(c);
        }
      }, {
        key: "twoNumberValidator",
        value: function twoNumberValidator(c) {
          var numbers = '0123456789';
          var count = 0;

          for (var i = 0; i < c.length; i++) {
            if (numbers.indexOf(c.charAt(i), 0) !== -1) {
              count++;
            }
          }

          return count >= 2;
        }
      }, {
        key: "specialCharacterValidator",
        value: function specialCharacterValidator(c) {
          return c.match('^[A-z0-9]+$');
        }
      }]);

      return Step3Component;
    }(_base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"]);

    Step3Component.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"]
      }, {
        type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]
      }];
    };

    Step3Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'register-step3',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./step3.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step3/step3.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./step3.component.scss */
      "./src/app/pages/register/step3/step3.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"], _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])], Step3Component);
    /***/
  }
}]);
//# sourceMappingURL=pages-register-register-module-es5.js.map