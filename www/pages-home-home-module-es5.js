function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/dashboard/dashboard.component.html":
  /*!*****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/dashboard/dashboard.component.html ***!
    \*****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesHomeDashboardDashboardComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<app-popup-gift-received\n        *ngIf=\"showPopupGiftReceived\"\n        [product]=\"product\"\n        (close)=\"closePopup1GB()\">\n</app-popup-gift-received>\n\n<ion-content #content class=\"allcont logsize\" style=\"overflow-y: auto; overflow-x: hidden\">\n\n    <app-header></app-header>\n\n    <app-menu></app-menu>\n\n    <div class=\"basicrow m-top-i lessm\">\n        <div *ngIf=\"typeBan.postpaid || typeBan.telephony\" class=\"container\">\n            <div class=\"autowcont dashapp m-top\">\n                <div class=\"basicrow msidespad\">\n                    <div class=\"b-title\">\n                        <div class=\"autocont f-black roboto-m f-bmed pull-left vcenter\">\n                            <div class=\"tabcell\">\n                                Balance\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow msidespad m-top-u-i\">\n\n                    <div class=\"balancesptr-i\">\n                        <label class=\"label-r\">N&uacute;mero de Cuenta</label>\n\n                        <div class=\"basicrow\">\n                            <app-account-select></app-account-select>\n                        </div>\n                    </div>\n\n                    <div class=\"balancesptr-iicont refdef\">\n                        <div class=\"balancesptr-ii text-center\">\n                      <span class=\"roboto-b\">\n                          Balance Pendiente\n                      </span>\n                            <br/>\n                            <span class=\"f-red f-lsemi roboto-b\">\n                          &#36;{{billBalance}}\n                      </span>\n                        </div>\n\n                        <div class=\"balancesptr-ii no-brd-i text-center\">\n                      <span class=\"roboto-b\">\n                          Su Factura Vence\n                      </span>\n                            <br/>\n                            <span class=\"f-black f-big roboto-b\">\n                          {{billDueDate}}\n                      </span>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-bott visible-md visible-sm\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow m-bott visible-xs\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"balancesptr-iv refdef f-reg roboto-r text-center\">\n                        <div class=\"basicrow m-top-u\">\n                            Fecha de Factura<br/>\n                            <span class=\"roboto-b\">\n                          {{billDate}}\n                      </span>\n                        </div>\n                    </div>\n\n                    <div class=\"balancesptr-iv refdef no-brd f-reg roboto-r text-center\">\n                        <div class=\"basicrow m-top-u\">\n                            &Uacute;ltimo Pago<br/>\n                            <span class=\"roboto-b\">\n                          &#36;{{lastPayment}}\n                      </span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n        <div *ngIf=\"typeBan.prepaid\" class=\"container\">\n            <div class=\"autowcont dashapp m-top\">\n                <div class=\"basicrow msidespad\">\n                    <div class=\"b-title\">\n                        <div class=\"autocont f-black roboto-m f-bmed pull-left vcenter\">\n                            <div class=\"tabcell\">\n                                Balance en Prepago\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div *ngIf=\"typeBan.prepaid\" class=\"basicrow msidespad m-top-u-i\">\n        <div class=\"balancesptr-ii fortimes rfpad text-left\">\n            <label class=\"label-f\">N&uacute;mero de Tel&eacute;fono</label>\n\n            <div class=\"basicrow\">\n                <select class=\"sel-f\" [(ngModel)]=\"selectedSubscriber\" (change)=\"changePrepaidSubscriber()\">\n                    <option *ngFor=\"let subscriber of subscribers\" [ngValue]=\"subscriber\">\n                        {{subscriber.subscriberNumberField}}\n                    </option>\n                </select>\n            </div>\n        </div>\n\n        <div class=\"balancesptr-ii fortimes h-nobrd-i text-center\">\n\t\t\t<span class=\"roboto-b\">\n\t\t\t\tBalance en Prepago\n\t\t\t</span>\n            <br/>\n            <span class=\"f-red f-lsemi roboto-b\">\n\t\t\t\t&#36;{{getPrepaidBalance()}}\n\t\t\t</span>\n        </div>\n\n        <div class=\"basicrow visible-sm m-bott\">\n            <div class=\"logline full\"></div>\n        </div>\n\n        <div class=\"balancesptr-ii text-center\">\n\t\t\t<span class=\"roboto-b\">\n\t\t\t\tPr&oacute;xima Recarga\n\t\t\t</span>\n            <br/>\n            <span class=\"f-black f-big roboto-b\">\n\t\t\t\t{{getNextRecharge()}}\n\t\t\t</span>\n        </div>\n    </div>\n\n    <div *ngIf=\"typeBan.prepaid\" class=\"basicrow m-top-i lessm bg-gray hidden\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n                    <div class=\"autowcont dashapp m-top\">\n                        <div class=\"basicrow msidespad\">\n                            <div class=\"b-title\">\n                                <div class=\"autocont f-black roboto-m f-bmed pull-left vcenter\">\n                                    <div class=\"tabcell\">\n                                        Balance en Mi Claro <span class=\"f-red\">Wallet</span>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n\n                        <div class=\"basicrow msidespad m-top-u-i\">\n                            <div class=\"balancesptr-ii fullon text-center\">\n                                <span class=\"roboto-b\">\n                                    Cr&eacute;ditos Disponibles\n                                </span>\n                                <br/>\n                                <span class=\"f-red f-lsemi roboto-b\">\n                                    &#36;{{ getWalletBalance() }}\n                                </span>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n<!--    <div class=\"basicrow\" [ngClass]=\"{'bg-gray': !typeBan.prepaid}\">-->\n    <div class=\"basicrow bg-gray\">\n        <div class=\"container\">\n            <div class=\"autowcont dashapp m-top-i\">\n                <div class=\"basicrow msidespad\">\n                    <div class=\"b-title\">\n                        <div class=\"autocont f-black roboto-m f-bmed pull-left vcenter\">\n                            <div class=\"tabcell\">\n                                Opciones de Pago\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow msidespad m-top-u-i\">\n\n                    <div *ngIf=\"typeBan.postpaid\">\n\n                        <div class=\"balancesptr-i refdef text-center\">\n                            <span class=\"roboto-b\">\n                                Descuentos Disponibles\n                            </span>\n                            <br/>\n                            <span class=\"f-blue f-lsemi roboto-b\">\n                                {{discount}}\n                            </span>\n                        </div>\n\n                        <div class=\"basicrow m-bott visible-xs\">\n                            <div class=\"logline full\"></div>\n                        </div>\n\n                        <div class=\"balancesptr-iii refdef\">\n                            <label class=\"label-r roboto-b\">Monto de Descuento a Aplicar</label>\n\n                            <div class=\"basicrow rel\">\n                                <img alt=\"\" [popper]=\"'Deberá tener este monto disponible en sus creditos.'\"\n                                     [popperTrigger]=\"'click'\"\n                                     [popperTimeoutAfterShow]=\"3000\"\n                                     class=\"q-t-pay\" src=\"assets/images/tooltip1.png\">\n\n                                <input value=\"{{creditAmount}}\" class=\"inp-f payup qt\" readonly>\n\n\n                                <ion-button [disabled]=\"!canApplyCredits\"\n                                            class=\"btns payup vcenter\"\n                                            color=\"secondary\" expand=\"full\" (click)=\"applyCredits()\">\n                                    Aplicar\n                                </ion-button>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div *ngIf=\"typeBan.telephony\">\n\n                        <div class=\"balancesptr-iii refdef text-center\">\n                            <label class=\"label-r roboto-b\">Descuento Disponibles</label>\n\n                            <div class=\"basicrow rel\">\n                                <img alt=\"\" [popper]=\"'El Monto aplicable de credito es el 50% de su Balance Pendiente.'\"\n                                     [popperTrigger]=\"'click'\"\n                                     [popperTimeoutAfterShow]=\"3000\"\n                                     class=\"q-t-credits-telephony\" src=\"assets/images/tooltip1.png\">\n                            </div>\n\n                            <span class=\"f-blue f-lsemi roboto-b\">\n\t\t\t\t\t\t\t    {{discount}}\n\t\t\t\t\t\t    </span>\n                        </div>\n\n                        <div class=\"balancesptr-iii refdef\">\n\n                            <div class=\"basicrow\">\n                                <ion-button [disabled]=\"!canApplyCredits\"\n                                            class=\"btns payup vcenter\"\n                                            color=\"secondary\" expand=\"full\" (click)=\"applyCredits()\">\n                                    Aplicar a Renta Mensual\n                                </ion-button>\n                            </div>\n                        </div>\n\n                    </div>\n\n                    <div class=\"basicrow m-bott visible-xs\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"balancesptr-iii refdef no-brd\">\n                        <label *ngIf=\"!typeBan.prepaid\" class=\"label-r roboto-b\">\n                            Monto a Pagar (&#36;)\n                        </label>\n                        <label *ngIf=\"typeBan.prepaid\" class=\"label-r roboto-b\">\n                            Monto a Recargar (&#36;)\n                        </label>\n\n                        <div class=\"basicrow rel\">\n                            <img alt=\"\" *ngIf=\"!typeBan.prepaid\" [popper]=\"'Ahora puedes abonar hasta $800.00. Si la cantidad indica CR, significa que usted tiene cr&eacute;dito en su cuenta.'\"\n                                 [popperTrigger]=\"'click'\"\n                                 [popperTimeoutAfterShow]=\"5000\"\n                                 class=\"q-t-pay\" src=\"assets/images/tooltip1.png\">\n                            <img alt=\"\" *ngIf=\"typeBan.prepaid\" [popper]=\"'Podras recargar entre $5.00 y $150.00 a tu n&uacute;mero prepago.'\"\n                                 [popperTrigger]=\"'click'\"\n                                 [popperTimeoutAfterShow]=\"3000\"\n                                 class=\"q-t-pay\" src=\"assets/images/tooltip1.png\">\n\n                            <input  [(ngModel)]=\"amountPayable\" value=\"{{amountPayable}}\" class=\"inp-f payup qt\" (keyup.enter)=\"typeBan.prepaid? recharge() : billPayment()\">\n\n                            <ion-button *ngIf=\"!typeBan.prepaid\"\n                                        class=\"btns payup vcenter\"\n                                        color=\"primary\" expand=\"full\" (click)=\"billPayment()\">\n                                Pagar\n                            </ion-button>\n                            <ion-button *ngIf=\"typeBan.prepaid\"\n                                        class=\"btns payup vcenter\"\n                                        color=\"primary\" expand=\"full\" (click)=\"recharge()\">\n                                Recargar\n                            </ion-button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"row\">\n                <div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-6\" *ngIf=\"!isGuest\">\n                    <div class=\"dashdirect-digf\">\n\n                        <img alt=\"\" class=\"digf-def-i\" width=\"100%\" src=\"assets/images/fact-dig.png\">\n\n                        <!--[INIT ESTADO SUBSCRITO]-->\n                        <div class=\"autocont nofloat alrd-mleft\" *ngIf=\"paperless\">\n                            <div class=\"autocont f-green-i vcenter\">\n                                <div class=\"tabcell\">\n                                    <span class=\"f-newfdig\">&nbsp; Suscrito a Factura Electr&oacute;nica&nbsp;</span>\n                                </div>\n                            </div>\n                        </div>\n\n                        <div class=\"autocont nofloat\" *ngIf=\"paperless\">\n                            <div class=\"autocont vcenter\">\n                                <div class=\"tabcell\">\n                                    <img alt=\"\" class=\"fdig-check\" width=\"100%\" src=\"assets/images/fact-dig-check.png\">\n                                </div>\n                            </div>\n                        </div>\n                        <!---[END ESTADO SUBSCRITO]-->\n\n\n                        <!--[INIT ESTADO NO SUBSCRITO]-->\n                        <div class=\"autocont nofloat m-septrt\" *ngIf=\"!paperless\">\n                            <div class=\"autocont vcenter\">\n                                <div class=\"tabcell\">\n                                    Factura Electr&oacute;nica:\n                                </div>\n                            </div>\n                        </div>\n\n                        <div class=\"autocont nofloat m-septrt\" *ngIf=\"!paperless\">\n                            <div class=\"autocont vcenter\">\n                                <div class=\"tabcell\">\n                                    <label class=\"switch m-left-i\">\n                                        <input type=\"checkbox\"\n                                               [value]=\"check\"\n                                               [(ngModel)]=\"check\"\n                                               (click)=\"changePaperless(!check)\">\n                                        <span class=\"slider\"></span>\n                                    </label>\n                                </div>\n                            </div>\n                        </div>\n                        <!--[INIT ESTADO NO SUBSCRITO]-->\n\n                    </div>\n                </div>\n\n                <div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-6\">\n                    <a class=\"dashdirect-chat vcenter\" (click)=\"openChat()\">\n                        <img alt=\"\" class=\"direct-chat-i\" width=\"100%\" src=\"assets/images/chatnow-icon.png\">\n\n                        <span class=\"tabcell\">\n                          Chatea con Nosotros\n                      </span>\n                    </a>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"basicrow m-top-i\">\n\n        <div class=\"row dashboard\">\n\n            <div class=\"dashconts {{getExtraClass(26)}}\" (click)=\"going(26)\" *ngIf=\"checkAccessPermission(26)\">\n                <div class=\"dashbox gray vcenter\">\n                      <span class=\"tabcell\">\n                          <span class=\"icon-dash-24\"></span>\n                          Recarga\n                      </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(9)}}\" (click)=\"going(9)\" *ngIf=\"checkAccessPermission(9)\">\n                <div class=\"dashbox gray vcenter\">\n                      <span class=\"tabcell\">\n                          <span class=\"icon-dash-01\"></span>\n                          Factura y Pago\n                      </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(14)}}\" (click)=\"going(14)\" *ngIf=\"checkAccessPermission(14) && typeBan.postpaid\">\n                <div class=\"dashbox blue vcenter\">\n                      <span class=\"tabcell\">\n                          <span class=\"icon-dash-02\"></span>\n                          Consumo de Voz, Mensajer&iacute;a y Roaming\n                      </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(14)}}\" (click)=\"going(14)\" *ngIf=\"checkAccessPermission(14) && typeBan.postpaid\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-03\"></span>\n                        Consumo de Datos\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(28)}}\" (click)=\"going(28)\" *ngIf=\"checkAccessPermission(28)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-02\"></span>\n                        Mi Consumo\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(16)}}\" (click)=\"going(16)\" *ngIf=\"checkAccessPermission(16)\">\n                <div class=\"dashbox white vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-25\"></span>\n                        Mis Servicios\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(34)}}\" (click)=\"going(34)\" *ngIf=\"checkAccessPermission(34)\">\n                <div class=\"dashbox white vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-25\"></span>\n                        Mis Servicios\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(21)}}\" (click)=\"going(21)\" *ngIf=\"checkAccessPermission(21)\">\n                <div class=\"dashbox blue vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-08\"></span>\n                        Compra de Data Adicional\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(17)}}\" (click)=\"going(17)\" *ngIf=\"checkAccessPermission(17)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-05\"></span>\n                        Cambio de Plan\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(35)}}\" (click)=\"going(35)\" *ngIf=\"checkAccessPermission(35)\">\n                <div class=\"dashbox blue vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-05\"></span>\n                        Cambio de Plan\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(33)}}\" (click)=\"going(33)\" *ngIf=\"checkAccessPermission(33)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-05\"></span>\n                        Reporta Interrupci&oacute;n\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(20)}}\" (click)=\"going(20)\" *ngIf=\"checkAccessPermission(20)\">\n                <div class=\"dashbox white vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-07\"></span>\n                        Tienda\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(24)}}\" (click)=\"going(24)\" *ngIf=\"checkAccessPermission(24)\">\n                <div class=\"dashbox gray vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-23\"></span>\n                        Regala 1 GB a un Amigo/a o Familiar Postpago​\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(25)}}\" (click)=\"going(25)\" *ngIf=\"checkAccessPermission(25)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-10\"></span>\n                        Regala una Recarga Prepago\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(13)}}\" (click)=\"going(13)\" *ngIf=\"checkAccessPermission(13)\">\n                <div class=\"dashbox gray vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-11\"></span>\n                        Factura Digital\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(2)}}\" (click)=\"going(2)\" *ngIf=\"checkAccessPermission(2)\">\n                <div class=\"dashbox blue vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-14\"></span>\n                        Configuraci&oacute;n de Cuenta\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(18)}}\" (click)=\"going(18)\" *ngIf=\"checkAccessPermission(18)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-16\"></span>\n                        Netflix\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(36)}}\" (click)=\"going(36)\" *ngIf=\"checkAccessPermission(36)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-16\"></span>\n                        Netflix\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(19)}}\" (click)=\"going(19)\" *ngIf=\"checkAccessPermission(19)\">\n                <div class=\"dashbox blue vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-21\"></span>\n                        Programa de Referidos\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(31)}}\" (click)=\"going(31)\" *ngIf=\"checkAccessPermission(31)\">\n                <div class=\"dashbox blue vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-21\"></span>\n                        Programa de Referidos\n                    </span>\n                </div>\n            </div>\n\n            <div class=\"dashconts {{getExtraClass(37)}}\" (click)=\"going(37)\" *ngIf=\"checkAccessPermission(37)\">\n                <div class=\"dashbox blue vcenter\">\n                    <span class=\"tabcell\">\n                        <span class=\"icon-dash-21\"></span>\n                        Programa de Referidos\n                    </span>\n                </div>\n            </div>\n\n\n            <div class=\"dashconts {{getExtraClass(321)}}\" (click)=\"going(321)\" *ngIf=\"checkAccessPermission(321)\">\n                <div class=\"dashbox red vcenter\">\n                    <span class=\"tabcell\">\n\t\t\t\t\t\t<span><img src=\"assets/images/icon_club.png\" style=\"height: 70px;display: block; margin: 0 auto 10px;\"></span>\n\t\t\t\t\t</span>\n                </div>\n            </div>\n\n        </div>\n    </div>\n\n    <div class=\"basicrow m-top-i m-bott\" id=\"banner-home\" (click)=\"going(321)\"  *ngIf=\"!typeBan.prepaid\">\n\t\t<div class=\"container\">\n            <img *ngIf=\"platform.android\" width=\"100%\" src=\"assets/images/claro_club_banner_android.jpg\">\n            <img *ngIf=\"platform.ios\" width=\"100%\" src=\"assets/images/claro_club_banner_ios.jpg\">\n\t\t</div>\n    </div>\n\n    <div class=\"basicrow m-top-i m-bott padding-bottom-footer\" *ngIf=\"typeBan.prepaid\">\n        <img alt=\"\" width=\"100%\" src=\"assets/images/banner-prepago.jpg\">\n    </div>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.component.html":
  /*!**************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.component.html ***!
    \**************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesHomeHomeComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n    <app-header></app-header>\n    <app-menu></app-menu>\n    <router-outlet></router-outlet>\n</div>\n\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/no-associated/no-associated.component.html":
  /*!*************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/no-associated/no-associated.component.html ***!
    \*************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesHomeNoAssociatedNoAssociatedComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"allcont logsize\">\n\n    <app-header></app-header>\n\n    <app-menu></app-menu>\n\n    <div class=\"basicrow mbott\">\n        <div class=\"container\">\n            <div class=\"basicrow m-top-i roboto-r text-center\">\n                <div class=\"shortcont\">\n                    <div class=\"basicrow f-black text-center noacc-icon\">\n                        <i class=\"fa fa-frown-o\" aria-hidden=\"true\"></i>\n                    </div>\n\n                    <div class=\"basicrow f-reg f-black m-top-ii roboto-b text-center\">\n                        No tienes productos Claro en este segmento.\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"btns red rippleR ascbtn-i centr vcenter\" (click)=\"openAccountsManage()\">\n                            <span class=\"tabcell\">\n                                Agrega tus Productos Claro Aquí\n                            </span>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>";
    /***/
  },

  /***/
  "./src/app/pages/home/dashboard/dashboard.component.scss":
  /*!***************************************************************!*\
    !*** ./src/app/pages/home/dashboard/dashboard.component.scss ***!
    \***************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesHomeDashboardDashboardComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".fdig-check {\n  width: 27px;\n  margin-top: 11px;\n}\n\n.q-t-credits-telephony {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  margin: auto;\n  cursor: pointer;\n  z-index: 80;\n  width: 25px;\n  height: 25px;\n  right: 40%;\n}\n\n@media (max-width: 768px) {\n  .q-t-credits-telephony {\n    top: 5px;\n    right: 8px;\n    margin: inherit;\n  }\n}\n\nion-button.btns.payup {\n  width: 100%;\n  margin-top: 20px;\n  border-radius: 3px;\n  -webkit-border-radius: 3px;\n  -moz-border-radius: 3px;\n}\n\n.balancesptr-iii.refdef.no-brd {\n  width: 100%;\n}\n\n.inp-f.payup.qt {\n  width: 100%;\n  border: 1px solid #cccccc;\n  font-size: 14px;\n  padding: 10px 12px;\n}\n\nimg.q-t-pay {\n  top: 5px;\n  right: 8px;\n  margin: inherit;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZG1pbi9Eb2N1bWVudHMvRSRHUy9SRUJSTkFESU5HL1Jlc3BhbGRvcy9pb25pYy9taWNsYXJvMy1pb25pYy12ZXJzaW9uL3NyYy9hcHAvcGFnZXMvaG9tZS9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy9ob21lL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUNDSjs7QURFQTtFQUNJO0lBQ0ksUUFBQTtJQUNBLFVBQUE7SUFDQSxlQUFBO0VDQ047QUFDRjs7QURFQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsMEJBQUE7RUFDQSx1QkFBQTtBQ0FKOztBREdBO0VBQ0ksV0FBQTtBQ0FKOztBREdBO0VBQ0ksV0FBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDQUo7O0FER0E7RUFDSSxRQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUNBSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mZGlnLWNoZWNrIHtcbiAgICB3aWR0aDogMjdweDtcbiAgICBtYXJnaW4tdG9wOiAxMXB4O1xufVxuXG4ucS10LWNyZWRpdHMtdGVsZXBob255IHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGJvdHRvbTogMDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHotaW5kZXg6IDgwO1xuICAgIHdpZHRoOiAyNXB4O1xuICAgIGhlaWdodDogMjVweDtcbiAgICByaWdodDogNDAlO1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogNzY4cHgpIHtcbiAgICAucS10LWNyZWRpdHMtdGVsZXBob255IHtcbiAgICAgICAgdG9wOiA1cHg7XG4gICAgICAgIHJpZ2h0OiA4cHg7XG4gICAgICAgIG1hcmdpbjogaW5oZXJpdDtcbiAgICB9XG59XG5cbmlvbi1idXR0b24uYnRucy5wYXl1cCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgLW1vei1ib3JkZXItcmFkaXVzOiAzcHg7XG59XG5cbi5iYWxhbmNlc3B0ci1paWkucmVmZGVmLm5vLWJyZCB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5pbnAtZi5wYXl1cC5xdCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjY2NjYztcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgcGFkZGluZzogMTBweCAxMnB4O1xufVxuXG5pbWcucS10LXBheSB7XG4gICAgdG9wOiA1cHg7XG4gICAgcmlnaHQ6IDhweDtcbiAgICBtYXJnaW46IGluaGVyaXQ7XG59IiwiLmZkaWctY2hlY2sge1xuICB3aWR0aDogMjdweDtcbiAgbWFyZ2luLXRvcDogMTFweDtcbn1cblxuLnEtdC1jcmVkaXRzLXRlbGVwaG9ueSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIG1hcmdpbjogYXV0bztcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB6LWluZGV4OiA4MDtcbiAgd2lkdGg6IDI1cHg7XG4gIGhlaWdodDogMjVweDtcbiAgcmlnaHQ6IDQwJTtcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5xLXQtY3JlZGl0cy10ZWxlcGhvbnkge1xuICAgIHRvcDogNXB4O1xuICAgIHJpZ2h0OiA4cHg7XG4gICAgbWFyZ2luOiBpbmhlcml0O1xuICB9XG59XG5pb24tYnV0dG9uLmJ0bnMucGF5dXAge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDNweDtcbiAgLW1vei1ib3JkZXItcmFkaXVzOiAzcHg7XG59XG5cbi5iYWxhbmNlc3B0ci1paWkucmVmZGVmLm5vLWJyZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uaW5wLWYucGF5dXAucXQge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjY2NjYztcbiAgZm9udC1zaXplOiAxNHB4O1xuICBwYWRkaW5nOiAxMHB4IDEycHg7XG59XG5cbmltZy5xLXQtcGF5IHtcbiAgdG9wOiA1cHg7XG4gIHJpZ2h0OiA4cHg7XG4gIG1hcmdpbjogaW5oZXJpdDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/home/dashboard/dashboard.component.ts":
  /*!*************************************************************!*\
    !*** ./src/app/pages/home/dashboard/dashboard.component.ts ***!
    \*************************************************************/

  /*! exports provided: DashboardComponent */

  /***/
  function srcAppPagesHomeDashboardDashboardComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DashboardComponent", function () {
      return DashboardComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/services.provider */
    "./src/app/services/services.provider.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_utils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../services/utils.service */
    "./src/app/services/utils.service.ts");
    /* harmony import */


    var _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../services/intent.provider */
    "./src/app/services/intent.provider.ts");
    /* harmony import */


    var _base_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../base.page */
    "./src/app/pages/base.page.ts");
    /* harmony import */


    var _utils_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../utils/utils */
    "./src/app/utils/utils.ts");
    /* harmony import */


    var _models_access_filter__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../models/access.filter */
    "./src/app/models/access.filter.ts");
    /* harmony import */


    var src_app_utils_const_keys__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! src/app/utils/const/keys */
    "./src/app/utils/const/keys.ts");
    /* harmony import */


    var _utils_const_appConstants__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../utils/const/appConstants */
    "./src/app/utils/const/appConstants.ts");
    /* harmony import */


    var _services_redirect_provider__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../../../services/redirect.provider */
    "./src/app/services/redirect.provider.ts");
    /* harmony import */


    var _services_browser_provider__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ../../../services/browser.provider */
    "./src/app/services/browser.provider.ts");

    var DashboardComponent = /*#__PURE__*/function (_base_page__WEBPACK_I) {
      _inherits(DashboardComponent, _base_page__WEBPACK_I);

      var _super = _createSuper(DashboardComponent);

      function DashboardComponent(router, storage, modelsServices, alertController, utilsService, userStorage, redirectProvider, browserProvider) {
        var _this;

        _classCallCheck(this, DashboardComponent);

        _this = _super.call(this, router, storage, modelsServices, alertController, utilsService, userStorage);
        _this.redirectProvider = redirectProvider;
        _this.browserProvider = browserProvider;
        _this.accounts = [];
        _this.subscribers = [];
        _this.billBalance = '';
        _this.lastPayment = '';
        _this.billDueDate = '';
        _this.billDate = '';
        _this.amountPayable = '';
        _this.check = false;
        _this.paperless = false;
        _this.typeBan = {
          postpaid: false,
          prepaid: false,
          telephony: false,
          byop: false
        };
        _this.access = [];
        _this.showPopupGiftReceived = false;
        _this.isGuest = false;
        _this.platform = _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].getPlatformInfo();
        _this.prepaidAddress = [];
        _this.prepaidPayments = [];
        _this.prepaidProducts = [];
        _this.totalAvailable = 0;
        _this.creditsToApply = 0;
        _this.tempAvailableCredits = 0;
        _this.checkAvailableCredits = false;
        _this.canApplyCredits = false;
        _this.discount = '$0.00';
        _this.creditAmount = '0.00';
        _this.productId = _utils_const_appConstants__WEBPACK_IMPORTED_MODULE_12__["APP"].PRODUCT_ID_INVOICE_PAYMENTS;
        _this.merchantId = '';

        _this.utils.registerScreen('dashboard');

        return _this;
      }

      _createClass(DashboardComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var data = this.cacheStorage();
          this.isGuest = data.isGuest;
          this.typeBan = {
            postpaid: data.isPostpaidAccount,
            prepaid: data.isPrepaidAccount,
            telephony: data.isTelephonyAccount,
            byop: data.isByop
          };
          this.access = data.access;
          this.billBalance = String(data.accountInfo.billBalanceField);
          this.lastPayment = _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(String(data.accountInfo.lastPaymentAmountField));
          this.billDueDate = data.accountInfo.billDueDateField;
          this.billDate = data.accountInfo.billDateField;
          var amountDue = data.accountInfo.billBalanceField;
          var amountPayable = amountDue.includes('CR') ? '0' : amountDue;
          amountPayable = parseFloat(String(amountPayable.replace(',', ''))).toFixed(2);
          this.amountPayable = amountPayable;
          this.paperless = data.accountInfo.paperlessField;
          this.check = this.paperless;
          this.subscribers = data.subscriberList;
        }
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {
          var _this2 = this;

          this.showProgress();
          this.loadData().then(function () {
            return _this2.dismissProgress();
          });
        }
      }, {
        key: "checkIfBanIsSuspend",
        value: function checkIfBanIsSuspend() {
          var _this3 = this;

          var accountInfo = this.cacheStorage().accountInfo;

          if (accountInfo.banStatusField === 'S' && !this.cacheStorage().hasConfirmDue) {
            this.showConfirmCustom('Aviso', 'Su cuenta esta suspendida. Para activar la misma, favor realice su pago.', 'Pagar', 'Cancelar', function () {
              _this3.getCreditCardPaymentOptions(String(accountInfo.bANField), accountInfo.defaultSubscriberField, _this3.amountPayable);
            }, function () {
              _this3.cacheStorage().hasConfirmDue = true;
            });
          }
        }
      }, {
        key: "loadData",
        value: function loadData() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (!this.typeBan.postpaid) {
                      _context.next = 3;
                      break;
                    }

                    _context.next = 3;
                    return this.get1GBReceived();

                  case 3:
                    if (!this.typeBan.prepaid) {
                      _context.next = 7;
                      break;
                    }

                    this.selectedSubscriber = this.cacheStorage().getFullCurrentSelectedSubscriber();
                    _context.next = 9;
                    break;

                  case 7:
                    _context.next = 9;
                    return this.getUserCredits();

                  case 9:
                    this.checkIfBanIsSuspend();
                    this.redirectProvider.checkIfRedirectIsWaiting();

                  case 11:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "getUserCredits",
        value: function getUserCredits() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this4 = this;

            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.services.getCredits(String(this.cacheStorage().accountInfo.bANField), this.cacheStorage().tokenSession).then(function (response) {
                      _this4.totalAvailable = 0;

                      if (response.CreditAsReferer !== null && response.CreditAsReferer !== undefined) {
                        if (response.CreditAsReferer.discount !== null && response.CreditAsReferer.discount !== undefined) {
                          _this4.totalAvailable = response.CreditAsReferer.discount;
                        }
                      }

                      if (response.CreditItems !== null && response.CreditItems !== undefined) {
                        if (response.CreditItems.length > 0) {
                          if (response.CreditItems[0].TotalAvailable > 0) {
                            if (response.CreditItems[0].TotalAvailable > _this4.totalAvailable) {
                              _this4.totalAvailable = response.CreditItems[0].TotalAvailable;
                            }
                          }
                        }
                      }

                      var mountToCompare = 50;

                      if (_this4.typeBan.prepaid || _this4.typeBan.byop) {
                        mountToCompare = 25;
                      }

                      if (_this4.totalAvailable >= mountToCompare) {
                        _this4.creditsToApply = mountToCompare;
                      } else {
                        _this4.creditsToApply = 0;
                      }

                      var discountType = '$';

                      if (_this4.typeBan.telephony) {
                        discountType = '%';
                        _this4.discount = _this4.creditsToApply + ' ' + discountType;
                      } else {
                        _this4.discount = discountType + '' + _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(_this4.totalAvailable);
                        _this4.creditAmount = discountType + '' + _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(_this4.creditsToApply);
                      }

                      _this4.canApplyCredits = _this4.totalAvailable > 0;

                      if (_this4.checkAvailableCredits) {
                        _this4.checkAvailableCredits = false;

                        if (_this4.tempAvailableCredits === _this4.totalAvailable) {
                          _this4.showAlert('Aviso', 'Gracias por su interés en nuestro Programa Refiere y Gana! Su ' + 'balance se estará actualizando próximamente.');
                        } else {
                          _this4.showProgress();

                          _this4.reloadCurrentAccount().then(function () {
                            _this4.dismissProgress();

                            _this4.goHomePage();
                          }, function (error) {
                            _this4.dismissProgress();

                            _this4.showError(error.message);
                          });
                        }
                      }

                      _this4.tempAvailableCredits = _this4.totalAvailable;
                    }, function (error) {
                      _this4.dismissProgress();
                    });

                  case 2:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "applyCredits",
        value: function applyCredits() {
          var _this5 = this;

          var accountInfo = this.cacheStorage().accountInfo;
          var paperless = accountInfo.paperlessField;
          var amount = 50;

          if (!this.typeBan.telephony) {
            amount = parseFloat(String(this.creditAmount.replace('$', '')));
          }

          var billBalance = accountInfo.billBalanceField;
          var debt = billBalance.includes('CR') ? 0 : billBalance;
          var creditsAvailable = this.totalAvailable;
          debt = parseFloat(String(debt));
          creditsAvailable = parseFloat(String(creditsAvailable));

          if (!paperless) {
            this.showError('Para participar en nuestro Programa Refiere y Gana, debe suscribirse a facturación ' + 'electrónica. Para detalles, verifique los términos y condiciones de este Programa.');
          } else if (this.typeBan.postpaid && (billBalance.includes('CR') || parseFloat(billBalance) === 0)) {
            this.showError('Para aplicar su cupón de descuento deberá tener un balance pendiente.');
          } else if (creditsAvailable === 0) {
            this.showError('En este momento no cuenta con cupones disponibles.');
          } else if (this.typeBan.telephony) {
            this.showConfirmCustom('Confirmación', 'Al aplicar su cupón, el mismo se verá reflejado en su próximo ciclo de facturación.', 'Aplicar', 'Regresar', function () {
              _this5.applyCreditsToAccount(amount);
            }, function () {});
          } else if (creditsAvailable < amount) {
            this.showError('En este momento no cuenta con cupones disponibles.');
          } else if (this.typeBan.prepaid || amount === debt) {
            this.showConfirmCustom('Confirmación', 'Una vez acepte redimir su cupón el mismo perderá su validez, este no podrá ser\n' + 'cancelado, reversado, reembolsado o transferido.', 'Aplicar', 'Regresar', function () {
              _this5.applyCreditsToAccount(amount);
            }, function () {});
          } else if (amount > debt) {
            this.showConfirmCustom('Confirmación', 'El valor del cupón (descuento) redimido es mayor al balance pendiente de su factura, al ' + 'redimir este cupón usted perderá el valor del descuento restante.', 'Aplicar', 'Regresar', function () {
              _this5.applyCreditsToAccount(amount);
            }, function () {});
          } else if (amount < debt) {
            this.showConfirmCustom('Confirmación', 'El valor del cupón (descuento) redimido es menor al balance pendiente de su factura. Favor ' + 'de realizar el pago remanente de su factura en o antes de la fecha de vencimiento.', 'Aplicar', 'Regresar', function () {
              _this5.applyCreditsToAccount(amount);
            }, function () {});
          }
        }
      }, {
        key: "applyCreditsToAccount",
        value: function applyCreditsToAccount(amount) {
          var _this6 = this;

          var accountInfo = this.cacheStorage().accountInfo;
          this.showProgress();
          this.services.applyCredits(String(accountInfo.bANField), accountInfo.defaultSubscriberField, amount, this.cacheStorage().tokenSession).then(function (response) {
            _this6.showAlert(response.errorDisplay, function () {
              _this6.checkAvailableCredits = true;

              _this6.getUserCredits();
            }, 'Continuar');
          }, function (error) {
            _this6.dismissProgress();

            _this6.showError(error.message);
          });
        }
      }, {
        key: "changePaperless",
        value: function changePaperless(check) {
          var _this7 = this;

          if (check) {
            var data = this.cacheStorage();
            setTimeout(function () {
              _this7.showProgress();

              _this7.services.updateBillParameters(String(data.accountInfo.bANField), data.tokenSession).then(function () {
                _this7.reloadCurrentAccount().then(function (response) {
                  _this7.dismissProgress();

                  _this7.paperless = true;
                  _this7.check = true;
                }, function (error) {
                  _this7.dismissProgress();

                  _this7.showError(error.message);
                });
              }, function (error) {
                _this7.dismissProgress();

                _this7.showError(error.message);

                _this7.paperless = false;
                _this7.check = false;
              });
            }, 500);
          }
        }
      }, {
        key: "checkAccessPermission",
        value: function checkAccessPermission(id) {
          var permitted = false;
          this.access.forEach(function (section) {
            section.Pages.forEach(function (page) {
              if (page.accessID === id) {
                permitted = true;
                return permitted;
              }
            });
          });
          return permitted;
        }
      }, {
        key: "getExtraClass",
        value: function getExtraClass(id) {
          var classE = '';
          this.access.forEach(function (section) {
            section.Pages.forEach(function (page) {
              if (page.accessID === id) {
                classE = page.extraClass;
                return classE;
              }
            });
          });
          return classE;
        }
      }, {
        key: "going",
        value: function going(id) {
          var _this8 = this;

          var page = this.cacheStorage().getAccessPageByID(id);

          if (this.cacheStorage().isGuest && !page.allowAsGuest) {
            this.showAlertAccessLimited();
          } else {
            if (id === 20) {
              this.openStore();
            } else {
              if (!this.isGuest && !this.services.isAccountDetailsUpdated() && this.pageIsNeedingSubscribers(page)) {
                var account = this.cacheStorage().accountInfo.bANField;
                var subscriber = this.cacheStorage().accountInfo.defaultSubscriberField;
                this.showProgress();
                this.services.loadAccount(String(account), subscriber, this.cacheStorage().tokenSession, this.cacheStorage().isGuest).then(function () {
                  _this8.dismissProgress();

                  _this8.services.setAccountDetailsUpdate();

                  _this8.filterAndGo(page);
                });
              } else {
                this.filterAndGo(page);
              }
            }
          }
        }
      }, {
        key: "pageIsNeedingSubscribers",
        value: function pageIsNeedingSubscribers(page) {
          var _iterator = _createForOfIteratorHelper(_models_access_filter__WEBPACK_IMPORTED_MODULE_10__["AccessFilter"].routesToGo),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var route = _step.value;

              if (page.accessID === route.accessID && route.path !== '') {
                if (route.needSubscribers) {
                  return true;
                }
              }
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          return false;
        }
      }, {
        key: "filterAndGo",
        value: function filterAndGo(page) {
          var _this9 = this;

          _models_access_filter__WEBPACK_IMPORTED_MODULE_10__["AccessFilter"].routesToGo.forEach(function (route) {
            if (page.accessID === route.accessID && route.path !== '') {
              _this9.goPage('module/' + route.path);

              return;
            }
          });
        }
      }, {
        key: "billPayment",
        value: function billPayment() {
          var _this10 = this;

          var accountInfo = this.cacheStorage().accountInfo;
          var creditAmountDue = accountInfo.billBalanceField.includes('CR') ? accountInfo.billBalanceField.replace('CR', '') : 0;
          var billBalance = accountInfo.billBalanceField.includes('CR') ? 0 : accountInfo.billBalanceField;
          creditAmountDue = parseFloat(String(creditAmountDue).replace(',', ''));
          billBalance = parseFloat(String(billBalance).replace(',', ''));
          this.amountPayable = parseFloat(String(this.amountPayable)).toFixed(2);

          if (Number(this.amountPayable) < 5) {
            this.showError('El monto no puede ser menor a $5.00');
          } else if (Number(this.amountPayable) > 800) {
            this.showError('El monto no puede ser mayor a $800.00');
          } else if (creditAmountDue > 0 && creditAmountDue + Number(this.amountPayable) > 800) {
            this.showError('El monto total abonado en su cuenta no puede ser mayor a $800.00');
          } else if (Number(this.amountPayable) > billBalance) {
            this.showConfirm('Confirmación', 'La cantidad ingresada es mayor al balance de su factura, la diferencia será acreditada a su cuenta.', function () {
              _this10.getCreditCardPaymentOptions(String(accountInfo.bANField), accountInfo.defaultSubscriberField, _this10.amountPayable);
            }, function () {// Nothing to do
            });
          } else {
            this.getCreditCardPaymentOptions(String(accountInfo.bANField), accountInfo.defaultSubscriberField, this.amountPayable);
          }
        }
      }, {
        key: "getCreditCardPaymentOptions",
        value: function getCreditCardPaymentOptions(account, subscriber, amount) {
          var _this11 = this;

          this.showProgress('Está siendo redirigido a la página de el Banco Popular para realizar el pago.');
          this.services.getPaymentOptions(this.productId, this.merchantId).then(function (response) {
            if (response.paymentOptions === undefined || response.paymentOptions == null || response.paymentOptions.length === 0) {
              _this11.dismissProgress();

              _this11.showError('Lo sentimos, actualmente no contamos con metodos de pagos disponibles.');
            } else {
              var options = response.paymentOptions[0];

              _this11.initPayment(account, subscriber, amount, options);
            }
          }, function (error) {
            _this11.dismissProgress();

            _this11.showError(error.message);
          });
        }
      }, {
        key: "initPayment",
        value: function initPayment(account, subscriber, amount, options) {
          var _this12 = this;

          var self = this;
          var data = this.cacheStorage();
          this.services.initiatePaymentProcess(this.productId, options.paymentOptionId, amount, data.accountInfo.emailField, subscriber, account, data.accountInfo.firstNameField + ' ' + data.accountInfo.lastNameField, options.paymentOptionName, this.merchantId).then(function (response) {
            _this12.store(_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_12__["APP"].PAYMENT_STATUS_STORED, response.paymentToken);

            _this12.browserProvider.openPaymentBrowser(response.redirectURL).then(function (done) {
              // if browser desktop, we wait 1 minute for payment
              if (_utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].getPlatformInfo().desktop) {
                setTimeout(function () {
                  self.verifyPayment();
                }, 60 * 1000);
              } else {
                // if mobile, we wait 15 seconds after payment done
                if (done) {
                  _this12.showProgress('Verificando pago...');

                  setTimeout(function () {
                    self.verifyPayment();
                  }, 30 * 1000); // 30 seconds after finish payment for verify
                } else {
                  _this12.dismissProgress();
                }
              }
            })["catch"](function (err) {
              _this12.dismissProgress();

              _this12.showError(err);
            });
          }, function (error) {
            _this12.dismissProgress();

            _this12.showError(error.message);
          });
        }
      }, {
        key: "verifyPayment",
        value: function verifyPayment() {
          var _this13 = this;

          this.fetch(_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_12__["APP"].PAYMENT_STATUS_STORED).then(function (result) {
            _this13.services.verifyPayment(result).then(function (response) {
              if (response.processEndState) {
                _this13.showProgress('Actualizando su balance...');

                _this13.reloadCurrentAccount().then(function () {
                  _this13.dismissProgress();

                  _this13.goHomePage();
                });
              } else {
                _this13.dismissProgress();

                _this13.showAlert('Su pago aun no ha sido procesado, si ya realizo el pago espere mientras lo procesamos.', function () {
                  _this13.goHomePage();
                });
              }
            }, function (error) {
              _this13.dismissProgress();

              _this13.showError(error.message);
            });
          });
        }
      }, {
        key: "recharge",
        value: function recharge() {
          var _this14 = this;

          var rechargeAmount = parseFloat(this.amountPayable);
          this.amountPayable = parseFloat(String(rechargeAmount)).toFixed(2);

          if (Number.isNaN(rechargeAmount)) {
            this.amountPayable = '0.00';
            this.showError('El monto de recarga no es un número válido.');
            return;
          } else if (rechargeAmount < 5) {
            this.showError('El monto de recarga no puede ser menor a $5.00');
          } else if (rechargeAmount > 150) {
            this.showError('El monto de recarga no puede ser mayor a $150.00');
          } else {
            this.showConfirm('Confirmación', '¿Desea proceder a recargar $' + _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(rechargeAmount) + '?', function () {
              _this14.getPrepaidProducts(rechargeAmount);
            }, function () {// Nothing to do
            });
          }
        }
      }, {
        key: "getPrepaidProducts",
        value: function getPrepaidProducts(rechargeAmount) {
          var _this15 = this;

          this.showProgress();
          var data = this.cacheStorage();
          this.services.listProductService(this.selectedSubscriber.subscriberNumberField, 2, data.tokenSession).then(function (response) {
            _this15.dismissProgress();

            _this15.prepaidProducts = response.formProducts;

            _this15.goToRechargeConfirm(rechargeAmount);
          }, function (error) {
            _this15.dismissProgress();

            _this15.showError(error.message);
          });
        }
      }, {
        key: "getOtherProduct",
        value: function getOtherProduct() {
          var item;
          this.prepaidProducts.forEach(function (product) {
            if (product.idProduct === 24) {
              item = product;
            }
          });
          return item;
        }
      }, {
        key: "goToRechargeConfirm",
        value: function goToRechargeConfirm(rechargeAmount) {
          var product = this.getOtherProduct();
          product.amountRecharge = rechargeAmount;

          var amount = _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(product.amountRecharge);

          var ivu = _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(product.amountRecharge * product.ivuState);

          var totalAmount = _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(Number(amount) + Number(ivu));

          this.cacheStorage().PaymentProcess = {
            subscriber: this.selectedSubscriber.subscriberNumberField,
            payments: this.prepaidPayments,
            products: this.prepaidProducts,
            address: this.prepaidAddress,
            selectedProduct: product,
            amount: amount,
            ivu: ivu,
            totalAmount: totalAmount
          };
          this.goPage('/module/recharge/confirm');
        }
      }, {
        key: "getWalletBalance",
        value: function getWalletBalance() {
          if (this.selectedSubscriber !== undefined) {
            return _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(this.selectedSubscriber.eWalletBalanceField);
          }

          return '0.00';
        }
      }, {
        key: "getPrepaidBalance",
        value: function getPrepaidBalance() {
          if (this.selectedSubscriber !== undefined) {
            return _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].formatAmount(this.selectedSubscriber.prepaidBalanceField);
          }

          return '0.00';
        }
      }, {
        key: "getNextRecharge",
        value: function getNextRecharge() {
          var nextRecharge = 'N/A';

          if (this.selectedSubscriber !== undefined && this.selectedSubscriber.planInfoField !== undefined) {
            nextRecharge = this.selectedSubscriber.planInfoField.endDateField;

            if (nextRecharge === null || nextRecharge === undefined || nextRecharge === '') {
              nextRecharge = 'N/A';
            }
          }

          return nextRecharge;
        }
      }, {
        key: "get1GBReceived",
        value: function get1GBReceived() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this16 = this;

            var data;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    data = this.cacheStorage();
                    _context3.next = 3;
                    return this.services.getGift1GBSend(String(data.accountInfo.bANField), data.tokenSession).then(function (response) {
                      if (response.Gift1GBsents.length > 0) {
                        _this16.product = response.Gift1GBsents[0];

                        _this16.store(src_app_utils_const_keys__WEBPACK_IMPORTED_MODULE_11__["keys"].GIFT.DATA_GIFT, _this16.product);

                        _this16.showPopupGiftReceived = true;
                      }
                    });

                  case 3:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "closePopup1GB",
        value: function closePopup1GB() {
          var _this17 = this;

          this.showPopupGiftReceived = false;
          this.showProgress();
          var data = this.cacheStorage();
          this.services.getGift1GBByGUI(this.product.BANReceiver, this.product.GUI, data.tokenSession).then(function (response) {
            _this17.dismissProgress();

            _this17.showAlert('El Regalo ha sido aceptado con Éxito', function () {
              _this17.goHomePage();
            });
          }, function (error) {
            _this17.dismissProgress();

            _this17.showError(error.message);
          });
        }
      }, {
        key: "changePrepaidSubscriber",
        value: function changePrepaidSubscriber() {
          this.showProgress();
          this.dismissProgress();
          this.cacheStorage().prepaidSelectedSubscriber = this.selectedSubscriber.subscriberNumberField;
          this.continueSelectAccount(this.cacheStorage().loginData, false);
        }
      }, {
        key: "openChat",
        value: function openChat() {
          this.redirectProvider.openChat();
        }
      }]);

      return DashboardComponent;
    }(_base_page__WEBPACK_IMPORTED_MODULE_8__["BasePage"]);

    DashboardComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]
      }, {
        type: _services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"]
      }, {
        type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__["IntentProvider"]
      }, {
        type: _services_redirect_provider__WEBPACK_IMPORTED_MODULE_13__["RedirectProvider"]
      }, {
        type: _services_browser_provider__WEBPACK_IMPORTED_MODULE_14__["BrowserProvider"]
      }];
    };

    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home-dashboard',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./dashboard.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/dashboard/dashboard.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./dashboard.component.scss */
      "./src/app/pages/home/dashboard/dashboard.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"], _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__["IntentProvider"], _services_redirect_provider__WEBPACK_IMPORTED_MODULE_13__["RedirectProvider"], _services_browser_provider__WEBPACK_IMPORTED_MODULE_14__["BrowserProvider"]])], DashboardComponent);
    /***/
  },

  /***/
  "./src/app/pages/home/home.component.scss":
  /*!************************************************!*\
    !*** ./src/app/pages/home/home.component.scss ***!
    \************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesHomeHomeComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/pages/home/home.component.ts":
  /*!**********************************************!*\
    !*** ./src/app/pages/home/home.component.ts ***!
    \**********************************************/

  /*! exports provided: HomeComponent */

  /***/
  function srcAppPagesHomeHomeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomeComponent", function () {
      return HomeComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var HomeComponent = /*#__PURE__*/function () {
      function HomeComponent() {
        _classCallCheck(this, HomeComponent);
      }

      _createClass(HomeComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return HomeComponent;
    }();

    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./home.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./home.component.scss */
      "./src/app/pages/home/home.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], HomeComponent);
    /***/
  },

  /***/
  "./src/app/pages/home/home.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/pages/home/home.module.ts ***!
    \*******************************************/

  /*! exports provided: routes, HomeModule */

  /***/
  function srcAppPagesHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "routes", function () {
      return routes;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomeModule", function () {
      return HomeModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./home.component */
    "./src/app/pages/home/home.component.ts");
    /* harmony import */


    var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../_shared/shared.module */
    "./src/app/pages/_shared/shared.module.ts");
    /* harmony import */


    var _no_associated_no_associated_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./no-associated/no-associated.component */
    "./src/app/pages/home/no-associated/no-associated.component.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./dashboard/dashboard.component */
    "./src/app/pages/home/dashboard/dashboard.component.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var ngx_popper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ngx-popper */
    "./node_modules/ngx-popper/fesm2015/ngx-popper.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var routes = [{
      path: 'dashboard',
      component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_8__["DashboardComponent"]
    }, {
      path: 'no-associated',
      component: _no_associated_no_associated_component__WEBPACK_IMPORTED_MODULE_5__["NoAssociatedComponent"]
    }];

    var HomeModule = function HomeModule() {
      _classCallCheck(this, HomeModule);
    };

    HomeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"], _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_8__["DashboardComponent"], _no_associated_no_associated_component__WEBPACK_IMPORTED_MODULE_5__["NoAssociatedComponent"]],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"]],
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forChild(routes), _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_11__["IonicModule"], ngx_popper__WEBPACK_IMPORTED_MODULE_10__["NgxPopperModule"].forRoot({
        placement: 'top',
        styles: {
          'background-color': 'white'
        }
      })]
    })], HomeModule);
    /***/
  },

  /***/
  "./src/app/pages/home/no-associated/no-associated.component.scss":
  /*!***********************************************************************!*\
    !*** ./src/app/pages/home/no-associated/no-associated.component.scss ***!
    \***********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesHomeNoAssociatedNoAssociatedComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvbm8tYXNzb2NpYXRlZC9uby1hc3NvY2lhdGVkLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/pages/home/no-associated/no-associated.component.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/pages/home/no-associated/no-associated.component.ts ***!
    \*********************************************************************/

  /*! exports provided: NoAssociatedComponent */

  /***/
  function srcAppPagesHomeNoAssociatedNoAssociatedComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NoAssociatedComponent", function () {
      return NoAssociatedComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/services.provider */
    "./src/app/services/services.provider.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_utils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../services/utils.service */
    "./src/app/services/utils.service.ts");
    /* harmony import */


    var _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../services/intent.provider */
    "./src/app/services/intent.provider.ts");
    /* harmony import */


    var _base_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../base.page */
    "./src/app/pages/base.page.ts");

    var NoAssociatedComponent = /*#__PURE__*/function (_base_page__WEBPACK_I2) {
      _inherits(NoAssociatedComponent, _base_page__WEBPACK_I2);

      var _super2 = _createSuper(NoAssociatedComponent);

      function NoAssociatedComponent(router, storage, modelsServices, alertController, utilsService, userStorage) {
        var _this18;

        _classCallCheck(this, NoAssociatedComponent);

        _this18 = _super2.call(this, router, storage, modelsServices, alertController, utilsService, userStorage);
        _this18.accounts = [];
        return _this18;
      }

      _createClass(NoAssociatedComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "openAccountsManage",
        value: function openAccountsManage() {
          this.goPage('module/add-account');
        }
      }]);

      return NoAssociatedComponent;
    }(_base_page__WEBPACK_IMPORTED_MODULE_8__["BasePage"]);

    NoAssociatedComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]
      }, {
        type: _services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"]
      }, {
        type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__["IntentProvider"]
      }];
    };

    NoAssociatedComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home-no-associated',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./no-associated.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/no-associated/no-associated.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./no-associated.component.scss */
      "./src/app/pages/home/no-associated/no-associated.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"], _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__["IntentProvider"]])], NoAssociatedComponent);
    /***/
  }
}]);
//# sourceMappingURL=pages-home-home-module-es5.js.map