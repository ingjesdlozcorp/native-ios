(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-update-update-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/questions/questions.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/questions/questions.component.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow mbott-i\">\n\n        <div class=\"container\">\n            <div class=\"basicrow f-black roboto-r f-reg m-top-i text-center\">\n\n                <div class=\"basicrow m-bott-ii\">\n                    <div class=\"basicrow f-black f-mildt roboto-m\">\n                        Actualiza las Preguntas de Seguridad\n                    </div>\n\n                    <div class=\"basicrow m-top-ii text-justify\">\n                        Por favor selecciona dos preguntas de seguridad. Esto nos ayudar&aacute; a verificar tu identidad en caso de que olvides tu contrase&ntilde;a.\n                    </div>\n\n                    <div class=\"basicrow m-top\">\n                        <label class=\"label-f\">\n                            &#42; Primera Pregunta de Seguridad\n                        </label>\n\n                        <select class=\"sel-f\" [(ngModel)]=\"question1\">\n                            <option *ngFor=\"let q of questions\" [ngValue]=\"q.questionID\">{{q.question}}</option>\n                        </select>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <label class=\"label-f\">\n                            &#42; Respuesta Secreta\n                        </label>\n\n                        <input class=\"inp-f\" [(ngModel)]=\"answer1\">\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <label class=\"label-f\">\n                            &#42; Segunda Pregunta de Seguridad\n                        </label>\n\n                        <select class=\"sel-f\" [(ngModel)]=\"question2\">\n                            <option *ngFor=\"let q of questions\" [ngValue]=\"q.questionID\">{{q.question}}</option>\n                        </select>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <label class=\"label-f\">\n                            &#42; Respuesta Secreta\n                        </label>\n\n                        <input class=\"inp-f\" [(ngModel)]=\"answer2\">\n                    </div>\n\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div (click)=\"save()\" [className]=\"answer1.length > 0 && answer2.length > 0 ?\n             'btns vcenter red rippleR' : 'btns vcenter gray'\">\n                        <div class=\"tabcell\">\n                            Continuar\n                        </div>\n                    </div>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/step1/step1.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/step1/step1.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                Actualización de Registro\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                    Hemos detectado cambios en la configuración de su cuenta. Por favor ingrese el número de telefono con el que desea acceso a Mi Claro.\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"basicrow\">\n                        <input type=\"tel\"\n                               maxlength=\"10\"\n                               inputmode=\"numeric\"\n                               pattern=\"^[0-9]*$\"\n                               placeholder=\"N&uacute;mero de Tel&eacute;fono\"\n                               class=\"inp-f\"\n                               [(ngModel)]=\"number\"\n                               (keyup.enter)=\"nextStep()\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"btns red vcenter rippleR\" (click)=\"nextStep()\">\n                        <div class=\"tabcell\">\n                            Continuar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/step2/step2.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/step2/step2.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                Actualización de Registro\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div *ngIf=\"isProductTypeG\">\n                    <div *ngIf=\"isTelephony\" class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                        Hemos enviado el código de verificación, vía EMAIL.\n                    </div>\n\n                    <div *ngIf=\"!isTelephony\" class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                        Hemos enviado el código de verificación, vía SMS.\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow rel\">\n                            <img alt=\"\" *ngIf=\"isTelephony\" [popper]=\"'PIN a tu correo electrónico.'\"\n                                 [popperTrigger]=\"'click'\"\n                                 [popperTimeoutAfterShow]=\"3000\"\n                                 class=\"q-t-pay\" src=\"assets/images/tooltip1.png\">\n                            <img alt=\"\" *ngIf=\"!isTelephony\" [popper]=\"'PIN a tu celular y correo electrónico.'\"\n                                 [popperTrigger]=\"'click'\"\n                                 [popperTimeoutAfterShow]=\"3000\"\n                                 class=\"q-t-pay\" src=\"assets/images/tooltip1.png\">\n                            <input type=\"tel\"\n                                   maxlength=\"6\"\n                                   inputmode=\"numeric\"\n                                   pattern=\"^[0-9]*$\"\n                                   placeholder=\"Código de confirmación\"\n                                   class=\"inp-f disc\"\n                                   [(ngModel)]=\"code\">\n                            <div class=\"basicrow m-top-u f-reg f-black roboto-r text-center\">\n                                &#191;No has recibido el c&oacute;digo de seguridad&#63; <a (click)=\"resend()\" class=\"linkdefs\">Reenviar</a>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div *ngIf=\"!isProductTypeG\">\n                    <div class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                        Por favor ingrese los últimos 4 dígitos de Seguro Social.\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"tel\"\n                                   maxlength=\"4\"\n                                   inputmode=\"numeric\"\n                                   pattern=\"^[0-9]*$\"\n                                   placeholder=\"4 &uacute;ltimos de SSN\"\n                                   class=\"inp-f disc\"\n                                   [(ngModel)]=\"ssn\">\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div (click)=\"nextStep()\"\n                         [className]=\"\n                        (isProductTypeG && code.length === 6)\n                        || (!isProductTypeG && ssn.length === 4)\n                         ? 'btns vcenter red rippleR' : 'btns vcenter gray'\">\n                        <div class=\"tabcell\">\n                            Continuar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/update.component.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/update.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <router-outlet></router-outlet>\n\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/username/username.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/username/username.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\" >\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center m-top-ii\">\n\n                <div class=\"shortcont\">\n\n                    <div class=\"updt-textcont roboto-r f-black\">\n                        <img alt=\"\" class=\"advrt-icn\" src=\"assets/images/icono_advertencia.png\" width=\"100%\">\n\n                        <div class=\"basicrow f-big roboto-b\">Actualiza tu Usuario y Contrase&ntilde;a</div>\n\n                        <div class=\"basicrow m-top-ii\">Estamos aplicando cambios que nos ayudar&aacute;n a unificar nuestros portales y mejorar tu experiencia de acceso.</div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        Ahora podr&aacute;s ingresar al portal Mi Claro o a la aplicaci&oacute;n m&oacute;vil Mi Claro PR con un correo electr&oacute;nico o n&uacute;mero de tel&eacute;fono.\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <label class=\"label-f\">\n                            &#42; Usuario Actual\n                        </label>\n\n                        <input class=\"inp-f off\" [(ngModel)]=\"oldUsername\">\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <label class=\"label-f\">\n                            &#42; Nuevo Usuario\n                        </label>\n\n                        <div class=\"basicrow rel\">\n                            <div class=\"ortxtcnt text-center f-reg roboto-r f-black\">&oacute;</div>\n\n                            <div class=\"row\">\n                                <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">\n                                    <input class=\"inp-f updt-inp\" type=\"email\" placeholder=\"usuario@claropr.com\" [(ngModel)]=\"newUsername\">\n                                </div>\n\n                                <div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\">\n                                    <input class=\"inp-f updt-inp\" type=\"number\" placeholder=\"N&uacute;mero de Tel&eacute;fono\" disabled [(ngModel)]=\"number\">\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow f-big m-top\">\n                        Actualiza tu Contrase&ntilde;a\n                    </div>\n\n                    <div class=\"basicrow bg-gray-u-i allpads bordsdef m-top-ii roboto-b text-justify\">\n                        El cambio de contrase&ntilde;a s&oacute;lo es efectivo si llenas los tres campos previstos: anterior, nuevo y confirmaci&oacute;n, y presionas Guardar Cambios.\n                    </div>\n\n                    <div class=\"basicrow m-top text-justify\">\n                        La contrase&ntilde;a debe tener entre 8 y 10 caracteres, incluyendo un m&iacute;nimo de 2 n&uacute;meros, diferenciando entre may&uacute;sculas y min&uacute;sculas.\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <label class=\"label-f\">\n                                &#42; Contrase&ntilde;a Actual\n                            </label>\n\n                            <input [(ngModel)]=\"oldPassword\" class=\"inp-f\" type=\"password\" placeholder=\"Escribe tu contrase&ntilde;a actual\">\n                        </div>\n\n                        <div class=\"basicrow m-top-i\">\n                            <label class=\"label-f\">\n                                &#42; Nueva Contrase&ntilde;a\n                            </label>\n\n                            <input [(ngModel)]=\"newPassword\" type=\"password\" placeholder=\"Escribe una nueva contrase&ntilde;a\" class=\"inp-f\">\n                        </div>\n\n                        <div class=\"basicrow m-top-i\">\n                            <label class=\"label-f\">\n                                &#42; Confirmar Nueva Contrase&ntilde;a\n                            </label>\n\n                            <input [(ngModel)]=\"repeatPassword\" type=\"password\" placeholder=\"Confirma la nueva contrase&ntilde;a\" class=\"inp-f\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"btns red vcenter rippleR\" (click)=\"save()\">\n                            <div class=\"tabcell\">\n                                Continuar\n                            </div>\n                        </div>\n                    </div>\n\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n\n\n");

/***/ }),

/***/ "./src/app/pages/update/questions/questions.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/update/questions/questions.component.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VwZGF0ZS9xdWVzdGlvbnMvcXVlc3Rpb25zLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/update/questions/questions.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/update/questions/questions.component.ts ***!
  \***************************************************************/
/*! exports provided: QuestionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuestionsComponent", function() { return QuestionsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");










let QuestionsComponent = class QuestionsComponent extends _base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.questions = [];
        this.answer1 = '';
        this.answer2 = '';
        this.loginData = {};
    }
    ngOnInit() {
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.DATA).then((data) => {
            this.loginData = data;
            this.getSecurityQuestions();
        });
    }
    getSecurityQuestions() {
        this.showProgress();
        this.services.getSecurityQuestions(this.loginData.token).then((response) => {
            this.questions = response.QuestionsItemsList;
            this.dismissProgress();
        }, error => {
            this.dismissProgress();
            this.showError(error.message);
            this.goLoginPage();
        });
    }
    save() {
        console.log(this.question1);
        console.log(this.question2);
        if (this.answer1.length === 0 || this.answer2.length === 0) {
            this.showError('Por favor introduzca sus respuestas de seguridad.');
        }
        else if (this.question1 === this.question2) {
            this.showError('Debe seleccionar dos preguntas de seguridad distintas.');
        }
        else {
            this.showProgress();
            this.services.setChallengeQuestions(this.question1, this.answer1, this.loginData.token).then((success) => {
                this.dismissProgress();
                this.saveSecondAnswer();
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
        }
    }
    saveSecondAnswer() {
        this.showProgress();
        this.services.setChallengeQuestions(this.question2, this.answer2, this.loginData.token).then((success) => {
            this.dismissProgress();
            this.showAlert('Se actualizo sus preguntas de seguridad con éxito.', () => {
                this.goLoginPage();
            });
        }, error => {
            this.dismissProgress();
            this.showError(error.message);
        });
    }
};
QuestionsComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
QuestionsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'questions-step1',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./questions.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/questions/questions.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./questions.component.scss */ "./src/app/pages/update/questions/questions.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], QuestionsComponent);



/***/ }),

/***/ "./src/app/pages/update/step1/step1.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/update/step1/step1.component.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VwZGF0ZS9zdGVwMS9zdGVwMS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/update/step1/step1.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/update/step1/step1.component.ts ***!
  \*******************************************************/
/*! exports provided: Step1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Step1Component", function() { return Step1Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../utils/utils */ "./src/app/utils/utils.ts");











let Step1Component = class Step1Component extends _base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }
    ngOnInit() {
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].UPDATE_REGISTRATION.TOKEN).then(success => {
            this.token = success;
        });
    }
    nextStep() {
        if (String(this.number).length !== 10) {
            this.showError('Debe ingresar un número de suscriptor válido.');
        }
        else {
            this.showProgress();
            this.services.validateSubscriberUpdate(String(this.number), this.token).then((success) => {
                this.dismissProgress();
                this.processResponse(success);
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
        }
    }
    processResponse(response) {
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].UPDATE_REGISTRATION.SUBSCRIBER, String(this.number));
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].UPDATE_REGISTRATION.PRODUCT_TYPE_G, response.productType === 'G');
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].UPDATE_REGISTRATION.IS_TELEPHONY, _utils_utils__WEBPACK_IMPORTED_MODULE_10__["Utils"].isTelephony(response.accountType, response.accountSubType, response.productType));
        this.goPage('update/step2');
    }
};
Step1Component.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
Step1Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'register-step1',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./step1.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/step1/step1.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./step1.component.scss */ "./src/app/pages/update/step1/step1.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], Step1Component);



/***/ }),

/***/ "./src/app/pages/update/step2/step2.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/update/step2/step2.component.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VwZGF0ZS9zdGVwMi9zdGVwMi5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/update/step2/step2.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/update/step2/step2.component.ts ***!
  \*******************************************************/
/*! exports provided: Step2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Step2Component", function() { return Step2Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");










let Step2Component = class Step2Component extends _base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.code = '';
        this.ssn = '';
        this.subscriber = '';
        this.isProductTypeG = true;
        this.isTelephony = false;
        this.token = '';
        this.username = '';
        this.password = '';
        this.keepAuthenticated = false;
    }
    ngOnInit() {
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].UPDATE_REGISTRATION.SUBSCRIBER).then(success => {
            this.subscriber = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].UPDATE_REGISTRATION.PRODUCT_TYPE_G).then(success => {
            this.isProductTypeG = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].UPDATE_REGISTRATION.IS_TELEPHONY).then(success => {
            this.isTelephony = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].UPDATE_REGISTRATION.TOKEN).then(success => {
            this.token = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].UPDATE_REGISTRATION.USERNAME_USED).then(success => {
            this.username = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].UPDATE_REGISTRATION.PASSWORD_USED).then(success => {
            this.password = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].UPDATE_REGISTRATION.KEEP_AUTH).then(success => {
            this.keepAuthenticated = success;
        });
    }
    nextStep() {
        if (this.isProductTypeG && this.code.length !== 6) {
            this.showError('Debe ingresar los datos solicitados.');
        }
        else if (!this.isProductTypeG && this.ssn.length !== 4) {
            this.showError('Debe ingresar los datos solicitados.');
        }
        else {
            this.showProgress();
            this.services.updateValidateAccount(this.subscriber, this.code, this.ssn, this.token).then((success) => {
                this.dismissProgress();
                this.processResponse(success);
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
        }
    }
    processResponse(response) {
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER, this.subscriber);
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.USERNAME, this.username);
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.PASSWORD, this.password);
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.KEEP, this.keepAuthenticated);
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.IS_LOGGED, true);
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.TRY_IN, true);
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].SESSION_TOKEN, this.token);
        this.goLoginPage();
    }
    resend() {
        this.showProgress();
        this.services.validateSubscriberUpdate(this.subscriber, this.token).then((success) => {
            this.dismissProgress();
            this.showAlert('Código de seguridad enviado.');
        }, error => {
            this.dismissProgress();
            this.showError(error.message);
        });
    }
};
Step2Component.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
Step2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'register-step2',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./step2.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/step2/step2.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./step2.component.scss */ "./src/app/pages/update/step2/step2.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], Step2Component);



/***/ }),

/***/ "./src/app/pages/update/update.component.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/update/update.component.ts ***!
  \**************************************************/
/*! exports provided: UpdateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateComponent", function() { return UpdateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let UpdateComponent = class UpdateComponent {
    constructor() { }
    ngOnInit() {
    }
};
UpdateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-update',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./update.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/update.component.html")).default
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], UpdateComponent);



/***/ }),

/***/ "./src/app/pages/update/update.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/update/update.module.ts ***!
  \***********************************************/
/*! exports provided: routes, UpdateModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateModule", function() { return UpdateModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_shared/shared.module */ "./src/app/pages/_shared/shared.module.ts");
/* harmony import */ var _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./step1/step1.component */ "./src/app/pages/update/step1/step1.component.ts");
/* harmony import */ var _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./step2/step2.component */ "./src/app/pages/update/step2/step2.component.ts");
/* harmony import */ var _update_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update.component */ "./src/app/pages/update/update.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var ngx_popper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-popper */ "./node_modules/ngx-popper/fesm2015/ngx-popper.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _username_username_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./username/username.component */ "./src/app/pages/update/username/username.component.ts");
/* harmony import */ var _questions_questions_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./questions/questions.component */ "./src/app/pages/update/questions/questions.component.ts");












const routes = [
    { path: 'step1', component: _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__["Step1Component"] },
    { path: 'step2', component: _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__["Step2Component"] },
    { path: 'username', component: _username_username_component__WEBPACK_IMPORTED_MODULE_10__["UsernameComponent"] },
    { path: 'questions', component: _questions_questions_component__WEBPACK_IMPORTED_MODULE_11__["QuestionsComponent"] },
];
let UpdateModule = class UpdateModule {
};
UpdateModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _username_username_component__WEBPACK_IMPORTED_MODULE_10__["UsernameComponent"],
            _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__["Step1Component"],
            _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__["Step2Component"],
            _update_component__WEBPACK_IMPORTED_MODULE_6__["UpdateComponent"],
            _questions_questions_component__WEBPACK_IMPORTED_MODULE_11__["QuestionsComponent"]
        ],
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_9__["RouterModule"].forChild(routes),
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"],
            ngx_popper__WEBPACK_IMPORTED_MODULE_8__["NgxPopperModule"].forRoot({ placement: 'top', styles: { 'background-color': 'white' } })
        ]
    })
], UpdateModule);



/***/ }),

/***/ "./src/app/pages/update/username/username.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/update/username/username.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3VwZGF0ZS91c2VybmFtZS91c2VybmFtZS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/pages/update/username/username.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/update/username/username.component.ts ***!
  \*************************************************************/
/*! exports provided: UsernameComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsernameComponent", function() { return UsernameComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");










let UsernameComponent = class UsernameComponent extends _base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.oldUsername = '';
        this.newUsername = '';
        this.number = '';
        this.oldPassword = '';
        this.newPassword = '';
        this.repeatPassword = '';
        this.loginData = {};
    }
    ngOnInit() {
        this.loginData = this.cacheStorage().loginData;
        this.oldUsername = this.loginData.username;
        this.number = this.loginData.subscriber;
    }
    save() {
        if (this.newUsername.length === 0) {
            this.showError('Debe ingresar su nuevo usuario.');
        }
        else if (!_utils_utils__WEBPACK_IMPORTED_MODULE_8__["Utils"].validateEmail(this.newUsername)) {
            this.showError('Debe ingresar un correo electrónico válido.');
        }
        else if (this.oldPassword.length === 0) {
            this.showError('Debe colocar su contaseña anterior.');
        }
        else if (this.newPassword.length < 8 || this.newPassword.length > 15) {
            this.showError('Su contaseña nueva debe tener entre 8 y 15 caracteres.');
        }
        else if (!this.lowercaseValidator(this.newPassword)) {
            this.showError('Su contaseña nueva debe tener al menos 1 letra minúscula.');
        }
        else if (!this.uppercaseValidator(this.newPassword)) {
            this.showError('Su contaseña nueva debe tener al menos 1 letra mayúscula.');
        }
        else if (!this.twoNumberValidator(this.newPassword)) {
            this.showError('Su contaseña nueva debe tener al menos 2 números.');
        }
        else if (!this.specialCharacterValidator(this.newPassword)) {
            this.showError('No estan permitidos caracteres especiales.');
        }
        else if (this.newPassword !== this.repeatPassword) {
            this.showError('Las contraseñas no coinciden.');
        }
        else {
            this.showProgress();
            this.services.updateUsername(this.newUsername, this.loginData.token).then((success) => {
                this.dismissProgress();
                this.updatePassword();
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
        }
    }
    updatePassword() {
        this.showProgress();
        this.services.updatePassword(this.newPassword, this.oldPassword, this.loginData.account, this.loginData.token).then((success) => {
            this.dismissProgress();
            this.showAlert('Se actualizo su usuario y contraseña con éxito.', () => {
                this.goLoginPage();
            });
        }, error => {
            this.dismissProgress();
            this.showError(error.message);
        });
    }
    lowercaseValidator(c) {
        const regex = /[a-z]/g;
        return regex.test(c);
    }
    uppercaseValidator(c) {
        const regex = /[A-Z]/g;
        return regex.test(c);
    }
    twoNumberValidator(c) {
        const numbers = '0123456789';
        let count = 0;
        for (let i = 0; i < c.length; i++) {
            if (numbers.indexOf(c.charAt(i), 0) !== -1) {
                count++;
            }
        }
        return count >= 2;
    }
    specialCharacterValidator(c) {
        return c.match('^[A-z0-9]+$');
    }
};
UsernameComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
UsernameComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-update-username',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./username.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update/username/username.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./username.component.scss */ "./src/app/pages/update/username/username.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], UsernameComponent);



/***/ })

}]);
//# sourceMappingURL=pages-update-update-module-es2015.js.map