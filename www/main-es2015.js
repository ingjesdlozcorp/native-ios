(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./pages/help/help.module": [
		"./src/app/pages/help/help.module.ts",
		"pages-help-help-module"
	],
	"./pages/home/home.module": [
		"./src/app/pages/home/home.module.ts",
		"pages-home-home-module"
	],
	"./pages/module/module.module": [
		"./src/app/pages/module/module.module.ts",
		"pages-module-module-module"
	],
	"./pages/recover/recover.module": [
		"./src/app/pages/recover/recover.module.ts",
		"pages-recover-recover-module"
	],
	"./pages/register/register.module": [
		"./src/app/pages/register/register.module.ts",
		"pages-register-register-module"
	],
	"./pages/update/update.module": [
		"./src/app/pages/update/update.module.ts",
		"pages-update-update-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet-controller_8.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-controller_8.entry.js",
		"common",
		0
	],
	"./ion-action-sheet-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-ios.entry.js",
		"common",
		1
	],
	"./ion-action-sheet-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-md.entry.js",
		"common",
		2
	],
	"./ion-alert-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-alert-ios.entry.js",
		"common",
		3
	],
	"./ion-alert-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-alert-md.entry.js",
		"common",
		4
	],
	"./ion-app_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-app_8-ios.entry.js",
		"common",
		5
	],
	"./ion-app_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-app_8-md.entry.js",
		"common",
		6
	],
	"./ion-avatar_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-avatar_3-ios.entry.js",
		"common",
		7
	],
	"./ion-avatar_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-avatar_3-md.entry.js",
		"common",
		8
	],
	"./ion-back-button-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-back-button-ios.entry.js",
		"common",
		9
	],
	"./ion-back-button-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-back-button-md.entry.js",
		"common",
		10
	],
	"./ion-backdrop-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-backdrop-ios.entry.js",
		11
	],
	"./ion-backdrop-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-backdrop-md.entry.js",
		12
	],
	"./ion-button_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-button_2-ios.entry.js",
		"common",
		13
	],
	"./ion-button_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-button_2-md.entry.js",
		"common",
		14
	],
	"./ion-card_5-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-card_5-ios.entry.js",
		"common",
		15
	],
	"./ion-card_5-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-card_5-md.entry.js",
		"common",
		16
	],
	"./ion-checkbox-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-checkbox-ios.entry.js",
		"common",
		17
	],
	"./ion-checkbox-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-checkbox-md.entry.js",
		"common",
		18
	],
	"./ion-chip-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-chip-ios.entry.js",
		"common",
		19
	],
	"./ion-chip-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-chip-md.entry.js",
		"common",
		20
	],
	"./ion-col_3.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-col_3.entry.js",
		21
	],
	"./ion-datetime_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-datetime_3-ios.entry.js",
		"common",
		22
	],
	"./ion-datetime_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-datetime_3-md.entry.js",
		"common",
		23
	],
	"./ion-fab_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-fab_3-ios.entry.js",
		"common",
		24
	],
	"./ion-fab_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-fab_3-md.entry.js",
		"common",
		25
	],
	"./ion-img.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-img.entry.js",
		26
	],
	"./ion-infinite-scroll_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-ios.entry.js",
		"common",
		27
	],
	"./ion-infinite-scroll_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-md.entry.js",
		"common",
		28
	],
	"./ion-input-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-input-ios.entry.js",
		"common",
		29
	],
	"./ion-input-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-input-md.entry.js",
		"common",
		30
	],
	"./ion-item-option_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item-option_3-ios.entry.js",
		"common",
		31
	],
	"./ion-item-option_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item-option_3-md.entry.js",
		"common",
		32
	],
	"./ion-item_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item_8-ios.entry.js",
		"common",
		33
	],
	"./ion-item_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item_8-md.entry.js",
		"common",
		34
	],
	"./ion-loading-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-loading-ios.entry.js",
		"common",
		35
	],
	"./ion-loading-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-loading-md.entry.js",
		"common",
		36
	],
	"./ion-menu_4-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-menu_4-ios.entry.js",
		"common",
		37
	],
	"./ion-menu_4-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-menu_4-md.entry.js",
		"common",
		38
	],
	"./ion-modal-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-modal-ios.entry.js",
		"common",
		39
	],
	"./ion-modal-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-modal-md.entry.js",
		"common",
		40
	],
	"./ion-nav_5.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-nav_5.entry.js",
		"common",
		41
	],
	"./ion-popover-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-popover-ios.entry.js",
		"common",
		42
	],
	"./ion-popover-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-popover-md.entry.js",
		"common",
		43
	],
	"./ion-progress-bar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-progress-bar-ios.entry.js",
		"common",
		44
	],
	"./ion-progress-bar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-progress-bar-md.entry.js",
		"common",
		45
	],
	"./ion-radio_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-radio_2-ios.entry.js",
		"common",
		46
	],
	"./ion-radio_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-radio_2-md.entry.js",
		"common",
		47
	],
	"./ion-range-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-range-ios.entry.js",
		"common",
		48
	],
	"./ion-range-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-range-md.entry.js",
		"common",
		49
	],
	"./ion-refresher_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-refresher_2-ios.entry.js",
		"common",
		50
	],
	"./ion-refresher_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-refresher_2-md.entry.js",
		"common",
		51
	],
	"./ion-reorder_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-reorder_2-ios.entry.js",
		"common",
		52
	],
	"./ion-reorder_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-reorder_2-md.entry.js",
		"common",
		53
	],
	"./ion-ripple-effect.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-ripple-effect.entry.js",
		54
	],
	"./ion-route_4.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-route_4.entry.js",
		"common",
		55
	],
	"./ion-searchbar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-searchbar-ios.entry.js",
		"common",
		56
	],
	"./ion-searchbar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-searchbar-md.entry.js",
		"common",
		57
	],
	"./ion-segment_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-segment_2-ios.entry.js",
		"common",
		58
	],
	"./ion-segment_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-segment_2-md.entry.js",
		"common",
		59
	],
	"./ion-select_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-select_3-ios.entry.js",
		"common",
		60
	],
	"./ion-select_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-select_3-md.entry.js",
		"common",
		61
	],
	"./ion-slide_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-slide_2-ios.entry.js",
		62
	],
	"./ion-slide_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-slide_2-md.entry.js",
		63
	],
	"./ion-spinner.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-spinner.entry.js",
		"common",
		64
	],
	"./ion-split-pane-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-split-pane-ios.entry.js",
		65
	],
	"./ion-split-pane-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-split-pane-md.entry.js",
		66
	],
	"./ion-tab-bar_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-ios.entry.js",
		"common",
		67
	],
	"./ion-tab-bar_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-md.entry.js",
		"common",
		68
	],
	"./ion-tab_2.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab_2.entry.js",
		"common",
		69
	],
	"./ion-text.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-text.entry.js",
		"common",
		70
	],
	"./ion-textarea-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-textarea-ios.entry.js",
		"common",
		71
	],
	"./ion-textarea-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-textarea-md.entry.js",
		"common",
		72
	],
	"./ion-toast-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toast-ios.entry.js",
		"common",
		73
	],
	"./ion-toast-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toast-md.entry.js",
		"common",
		74
	],
	"./ion-toggle-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toggle-ios.entry.js",
		"common",
		75
	],
	"./ion-toggle-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toggle-md.entry.js",
		"common",
		76
	],
	"./ion-virtual-scroll.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-virtual-scroll.entry.js",
		77
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<ion-app>\n    <app-loading></app-loading>\n    <div id=\"wrapper\" class=\"appsize\">\n        <ion-router-outlet></ion-router-outlet>\n        <app-footer></app-footer>\n    </div>\n</ion-app>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/account-select/account-select.component.html":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/account-select/account-select.component.html ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<select class=\"sel-f\" [(ngModel)]=\"selectedBan\" (change)=\"onChange()\">\n    <option *ngFor=\"let account of accounts\" value=\"{{account.account}}\">{{account.account}}</option>\n</select>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/footer/footer.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/footer/footer.component.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div *ngIf=\"shouldShow()\" class=\"allfootcont\" (click)=\"goPage('help/home')\" routerLink=\"help\" [routerLinkActiveOptions]=\"{exact:false}\" [routerLinkActive]=\"['hidden']\">\n    <div class=\"footcont\">\n        <div class=\"container vcenter\">\n            <div class=\"basicrow text-center\">\n                <img class=\"chaticon\" width=\"100%\" src=\"assets/images/chaticon.png\" alt=\"\">\n            </div>\n\n            <div class=\"basicrow f-white f-small text-center\">\n                Ayuda\n            </div>\n        </div>\n    </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/header-static/header-static.component.html":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/header-static/header-static.component.html ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"basicrow\">\n    <div class=\"menubar\">\n        <div class=\"container\">\n            <div class=\"basicrow text-center\">\n                <div class=\"mc-logo center\">\n                    <span class=\"autocont vcenter\">\n                        <span class=\"tabcell\">\n                            <img width=\"100%\" src=\"assets/images/miclaro-logo.png\" alt=\"\">\n                        </span>\n                    </span>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/header/header.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/header/header.component.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- Header Bar -->\n<div class=\"menubar\">\n\n    <div class=\"header container rel\">\n\n        <div class=\"mc-logo mcenter\" (click)=\"goHomePage()\">\n            <span class=\"autocont vcenter\">\n                <span class=\"tabcell\">\n                    <img alt=\"\" width=\"100%\" src=\"assets/images/miclaro-logo.png\">\n                </span>\n            </span>\n        </div>\n\n\n\n        <!--<div class=\"item pull-left\" (click)=\"goBack()\">-->\n            <!--<img alt=\"\" class=\"dispicon\" src=\"assets/images/back.svg\">-->\n        <!--</div>-->\n\n        <div *ngIf=\"!backButtonHidden()\" class=\"item pull-left\" (click)=\"goBack()\">\n            <img alt=\"\" class=\"dispicon\" src=\"assets/images/back.svg\">\n        </div>\n\n        <div *ngIf=\"router.isActive('/home/dashboard', false)\" class=\"item pull-left\" (click)=\"notifications()\">\n            <div *ngIf=\"countNotifications > 0\" class=\"autocont ntif f-reg roboto-m\">\n                <div class=\"notifc\">\n                    {{countNotifications}}\n                </div>\n            </div>\n            <img alt=\"\" class=\"dispicon\" src=\"assets/images/user.svg\">\n        </div>\n\n        <div *ngIf=\"close && !router.isActive('/home/no-associated', false) && !menuHidden()\" class=\"item pull-right\" (click)=\"openNav()\">\n            <img alt=\"\" class=\"dispicon\" src=\"assets/images/bars.svg\">\n        </div>\n\n        <div *ngIf=\"!close\" class=\"item pull-right\" (click)=\"closeNav()\">\n            <img alt=\"\" class=\"dispicon\" src=\"assets/images/times.svg\">\n        </div>\n\n    </div>\n</div>\n<!-- Header Bar -->\n\n\n<!-- Header Home -->\n<div class=\"basicrow m-top-ii m-bott-i f-black f-lsemi roboto-l visible-sm visible-xs\" [ngClass]=\"router.isActive('/home/dashboard', false) ? '':'hidden'\">\n    <div class=\"container\">\n        Hola {{name}}\n    </div>\n</div>\n\n<div class=\"basicrow\" *ngIf=\"!tabHidden()\">\n    <div class=\"acctypescont text-center\">\n        <div class=\"acctype\" (click)=typeOfClient(1) [ngClass]=\"typeBan.postpaid ? 'acctype on':'acctype'\">\n            <div class=\"autocont full vcenter\">\n                <div class=\"tabcell\" >\n                    <img alt=\"\" width=\"100%\" src=\"assets/images/postpaid-icon.png\"> Pospago\n                </div>\n            </div>\n        </div>\n\n        <div class=\"acctype\" (click)=typeOfClient(2) [ngClass]=\"typeBan.prepaid ? 'acctype on':'acctype'\">\n            <div class=\"autocont full vcenter\" >\n                <div class=\"tabcell\">\n                    <img alt=\"\" width=\"100%\" src=\"assets/images/prepaid-icon.png\"> Prepago\n                </div>\n            </div>\n        </div>\n\n        <div class=\"acctype\" (click)=typeOfClient(3) [ngClass]=\"typeBan.telephony ? 'acctype on':'acctype'\">\n            <div class=\"autocont full vcenter\" >\n                <div class=\"tabcell\">\n                    <img alt=\"\" width=\"100%\" src=\"assets/images/fixedphone-icon.png\"> Servicios Fijos\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<!-- Header Home -->\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/loading/loading.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/loading/loading.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"popupbg\" *ngIf=\"show\">\n    <div class=\"popupcont morew\">\n        <div class=\"basicrow text-center\">\n            <img width=\"100%\" class=\"bigclarogif\" style=\"width: 40px; height: 40px;\" src=\"../../../../../assets/images/claroloadgif.gif\"  alt=\"\"/>\n        </div>\n\n        <div class=\"basicrow f-bmed f-black roboto-r\">\n            <div class=\"row\">\n                <div class=\"col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1\">\n                    <div class=\"basicrow m-top-i text-center\">\n                        {{message}}\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/menu/menu.component.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/menu/menu.component.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- Navigation Menu -->\n<div class=\"mySideNav\" [ngClass]=\"close ? 'menuIsClosed':'menuIsOpen'\">\n\n    <div *ngFor=\"let section of access\">\n\n        <div *ngIf=\"section.Pages.length > 1\">\n\n            <div data-toggle=\"collapse\" (click)='showingMenu(section.index)' class=\"m-menubar vcenter\" [ngClass]=\"{'important': section.sectionName == 'Mi Cuenta'}\">\n                <span class=\"tabcell\">\n                    {{section.sectionName}}\n                    <i class=\"fa fa-plus\" aria-hidden=\"true\" [ngClass]=\"selectedSection == section.index ? 'fa fa-minus':'fa fa-plus'\"></i>\n                </span>\n            </div>\n\n            <div class=\"collapse\" [ngClass]=\"{'in': selectedSection == section.index, 'collapse': selectedSection != section.index}\">\n\n                <div *ngFor=\"let page of section.Pages\" class=\"m-menuin vcenter\" (click)=\"going(page)\" [ngClass]=\"{'offnouse': isGuest && !page.allowAsGuest}\">\n                    <span class=\"tabcell\">\n                        <p [ngClass]=\"{'f-bold': isActive(page)}\">{{page.pageName}}</p>\n                         <i *ngIf=\"page.accessID == 8\" class=\"fa fa-times-circle\" aria-hidden=\"true\"></i>\n                    </span>\n                </div>\n\n            </div>\n\n        </div>\n\n        <div *ngIf=\"section.Pages.length == 1\">\n\n            <div class=\"m-menubar vcenter\" (click)=\"going(section.Pages[0])\" [ngClass]=\"{'offnouse': isGuest && !section.Pages[0].allowAsGuest}\">\n                <span class=\"tabcell\">\n                    <p [ngClass]=\"{'f-bold': isActive(section.Pages[0]) }\">\n                        {{section.Pages[0].pageName}}\n                    </p>\n                    <i class=\"fa fa-angle-right\" aria-hidden=\"true\"></i>\n                </span>\n            </div>\n\n        </div>\n\n    </div>\n\n</div>\n<!-- Navigation Menu -->");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/popup/general-terms/popup-general-terms.component.html":
/*!***************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/popup/general-terms/popup-general-terms.component.html ***!
  \***************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"popupbg\">\n\n    <div class=\"popupcont\">\n        <div class=\"basicrow f-black f-mildt roboto-m\">\n            T&eacute;rminos y Condiciones\n        </div>\n\n        <div class=\"basicrow m-top\">\n            <div class=\"termgbg\">\n                <div class=\"termr-t\">\n                    <strong>1. Aceptación de Términos y Condiciones Mi Claro</strong>\n\n                    Toda persona que acceda a Mi Claro Puerto Rico (“Mi Claro”)  y las aplicaciones y sitios web vinculados a este sitio, afirma conocer y acuerda cumplir con los Términos y Condiciones expuestos por Puerto Rico Telephone Company Inc. h/n/c Claro Puerto Rico en este documento.  Claro Puerto Rico se reserva la facultad de modificar de tiempo en tiempo estos Términos y Condiciones. El uso continuo de este sitio y cualquiera de los sitios vinculados luego de la fecha de modificación, constituirá la aceptación de los Términos y Condiciones revisados.\n                    <br>\n                    <strong>2. Usuarios ¿Cuáles son los requisitos para ser usuario?</strong>\n\n                    2.1. Sólo podrán registrarse como usuarios de Mi Claro personas mayores de edad que acrediten ser clientes de Claro Puerto Rico. Los menores de 21 años que no emancipados accederán a Mi Claro a través de una cuenta de usuario creada con la autorización de su padre o tutor.  Los menores de trece (13) años no podrán suscribirse como usuarios de este servicio.\n\n                    2.2. Registro de Cuenta: El cliente deberá registrar su nombre de usuario y contraseña en la aplicación para poder crear su cuenta de usuario, conforme se describe a continuación:\n                    1)\tCliente ingresa número de teléfono fijo o móvil o correo electrónico.\n                    2)\tEl sistema enviará una clave temporal PIN vía SMS o correo electrónico\n                    3)\tIngresa PIN, 4 últimos del SSN, correo electrónico valido.\n                    4)\tCrear una contraseña, según los requerimientos.\n                    5)\tAceptar términos y condiciones de Mi Claro.\n                    <br>\n                    <strong>3. Uso General Mi Claro Puerto Rico</strong>\n\n                    3.1. <strong>Mi Claro app</strong>: no tiene costo de activación. Se accede al mismo únicamente a través de la descarga en terminales móviles inteligentes con sistemas operativos Android (versiones superiores a 4.2) e iOS (versiones superiores a 9.0).\n\n                    3.2. <strong>Mi Claro web</strong>: Los usuarios pueden navegar libremente por el sitio web Mi Claro por distintos navegadores. Para ingresar deben estar registrados previamente, para lo cual es obligatorio completar el formulario correctamente en todos los campos con datos válidos y verdaderos; el registro no tiene ningún costo. Claro se reserva el derecho de suspender temporal o definitivamente a los usuarios en caso de incumplimiento de los presentes Términos y Condiciones y/o de los que se le informen en el futuro.\n\n                    3.3. El Usuario reconoce y acepta que en la Autogestión &quot;Mi Claro Puerto Rico&quot;:\n                    (En su versión App o Web)\n\n                    (a)\tLa factura del último mes estará disponible en Mi Claro App y Mi Claro Web, cinco (5) días    después de finalizado el período de facturación.\n                    (b)\tAlgunos pagos pueden tardar entre 24-48 horas en verse reflejados en su cuenta.\n                    (c)\tEn Mi Claro App y Mi Claro Web podrá consultar y descargar las últimas trece (13) facturas cuando lo requiera.\n                    (d)\tTiene derecho a solicitar y obtener aclaración de la información mostrada en la autogestión Mi Claro, quedando Claro en la obligación de proporcionar la información solicitada. El usuario podrá ejercer el derecho de rectificación, cuando los datos que se posean fueran incorrectos.\n                    (e)\tLa mayoría de las inquietudes de los usuarios se pueden resolver de manera rápida y satisfactoria llamando a nuestro Departamento de Servicio al Cliente a los siguientes números telefónicos: 787-775-000 (individuos) o 787-792-6262 (corporativo).\n\n                    3.4. Para tener acceso a Mi Claro es necesario utilizar una computadora personal o un equipo celular, con línea de acceso a Internet y un navegador con capa de conexiones seguras (SSL, por sus siglas en inglés) y encriptación de 256 bits. Los navegadores recomendados son: Internet Explorer versión 8 o mayor para Windows; Google Chrome versión 13 o mayor para Windows; Mozilla Firefox versión 6 o mayor para Windows o Mac OS X.  Safari versión 7.0 o mayor para Mac OS X o Windows; Acrobat Reader® es necesario para ver tus facturas electrónicas.\n                    <br>\n                    <strong>4. Derecho para Reclamar Cargos</strong>\n\n                    4.1 Por disposición de la Ley 213 del 12 de septiembre de 1996 y la Ley 33 del 27 de junio de 1985, el Usuario/cliente tiene hasta veinte (20) días a partir del envío de la factura, para pagar u objetar los cargos contenidos en la misma. Para objetar y solicitar una investigación, comuníquese al (787) 775-0000, o por escrito al PO BOX 70367 San Juan PR 00936-8367. También puedes visitar nuestra página electrónica www.clarotodo.com. De no haberse efectuado el pago ni objetado dentro del plazo de 20 días, la compañía podrá suspender, desconectar y dar de baja el/los servicios. La objeción de un cargo no releva al cliente del pago de los cargos no objetados.\n\n                    4.2 La Junta Reglamentadora de Telecomunicaciones de Puerto Rico es el ente revisor de las controversias entre consumidores y compañía de telecomunicaciones. Si el Usuario/cliente no está de acuerdo con el resultado de la investigación, podrá acudir en revisión a la Junta, dentro de 30 días contados a partir de la notificación del resultado de la investigación. Para este y otros propósitos, puedes dirigir su comunicación a la siguiente dirección: 500 Ave. Roberto H. Todd, Santurce, PR 00907-3941, visitar personalmente las oficinas ubicadas en el 500 de la Ave. Roberto H. Todd, Pda. 18 en Santurce, o llamar al teléfono Metro 787-756-0804; Isla 1-866-578- 5500; Oficina de Analistas de Querellas / Servicio al Cliente 787-722-8606; correspondencia electrónica: correspondencia@jrpr.gobierno.pr\n                    <br>\n                    <strong>5. Seguridad</strong>\n\n                    5.1 La información que el Usuario ingrese a Mi Claro estará protegida por la contraseña que establezca al momento de registrar su cuenta de Usuario. La confidencialidad de la contraseña de acceso a su cuenta es de entera responsabilidad del Usuario, quien, deberá mantener seguro el ambiente de su teléfono móvil y su computadora, mediante el uso de herramientas disponibles, tales como antivirus, entre otras, actualizadas, de modo a contribuir en la prevención de riesgos electrónicos del lado del usuario, desde que estas herramientas de protección no interfieran en la utilización del Servicio. Así mismo, es responsabilidad del Usuario proporcionar, por sus propios medios, equipos (hardware) y programas (software) capaces de reproducir los archivos digitales disponibles en el Servicio.\n\n                    5.2 EL Usuario asume la plena responsabilidad frente a Claro Puerto Rico por todas las transacciones realizadas y que sean validadas a través de su cuenta de Usuario en Mi Claro, las cuales presuponen su consentimiento y autorización, incluyendo, pero sin limitarse a transacciones realizadas mediante el acceso no autorizado de su cuenta.\n\n                    5.3 EL Usuario, se obliga a notificar por escrito o por el medio más expedito a Claro Puerto Rico cualquiera de las siguientes situaciones: (i) Pérdida o robo de su clave o contraseña (ii) Uso no autorizado de su clave o contraseña ; (iii) Uso no autorizado de Mi Claro o de algún tipo de información contenida en la aplicación; (iv) Alguna falla, error o hecho inusual al recibir algún mensaje relacionado con una transacción iniciada por EL Usuario a través de Mi Claro o que haya sido recibida y/o ejecutada a través del mismo.\n                    <br>\n                    <strong>6. Uso de Información</strong>\n\n                    Claro Puerto Rico se reserva el derecho recopilar información sobre las actividades en línea del usuario por medio de “cookies” o cualquiera otro tipo de tecnología disponible para esos fines. El usuario autoriza de manera libre, voluntaria y expresa a Claro Puerto Rico el tratamiento de sus datos relacionados con su comportamiento o hábitos de consumo, tales como, pero no limitado a marca y modelo de su teléfono móvil, ubicación (previa autorización en la App y/o autorización en el sistema operativo del terminal que utiliza para acceder a Mi Claro), tipo de conexión empleada (Wi-Fi o datos móvil), aplicaciones instaladas y corriendo (previa autorización en la App), datos estadísticos de uso, de forma agregada y anónima, con la finalidad de enviarle publicaciones publicitarias digitales o cualquiera otro uso permitido por Ley.\n                    <br>\n                    <strong>7. Marcas, Derechos de Autor y Propiedad Intelectual</strong>\n\n                    7.1. Claro Puerto Rico es una marca de América Móvil y las demás marcas de fábrica, logotipos y marcas de servicio (denominadas colectivamente \"Marcas \") utilizados en este sitio Web pertenecen a Claro Puerto Rico o a sus respectivos propietarios. Nada de lo contenido en este sitio debe interpretarse como una concesión, ya sea por implicación, exclusión o de otro modo, de una licencia o derecho de utilizar la marca Claro Puerto Rico o cualquier otra Marca mostrada en este sitio Web sin el permiso por escrito de Claro Puerto Rico o de su respectivo propietario.\n\n                    7.2. Excepto según se dispone en estos Términos y Condiciones, Claro Puerto Rico ni concede ni otorga derecho, título, interés o licencia sobre derecho de autor, marcas, patente o cualquier propiedad intelectual sobre el Contenido de este sitio a los usuarios de Mi Claro. Todos los derechos de autor, derechos sobre marcas, patentes, o cualquier otro derecho de propiedad intelectual conocidos en la actualidad o reconocidos posteriormente le pertenecen a Claro Puerto Rico.\n\n                    7.3. TMETE A INDEMNIZAR A CLAROPR, SUS AFILIADAS, DIRECTORES, OFICIALES, EMPLEADOS AGENTES O REPRESENTANTES POR Y CONTRA TODO DAÑO, TODO USUARIO DE ESTE SITIO RELEVA DE RESPONSABILIDAD Y SE COMPROMETE A INDEMNIZAR A CLARO PUERTO RICO, SUS AFILIADAS, DIRECTORES, OFICIALES, EMPLEADOS AGENTES O REPRESENTANTES POR Y CONTRA TODO DAÑO, PÉRDIDA, MULTA O PENALIDAD, INCLUYENDO HONORARIOS DE ABOGADO QUE SURJAN O ESTÉN RELACIONADOS CON EL CONTENIDO DEL  MATERIAL QUE EL USUARIO SOMETA, ENVÍE O TRANSMITA A TRAVÉS DE ESTE  SITIO O SU ACCESO AL SITIO, LA VIOLACIÓN DE ESTOS TÉRMINOS Y CONDICIONES O LA VIOLACIÓN DE LOS DERECHOS DE TERCERO, INCLUYENDO, PERO SIN LIMITARSE A CUALQUIER VIOLACIÓN DE DERECHOS DE AUTOR, DERECHOS SOBRE MARCAS O DE CUALQUIER OTRO DERECHO DE PROPIEDAD INTELECTUAL COMETIDA POR EL USUARIO. CLARO PUERTO RICO SE RESERVA EL DERECHO DE ASUMIR EL CONTROL DE CUALQUIER ASUNTO POR EL CUAL USTED LE DEBA INDEMNIZAR, EN CUYO CASO USTED SE COMPROMETE A COOPERAR CON CLARO PUERTO RICO SEGÚN SE LE REQUIERA.\n                    <br>\n                    <strong>8. Incumplimiento con los Términos y Condiciones</strong>\n\n                    Claro Puerto Rico se reserva el derecho de reclamar todo remedio disponible en Ley o equidad incluyendo, pero sin limitarse a la cancelación de la cuenta de usuario, en caso de que cualquier usuario incumpla con estos Términos y Condiciones o la Ley aplicable al uso de este sitio.\n                    <br>\n                    <strong>9. Ausencia de Garantías</strong>\n\n                    9.1. EL CONTENIDO QUE SE OFRECE EN MI CLARO PUERTO RICO SE PROPORCIONA \"TAL CUAL\" Y \"SEGÚN ESTÉ DISPONIBLE\", SIN GARANTÍA DE NINGÚN TIPO, YA SEA EXPRESA O IMPLÍCITA, INCLUYENDO, PERO SIN LIMITARSE A GARANTÍAS DE DERECHOS DE PROPIEDAD, NO VIOLACIÓN O GARANTÍAS IMPLÍCITAS DE COMERCIABILIDAD O ADECUACIDAD PARA UN FIN ESPECÍFICO. NINGÚN CONSEJO O CONTENIDO OFRECIDO POR CLARO PUERTO RICO, SUS COMPAÑÍAS AFILIADAS O SUS EMPLEADOS RESPECTIVAMENTE CREARÁ NINGÚN TIPO DE GARANTÍA. NI CLARO PUERTO RICO NI SUS COMPAÑÍAS, AFILIADAS GARANTIZAN QUE EL CONTENIDO DE ESTE SERVICIO O DE INTERNET EN GENERAL SERÁN ININTERRUMPIBLES O QUE CARECERAN DE ERRORES, NI QUE CUALQUIER CONTENIDO, SOFTWARE U OTRO MATERIAL AL QUE SE ACCEDA DESDE ESTE SERVICIO NO CONTENDRÁ NINGÚN VIRUS U OTRO ELEMENTO PERJUDICIAL. ESTA EXCLUSIÓN DE GARANTÍAS CONSTITUYE UNA PARTE ESENCIAL DE LA DECLARACIÓN DE PRIVACIDAD.\n\n                    9.2. ES RESPONSABILIDAD DEL USUARIO EVALUAR LA EXACTITUD, LA COMPLEJIDAD O LA UTILIDAD DE CUALQUIER OPINIÓN, CONSEJO U OTRO CONTENIDO DISPONIBLE A TRAVÉS DEL SITIO, U OBTENIDO DE UN SITIO VINCULADO.\n                    <br>\n                    <strong>10. Limitación de responsabilidad</strong>\n\n                    10.1. Teniendo en cuenta que la utilización de los servicios implica hardware, software y acceso a Internet, así como equipos celulares compatibles con los servicios disponibles, su capacidad de usarlos puede ser afectada por el desempeño de estos factores. Claro Puerto Rico no es responsable por la calidad de servicio a la que el usuario se conecta para utilizar el Servicio.\n\n                    10.2. Cualquier daño o perjuicio que sufra el usuario por el uso de Mi Claro, cualquier pérdida que llegare a sufrir por tener acceso o por ejecutar operaciones a través de Mi Claro, es responsabilidad exclusiva del usuario y por lo tanto el usuario declara que mantendrá indemne y libre de toda responsabilidad a Claro Puerto Rico y sus compañías afiliadas.  Igualmente, el usuario releva de cualquier responsabilidad a Claro Puerto Rico y sus compañías afiliadas por los daños que llegare a sufrir el hardware y el software, o cualquier aparato electrónico a través del cual el usuario acceda a Mi Claro Puerto Rico. En ningún caso Claro Puerto Rico responsable por daños ni por las pérdidas o gastos que pudiesen surgir por conectarse a Mi Claro Puerto Rico o por el uso que se haga del mismo, o por la incapacidad para hacer uso del mismo por parte de cualquier persona, o por cualquier falla en la ejecución, error, omisión, interrupción, defecto, demora en la transmisión de la información o en la información transmitida o en la conexión, o por la existencia de un virus en la computadora o equipo celular o por fallas del sistema o en la línea, o por su suspensión o cancelación de su cuenta.\n\n                    10.3. BAJO NINGUNA CIRCUNSTANCIA CLARO PUERTO RICO SERÁ RESPONSABLE POR DAÑOS Y PERJUICIOS ESPECIALES, FORTUITOS, RESULTANTES O PUNITIVOS, INCLUYENDO HONORARIOS DE ABOGADOS, NI POR DAÑOS Y PERJUICIOS QUE SE DERIVEN DE LA PÉRDIDA DE USO, DATOS, BENEFICIOS O GANANCIAS, YA SEAN CONTRACTUALES, EXTRACONTRACTUALES, POR NEGLIGENCIA O POR CUALQUIER OTRA ACCIÓN TORTICERA, QUE SURJAN DE O QUE ESTÉN RELACIONADAS CON EL USO O EL NIVEL DE DESEMPEÑO DEL CONTENIDO, ESTE SERVIDOR O INTERNET EN GENERAL.\n\n                    10.4. El usuario  acuerda indemnizar y mantener a Claro Puerto Rico indemne frente a toda pérdida, costo, daño, gasto y reclamo derivados de sus reclamos o reclamos de terceros como resultado de: (i) su incumplimiento con estos Términos y Condiciones; (ii) el uso de Mi Claro; (iii) cualquier violación de la Ley aplicable por parte del usuario o de derechos de terceros; (iv) todo reclamo relacionado con el uso (por parte de (Claro Puerto Rico o Terceros) de la información provista por el usuario en este sitio  y los sitios vinculados.\n                    <br>\n                    <strong>11. Ley de Derechos de Autor del Milenio Digital</strong>\n\n                    11.1. En virtud del Capítulo II de la Ley de Derechos de Autor del Milenio Digital (Digital Millennium Copyright Acta o “DCMA”), todas las reclamaciones de infracción de derechos de autor por material que se crea está contenido en el sistema o la red de Claro, deben enviarse prontamente mediante comunicación escrita al Asesor Jurídico de Claro Puerto Rico, agente designado de Claro Puerto Rico conforme la DCMA a la siguiente direcciones de correo electrónico fsilva@claropr.com / jdeliz@claropr.com  o a la siguiente dirección postal: P.O. Box 70367, San Juan, PR 00936-8367.\n\n                    11.2. Todas las reclamaciones deben incluir la siguiente información:\n\n                    (a)\tUna firma física o electrónica de una persona autorizada a actuar en nombre del propietario de un derecho exclusivo que se alegue está siendo infringido;\n                    (b)\tIdentificación del trabajo u obra protegida por derechos de autor que se reclama haber sido infringido, o si múltiples trabajos protegidos por derechos de autor en un solo sitio de Internet están cubiertos por una sola notificación, una lista representativa de dichos trabajos en ese sitio Web;\n                    (c)\tIdentificación del material que se reclama haber sido infringido o ser objeto de una actividad de infracción y que debe eliminarse o cuyo acceso debe desactivarse, e información razonablemente suficiente para permitir que el proveedor de servicio localice dicho material;\n                    (d)\tInformación razonablemente suficiente para permitir que el proveedor de servicio se ponga en contacto con la Parte Reclamante, tal como una dirección, un número de teléfono y, si existe, una dirección de correo electrónico en la que se pueda contactar a la Parte Reclamante;\n                    (e)\tUna declaración de que la Parte Reclamante tiene la creencia de buena fe de que el uso del material en la manera denunciada no está autorizado por el propietario de los derechos de autor, su agente o la ley, y\n                    (f)\tUna declaración indicando que la información de la notificación es correcta y, bajo pena de perjurio que la Parte Reclamante está autorizada a actuar en nombre del propietario de un derecho exclusivo que se alega está siendo infringido.\n                    <br>\n                    <strong>12. Ley Aplicable</strong>\n\n                    12.1. Exceptuando las leyes aplicables del Gobierno Federales de los Estados Unidos, las disposiciones anteriores se regirán por y se interpretarán de acuerdo con las leyes primarias de Puerto Rico, sin tener en cuenta los principios de conflicto de derecho de este.\n\n                    12.2. El usuario de este sitio se somete voluntariamente a la jurisdicción y competencia de los tribunales federales y estatales del Estado Libre Asociado de Puerto Rico y renuncia a cualquier defensa de falta de jurisdicción o competencia. El usuario se compromete a que, de tener alguna reclamación en contra de Claro que surja por el uso de este sitio o a base de estos Términos y Condiciones, la misma deberá ser presentada en los tribunales estatales o federales de Puerto Rico.\n                    <br>\n                    <strong>13. Títulos.</strong>\n\n                    Los títulos utilizados en las secciones de estos Términos y Condiciones tienen el único propósito de facilitar su uso al hacer referencia a ellos y no forman parte del contrato ni afectarán en modo alguna la interpretación de estos.\n                    <br>\n                    <strong>14. Separabilidad.</strong>\n\n                    Si alguno de estos Términos y Condiciones fuera declarado nulo o invalido por un tribunal o agencia administrativa con jurisdicción y competencia para ello, tal declaración no afectará el resto de los Términos y Condiciones, los cuales se mantendrán firmes y obligatorias tanto para Claro Puerto Rico como para los usuarios de Mi Claro.\n                    <br>\n                    <strong>15. Acuerdo total.</strong>\n\n                    Estos Términos y Condiciones y los que se incorporen por referencia a los mismos, constituyen el acuerdo total y completo entre Claro Puerto Rico y el usuario por el uso de Mi Claro. Estos Términos y Condiciones reemplazan y/o revocan cualquier Términos y Condiciones con fecha precedente.\n                    <br>\n                    <strong>16. Política de Privacidad</strong>\n\n                    Para cualquier consulta referente a la Política de Privacidad de Claro Puerto Rico ingrese a\n                    <a (click)=\"openExternalBrowser('https://miclaro.claropr.com/politics/politicaPrivacidad/')\">https://miclaro.claropr.com/politics/</a>\n                    <br>\n                    <strong>17. Legal y Regulatorio</strong>\n\n                    Para cualquier consulta referente a concepto Legales y Regulatorios ingrese a\n                    <a (click)=\"openExternalBrowser('https://miclaro.claropr.com/politics/politicaPrivacidad/')\">https://www.claropr.com/legal-regulatorio/</a>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"basicrow text-center m-top-i lessm\">\n            <ion-button class=\"btns def-i red centr vcenter\"\n                        (click)=\"closeTerms()\"\n                        color=\"primary\"\n                        expand=\"full\">\n                Cerrar\n            </ion-button>\n        </div>\n    </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/popup/gift-received/gift-received.component.html":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/popup/gift-received/gift-received.component.html ***!
  \*********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"popupbg popup-1gb\">\n    <div class=\"reg-popcont\">\n        <div class=\"regredbar\">\n            <img width=\"100%\" src=\"assets/images/regala1gbbanv3.png\" alt=\"\">\n\n            <div class=\"regpopmsg vcenter f-white roboto-r\">\n                <div class=\"tabcell\">\n                    <span class=\"f-bigtitle roboto-b\">\n                        USTED\n                    </span>\n                    <br/>\n                    <span class=\"f-big\">\n                        HA RECIBIDO UN REGALO DE DATA\n                    </span>\n                </div>\n            </div>\n        </div>\n\n        <div class=\"basicrow allpads roboto-r f-black f-med\">\n            <div class=\"basicrow m-top-iv m-bott-i text-center\">\n                <div class=\"basicrow roboto-m f-big sender-name\">\n                    {{product.NameSender}}\n                </div>\n\n                <div class=\"basicrow f-bmed\">\n                    Le ha enviado un regalo de 1GB de Data de regalo.\n                </div>\n\n                <div class=\"basicrow m-top-ii\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow m-top-ii sender-message\">\n                    {{product.Message}}\n                </div>\n\n                <div class=\"basicrow m-top-ii\">\n                    <div class=\"btns red def-i centr vcenter rippleR\" id=\"close-1gb\" (click)=\"closePopup()\">\n                        <span class=\"tabcell\">\n                            OK\n                        </span>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dummy/dummy.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dummy/dummy.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top text-center\">\n                        <img width=\"100%\" class=\"difloglogo extra-margin\" src=\"assets/images/miclaro-logov2.png\" alt=\"\">\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/guest/guest.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/guest/guest.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n                <div class=\"shortcont\">\n\n                    <div class=\"basicrow m-top text-center\">\n                        <img width=\"100%\" class=\"difloglogo extra-margin\" src=\"assets/images/miclaro-logov2.png\" alt=\"\">\n                    </div>\n\n                    <div class=\"basicrow\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <div class=\"basicrow\" style=\"width: 100%; display: flex; align-content: center; justify-content: center; margin-bottom: -4px\">\n                                    <div class=\"basicrow\" style=\"width: 220px; text-align: right; font-size: 18px; font-weight: bold; color: #ef3829\">\n                                        Nuevo\n                                    </div>\n                                </div>\n                                <div class=\"basicrow\">\n                                    Acceso R&aacute;pido\n                                </div>\n                                <div class=\"basicrow f-med roboto-b\">\n                                    Ingresar como Usuario Invitado\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow f-reg f-black m-top-ii text-justify\">\n                        Ahora Mi Claro PR te permite que cada usuario asociado a tu cuenta pueda ingresar como invitado. El Acceso permite ver consumo, comprar paquetes adicionales y realizar pagos.\n                    </div>\n\n                    <div class=\"basicrow m-top-ii\">\n                        <div class=\"basicrow rel input-guest\">\n                            <img [popper]=\"'Ingresar como Invitado: Usuarios adicionales asociados a tu cuenta. Pueden ver consumo, pagar y comprar data adicional.'\"\n                                 [popperTrigger]=\"'click'\"\n                                 [popperTimeoutAfterShow]=\"6000\"\n                                 class=\"q-t-pay\" src=\"assets/images/tooltip1.png\" alt=\"\">\n                            <input type=\"number\"\n                                   onKeyPress=\"if(this.value.length==10) return false;\"\n                                   placeholder=\"N&uacute;mero de Tel&eacute;fono\"\n                                   class=\"inp-f\"\n                                   [(ngModel)]=\"number\"\n                                   (keyup.enter)=\"login()\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"btns red vcenter rippleR\" (click)=\"login()\">\n                            <span class=\"tabcell\">\n                                Continuar\n                            </span>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow text-center m-top-i\">\n                        <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                    </div>\n\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login/login.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login/login.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top text-center\">\n                        <img width=\"100%\" class=\"difloglogo extra-margin\" src=\"assets/images/miclaro-logov2.png\" alt=\"\">\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow rel\">\n                            <input placeholder=\"Correo o N&uacute;mero de Tel&eacute;fono\"\n                                   class=\"inp-f\"\n                                   autocapitalize=\"off\"\n                                   [(ngModel)]=\"username\"\n                                   (change)=\"identityEdited()\"\n                                   (keyup.enter)=\"login()\"\n                                   autocomplete=\"on\"\n                                   id=\"username\">\n                        </div>\n                    </div>\n                    <div class=\"basicrow m-top\">\n                        <form class=\"basicrow\">\n                            <input type=\"password\"\n                                   placeholder=\"Contrase&ntilde;a\"\n                                   class=\"inp-f\" autocapitalize=\"off\"\n                                   [(ngModel)]='password'\n                                   (change)=\"identityEdited()\"\n                                   (keyup.enter)=\"login()\"\n                                   [ngModelOptions]=\"{standalone: true}\"\n                                   autocomplete=\"off\"\n                                   id=\"password\">\n                        </form>\n                    </div>\n\n                    <div class=\"basicrow m-top-ii\">\n                        <div class=\"basicrow f-reg f-black text-justify\" style=\"width: auto\" *ngIf=\"showKeepAuthenticate\">\n                            <input id=\"remember\" type=\"checkbox\" [(ngModel)]=\"keepAuthActivated\" class=\"css-checkbox check-login\" (change)=\"onKeepAuthenticated()\"/>\n                            <label class=\"css-label radGroup1\" for=\"remember\">&nbsp;Mantener Autenticado</label>\n                        </div>\n\n                        <div class=\"container-touch\" *ngIf=\"biometricAvailable && showBiometricLogin\">\n                            <input id=\"touch\" type=\"checkbox\" name=\"touch\" class=\"css-checkbox check-login\" [(ngModel)]=\"biometricActivated\" (change)=\"onMarkBiometric()\"/>\n                            <label *ngIf=\"isTouchBiometric\" for=\"touch\" class=\"label-touch\">&nbsp;{{ ios ? 'Touch ID' : 'Fingerprint' }}&nbsp;</label>\n                            <label *ngIf=\"!isTouchBiometric\" for=\"touch\" class=\"label-touch\">&nbsp;Face ID&nbsp;</label>\n                            <img *ngIf=\"isTouchBiometric\" width=\"100%\" class=\"img-touch\" src=\"assets/images/touchrecog.png\">\n                            <img *ngIf=\"!isTouchBiometric\" width=\"100%\" class=\"img-touch\" src=\"assets/images/facerecog.png\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"btns red vcenter rippleR\" (click)=\"login()\">\n                            <div class=\"tabcell\">\n                                Iniciar Sesi&oacute;n\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow text-center m-top-ii\">\n                        <a class=\"linkdefs\" (click)=\"goPage('recover/step1')\">\n                            &#191;Olvidaste tu contrase&ntilde;a&#63;\n                        </a>\n                    </div>\n\n                    <div class=\"basicrow m-top-u-ii\">\n                        <div class=\"btns blue vcenter rippleB\" (click)=\"goPage('register/step1')\">\n                            <div class=\"tabcell\">\n                                &#191;No tienes cuenta&#63; Reg&iacute;strate\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"btns red vcenter rippleR\" (click)=\"goPage('guest')\">\n                            <div class=\"tabcell\">\n                                Ingresar como Invitado\n                            </div>\n                        </div>\n                    </div>\n\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/touch/touch.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/touch/touch.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\" style=\"overflow-y: auto\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow m-top mbott\">\n\n        <div class=\"container\">\n\n            <div *ngIf=\"biometricIsTouch\">\n                <div class=\"basicrow f-big roboto-r text-center\">\n                    <div class=\"basicrow\">\n                        Configuraci&oacute;n {{ ios ? 'Touch ID' : 'Fingerprint' }}\n                    </div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <div class=\"basicrow\">\n                        <img style=\"width: 100px; height: 100px\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/touch-cap.png\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i f-med roboto-b text-center\">\n                    <label>Ahora iniciar sesi&oacute;n es mas facil</label>\n                </div>\n\n                <div class=\"basicrow m-top f-mini-i roboto-m text-justify\">\n                    Utiliza el {{ ios ? 'Touch ID' : 'Fingerprint' }} de tu dispositivo para iniciar sesi&oacute;n en Mi Claro App, solo tienes que configurar y registrar tu Huella Digital con unos simples pasos.\n                </div>\n\n                <div class=\"basicrow m-top roboto-b text-left\">\n                    Nota:\n                </div>\n\n                <div class=\"basicrow f-mini roboto-m text-left m-top-u-ii\">\n                    • Las huellas dactilares almacenadas en el dispositivo podr&aacute;n ser utilizadas para acceder a la aplicaci&oacute;n Mi Claro.\n                </div>\n\n                <div class=\"basicrow f-mini roboto-m text-left\">\n                    • S&oacute;lo se puede configurar {{ ios ? 'Touch ID' : 'Fingerprint' }} para un ID en l&iacute;nea a la vez.\n                </div>\n            </div>\n\n            <div *ngIf=\"!biometricIsTouch\">\n                <div class=\"basicrow f-big roboto-r text-center\">\n                    <div class=\"basicrow\">\n                        Configuraci&oacute;n Face ID\n                    </div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <div class=\"basicrow\">\n                        <img style=\"width: 100px; height: 100px\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/face-cap.png\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i f-med roboto-b text-center\">\n                    <label>Ahora iniciar sesi&oacute;n es mas facil</label>\n                </div>\n\n                <div class=\"basicrow m-top f-mini-i text-justify\">\n                    Utiliza el Face ID de tu dispositivo para iniciar sesi&oacute;n en Mi Claro App, solo tienes que configurar y registrar tu rostro con unos simples pasos.\n                </div>\n\n                <div class=\"basicrow m-top-i roboto-b text-left\">\n                    Nota:\n                </div>\n\n                <div class=\"basicrow m-top-u-ii f-mini roboto-m text-left\">\n                    • Los reconocimientos faciales almacenadas en el dispositivo podr&aacute;n ser utilizadas para acceder a la aplicaci&oacute;n Mi Claro.\n                </div>\n\n                <div class=\"basicrow f-mini roboto-m text-left\">\n                    • S&oacute;lo se puede configurar Face ID para un ID en l&iacute;nea a la vez.\n                </div>\n            </div>\n\n            <div class=\"basicrow m-top-i m-bott-i\">\n                <div class=\"logline full\"></div>\n            </div>\n\n            <div class=\"row\">\n                <div class=\"container\">\n                    <div class=\"basicrow text-center roboto-b f-black\">\n                        Configurar: <span class=\"f-red\">{{username}}</span>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"basicrow m-top\">\n                <div class=\"logline full\"></div>\n            </div>\n\n            <div class=\"basicrow m-top-i\">\n\n                <ion-button (click)=\"configure()\"\n                            color=\"primary\"\n                            expand=\"full\"\n                            class=\"btns red vcenter\">\n                    Configurar\n                </ion-button>\n\n                <ion-button (click)=\"cancel()\"\n                            class=\"btns vcenter m-top\" color=\"gray\" expand=\"full\">\n                    Cancelar\n                </ion-button>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update-app/update-app.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update-app/update-app.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow m-top-i mbott text-center\">\n\n                <div class=\"basicrow m-top-i\">\n\n                    <div class=\"basicrow text-center\">\n                        <img class=\"updateicon\" width=\"100%\" src=\"assets/images/update_access.png\">\n                    </div>\n\n                    <div class=\"basicrow f-bmed roboto-b m-top-i\">\n                        Estimado cliente,\n                    </div>\n\n                    <div class=\"basicrow f-med f-black roboto-r m-top-i text-justify\">\n                        Mi Claro se ha actualizado y esta versi&oacute;n ya no se podr&aacute; utilizar.<br/><br/>\n                        Descarga la nueva versi&oacute;n y actualiza tu experiencia m&oacute;vil ahora mismo.\n                    </div>\n\n                </div>\n\n                <div class=\"basicrow m-top-i text-center\">\n                    <ion-button color=\"primary\" expand=\"full\" (click)=\"openAppStore()\">\n                        Actualizar App\n                    </ion-button>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n\n");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __createBinding, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__createBinding", function() { return __createBinding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function() { return __classPrivateFieldGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function() { return __classPrivateFieldSet; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __createBinding(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}

function __exportStar(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}

function __classPrivateFieldGet(receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
}

function __classPrivateFieldSet(receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: routes, AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _pages_login_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/login/login/login.component */ "./src/app/pages/login/login/login.component.ts");
/* harmony import */ var _pages_login_guest_guest_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/login/guest/guest.component */ "./src/app/pages/login/guest/guest.component.ts");
/* harmony import */ var _pages_dummy_dummy_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/dummy/dummy.component */ "./src/app/pages/dummy/dummy.component.ts");
/* harmony import */ var _pages_login_touch_touch_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/login/touch/touch.component */ "./src/app/pages/login/touch/touch.component.ts");
/* harmony import */ var _pages_update_app_update_app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages/update-app/update-app.component */ "./src/app/pages/update-app/update-app.component.ts");








const routes = [
    { path: 'login', component: _pages_login_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'update-app', component: _pages_update_app_update_app_component__WEBPACK_IMPORTED_MODULE_7__["UpdateAppComponent"] },
    { path: 'guest', component: _pages_login_guest_guest_component__WEBPACK_IMPORTED_MODULE_4__["GuestComponent"] },
    { path: 'touch', component: _pages_login_touch_touch_component__WEBPACK_IMPORTED_MODULE_6__["TouchComponent"] },
    { path: 'dummy', component: _pages_dummy_dummy_component__WEBPACK_IMPORTED_MODULE_5__["DummyComponent"] },
    { path: 'register', loadChildren: './pages/register/register.module#RegisterModule' },
    { path: 'recover', loadChildren: './pages/recover/recover.module#RecoverModule' },
    { path: 'update', loadChildren: './pages/update/update.module#UpdateModule' },
    { path: 'home', loadChildren: './pages/home/home.module#HomeModule' },
    { path: 'help', loadChildren: './pages/help/help.module#HelpModule' },
    { path: 'module', loadChildren: './pages/module/module.module#ModuleModule' },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, {
                scrollPositionRestoration: 'enabled'
            })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        providers: []
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _pages_base_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _ionic_native_keychain_touch_id_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/keychain-touch-id/ngx */ "./node_modules/@ionic-native/keychain-touch-id/ngx/index.js");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/firebase-x/ngx */ "./node_modules/@ionic-native/firebase-x/ngx/index.js");
/* harmony import */ var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/app-version/ngx */ "./node_modules/@ionic-native/app-version/ngx/index.js");
/* harmony import */ var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/network/ngx */ "./node_modules/@ionic-native/network/ngx/index.js");
/* harmony import */ var _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ionic-native/keyboard/ngx */ "./node_modules/@ionic-native/keyboard/ngx/index.js");
/* harmony import */ var _services_redirect_provider__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./services/redirect.provider */ "./src/app/services/redirect.provider.ts");
/* harmony import */ var _services_storage_provider__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./services/storage.provider */ "./src/app/services/storage.provider.ts");
/* harmony import */ var _utils_const_pages__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./utils/const/pages */ "./src/app/utils/const/pages.ts");






















let AppComponent = class AppComponent extends _pages_base_page__WEBPACK_IMPORTED_MODULE_8__["BasePage"] {
    constructor(redirectProvider, storageProvider, loader, platform, splashScreen, statusBar, biometric, firebase, appVersion, network, keyboard, router, storage, modelsServices, alertController, utilsService, userStorage, ngZone) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.redirectProvider = redirectProvider;
        this.storageProvider = storageProvider;
        this.loader = loader;
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.biometric = biometric;
        this.firebase = firebase;
        this.appVersion = appVersion;
        this.network = network;
        this.keyboard = keyboard;
        this.ngZone = ngZone;
        this.TIME_IN_MS = 1000 * 60 * 10;
        this.initializeApp();
    }
    initializeApp() {
        const self = this;
        this.appReset();
        this.goPage(_utils_const_pages__WEBPACK_IMPORTED_MODULE_20__["pages"].DUMMY); // To reset current page
        this.platform.ready().then(() => {
            self.splashScreen.show();
            setTimeout(() => self.loadBiometricOptions(), 5000);
        });
        this.platform.backButton.subscribeWithPriority(0, () => {
            this.goBack();
        });
        this.bindEvents();
        this.utils.resetTimer.subscribe(() => {
            this.resetTimer();
        });
        this.activeActionsFromRedirect();
    }
    activeActionsFromRedirect() {
        window.handleOpenURL = (url) => {
            setTimeout(() => {
                this.ngZone.run(() => {
                    this.redirectProvider.redirect(url);
                });
            }, 0);
        };
    }
    loadBiometricOptions() {
        if (!_utils_utils__WEBPACK_IMPORTED_MODULE_13__["Utils"].getPlatformInfo().desktop) {
            this.biometric.isAvailable()
                .then((res) => {
                console.log('biometric is available: ' + res);
                this.utils.biometricOptions.available = true;
                if (_utils_utils__WEBPACK_IMPORTED_MODULE_13__["Utils"].getPlatformInfo().ios) {
                    this.utils.biometricOptions.type = res === 'face' ? 'face' : 'touch';
                }
                else {
                    this.utils.biometricOptions.type = 'touch';
                }
            })
                .catch((error) => {
                console.error('biometric is unavailable: ' + error);
                this.utils.biometricOptions.available = false;
            })
                .finally(() => {
                this.loadModules().then();
            });
        }
        else {
            this.loadModules().then();
        }
    }
    loadModules() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.loader.load('./pages/register/register.module#RegisterModule');
            yield this.loader.load('./pages/recover/recover.module#RecoverModule');
            yield this.loader.load('./pages/update/update.module#UpdateModule');
            yield this.loader.load('./pages/home/home.module#HomeModule');
            yield this.loader.load('./pages/help/help.module#HelpModule');
            yield this.loader.load('./pages/module/module.module#ModuleModule');
            yield this.loadVersion();
        });
    }
    loadVersion() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            // direct load app if testing
            if (_utils_utils__WEBPACK_IMPORTED_MODULE_13__["Utils"].getPlatformInfo().desktop) {
                this.loadApp();
                return;
            }
            if (this.network.type === this.network.Connection.NONE || this.network.type === this.network.Connection.UNKNOWN) {
                this.statusBar.styleLightContent();
                this.splashScreen.hide();
                this.showConfirmCustom('Sin conexión', 'Por favor compruebe su conexión a internet.', 'Volver a Intentar', 'Salir', () => {
                    location.reload();
                }, () => {
                    navigator['app'].exitApp();
                });
            }
            else {
                const so = _utils_utils__WEBPACK_IMPORTED_MODULE_13__["Utils"].getPlatformInfo().ios ? 'IOS' : 'ANDROID';
                const version = yield this.appVersion.getVersionNumber();
                this.services.checkAppVersion(so, version).then((response) => {
                    if (response && response.object && response.object.enabled === 'Y') {
                        this.loadApp();
                    }
                    else {
                        this.goUpdate();
                    }
                }).catch(error => {
                    if (error.withStatus200 === true) {
                        this.goUpdate();
                    }
                    else {
                        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_7__["keys"].APP.OUTDATED_APP).then(value => {
                            if (value) {
                                this.goUpdate();
                            }
                            else {
                                this.loadApp();
                            }
                        });
                    }
                });
            }
        });
    }
    goUpdate() {
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_7__["keys"].APP.OUTDATED_APP, true);
        this.goPage(_utils_const_pages__WEBPACK_IMPORTED_MODULE_20__["pages"].UPDATE_APP);
        this.statusBar.styleLightContent();
        this.splashScreen.hide();
    }
    loadApp() {
        this.cacheStorage().isAppReady = true;
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_7__["keys"].APP.OUTDATED_APP, false);
        this.getTokenNotification();
        this.statusBar.styleLightContent();
        this.splashScreen.hide();
        if (_utils_utils__WEBPACK_IMPORTED_MODULE_13__["Utils"].getPlatformInfo().ios) {
            this.keyboard.hideFormAccessoryBar(false);
        }
        // if (this.cacheStorage().redirectData) {
        //     this.redirectProvider.redirect(this.cacheStorage().redirectData.url);
        // } else {
        this.openLogin();
        // }
    }
    openLogin() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const isLogged = yield this.storageProvider.isLogged();
            const isGuest = yield this.storageProvider.isGuest();
            this.goLoginPage(isLogged && isGuest);
        });
    }
    bindEvents() {
        addEventListener('mousemove', this.resetTimer.bind(this), false);
        addEventListener('mousedown', this.resetTimer.bind(this), false);
        addEventListener('keypress', this.resetTimer.bind(this), false);
        addEventListener('DOMMouseScroll', this.resetTimer.bind(this), false);
        addEventListener('mousewheel', this.resetTimer.bind(this), false);
        addEventListener('touchmove', this.resetTimer.bind(this), false);
        addEventListener('MSPointerMove', this.resetTimer.bind(this), false);
    }
    startTimer() {
        this.timer = setTimeout(this.evaluateSession.bind(this), this.TIME_IN_MS);
    }
    evaluateSession() {
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_7__["keys"].LOGIN.IS_LOGGED).then(isLogged => {
            if (isLogged && this.cacheStorage().logged) {
                const keep = this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_7__["keys"].LOGIN.KEEP);
                if (keep) {
                    console.warn('automatic re-login full');
                    location.reload();
                }
                else {
                    clearTimeout(this.timer);
                    this.showAlertSessionExpired();
                }
            }
            else {
                this.resetTimer();
            }
        });
    }
    resetTimer() {
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.startTimer();
    }
    getTokenNotification() {
        // TODO, here should subscribe to notifications
    }
};
AppComponent.ctorParameters = () => [
    { type: _services_redirect_provider__WEBPACK_IMPORTED_MODULE_18__["RedirectProvider"] },
    { type: _services_storage_provider__WEBPACK_IMPORTED_MODULE_19__["StorageProvider"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["SystemJsNgModuleLoader"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"] },
    { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"] },
    { type: _ionic_native_keychain_touch_id_ngx__WEBPACK_IMPORTED_MODULE_12__["KeychainTouchId"] },
    { type: _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_14__["FirebaseX"] },
    { type: _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_15__["AppVersion"] },
    { type: _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_16__["Network"] },
    { type: _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_17__["Keyboard"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_10__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_11__["IntentProvider"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        providers: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["SystemJsNgModuleLoader"]],
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_redirect_provider__WEBPACK_IMPORTED_MODULE_18__["RedirectProvider"],
        _services_storage_provider__WEBPACK_IMPORTED_MODULE_19__["StorageProvider"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["SystemJsNgModuleLoader"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"],
        _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"],
        _ionic_native_keychain_touch_id_ngx__WEBPACK_IMPORTED_MODULE_12__["KeychainTouchId"],
        _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_14__["FirebaseX"],
        _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_15__["AppVersion"],
        _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_16__["Network"],
        _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_17__["Keyboard"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["Storage"],
        _services_services_provider__WEBPACK_IMPORTED_MODULE_10__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        _services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_11__["IntentProvider"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]])
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/device/ngx */ "./node_modules/@ionic-native/device/ngx/index.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/launch-navigator/ngx */ "./node_modules/@ionic-native/launch-navigator/ngx/index.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _utils_helpers_response_interceptor__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./utils/_helpers/response.interceptor */ "./src/app/utils/_helpers/response.interceptor.ts");
/* harmony import */ var _pages_shared_shared_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./pages/_shared/shared.module */ "./src/app/pages/_shared/shared.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _pages_login_guest_guest_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./pages/login/guest/guest.component */ "./src/app/pages/login/guest/guest.component.ts");
/* harmony import */ var _pages_login_login_login_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./pages/login/login/login.component */ "./src/app/pages/login/login/login.component.ts");
/* harmony import */ var ngx_popper__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ngx-popper */ "./node_modules/ngx-popper/fesm2015/ngx-popper.js");
/* harmony import */ var _pages_dummy_dummy_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./pages/dummy/dummy.component */ "./src/app/pages/dummy/dummy.component.ts");
/* harmony import */ var ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ngx-swiper-wrapper */ "./node_modules/ngx-swiper-wrapper/dist/ngx-swiper-wrapper.es5.js");
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ "./node_modules/@ionic-native/social-sharing/ngx/index.js");
/* harmony import */ var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @ionic-native/app-version/ngx */ "./node_modules/@ionic-native/app-version/ngx/index.js");
/* harmony import */ var _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @ionic-native/firebase-x/ngx */ "./node_modules/@ionic-native/firebase-x/ngx/index.js");
/* harmony import */ var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @ionic-native/network/ngx */ "./node_modules/@ionic-native/network/ngx/index.js");
/* harmony import */ var _ionic_native_keychain_touch_id_ngx__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @ionic-native/keychain-touch-id/ngx */ "./node_modules/@ionic-native/keychain-touch-id/ngx/index.js");
/* harmony import */ var _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @ionic-native/keyboard/ngx */ "./node_modules/@ionic-native/keyboard/ngx/index.js");
/* harmony import */ var _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @ionic-native/clipboard/ngx */ "./node_modules/@ionic-native/clipboard/ngx/index.js");
/* harmony import */ var _pages_login_touch_touch_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./pages/login/touch/touch.component */ "./src/app/pages/login/touch/touch.component.ts");
/* harmony import */ var _ionic_native_app_preferences_ngx__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @ionic-native/app-preferences/ngx */ "./node_modules/@ionic-native/app-preferences/ngx/index.js");
/* harmony import */ var _pages_update_app_update_app_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./pages/update-app/update-app.component */ "./src/app/pages/update-app/update-app.component.ts");
/* harmony import */ var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @ionic-native/file-opener/ngx */ "./node_modules/@ionic-native/file-opener/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");









































const DEFAULT_SWIPER_CONFIG = {
    direction: 'horizontal',
    slidesPerView: 'auto'
};
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        entryComponents: [],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"].forRoot(),
            _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["IonicStorageModule"].forRoot(),
            _angular_forms__WEBPACK_IMPORTED_MODULE_19__["FormsModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_12__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_16__["HttpClientModule"],
            _pages_shared_shared_module__WEBPACK_IMPORTED_MODULE_18__["SharedModule"],
            ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_26__["SwiperModule"],
            ngx_popper__WEBPACK_IMPORTED_MODULE_24__["NgxPopperModule"].forRoot({ placement: 'top', styles: { 'background-color': 'white' } })
        ],
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_13__["AppComponent"],
            _pages_login_login_login_component__WEBPACK_IMPORTED_MODULE_23__["LoginComponent"],
            _pages_login_guest_guest_component__WEBPACK_IMPORTED_MODULE_22__["GuestComponent"],
            _pages_dummy_dummy_component__WEBPACK_IMPORTED_MODULE_25__["DummyComponent"],
            _pages_login_touch_touch_component__WEBPACK_IMPORTED_MODULE_34__["TouchComponent"],
            _pages_update_app_update_app_component__WEBPACK_IMPORTED_MODULE_36__["UpdateAppComponent"]
        ],
        exports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]
        ],
        providers: [
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_7__["StatusBar"],
            _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_6__["SplashScreen"],
            _services_services_provider__WEBPACK_IMPORTED_MODULE_14__["ServicesProvider"],
            _services_utils_service__WEBPACK_IMPORTED_MODULE_15__["UtilsService"],
            _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_8__["Device"],
            _utils_utils__WEBPACK_IMPORTED_MODULE_20__["Utils"],
            _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_9__["GoogleMaps"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"],
            _services_intent_provider__WEBPACK_IMPORTED_MODULE_21__["IntentProvider"],
            _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_11__["LaunchNavigator"],
            _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_27__["SocialSharing"],
            _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_28__["AppVersion"],
            _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_29__["FirebaseX"],
            _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_30__["Network"],
            _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_37__["FileOpener"],
            _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_38__["File"],
            _ionic_native_keychain_touch_id_ngx__WEBPACK_IMPORTED_MODULE_31__["KeychainTouchId"],
            _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_32__["Keyboard"],
            _ionic_native_clipboard_ngx__WEBPACK_IMPORTED_MODULE_33__["Clipboard"],
            _ionic_native_app_preferences_ngx__WEBPACK_IMPORTED_MODULE_35__["AppPreferences"],
            _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_39__["InAppBrowser"],
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicRouteStrategy"] },
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_16__["HTTP_INTERCEPTORS"], useClass: _utils_helpers_response_interceptor__WEBPACK_IMPORTED_MODULE_17__["ResponseInterceptor"], multi: true },
            { provide: ngx_swiper_wrapper__WEBPACK_IMPORTED_MODULE_26__["SWIPER_CONFIG"], useValue: DEFAULT_SWIPER_CONFIG }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_13__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/models/access.filter.ts":
/*!*****************************************!*\
  !*** ./src/app/models/access.filter.ts ***!
  \*****************************************/
/*! exports provided: AccessFilter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccessFilter", function() { return AccessFilter; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/utils */ "./src/app/utils/utils.ts");


class AccessFilter {
    static isRouteActive(id, currentUrl) {
        let isActive = false;
        if (currentUrl.includes('/module/')) {
            currentUrl = currentUrl.replace('/module/', '');
        }
        AccessFilter.routesToGo.forEach(route => {
            if (route.path !== '' && currentUrl === route.path && id === route.accessID) {
                isActive = true;
            }
        });
        return isActive;
    }
    static do(value, isGuest, accountType, accountSubType, productType) {
        value.push(value.shift());
        const newList = [];
        /**
         * si es business no chequea los accesos y le coloca solo cerrar session
         */
        if (!_utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].isBusinessAccount(accountType)) {
            value.forEach((section, index) => {
                const newPages = [];
                section.index = index;
                if (section.sectionName === 'MENU') {
                    section.sectionName = 'MI CUENTA';
                }
                // section.sectionName = AccessFilter.capitalize(section.sectionName);
                section.Pages.forEach((page) => {
                    // page.pageName = AccessFilter.capitalize(page.pageName);
                    if (!page.allowAsGuest && isGuest) {
                        page.extraClass = 'offnouse';
                    }
                    else {
                        page.extraClass = '';
                    }
                    if (page.accessID !== 11 // TODO, modulo detalles de facturar
                        && page.accessID !== 15 // TODO, modulo detalles de llamadas
                        && page.accessID !== 22 // TODO, modulo SVA (Compra de servicios de Valor Agregado)
                        && !(page.accessID === 24 && _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].isTelephony(accountType, accountSubType, productType)) // TODO, modulo regala 1 gb en fijo
                        && !(page.accessID === 25 && _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].isTelephony(accountType, accountSubType, productType)) // TODO, modulo regala 1 recarga en fijo
                        && page.accessID !== 27 // TODO, modulo de historico de recargas (prepago)
                        && page.accessID !== 29 // TODO, modulo de transaferencias (prepago)
                        && !(page.accessID === 19 && _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].isPrepaid(accountType, accountSubType)) // TODO, modulo de referidos en prepago
                        && !(page.accessID === 31 && _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].isPrepaid(accountType, accountSubType)) // TODO, modulo de referidos en prepago
                        && !(page.accessID === 37 && _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].isPrepaid(accountType, accountSubType)) // TODO, modulo de referidos en prepago
                        && !(page.accessID === 18 && _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].getPlatformInfo().ios) // TODO, modulo de netflix en IOS
                        && !(page.accessID === 36 && _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].getPlatformInfo().ios) // TODO, modulo de netflix en IOS
                    ) {
                        newPages.push(page);
                    }
                });
                if (section.sectionName.toLowerCase() === 'MI CONSUMO'.toLowerCase()) {
                    if (_utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].isPrepaid(accountType, accountSubType)) {
                        newList.push({
                            sectionName: 'FACTURA Y PAGO',
                            Pages: [
                                {
                                    userID: 0,
                                    pageName: 'RESUMEN DE CUENTA',
                                    allowAsGuest: false,
                                    accessID: 77
                                }
                            ]
                        });
                    }
                }
                if ((section.sectionName.toLowerCase() === 'MIS EQUIPOS Y SERVICIOS'.toLowerCase() ||
                    section.sectionName.toLowerCase() === 'MIS SERVICIOS'.toLowerCase()) &&
                    !_utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].isPrepaid(accountType, accountSubType)) { // TODO, if account is postpaid or telephony it will add Claro Club on Services section
                    newPages.push({
                        userID: 0,
                        pageName: 'CLARO CLUB',
                        allowAsGuest: true,
                        accessID: 321
                    });
                }
                if (section.sectionName.toLowerCase() === 'MI CUENTA'.toLowerCase() &&
                    _utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].isPrepaid(accountType, accountSubType)) { // TODO, if account is prepaid should add Claro Club before Mi Cuenta section
                    newList.push({
                        sectionName: 'CLARO CLUB',
                        Pages: [
                            {
                                userID: 0,
                                pageName: 'CLARO CLUB',
                                allowAsGuest: true,
                                accessID: 321
                            }
                        ]
                    });
                }
                if (newPages.length > 0) {
                    section.Pages = newPages;
                    newList.push(section);
                }
            });
        }
        if (newList.length === 0) {
            const section = {
                sectionName: 'MENU',
                Pages: [
                    {
                        userID: 0,
                        pageName: 'CERRAR SESION',
                        allowAsGuest: false,
                        accessID: 8
                    }
                ]
            };
            newList.push(section);
        }
        return newList;
    }
    static capitalize(str) {
        return str.replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
    }
}
AccessFilter.routesToGo = [
    { pageName: 'TUS NOTIFICACIONES', accessID: 1, path: 'notifications', needSubscribers: false },
    { pageName: 'MIS CUENTAS', accessID: 3, path: 'add-account', needSubscribers: true },
    { pageName: 'MI PERFIL', accessID: 2, path: 'profile', needSubscribers: false },
    { pageName: 'CAMBIAR CONTRASENA', accessID: 4, path: 'profile/password', needSubscribers: false },
    { pageName: 'ACTUALIZA TU CORREO ELECTRONICO', accessID: 5, path: 'profile/email', needSubscribers: false },
    { pageName: 'PREFERENCIA DE NOTIFICACIONES', accessID: 6, path: 'notifications-preference', needSubscribers: false },
    { pageName: 'SOPORTE', accessID: 7, path: 'support', needSubscribers: false },
    { pageName: 'CERRAR SESION', accessID: 8, path: '', needSubscribers: false },
    { pageName: 'RESUMEN DE CUENTA', accessID: 77, path: 'prepaid-summary', needSubscribers: false },
    { pageName: 'RESUMEN DE FACTURA', accessID: 9, path: 'invoice-summary', needSubscribers: false },
    { pageName: 'DESCARGA TU FACTURA', accessID: 10, path: 'invoice-download', needSubscribers: false },
    { pageName: 'DETALLE DE FACTURA', accessID: 11, path: 'invoice-detail', needSubscribers: false },
    { pageName: 'HISTORIAL DE PAGOS', accessID: 12, path: 'payment-history', needSubscribers: false },
    { pageName: 'FACTURA ELECTRONICA', accessID: 13, path: 'electronic-bill', needSubscribers: false },
    { pageName: 'DEBITO DIRECTO', accessID: 32, path: 'direct-debit', needSubscribers: false },
    { pageName: 'DETALLE DE CONSUMOS', accessID: 14, path: 'consumption', needSubscribers: true },
    { pageName: 'MI CONSUMOS', accessID: 28, path: 'consumption/prepaid', needSubscribers: true },
    { pageName: 'DETALLE DE LLAMADAS', accessID: 15, path: 'call-detail', needSubscribers: true },
    { pageName: 'DETALLE DE TU EQUIPO Y PLAN', accessID: 16, path: 'device', needSubscribers: true },
    { pageName: 'DETALLE DE TU PLAN', accessID: 34, path: 'device', needSubscribers: true },
    { pageName: 'CAMBIO DE PLAN', accessID: 17, path: 'change-plan', needSubscribers: true },
    { pageName: 'CAMBIO DE PLAN', accessID: 35, path: 'change-plan', needSubscribers: true },
    { pageName: 'COMPRA DE DATA ADICIONAL', accessID: 21, path: 'data-plan', needSubscribers: false },
    { pageName: 'NETFLIX', accessID: 18, path: 'netflix', needSubscribers: false },
    { pageName: 'NETFLIX', accessID: 36, path: 'netflix', needSubscribers: false },
    { pageName: 'PROGRAMA DE REFERIDOS', accessID: 19, path: 'refer/home', needSubscribers: false },
    { pageName: 'PROGRAMA DE REFERIDOS', accessID: 31, path: 'refer/home', needSubscribers: false },
    { pageName: 'PROGRAMA DE REFERIDOS', accessID: 37, path: 'refer/home', needSubscribers: false },
    { pageName: 'CLARO CLUB', accessID: 321, path: 'club/home', needSubscribers: false },
    { pageName: 'TIENDA', accessID: 20, path: '', needSubscribers: false },
    { pageName: 'COMPRAS', accessID: 38, path: 'purchases', needSubscribers: false },
    { pageName: 'COMPRA DE SERVICIOS DE VALOR AGREGADO', accessID: 22, path: '', needSubscribers: false },
    { pageName: 'HISTORIAL DE COMPRAS', accessID: 23, path: 'shopping-history', needSubscribers: false },
    { pageName: 'REGALA 1GB', accessID: 24, path: 'gift/one-gb', needSubscribers: false },
    { pageName: 'REGALA 1 RECARGA', accessID: 25, path: 'gift/recharge', needSubscribers: false },
    { pageName: 'RECARGA', accessID: 26, path: 'recharge', needSubscribers: false },
    { pageName: 'TRANSACCIONES', accessID: 30, path: 'recharge/history', needSubscribers: false },
    { pageName: 'REPORTE DE AVERIA', accessID: 33, path: 'fault/step1', needSubscribers: false }
];


/***/ }),

/***/ "./src/app/pages/_shared/components/account-select/account-select.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/_shared/components/account-select/account-select.component.scss ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mySideNav {\n  height: auto;\n  /* 100% Full-height */\n  width: 100%;\n  /* 0 width - change this with JavaScript */\n  position: absolute;\n  /* Stay in place */\n  z-index: 10000;\n  /* Stay on top */\n  top: 84px;\n  right: 100%;\n  overflow-x: auto;\n  /* Disable horizontal scroll */\n  transition: 0.4s;\n  /* 0.5 second transition effect to slide in the sidenav */\n  -webkit-transition: 0.4s;\n  -moz-transition: 0.4s;\n  -o-transition: 0.4s;\n  overflow-y: hidden;\n  border-bottom: 1px solid #dddddd;\n}\n\n.mySideNav {\n  margin-top: -1px;\n}\n\n.mySideNav {\n  top: 50px;\n}\n\n@media only screen and (orientation: landscape) {\n  .mySideNav {\n    top: 49px;\n    /* fixed header for ios version */\n  }\n}\n\n/* iphone X and iphone Xs*/\n\n@media only screen and (min-device-width: 375px) and (max-device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone Xr TEST*/\n\n@media only screen and (min-device-width: 375px) and (max-device-height: 812px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone Xr */\n\n@media only screen and (min-device-width: 414px) and (max-device-height: 896px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone Xs Max */\n\n@media only screen and (min-device-width: 414px) and (max-device-height: 896px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n.and {\n  margin-top: -35px !important;\n}\n\n.theRed {\n  color: #ef3829 !important;\n}\n\n.theWhite {\n  color: #fcf7f7 !important;\n}\n\n.menuIsOpen {\n  right: 0;\n}\n\n.menuIsClosed {\n  right: 100%;\n}\n\n.m-menuin,\n.m-menubar {\n  height: 50px;\n  width: 100%;\n  border-top: 1px solid #ffffff;\n  float: left;\n  position: relative;\n  font-size: 19.2px;\n  color: #ffffff;\n  padding: 0 20px;\n  font-family: \"Roboto\", sans-serif;\n  font-weight: 400;\n  text-decoration: none;\n  background-color: #ef3829;\n}\n\n.m-menuin {\n  color: #222222;\n  background-color: #ffffff;\n  font-size: 14px;\n  border-top: 1px solid #ffffff;\n  border-bottom: 2px solid #dddddd;\n}\n\n.m-menuin.lastlink {\n  border: 0;\n}\n\na.m-menuin {\n  color: #222222;\n  text-decoration: none;\n}\n\na.m-menubar {\n  color: #ffffff;\n  text-decoration: none;\n}\n\n.m-menuin,\n.m-menubar {\n  padding-right: 40px;\n}\n\n.m-menubar i {\n  font-size: 20px;\n  line-height: 20px;\n  position: absolute;\n  height: 20px;\n  width: 20px;\n  top: 0;\n  bottom: 0;\n  right: 7px;\n  color: #ffffff;\n  margin: auto;\n}\n\n.m-menuin i {\n  font-size: 16px;\n  line-height: 16px;\n  position: absolute;\n  height: 16px;\n  width: 16px;\n  top: 0;\n  bottom: 0;\n  right: 7px;\n  color: #222222;\n  margin: auto;\n}\n\n.m-menubar i.fa-minus,\n.m-menubar i.fa-plus {\n  right: 14px;\n}\n\n.m-menuin i.fa-times-circle {\n  font-size: 18px;\n  line-height: 18px;\n  position: absolute;\n  height: 18px;\n  width: 18px;\n  right: 11px;\n  color: #b7202e;\n}\n\n.m-menubar.open-ins i.fa-plus,\n.m-menubar i.minus {\n  display: none;\n}\n\n.m-menubar.open-ins i.fa-minus {\n  display: inline;\n}\n\n.m-menubar i.fa-times-circle {\n  right: 13px;\n}\n\n.m-menubar.important {\n  background-color: #b7202e;\n}\n\n.mclosex {\n  font-size: 22px;\n  line-height: 22px;\n  height: 22px;\n  width: 22px;\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  right: 7px;\n  margin: auto;\n  color: #abaead;\n  cursor: pointer;\n}\n\n.mc-logo.mcenter {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  margin: auto;\n  width: 123px;\n}\n\n.hidden {\n  display: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZG1pbi9Eb2N1bWVudHMvRSRHUy9SRUJSTkFESU5HL1Jlc3BhbGRvcy9pb25pYy9taWNsYXJvMy1pb25pYy12ZXJzaW9uL3NyYy9hcHAvcGFnZXMvX3NoYXJlZC9jb21wb25lbnRzL2FjY291bnQtc2VsZWN0L2FjY291bnQtc2VsZWN0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy9fc2hhcmVkL2NvbXBvbmVudHMvYWNjb3VudC1zZWxlY3QvYWNjb3VudC1zZWxlY3QuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQWMscUJBQUE7RUFDZCxXQUFBO0VBQWEsMENBQUE7RUFDYixrQkFBQTtFQUFvQixrQkFBQTtFQUNwQixjQUFBO0VBQWdCLGdCQUFBO0VBQ2hCLFNBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFBa0IsOEJBQUE7RUFDbEIsZ0JBQUE7RUFBa0IseURBQUE7RUFDbEIsd0JBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQ0FBQTtBQ09KOztBREpBO0VBQ0ksZ0JBQUE7QUNPSjs7QURKQTtFQUNJLFNBQUE7QUNPSjs7QURKQTtFQUNJO0lBQ0ksU0FBQTtJQUNBLGlDQUFBO0VDT047QUFDRjs7QURKQSwwQkFBQTs7QUFDQTtFQUNJO0lBQ0ksU0FBQTtFQ01OO0FBQ0Y7O0FESEEsa0JBQUE7O0FBQ0E7RUFDSTtJQUNJLFNBQUE7RUNLTjtBQUNGOztBREZBLGNBQUE7O0FBQ0E7RUFDSTtJQUNJLFNBQUE7RUNJTjtBQUNGOztBRERBLGtCQUFBOztBQUNBO0VBQ0k7SUFDSSxTQUFBO0VDR047QUFDRjs7QURBQTtFQUNJLDRCQUFBO0FDRUo7O0FEQ0E7RUFDSSx5QkFBQTtBQ0VKOztBREFBO0VBQ0kseUJBQUE7QUNHSjs7QURBQTtFQUNJLFFBQUE7QUNHSjs7QUREQTtFQUNJLFdBQUE7QUNJSjs7QURGQTs7RUFFSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLDZCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLHlCQUFBO0FDS0o7O0FERkE7RUFDSSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EsNkJBQUE7RUFDQSxnQ0FBQTtBQ0tKOztBREZBO0VBQ0ksU0FBQTtBQ0tKOztBREZBO0VBQ0ksY0FBQTtFQUNBLHFCQUFBO0FDS0o7O0FERkE7RUFDSSxjQUFBO0VBQ0EscUJBQUE7QUNLSjs7QURGQTs7RUFFSSxtQkFBQTtBQ0tKOztBREZBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FDS0o7O0FERkE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7QUNLSjs7QURGQTs7RUFFSSxXQUFBO0FDS0o7O0FERkE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7QUNLSjs7QURGQTs7RUFFSSxhQUFBO0FDS0o7O0FERkE7RUFDSSxlQUFBO0FDS0o7O0FERkE7RUFDSSxXQUFBO0FDS0o7O0FERkE7RUFDSSx5QkFBQTtBQ0tKOztBREZBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQ0tKOztBREZBO0VBQ0ksa0JBQUE7RUFDQSxNQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUNLSjs7QURGQTtFQUNJLHdCQUFBO0FDS0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9fc2hhcmVkL2NvbXBvbmVudHMvYWNjb3VudC1zZWxlY3QvYWNjb3VudC1zZWxlY3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubXlTaWRlTmF2IHtcbiAgICBoZWlnaHQ6IGF1dG87IC8qIDEwMCUgRnVsbC1oZWlnaHQgKi9cbiAgICB3aWR0aDogMTAwJTsgLyogMCB3aWR0aCAtIGNoYW5nZSB0aGlzIHdpdGggSmF2YVNjcmlwdCAqL1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTsgLyogU3RheSBpbiBwbGFjZSAqL1xuICAgIHotaW5kZXg6IDEwMDAwOyAvKiBTdGF5IG9uIHRvcCAqL1xuICAgIHRvcDogODRweDtcbiAgICByaWdodDogMTAwJTtcbiAgICBvdmVyZmxvdy14OiBhdXRvOyAvKiBEaXNhYmxlIGhvcml6b250YWwgc2Nyb2xsICovXG4gICAgdHJhbnNpdGlvbjogMC40czsgLyogMC41IHNlY29uZCB0cmFuc2l0aW9uIGVmZmVjdCB0byBzbGlkZSBpbiB0aGUgc2lkZW5hdiAqL1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogMC40cztcbiAgICAtbW96LXRyYW5zaXRpb246IDAuNHM7XG4gICAgLW8tdHJhbnNpdGlvbjogMC40cztcbiAgICBvdmVyZmxvdy15OiBoaWRkZW47XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZGRkZGQ7XG59XG5cbi5teVNpZGVOYXYge1xuICAgIG1hcmdpbi10b3A6IC0xcHg7XG59XG5cbi5teVNpZGVOYXYge1xuICAgIHRvcDogNTBweDtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAob3JpZW50YXRpb24gOiBsYW5kc2NhcGUpe1xuICAgIC5teVNpZGVOYXYge1xuICAgICAgICB0b3A6IDQ5cHg7XG4gICAgICAgIC8qIGZpeGVkIGhlYWRlciBmb3IgaW9zIHZlcnNpb24gKi9cbiAgICB9XG59XG5cbi8qIGlwaG9uZSBYIGFuZCBpcGhvbmUgWHMqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogMzc1cHgpIGFuZCAobWF4LWRldmljZS1oZWlnaHQ6IDgxMnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAzKSAgYW5kIChvcmllbnRhdGlvbiA6IHBvcnRyYWl0KSB7XG4gICAgLm15U2lkZU5hdiB7XG4gICAgICAgIHRvcDogODRweDtcbiAgICB9XG59XG5cbi8qIGlwaG9uZSBYciBURVNUKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDM3NXB4KSBhbmQgKG1heC1kZXZpY2UtaGVpZ2h0OiA4MTJweCkgYW5kICgtd2Via2l0LWRldmljZS1waXhlbC1yYXRpbzogMikgIGFuZCAob3JpZW50YXRpb24gOiBwb3J0cmFpdCkge1xuICAgIC5teVNpZGVOYXYge1xuICAgICAgICB0b3A6IDg0cHg7XG4gICAgfVxufVxuXG4vKiBpcGhvbmUgWHIgKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDQxNHB4KSBhbmQgKG1heC1kZXZpY2UtaGVpZ2h0OiA4OTZweCkgYW5kICgtd2Via2l0LWRldmljZS1waXhlbC1yYXRpbzogMikgIGFuZCAob3JpZW50YXRpb24gOiBwb3J0cmFpdCkge1xuICAgIC5teVNpZGVOYXYge1xuICAgICAgICB0b3A6IDg0cHg7XG4gICAgfVxufVxuXG4vKiBpcGhvbmUgWHMgTWF4ICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiA0MTRweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogODk2cHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDMpICBhbmQgKG9yaWVudGF0aW9uIDogcG9ydHJhaXQpIHtcbiAgICAubXlTaWRlTmF2IHtcbiAgICAgICAgdG9wOiA4NHB4O1xuICAgIH1cbn1cblxuLmFuZHtcbiAgICBtYXJnaW4tdG9wOiAtMzVweCAhaW1wb3J0YW50O1xufVxuXG4udGhlUmVke1xuICAgIGNvbG9yOiAjZWYzODI5ICFpbXBvcnRhbnQ7XG59XG4udGhlV2hpdGV7XG4gICAgY29sb3I6IGhzbCgwLCA1MCUsIDk4JSkgIWltcG9ydGFudDtcbn1cblxuLm1lbnVJc09wZW57XG4gICAgcmlnaHQ6IDA7XG59XG4ubWVudUlzQ2xvc2Vke1xuICAgIHJpZ2h0OiAxMDAlO1xufVxuLm0tbWVudWluLFxuLm0tbWVudWJhciB7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZmZmZmZmO1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBmb250LXNpemU6IDE5LjJweDtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBwYWRkaW5nOiAwIDIwcHg7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8nLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlZjM4Mjk7XG59XG5cbi5tLW1lbnVpbiB7XG4gICAgY29sb3I6ICMyMjIyMjI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNmZmZmZmY7XG4gICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNkZGRkZGQ7XG59XG5cbi5tLW1lbnVpbi5sYXN0bGluayB7XG4gICAgYm9yZGVyOiAwO1xufVxuXG5hLm0tbWVudWluIHtcbiAgICBjb2xvcjogIzIyMjIyMjtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbmEubS1tZW51YmFyIHtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi5tLW1lbnVpbixcbi5tLW1lbnViYXIge1xuICAgIHBhZGRpbmctcmlnaHQ6IDQwcHg7XG59XG5cbi5tLW1lbnViYXIgaSB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgdG9wOiAwO1xuICAgIGJvdHRvbTogMDtcbiAgICByaWdodDogN3B4O1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIG1hcmdpbjogYXV0bztcbn1cblxuLm0tbWVudWluIGkge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAxNnB4O1xuICAgIHdpZHRoOiAxNnB4O1xuICAgIHRvcDogMDtcbiAgICBib3R0b206IDA7XG4gICAgcmlnaHQ6IDdweDtcbiAgICBjb2xvcjogIzIyMjIyMjtcbiAgICBtYXJnaW46IGF1dG87XG59XG5cbi5tLW1lbnViYXIgaS5mYS1taW51cyxcbi5tLW1lbnViYXIgaS5mYS1wbHVzIHtcbiAgICByaWdodDogMTRweDtcbn1cblxuLm0tbWVudWluIGkuZmEtdGltZXMtY2lyY2xlIHtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogMThweDtcbiAgICB3aWR0aDogMThweDtcbiAgICByaWdodDogMTFweDtcbiAgICBjb2xvcjogI2I3MjAyZTtcbn1cblxuLm0tbWVudWJhci5vcGVuLWlucyBpLmZhLXBsdXMsXG4ubS1tZW51YmFyIGkubWludXMge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5tLW1lbnViYXIub3Blbi1pbnMgaS5mYS1taW51cyB7XG4gICAgZGlzcGxheTogaW5saW5lO1xufVxuXG4ubS1tZW51YmFyIGkuZmEtdGltZXMtY2lyY2xlIHtcbiAgICByaWdodDogMTNweDtcbn1cblxuLm0tbWVudWJhci5pbXBvcnRhbnQge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNiNzIwMmU7XG59XG5cbi5tY2xvc2V4IHtcbiAgICBmb250LXNpemU6IDIycHg7XG4gICAgbGluZS1oZWlnaHQ6IDIycHg7XG4gICAgaGVpZ2h0OiAyMnB4O1xuICAgIHdpZHRoOiAyMnB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIHJpZ2h0OiA3cHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGNvbG9yOiAjYWJhZWFkO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLm1jLWxvZ28ubWNlbnRlciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICBib3R0b206IDA7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgd2lkdGg6IDEyM3B4O1xufVxuXG4uaGlkZGVuIHtcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59XG4iLCIubXlTaWRlTmF2IHtcbiAgaGVpZ2h0OiBhdXRvO1xuICAvKiAxMDAlIEZ1bGwtaGVpZ2h0ICovXG4gIHdpZHRoOiAxMDAlO1xuICAvKiAwIHdpZHRoIC0gY2hhbmdlIHRoaXMgd2l0aCBKYXZhU2NyaXB0ICovXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgLyogU3RheSBpbiBwbGFjZSAqL1xuICB6LWluZGV4OiAxMDAwMDtcbiAgLyogU3RheSBvbiB0b3AgKi9cbiAgdG9wOiA4NHB4O1xuICByaWdodDogMTAwJTtcbiAgb3ZlcmZsb3cteDogYXV0bztcbiAgLyogRGlzYWJsZSBob3Jpem9udGFsIHNjcm9sbCAqL1xuICB0cmFuc2l0aW9uOiAwLjRzO1xuICAvKiAwLjUgc2Vjb25kIHRyYW5zaXRpb24gZWZmZWN0IHRvIHNsaWRlIGluIHRoZSBzaWRlbmF2ICovXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogMC40cztcbiAgLW1vei10cmFuc2l0aW9uOiAwLjRzO1xuICAtby10cmFuc2l0aW9uOiAwLjRzO1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGRkZGRkO1xufVxuXG4ubXlTaWRlTmF2IHtcbiAgbWFyZ2luLXRvcDogLTFweDtcbn1cblxuLm15U2lkZU5hdiB7XG4gIHRvcDogNTBweDtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSkge1xuICAubXlTaWRlTmF2IHtcbiAgICB0b3A6IDQ5cHg7XG4gICAgLyogZml4ZWQgaGVhZGVyIGZvciBpb3MgdmVyc2lvbiAqL1xuICB9XG59XG4vKiBpcGhvbmUgWCBhbmQgaXBob25lIFhzKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDM3NXB4KSBhbmQgKG1heC1kZXZpY2UtaGVpZ2h0OiA4MTJweCkgYW5kICgtd2Via2l0LWRldmljZS1waXhlbC1yYXRpbzogMykgYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpIHtcbiAgLm15U2lkZU5hdiB7XG4gICAgdG9wOiA4NHB4O1xuICB9XG59XG4vKiBpcGhvbmUgWHIgVEVTVCovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiAzNzVweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogODEycHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KSB7XG4gIC5teVNpZGVOYXYge1xuICAgIHRvcDogODRweDtcbiAgfVxufVxuLyogaXBob25lIFhyICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiA0MTRweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogODk2cHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KSB7XG4gIC5teVNpZGVOYXYge1xuICAgIHRvcDogODRweDtcbiAgfVxufVxuLyogaXBob25lIFhzIE1heCAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogNDE0cHgpIGFuZCAobWF4LWRldmljZS1oZWlnaHQ6IDg5NnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAzKSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkge1xuICAubXlTaWRlTmF2IHtcbiAgICB0b3A6IDg0cHg7XG4gIH1cbn1cbi5hbmQge1xuICBtYXJnaW4tdG9wOiAtMzVweCAhaW1wb3J0YW50O1xufVxuXG4udGhlUmVkIHtcbiAgY29sb3I6ICNlZjM4MjkgIWltcG9ydGFudDtcbn1cblxuLnRoZVdoaXRlIHtcbiAgY29sb3I6ICNmY2Y3ZjcgIWltcG9ydGFudDtcbn1cblxuLm1lbnVJc09wZW4ge1xuICByaWdodDogMDtcbn1cblxuLm1lbnVJc0Nsb3NlZCB7XG4gIHJpZ2h0OiAxMDAlO1xufVxuXG4ubS1tZW51aW4sXG4ubS1tZW51YmFyIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNmZmZmZmY7XG4gIGZsb2F0OiBsZWZ0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGZvbnQtc2l6ZTogMTkuMnB4O1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogMCAyMHB4O1xuICBmb250LWZhbWlseTogXCJSb2JvdG9cIiwgc2Fucy1zZXJpZjtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWYzODI5O1xufVxuXG4ubS1tZW51aW4ge1xuICBjb2xvcjogIzIyMjIyMjtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2ZmZmZmZjtcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNkZGRkZGQ7XG59XG5cbi5tLW1lbnVpbi5sYXN0bGluayB7XG4gIGJvcmRlcjogMDtcbn1cblxuYS5tLW1lbnVpbiB7XG4gIGNvbG9yOiAjMjIyMjIyO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbmEubS1tZW51YmFyIHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuLm0tbWVudWluLFxuLm0tbWVudWJhciB7XG4gIHBhZGRpbmctcmlnaHQ6IDQwcHg7XG59XG5cbi5tLW1lbnViYXIgaSB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAyMHB4O1xuICB3aWR0aDogMjBweDtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiA3cHg7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5tLW1lbnVpbiBpIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMTZweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDE2cHg7XG4gIHdpZHRoOiAxNnB4O1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDdweDtcbiAgY29sb3I6ICMyMjIyMjI7XG4gIG1hcmdpbjogYXV0bztcbn1cblxuLm0tbWVudWJhciBpLmZhLW1pbnVzLFxuLm0tbWVudWJhciBpLmZhLXBsdXMge1xuICByaWdodDogMTRweDtcbn1cblxuLm0tbWVudWluIGkuZmEtdGltZXMtY2lyY2xlIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogMThweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDE4cHg7XG4gIHdpZHRoOiAxOHB4O1xuICByaWdodDogMTFweDtcbiAgY29sb3I6ICNiNzIwMmU7XG59XG5cbi5tLW1lbnViYXIub3Blbi1pbnMgaS5mYS1wbHVzLFxuLm0tbWVudWJhciBpLm1pbnVzIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLm0tbWVudWJhci5vcGVuLWlucyBpLmZhLW1pbnVzIHtcbiAgZGlzcGxheTogaW5saW5lO1xufVxuXG4ubS1tZW51YmFyIGkuZmEtdGltZXMtY2lyY2xlIHtcbiAgcmlnaHQ6IDEzcHg7XG59XG5cbi5tLW1lbnViYXIuaW1wb3J0YW50IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2I3MjAyZTtcbn1cblxuLm1jbG9zZXgge1xuICBmb250LXNpemU6IDIycHg7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICBoZWlnaHQ6IDIycHg7XG4gIHdpZHRoOiAyMnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICByaWdodDogN3B4O1xuICBtYXJnaW46IGF1dG87XG4gIGNvbG9yOiAjYWJhZWFkO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5tYy1sb2dvLm1jZW50ZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogMTIzcHg7XG59XG5cbi5oaWRkZW4ge1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/_shared/components/account-select/account-select.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/_shared/components/account-select/account-select.component.ts ***!
  \*************************************************************************************/
/*! exports provided: AccountSelectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountSelectComponent", function() { return AccountSelectComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/intent.provider */ "./src/app/services/intent.provider.ts");









let AccountSelectComponent = class AccountSelectComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_3__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.router = router;
        this.accounts = [];
    }
    ngOnInit() {
        const data = this.cacheStorage();
        if (data.isPostpaidAccount) {
            this.accounts = data.postpaidAccounts;
        }
        if (data.isPrepaidAccount) {
            this.accounts = data.prepaidAccounts;
        }
        if (data.isTelephonyAccount) {
            this.accounts = data.telephonyAccounts;
        }
        this.selectedBan = String(data.accountInfo.bANField);
    }
    onChange() {
        const accountSelected = this.cacheStorage().getAccountByBan(this.selectedBan);
        this.selectAccount(accountSelected);
    }
};
AccountSelectComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_2__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] }
];
AccountSelectComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-account-select',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./account-select.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/account-select/account-select.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./account-select.component.scss */ "./src/app/pages/_shared/components/account-select/account-select.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_2__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"]])
], AccountSelectComponent);



/***/ }),

/***/ "./src/app/pages/_shared/components/footer/footer.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/_shared/components/footer/footer.component.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".hidden {\n  display: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZG1pbi9Eb2N1bWVudHMvRSRHUy9SRUJSTkFESU5HL1Jlc3BhbGRvcy9pb25pYy9taWNsYXJvMy1pb25pYy12ZXJzaW9uL3NyYy9hcHAvcGFnZXMvX3NoYXJlZC9jb21wb25lbnRzL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL19zaGFyZWQvY29tcG9uZW50cy9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksd0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL19zaGFyZWQvY29tcG9uZW50cy9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhpZGRlbiB7XG4gICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xufSIsIi5oaWRkZW4ge1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/_shared/components/footer/footer.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/_shared/components/footer/footer.component.ts ***!
  \*********************************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../base.page */ "./src/app/pages/base.page.ts");









let FooterComponent = class FooterComponent extends _base_page__WEBPACK_IMPORTED_MODULE_8__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.router = router;
    }
    ngOnInit() { }
    shouldShow() {
        return !this.router.isActive('/update-app', false);
    }
};
FooterComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__["IntentProvider"] }
];
FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-footer',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./footer.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/footer/footer.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./footer.component.scss */ "./src/app/pages/_shared/components/footer/footer.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_6__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__["IntentProvider"]])
], FooterComponent);



/***/ }),

/***/ "./src/app/pages/_shared/components/header-static/header-static.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/_shared/components/header-static/header-static.component.scss ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL19zaGFyZWQvY29tcG9uZW50cy9oZWFkZXItc3RhdGljL2hlYWRlci1zdGF0aWMuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/_shared/components/header-static/header-static.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/_shared/components/header-static/header-static.component.ts ***!
  \***********************************************************************************/
/*! exports provided: HeaderStaticComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderStaticComponent", function() { return HeaderStaticComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HeaderStaticComponent = class HeaderStaticComponent {
    constructor() {
    }
};
HeaderStaticComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header-static',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header-static.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/header-static/header-static.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./header-static.component.scss */ "./src/app/pages/_shared/components/header-static/header-static.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], HeaderStaticComponent);



/***/ }),

/***/ "./src/app/pages/_shared/components/header/header.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/pages/_shared/components/header/header.component.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mySideNav {\n  height: auto;\n  /* 100% Full-height */\n  width: 100%;\n  /* 0 width - change this with JavaScript */\n  position: absolute;\n  /* Stay in place */\n  z-index: 10000;\n  /* Stay on top */\n  top: 84px;\n  right: 100%;\n  overflow-x: auto;\n  /* Disable horizontal scroll */\n  transition: 0.4s;\n  /* 0.5 second transition effect to slide in the sidenav */\n  -webkit-transition: 0.4s;\n  -moz-transition: 0.4s;\n  -o-transition: 0.4s;\n  overflow-y: hidden;\n  border-bottom: 1px solid #dddddd;\n}\n\n.mySideNav {\n  margin-top: -1px;\n}\n\n.mySideNav {\n  top: 50px;\n}\n\n@media only screen and (orientation: landscape) {\n  .mySideNav {\n    top: 49px;\n    /* fixed header for ios version */\n  }\n}\n\n/* iphone X and iphone Xs*/\n\n@media only screen and (min-device-width: 375px) and (max-device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone Xr TEST*/\n\n@media only screen and (min-device-width: 375px) and (max-device-height: 812px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone Xr */\n\n@media only screen and (min-device-width: 414px) and (max-device-height: 896px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone Xs Max */\n\n@media only screen and (min-device-width: 414px) and (max-device-height: 896px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone 11*/\n\n@media only screen and (min-device-width: 414px) and (max-device-height: 1792px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone 11 pro*/\n\n@media only screen and (min-device-width: 414px) and (max-device-height: 2688px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n.and {\n  margin-top: -35px !important;\n}\n\n.theRed {\n  color: #ef3829 !important;\n}\n\n.theWhite {\n  color: #fcf7f7 !important;\n}\n\n.menuIsOpen {\n  right: 0;\n}\n\n.menuIsClosed {\n  right: 100%;\n}\n\n.m-menuin,\n.m-menubar {\n  height: 50px;\n  width: 100%;\n  border-top: 1px solid #ffffff;\n  float: left;\n  position: relative;\n  font-size: 19.2px;\n  color: #ffffff;\n  padding: 0 20px;\n  font-family: \"Roboto\", sans-serif;\n  font-weight: 400;\n  text-decoration: none;\n  background-color: #ef3829;\n}\n\n.m-menuin {\n  color: #222222;\n  background-color: #ffffff;\n  font-size: 14px;\n  border-top: 1px solid #ffffff;\n  border-bottom: 2px solid #dddddd;\n}\n\n.m-menuin.lastlink {\n  border: 0;\n}\n\na.m-menuin {\n  color: #222222;\n  text-decoration: none;\n}\n\na.m-menubar {\n  color: #ffffff;\n  text-decoration: none;\n}\n\n.m-menuin,\n.m-menubar {\n  padding-right: 40px;\n}\n\n.m-menubar i {\n  font-size: 20px;\n  line-height: 20px;\n  position: absolute;\n  height: 20px;\n  width: 20px;\n  top: 0;\n  bottom: 0;\n  right: 7px;\n  color: #ffffff;\n  margin: auto;\n}\n\n.m-menuin i {\n  font-size: 16px;\n  line-height: 16px;\n  position: absolute;\n  height: 16px;\n  width: 16px;\n  top: 0;\n  bottom: 0;\n  right: 7px;\n  color: #222222;\n  margin: auto;\n}\n\n.m-menubar i.fa-minus,\n.m-menubar i.fa-plus {\n  right: 14px;\n}\n\n.m-menuin i.fa-times-circle {\n  font-size: 18px;\n  line-height: 18px;\n  position: absolute;\n  height: 18px;\n  width: 18px;\n  right: 11px;\n  color: #b7202e;\n}\n\n.m-menubar.open-ins i.fa-plus,\n.m-menubar i.minus {\n  display: none;\n}\n\n.m-menubar.open-ins i.fa-minus {\n  display: inline;\n}\n\n.m-menubar i.fa-times-circle {\n  right: 13px;\n}\n\n.m-menubar.important {\n  background-color: #b7202e;\n}\n\n.mclosex {\n  font-size: 22px;\n  line-height: 22px;\n  height: 22px;\n  width: 22px;\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  right: 7px;\n  margin: auto;\n  color: #abaead;\n  cursor: pointer;\n}\n\n.mc-logo.mcenter {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  margin: auto;\n  width: 123px;\n}\n\n.hidden {\n  display: none !important;\n}\n\n.notifc {\n  margin-left: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZG1pbi9Eb2N1bWVudHMvRSRHUy9SRUJSTkFESU5HL1Jlc3BhbGRvcy9pb25pYy9taWNsYXJvMy1pb25pYy12ZXJzaW9uL3NyYy9hcHAvcGFnZXMvX3NoYXJlZC9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL19zaGFyZWQvY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUFjLHFCQUFBO0VBQ2QsV0FBQTtFQUFhLDBDQUFBO0VBQ2Isa0JBQUE7RUFBb0Isa0JBQUE7RUFDcEIsY0FBQTtFQUFnQixnQkFBQTtFQUNoQixTQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQWtCLDhCQUFBO0VBQ2xCLGdCQUFBO0VBQWtCLHlEQUFBO0VBQ2xCLHdCQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0NBQUE7QUNPSjs7QURKQTtFQUNJLGdCQUFBO0FDT0o7O0FESkE7RUFDSSxTQUFBO0FDT0o7O0FESkE7RUFDSTtJQUNJLFNBQUE7SUFDQSxpQ0FBQTtFQ09OO0FBQ0Y7O0FESkEsMEJBQUE7O0FBQ0E7RUFDSTtJQUNJLFNBQUE7RUNNTjtBQUNGOztBREhBLGtCQUFBOztBQUNBO0VBQ0k7SUFDSSxTQUFBO0VDS047QUFDRjs7QURGQSxjQUFBOztBQUNBO0VBQ0k7SUFDSSxTQUFBO0VDSU47QUFDRjs7QUREQSxrQkFBQTs7QUFDQTtFQUNJO0lBQ0ksU0FBQTtFQ0dOO0FBQ0Y7O0FEQUEsYUFBQTs7QUFDQTtFQUNJO0lBQ0ksU0FBQTtFQ0VOO0FBQ0Y7O0FEQ0EsaUJBQUE7O0FBQ0E7RUFDSTtJQUNJLFNBQUE7RUNDTjtBQUNGOztBREVBO0VBQ0ksNEJBQUE7QUNBSjs7QURHQTtFQUNJLHlCQUFBO0FDQUo7O0FERUE7RUFDSSx5QkFBQTtBQ0NKOztBREVBO0VBQ0ksUUFBQTtBQ0NKOztBRENBO0VBQ0ksV0FBQTtBQ0VKOztBREFBOztFQUVJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUNBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7QUNHSjs7QURBQTtFQUNJLGNBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0FDR0o7O0FEQUE7RUFDSSxTQUFBO0FDR0o7O0FEQUE7RUFDSSxjQUFBO0VBQ0EscUJBQUE7QUNHSjs7QURBQTtFQUNJLGNBQUE7RUFDQSxxQkFBQTtBQ0dKOztBREFBOztFQUVJLG1CQUFBO0FDR0o7O0FEQUE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7QUNHSjs7QURBQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxNQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ0dKOztBREFBOztFQUVJLFdBQUE7QUNHSjs7QURBQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtBQ0dKOztBREFBOztFQUVJLGFBQUE7QUNHSjs7QURBQTtFQUNJLGVBQUE7QUNHSjs7QURBQTtFQUNJLFdBQUE7QUNHSjs7QURBQTtFQUNJLHlCQUFBO0FDR0o7O0FEQUE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDR0o7O0FEQUE7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQ0dKOztBREFBO0VBQ0ksd0JBQUE7QUNHSjs7QURBQTtFQUNJLGlCQUFBO0FDR0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9fc2hhcmVkL2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5teVNpZGVOYXYge1xuICAgIGhlaWdodDogYXV0bzsgLyogMTAwJSBGdWxsLWhlaWdodCAqL1xuICAgIHdpZHRoOiAxMDAlOyAvKiAwIHdpZHRoIC0gY2hhbmdlIHRoaXMgd2l0aCBKYXZhU2NyaXB0ICovXG4gICAgcG9zaXRpb246IGFic29sdXRlOyAvKiBTdGF5IGluIHBsYWNlICovXG4gICAgei1pbmRleDogMTAwMDA7IC8qIFN0YXkgb24gdG9wICovXG4gICAgdG9wOiA4NHB4O1xuICAgIHJpZ2h0OiAxMDAlO1xuICAgIG92ZXJmbG93LXg6IGF1dG87IC8qIERpc2FibGUgaG9yaXpvbnRhbCBzY3JvbGwgKi9cbiAgICB0cmFuc2l0aW9uOiAwLjRzOyAvKiAwLjUgc2Vjb25kIHRyYW5zaXRpb24gZWZmZWN0IHRvIHNsaWRlIGluIHRoZSBzaWRlbmF2ICovXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjRzO1xuICAgIC1tb3otdHJhbnNpdGlvbjogMC40cztcbiAgICAtby10cmFuc2l0aW9uOiAwLjRzO1xuICAgIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RkZGRkZDtcbn1cblxuLm15U2lkZU5hdiB7XG4gICAgbWFyZ2luLXRvcDogLTFweDtcbn1cblxuLm15U2lkZU5hdiB7XG4gICAgdG9wOiA1MHB4O1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChvcmllbnRhdGlvbiA6IGxhbmRzY2FwZSl7XG4gICAgLm15U2lkZU5hdiB7XG4gICAgICAgIHRvcDogNDlweDtcbiAgICAgICAgLyogZml4ZWQgaGVhZGVyIGZvciBpb3MgdmVyc2lvbiAqL1xuICAgIH1cbn1cblxuLyogaXBob25lIFggYW5kIGlwaG9uZSBYcyovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiAzNzVweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogODEycHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDMpICBhbmQgKG9yaWVudGF0aW9uIDogcG9ydHJhaXQpIHtcbiAgICAubXlTaWRlTmF2IHtcbiAgICAgICAgdG9wOiA4NHB4O1xuICAgIH1cbn1cblxuLyogaXBob25lIFhyIFRFU1QqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogMzc1cHgpIGFuZCAobWF4LWRldmljZS1oZWlnaHQ6IDgxMnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAyKSAgYW5kIChvcmllbnRhdGlvbiA6IHBvcnRyYWl0KSB7XG4gICAgLm15U2lkZU5hdiB7XG4gICAgICAgIHRvcDogODRweDtcbiAgICB9XG59XG5cbi8qIGlwaG9uZSBYciAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogNDE0cHgpIGFuZCAobWF4LWRldmljZS1oZWlnaHQ6IDg5NnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAyKSAgYW5kIChvcmllbnRhdGlvbiA6IHBvcnRyYWl0KSB7XG4gICAgLm15U2lkZU5hdiB7XG4gICAgICAgIHRvcDogODRweDtcbiAgICB9XG59XG5cbi8qIGlwaG9uZSBYcyBNYXggKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDQxNHB4KSBhbmQgKG1heC1kZXZpY2UtaGVpZ2h0OiA4OTZweCkgYW5kICgtd2Via2l0LWRldmljZS1waXhlbC1yYXRpbzogMykgIGFuZCAob3JpZW50YXRpb24gOiBwb3J0cmFpdCkge1xuICAgIC5teVNpZGVOYXYge1xuICAgICAgICB0b3A6IDg0cHg7XG4gICAgfVxufVxuXG4vKiBpcGhvbmUgMTEqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogNDE0cHgpIGFuZCAobWF4LWRldmljZS1oZWlnaHQ6IDE3OTJweCkgYW5kICgtd2Via2l0LWRldmljZS1waXhlbC1yYXRpbzogMykgIGFuZCAob3JpZW50YXRpb24gOiBwb3J0cmFpdCkge1xuICAgIC5teVNpZGVOYXYge1xuICAgICAgICB0b3A6IDg0cHg7XG4gICAgfVxufVxuXG4vKiBpcGhvbmUgMTEgcHJvKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDQxNHB4KSBhbmQgKG1heC1kZXZpY2UtaGVpZ2h0OiAyNjg4cHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDMpICBhbmQgKG9yaWVudGF0aW9uIDogcG9ydHJhaXQpIHtcbiAgICAubXlTaWRlTmF2IHtcbiAgICAgICAgdG9wOiA4NHB4O1xuICAgIH1cbn1cblxuLmFuZHtcbiAgICBtYXJnaW4tdG9wOiAtMzVweCAhaW1wb3J0YW50O1xufVxuXG4udGhlUmVke1xuICAgIGNvbG9yOiAjZWYzODI5ICFpbXBvcnRhbnQ7XG59XG4udGhlV2hpdGV7XG4gICAgY29sb3I6IGhzbCgwLCA1MCUsIDk4JSkgIWltcG9ydGFudDtcbn1cblxuLm1lbnVJc09wZW57XG4gICAgcmlnaHQ6IDA7XG59XG4ubWVudUlzQ2xvc2Vke1xuICAgIHJpZ2h0OiAxMDAlO1xufVxuLm0tbWVudWluLFxuLm0tbWVudWJhciB7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZmZmZmZmO1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBmb250LXNpemU6IDE5LjJweDtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBwYWRkaW5nOiAwIDIwcHg7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8nLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlZjM4Mjk7XG59XG5cbi5tLW1lbnVpbiB7XG4gICAgY29sb3I6ICMyMjIyMjI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNmZmZmZmY7XG4gICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNkZGRkZGQ7XG59XG5cbi5tLW1lbnVpbi5sYXN0bGluayB7XG4gICAgYm9yZGVyOiAwO1xufVxuXG5hLm0tbWVudWluIHtcbiAgICBjb2xvcjogIzIyMjIyMjtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbmEubS1tZW51YmFyIHtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi5tLW1lbnVpbixcbi5tLW1lbnViYXIge1xuICAgIHBhZGRpbmctcmlnaHQ6IDQwcHg7XG59XG5cbi5tLW1lbnViYXIgaSB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgdG9wOiAwO1xuICAgIGJvdHRvbTogMDtcbiAgICByaWdodDogN3B4O1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIG1hcmdpbjogYXV0bztcbn1cblxuLm0tbWVudWluIGkge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAxNnB4O1xuICAgIHdpZHRoOiAxNnB4O1xuICAgIHRvcDogMDtcbiAgICBib3R0b206IDA7XG4gICAgcmlnaHQ6IDdweDtcbiAgICBjb2xvcjogIzIyMjIyMjtcbiAgICBtYXJnaW46IGF1dG87XG59XG5cbi5tLW1lbnViYXIgaS5mYS1taW51cyxcbi5tLW1lbnViYXIgaS5mYS1wbHVzIHtcbiAgICByaWdodDogMTRweDtcbn1cblxuLm0tbWVudWluIGkuZmEtdGltZXMtY2lyY2xlIHtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogMThweDtcbiAgICB3aWR0aDogMThweDtcbiAgICByaWdodDogMTFweDtcbiAgICBjb2xvcjogI2I3MjAyZTtcbn1cblxuLm0tbWVudWJhci5vcGVuLWlucyBpLmZhLXBsdXMsXG4ubS1tZW51YmFyIGkubWludXMge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5tLW1lbnViYXIub3Blbi1pbnMgaS5mYS1taW51cyB7XG4gICAgZGlzcGxheTogaW5saW5lO1xufVxuXG4ubS1tZW51YmFyIGkuZmEtdGltZXMtY2lyY2xlIHtcbiAgICByaWdodDogMTNweDtcbn1cblxuLm0tbWVudWJhci5pbXBvcnRhbnQge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNiNzIwMmU7XG59XG5cbi5tY2xvc2V4IHtcbiAgICBmb250LXNpemU6IDIycHg7XG4gICAgbGluZS1oZWlnaHQ6IDIycHg7XG4gICAgaGVpZ2h0OiAyMnB4O1xuICAgIHdpZHRoOiAyMnB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIHJpZ2h0OiA3cHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGNvbG9yOiAjYWJhZWFkO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLm1jLWxvZ28ubWNlbnRlciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICBib3R0b206IDA7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgd2lkdGg6IDEyM3B4O1xufVxuXG4uaGlkZGVuIHtcbiAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5ub3RpZmMge1xuICAgIG1hcmdpbi1sZWZ0OiAxMnB4XG59XG4iLCIubXlTaWRlTmF2IHtcbiAgaGVpZ2h0OiBhdXRvO1xuICAvKiAxMDAlIEZ1bGwtaGVpZ2h0ICovXG4gIHdpZHRoOiAxMDAlO1xuICAvKiAwIHdpZHRoIC0gY2hhbmdlIHRoaXMgd2l0aCBKYXZhU2NyaXB0ICovXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgLyogU3RheSBpbiBwbGFjZSAqL1xuICB6LWluZGV4OiAxMDAwMDtcbiAgLyogU3RheSBvbiB0b3AgKi9cbiAgdG9wOiA4NHB4O1xuICByaWdodDogMTAwJTtcbiAgb3ZlcmZsb3cteDogYXV0bztcbiAgLyogRGlzYWJsZSBob3Jpem9udGFsIHNjcm9sbCAqL1xuICB0cmFuc2l0aW9uOiAwLjRzO1xuICAvKiAwLjUgc2Vjb25kIHRyYW5zaXRpb24gZWZmZWN0IHRvIHNsaWRlIGluIHRoZSBzaWRlbmF2ICovXG4gIC13ZWJraXQtdHJhbnNpdGlvbjogMC40cztcbiAgLW1vei10cmFuc2l0aW9uOiAwLjRzO1xuICAtby10cmFuc2l0aW9uOiAwLjRzO1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZGRkZGRkO1xufVxuXG4ubXlTaWRlTmF2IHtcbiAgbWFyZ2luLXRvcDogLTFweDtcbn1cblxuLm15U2lkZU5hdiB7XG4gIHRvcDogNTBweDtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSkge1xuICAubXlTaWRlTmF2IHtcbiAgICB0b3A6IDQ5cHg7XG4gICAgLyogZml4ZWQgaGVhZGVyIGZvciBpb3MgdmVyc2lvbiAqL1xuICB9XG59XG4vKiBpcGhvbmUgWCBhbmQgaXBob25lIFhzKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDM3NXB4KSBhbmQgKG1heC1kZXZpY2UtaGVpZ2h0OiA4MTJweCkgYW5kICgtd2Via2l0LWRldmljZS1waXhlbC1yYXRpbzogMykgYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpIHtcbiAgLm15U2lkZU5hdiB7XG4gICAgdG9wOiA4NHB4O1xuICB9XG59XG4vKiBpcGhvbmUgWHIgVEVTVCovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiAzNzVweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogODEycHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KSB7XG4gIC5teVNpZGVOYXYge1xuICAgIHRvcDogODRweDtcbiAgfVxufVxuLyogaXBob25lIFhyICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiA0MTRweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogODk2cHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KSB7XG4gIC5teVNpZGVOYXYge1xuICAgIHRvcDogODRweDtcbiAgfVxufVxuLyogaXBob25lIFhzIE1heCAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogNDE0cHgpIGFuZCAobWF4LWRldmljZS1oZWlnaHQ6IDg5NnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAzKSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkge1xuICAubXlTaWRlTmF2IHtcbiAgICB0b3A6IDg0cHg7XG4gIH1cbn1cbi8qIGlwaG9uZSAxMSovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiA0MTRweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogMTc5MnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAzKSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkge1xuICAubXlTaWRlTmF2IHtcbiAgICB0b3A6IDg0cHg7XG4gIH1cbn1cbi8qIGlwaG9uZSAxMSBwcm8qL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogNDE0cHgpIGFuZCAobWF4LWRldmljZS1oZWlnaHQ6IDI2ODhweCkgYW5kICgtd2Via2l0LWRldmljZS1waXhlbC1yYXRpbzogMykgYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpIHtcbiAgLm15U2lkZU5hdiB7XG4gICAgdG9wOiA4NHB4O1xuICB9XG59XG4uYW5kIHtcbiAgbWFyZ2luLXRvcDogLTM1cHggIWltcG9ydGFudDtcbn1cblxuLnRoZVJlZCB7XG4gIGNvbG9yOiAjZWYzODI5ICFpbXBvcnRhbnQ7XG59XG5cbi50aGVXaGl0ZSB7XG4gIGNvbG9yOiAjZmNmN2Y3ICFpbXBvcnRhbnQ7XG59XG5cbi5tZW51SXNPcGVuIHtcbiAgcmlnaHQ6IDA7XG59XG5cbi5tZW51SXNDbG9zZWQge1xuICByaWdodDogMTAwJTtcbn1cblxuLm0tbWVudWluLFxuLm0tbWVudWJhciB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZmZmZmZmO1xuICBmbG9hdDogbGVmdDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXNpemU6IDE5LjJweDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDAgMjBweDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VmMzgyOTtcbn1cblxuLm0tbWVudWluIHtcbiAgY29sb3I6ICMyMjIyMjI7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNmZmZmZmY7XG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjZGRkZGRkO1xufVxuXG4ubS1tZW51aW4ubGFzdGxpbmsge1xuICBib3JkZXI6IDA7XG59XG5cbmEubS1tZW51aW4ge1xuICBjb2xvcjogIzIyMjIyMjtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG5hLm0tbWVudWJhciB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi5tLW1lbnVpbixcbi5tLW1lbnViYXIge1xuICBwYWRkaW5nLXJpZ2h0OiA0MHB4O1xufVxuXG4ubS1tZW51YmFyIGkge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMjBweDtcbiAgd2lkdGg6IDIwcHg7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICByaWdodDogN3B4O1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4ubS1tZW51aW4gaSB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAxNnB4O1xuICB3aWR0aDogMTZweDtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiA3cHg7XG4gIGNvbG9yOiAjMjIyMjIyO1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5tLW1lbnViYXIgaS5mYS1taW51cyxcbi5tLW1lbnViYXIgaS5mYS1wbHVzIHtcbiAgcmlnaHQ6IDE0cHg7XG59XG5cbi5tLW1lbnVpbiBpLmZhLXRpbWVzLWNpcmNsZSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAxOHB4O1xuICB3aWR0aDogMThweDtcbiAgcmlnaHQ6IDExcHg7XG4gIGNvbG9yOiAjYjcyMDJlO1xufVxuXG4ubS1tZW51YmFyLm9wZW4taW5zIGkuZmEtcGx1cyxcbi5tLW1lbnViYXIgaS5taW51cyB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5tLW1lbnViYXIub3Blbi1pbnMgaS5mYS1taW51cyB7XG4gIGRpc3BsYXk6IGlubGluZTtcbn1cblxuLm0tbWVudWJhciBpLmZhLXRpbWVzLWNpcmNsZSB7XG4gIHJpZ2h0OiAxM3B4O1xufVxuXG4ubS1tZW51YmFyLmltcG9ydGFudCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNiNzIwMmU7XG59XG5cbi5tY2xvc2V4IHtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBsaW5lLWhlaWdodDogMjJweDtcbiAgaGVpZ2h0OiAyMnB4O1xuICB3aWR0aDogMjJweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDdweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBjb2xvcjogI2FiYWVhZDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4ubWMtbG9nby5tY2VudGVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIG1hcmdpbjogYXV0bztcbiAgd2lkdGg6IDEyM3B4O1xufVxuXG4uaGlkZGVuIHtcbiAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4ubm90aWZjIHtcbiAgbWFyZ2luLWxlZnQ6IDEycHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/_shared/components/header/header.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/_shared/components/header/header.component.ts ***!
  \*********************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/intent.provider */ "./src/app/services/intent.provider.ts");









let HeaderComponent = class HeaderComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_3__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.router = router;
        this.name = '';
        this.close = true;
        this.countNotifications = 0;
        this.typeBan = {
            postpaid: false,
            prepaid: false,
            telephony: false
        };
    }
    ngOnInit() {
        this.name = this.cacheStorage().accountInfo.firstNameField;
        const data = this.cacheStorage();
        if (this.router.isActive('/home/no-associated', true)) {
            this.typeBan = {
                postpaid: data.tab === 0,
                prepaid: data.tab === 1,
                telephony: data.tab === 2
            };
        }
        else {
            this.typeBan = {
                postpaid: data.isPostpaidAccount,
                prepaid: data.isPrepaidAccount,
                telephony: data.isTelephonyAccount
            };
        }
        this.utils.openMenu.subscribe(open => {
            this.close = !open;
        });
        this.countNotifications = data.countPendingNotifications;
    }
    openNav() {
        this.close = false;
        this.utils.openMenu.emit(true);
    }
    closeNav() {
        this.close = true;
        this.utils.openMenu.emit(false);
    }
    typeOfClient(param) {
        switch (param) {
            case 1:
                if (!this.typeBan.postpaid) {
                    if (this.cacheStorage().isGuest) {
                        this.showAlertAccessLimited();
                    }
                    else {
                        this.utils.registerScreen('postpaid');
                        this.goPostpaid();
                    }
                }
                break;
            case 2:
                if (!this.typeBan.prepaid) {
                    if (this.cacheStorage().isGuest) {
                        this.showAlertAccessLimited();
                    }
                    else {
                        this.goPrepaid();
                    }
                }
                break;
            case 3:
                if (!this.typeBan.telephony) {
                    if (this.cacheStorage().isGuest) {
                        this.showAlertAccessLimited();
                    }
                    else {
                        this.goTelephony();
                    }
                }
                break;
        }
    }
    goPostpaid() {
        const accounts = this.cacheStorage().postpaidAccounts;
        if (accounts.length > 0) {
            this.selectAccount(this.cacheStorage().getAccountByBan(accounts[0].account));
        }
        else {
            this.cacheStorage().tab = 0;
            this.goPageNoAssociated();
        }
        this.typeBan.postpaid = true;
        this.typeBan.prepaid = false;
        this.typeBan.telephony = false;
    }
    goPrepaid() {
        const accounts = this.cacheStorage().prepaidAccounts;
        if (accounts.length > 0) {
            this.selectAccount(this.cacheStorage().getAccountByBan(accounts[0].account));
        }
        else {
            this.cacheStorage().tab = 1;
            this.goPageNoAssociated();
        }
        this.typeBan.postpaid = false;
        this.typeBan.prepaid = true;
        this.typeBan.telephony = false;
    }
    goTelephony() {
        const accounts = this.cacheStorage().telephonyAccounts;
        if (accounts.length > 0) {
            this.selectAccount(this.cacheStorage().getAccountByBan(accounts[0].account));
        }
        else {
            this.cacheStorage().tab = 2;
            this.goPageNoAssociated();
        }
        this.typeBan.postpaid = false;
        this.typeBan.prepaid = false;
        this.typeBan.telephony = true;
    }
    notifications() {
        this.goPage('/module/notifications');
    }
    // Add here all module where should be hide
    tabHidden() {
        return this.router.isActive('/module/purchases', false) ||
            this.router.isActive('/module/support', false) ||
            this.router.isActive('/module/netflix', false) ||
            this.router.isActive('/module/netflix/terms', false) ||
            this.router.isActive('/module/netflix/faq', false) ||
            this.router.isActive('/module/netflix/support', false) ||
            this.router.isActive('/module/netflix/subscription', false) ||
            this.router.isActive('/module/netflix/redirect', false) ||
            this.router.isActive('/module/data-plan/success', false) ||
            this.router.isActive('/module/notifications', false) ||
            this.router.isActive('/module/change-plan/confirm', false) ||
            this.router.isActive('/module/change-plan/success', false) ||
            this.router.isActive('/module/refer/home', false) ||
            this.router.isActive('/module/refer/invite', false) ||
            this.router.isActive('/module/refer/redeem', false) ||
            this.router.isActive('/module/refer-questions', false) ||
            this.router.isActive('/module/gift/sent', false) ||
            this.router.isActive('/module/add-account', false) ||
            this.router.isActive('/module/club/home', false) ||
            this.router.isActive('/module/club/terms', false) ||
            this.router.isActive('/module/club/faq', false) ||
            this.cacheStorage().isBusiness();
    }
    menuHidden() {
        return this.router.isActive('/module/change-plan/confirm', false) ||
            this.router.isActive('/module/data-plan/success', false) ||
            this.router.isActive('/module/change-plan/success', false) ||
            this.router.isActive('/module/gift/sent', false) ||
            this.router.isActive('/module/payment/step1', false) ||
            this.router.isActive('/module/payment/step2', false);
    }
    backButtonHidden() {
        return this.router.isActive('/home/dashboard', false) ||
            (this.router.isActive('/module/consumption', false) && this.cacheStorage().isBusiness());
    }
};
HeaderComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_2__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] }
];
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/header/header.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./header.component.scss */ "./src/app/pages/_shared/components/header/header.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_2__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"]])
], HeaderComponent);



/***/ }),

/***/ "./src/app/pages/_shared/components/loading/loading.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/_shared/components/loading/loading.component.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL19zaGFyZWQvY29tcG9uZW50cy9sb2FkaW5nL2xvYWRpbmcuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/_shared/components/loading/loading.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/_shared/components/loading/loading.component.ts ***!
  \***********************************************************************/
/*! exports provided: LoadingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingComponent", function() { return LoadingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/utils.service */ "./src/app/services/utils.service.ts");



const GENERAL_MESSAGE = 'Espere un momento mientras es procesada la información.';
let LoadingComponent = class LoadingComponent {
    constructor(service) {
        this.service = service;
        this.message = GENERAL_MESSAGE;
        this.show = false;
        this.service.showLoader.subscribe((data) => {
            this.show = data.show;
            this.message = data.message ? data.message : GENERAL_MESSAGE;
        });
    }
    ngOnInit() { }
};
LoadingComponent.ctorParameters = () => [
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_2__["UtilsService"] }
];
LoadingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-loading',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./loading.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/loading/loading.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./loading.component.scss */ "./src/app/pages/_shared/components/loading/loading.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_utils_service__WEBPACK_IMPORTED_MODULE_2__["UtilsService"]])
], LoadingComponent);



/***/ }),

/***/ "./src/app/pages/_shared/components/menu/menu.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/pages/_shared/components/menu/menu.component.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mySideNav {\n  height: auto;\n  /* 100% Full-height */\n  width: 100%;\n  /* 0 width - change this with JavaScript */\n  position: absolute;\n  /* Stay in place */\n  z-index: 10000;\n  /* Stay on top */\n  top: 84px;\n  right: 100%;\n  overflow-x: auto;\n  /* Disable horizontal scroll */\n  transition: 0.4s;\n  /* 0.5 second transition effect to slide in the sidenav */\n  -webkit-transition: 0.4s;\n  -moz-transition: 0.4s;\n  -o-transition: 0.4s;\n  overflow-y: auto;\n  border-bottom: 1px solid #dddddd;\n  margin-bottom: 54px;\n  /* margin for footer height */\n}\n\n.mySideNav {\n  margin-top: -1px;\n}\n\n.mySideNav {\n  top: 50px;\n}\n\n@media only screen and (orientation: landscape) {\n  .mySideNav {\n    top: 49px;\n    /* fixed header for ios version */\n  }\n}\n\n/* iphone X and iphone Xs*/\n\n@media only screen and (min-device-width: 375px) and (max-device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone Xr TEST*/\n\n@media only screen and (min-device-width: 375px) and (max-device-height: 812px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone Xr */\n\n@media only screen and (min-device-width: 414px) and (max-device-height: 896px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n/* iphone Xs Max */\n\n@media only screen and (min-device-width: 414px) and (max-device-height: 896px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait) {\n  .mySideNav {\n    top: 84px;\n  }\n}\n\n.and {\n  margin-top: -35px !important;\n}\n\n.theRed {\n  color: #ef3829 !important;\n}\n\n.theWhite {\n  color: #fcf7f7 !important;\n}\n\n.menuIsOpen {\n  right: 0;\n}\n\n.menuIsClosed {\n  right: 100%;\n}\n\n.m-menuin,\n.m-menubar {\n  height: 50px;\n  width: 100%;\n  border-top: 1px solid #ffffff;\n  float: left;\n  position: relative;\n  font-size: 19.2px;\n  color: #ffffff;\n  padding: 0 20px;\n  font-family: \"Roboto\", sans-serif;\n  font-weight: 400;\n  text-decoration: none;\n  background-color: #ef3829;\n}\n\n.m-menuin {\n  color: #222222;\n  background-color: #ffffff;\n  font-size: 14px;\n  border-top: 1px solid #ffffff;\n  border-bottom: 2px solid #dddddd;\n}\n\n.m-menuin.lastlink {\n  border: 0;\n}\n\na.m-menuin {\n  color: #222222;\n  text-decoration: none;\n}\n\na.m-menubar {\n  color: #ffffff;\n  text-decoration: none;\n}\n\n.m-menuin,\n.m-menubar {\n  padding-right: 40px;\n}\n\n.m-menubar i {\n  font-size: 20px;\n  line-height: 20px;\n  position: absolute;\n  height: 20px;\n  width: 20px;\n  top: 0;\n  bottom: 0;\n  right: 7px;\n  color: #ffffff;\n  margin: auto;\n}\n\n.m-menuin i {\n  font-size: 16px;\n  line-height: 16px;\n  position: absolute;\n  height: 16px;\n  width: 16px;\n  top: 0;\n  bottom: 0;\n  right: 7px;\n  color: #222222;\n  margin: auto;\n}\n\n.m-menubar i.fa-minus,\n.m-menubar i.fa-plus {\n  right: 14px;\n}\n\n.m-menuin i.fa-times-circle {\n  font-size: 18px;\n  line-height: 18px;\n  position: absolute;\n  height: 18px;\n  width: 18px;\n  right: 11px;\n  color: #b7202e;\n}\n\n.m-menubar.open-ins i.fa-plus,\n.m-menubar i.minus {\n  display: none;\n}\n\n.m-menubar.open-ins i.fa-minus {\n  display: inline;\n}\n\n.m-menubar i.fa-times-circle {\n  right: 13px;\n}\n\n.m-menubar.important {\n  background-color: #b7202e;\n}\n\n.mclosex {\n  font-size: 22px;\n  line-height: 22px;\n  height: 22px;\n  width: 22px;\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  right: 7px;\n  margin: auto;\n  color: #abaead;\n  cursor: pointer;\n}\n\n.mc-logo.mcenter {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  margin: auto;\n  width: 123px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZG1pbi9Eb2N1bWVudHMvRSRHUy9SRUJSTkFESU5HL1Jlc3BhbGRvcy9pb25pYy9taWNsYXJvMy1pb25pYy12ZXJzaW9uL3NyYy9hcHAvcGFnZXMvX3NoYXJlZC9jb21wb25lbnRzL21lbnUvbWVudS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcGFnZXMvX3NoYXJlZC9jb21wb25lbnRzL21lbnUvbWVudS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFBYyxxQkFBQTtFQUNkLFdBQUE7RUFBYSwwQ0FBQTtFQUNiLGtCQUFBO0VBQW9CLGtCQUFBO0VBQ3BCLGNBQUE7RUFBZ0IsZ0JBQUE7RUFDaEIsU0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUFrQiw4QkFBQTtFQUNsQixnQkFBQTtFQUFrQix5REFBQTtFQUNsQix3QkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdDQUFBO0VBQ0EsbUJBQUE7RUFBcUIsNkJBQUE7QUNRekI7O0FETEE7RUFDSSxnQkFBQTtBQ1FKOztBRExBO0VBQ0ksU0FBQTtBQ1FKOztBRExBO0VBQ0k7SUFDSSxTQUFBO0lBQ0EsaUNBQUE7RUNRTjtBQUNGOztBRExBLDBCQUFBOztBQUNBO0VBQ0k7SUFDSSxTQUFBO0VDT047QUFDRjs7QURKQSxrQkFBQTs7QUFDQTtFQUNJO0lBQ0ksU0FBQTtFQ01OO0FBQ0Y7O0FESEEsY0FBQTs7QUFDQTtFQUNJO0lBQ0ksU0FBQTtFQ0tOO0FBQ0Y7O0FERkEsa0JBQUE7O0FBQ0E7RUFDSTtJQUNJLFNBQUE7RUNJTjtBQUNGOztBRERBO0VBQ0ksNEJBQUE7QUNHSjs7QURBQTtFQUNJLHlCQUFBO0FDR0o7O0FEREE7RUFDSSx5QkFBQTtBQ0lKOztBRERBO0VBQ0ksUUFBQTtBQ0lKOztBREZBO0VBQ0ksV0FBQTtBQ0tKOztBREhBOztFQUVJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUNBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7QUNNSjs7QURIQTtFQUNJLGNBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSw2QkFBQTtFQUNBLGdDQUFBO0FDTUo7O0FESEE7RUFDSSxTQUFBO0FDTUo7O0FESEE7RUFDSSxjQUFBO0VBQ0EscUJBQUE7QUNNSjs7QURIQTtFQUNJLGNBQUE7RUFDQSxxQkFBQTtBQ01KOztBREhBOztFQUVJLG1CQUFBO0FDTUo7O0FESEE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7QUNNSjs7QURIQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxNQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQ01KOztBREhBOztFQUVJLFdBQUE7QUNNSjs7QURIQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtBQ01KOztBREhBOztFQUVJLGFBQUE7QUNNSjs7QURIQTtFQUNJLGVBQUE7QUNNSjs7QURIQTtFQUNJLFdBQUE7QUNNSjs7QURIQTtFQUNJLHlCQUFBO0FDTUo7O0FESEE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDTUo7O0FESEE7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQ01KIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvX3NoYXJlZC9jb21wb25lbnRzL21lbnUvbWVudS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5teVNpZGVOYXYge1xuICAgIGhlaWdodDogYXV0bzsgLyogMTAwJSBGdWxsLWhlaWdodCAqL1xuICAgIHdpZHRoOiAxMDAlOyAvKiAwIHdpZHRoIC0gY2hhbmdlIHRoaXMgd2l0aCBKYXZhU2NyaXB0ICovXG4gICAgcG9zaXRpb246IGFic29sdXRlOyAvKiBTdGF5IGluIHBsYWNlICovXG4gICAgei1pbmRleDogMTAwMDA7IC8qIFN0YXkgb24gdG9wICovXG4gICAgdG9wOiA4NHB4O1xuICAgIHJpZ2h0OiAxMDAlO1xuICAgIG92ZXJmbG93LXg6IGF1dG87IC8qIERpc2FibGUgaG9yaXpvbnRhbCBzY3JvbGwgKi9cbiAgICB0cmFuc2l0aW9uOiAwLjRzOyAvKiAwLjUgc2Vjb25kIHRyYW5zaXRpb24gZWZmZWN0IHRvIHNsaWRlIGluIHRoZSBzaWRlbmF2ICovXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAwLjRzO1xuICAgIC1tb3otdHJhbnNpdGlvbjogMC40cztcbiAgICAtby10cmFuc2l0aW9uOiAwLjRzO1xuICAgIG92ZXJmbG93LXk6IGF1dG87XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZGRkZGQ7XG4gICAgbWFyZ2luLWJvdHRvbTogNTRweDsgLyogbWFyZ2luIGZvciBmb290ZXIgaGVpZ2h0ICovXG59XG5cbi5teVNpZGVOYXYge1xuICAgIG1hcmdpbi10b3A6IC0xcHg7XG59XG5cbi5teVNpZGVOYXYge1xuICAgIHRvcDogNTBweDtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAob3JpZW50YXRpb24gOiBsYW5kc2NhcGUpe1xuICAgIC5teVNpZGVOYXYge1xuICAgICAgICB0b3A6IDQ5cHg7XG4gICAgICAgIC8qIGZpeGVkIGhlYWRlciBmb3IgaW9zIHZlcnNpb24gKi9cbiAgICB9XG59XG5cbi8qIGlwaG9uZSBYIGFuZCBpcGhvbmUgWHMqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogMzc1cHgpIGFuZCAobWF4LWRldmljZS1oZWlnaHQ6IDgxMnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAzKSAgYW5kIChvcmllbnRhdGlvbiA6IHBvcnRyYWl0KSB7XG4gICAgLm15U2lkZU5hdiB7XG4gICAgICAgIHRvcDogODRweDtcbiAgICB9XG59XG5cbi8qIGlwaG9uZSBYciBURVNUKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDM3NXB4KSBhbmQgKG1heC1kZXZpY2UtaGVpZ2h0OiA4MTJweCkgYW5kICgtd2Via2l0LWRldmljZS1waXhlbC1yYXRpbzogMikgIGFuZCAob3JpZW50YXRpb24gOiBwb3J0cmFpdCkge1xuICAgIC5teVNpZGVOYXYge1xuICAgICAgICB0b3A6IDg0cHg7XG4gICAgfVxufVxuXG4vKiBpcGhvbmUgWHIgKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDQxNHB4KSBhbmQgKG1heC1kZXZpY2UtaGVpZ2h0OiA4OTZweCkgYW5kICgtd2Via2l0LWRldmljZS1waXhlbC1yYXRpbzogMikgIGFuZCAob3JpZW50YXRpb24gOiBwb3J0cmFpdCkge1xuICAgIC5teVNpZGVOYXYge1xuICAgICAgICB0b3A6IDg0cHg7XG4gICAgfVxufVxuXG4vKiBpcGhvbmUgWHMgTWF4ICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiA0MTRweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogODk2cHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDMpICBhbmQgKG9yaWVudGF0aW9uIDogcG9ydHJhaXQpIHtcbiAgICAubXlTaWRlTmF2IHtcbiAgICAgICAgdG9wOiA4NHB4O1xuICAgIH1cbn1cblxuLmFuZHtcbiAgICBtYXJnaW4tdG9wOiAtMzVweCAhaW1wb3J0YW50O1xufVxuXG4udGhlUmVke1xuICAgIGNvbG9yOiAjZWYzODI5ICFpbXBvcnRhbnQ7XG59XG4udGhlV2hpdGV7XG4gICAgY29sb3I6IGhzbCgwLCA1MCUsIDk4JSkgIWltcG9ydGFudDtcbn1cblxuLm1lbnVJc09wZW57XG4gICAgcmlnaHQ6IDA7XG59XG4ubWVudUlzQ2xvc2Vke1xuICAgIHJpZ2h0OiAxMDAlO1xufVxuLm0tbWVudWluLFxuLm0tbWVudWJhciB7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZmZmZmZmO1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBmb250LXNpemU6IDE5LjJweDtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBwYWRkaW5nOiAwIDIwcHg7XG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8nLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlZjM4Mjk7XG59XG5cbi5tLW1lbnVpbiB7XG4gICAgY29sb3I6ICMyMjIyMjI7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNmZmZmZmY7XG4gICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNkZGRkZGQ7XG59XG5cbi5tLW1lbnVpbi5sYXN0bGluayB7XG4gICAgYm9yZGVyOiAwO1xufVxuXG5hLm0tbWVudWluIHtcbiAgICBjb2xvcjogIzIyMjIyMjtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbmEubS1tZW51YmFyIHtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi5tLW1lbnVpbixcbi5tLW1lbnViYXIge1xuICAgIHBhZGRpbmctcmlnaHQ6IDQwcHg7XG59XG5cbi5tLW1lbnViYXIgaSB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgdG9wOiAwO1xuICAgIGJvdHRvbTogMDtcbiAgICByaWdodDogN3B4O1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIG1hcmdpbjogYXV0bztcbn1cblxuLm0tbWVudWluIGkge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBsaW5lLWhlaWdodDogMTZweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAxNnB4O1xuICAgIHdpZHRoOiAxNnB4O1xuICAgIHRvcDogMDtcbiAgICBib3R0b206IDA7XG4gICAgcmlnaHQ6IDdweDtcbiAgICBjb2xvcjogIzIyMjIyMjtcbiAgICBtYXJnaW46IGF1dG87XG59XG5cbi5tLW1lbnViYXIgaS5mYS1taW51cyxcbi5tLW1lbnViYXIgaS5mYS1wbHVzIHtcbiAgICByaWdodDogMTRweDtcbn1cblxuLm0tbWVudWluIGkuZmEtdGltZXMtY2lyY2xlIHtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogMThweDtcbiAgICB3aWR0aDogMThweDtcbiAgICByaWdodDogMTFweDtcbiAgICBjb2xvcjogI2I3MjAyZTtcbn1cblxuLm0tbWVudWJhci5vcGVuLWlucyBpLmZhLXBsdXMsXG4ubS1tZW51YmFyIGkubWludXMge1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5tLW1lbnViYXIub3Blbi1pbnMgaS5mYS1taW51cyB7XG4gICAgZGlzcGxheTogaW5saW5lO1xufVxuXG4ubS1tZW51YmFyIGkuZmEtdGltZXMtY2lyY2xlIHtcbiAgICByaWdodDogMTNweDtcbn1cblxuLm0tbWVudWJhci5pbXBvcnRhbnQge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNiNzIwMmU7XG59XG5cbi5tY2xvc2V4IHtcbiAgICBmb250LXNpemU6IDIycHg7XG4gICAgbGluZS1oZWlnaHQ6IDIycHg7XG4gICAgaGVpZ2h0OiAyMnB4O1xuICAgIHdpZHRoOiAyMnB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIHJpZ2h0OiA3cHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIGNvbG9yOiAjYWJhZWFkO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLm1jLWxvZ28ubWNlbnRlciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICBib3R0b206IDA7XG4gICAgbGVmdDogMDtcbiAgICByaWdodDogMDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgd2lkdGg6IDEyM3B4O1xufVxuIiwiLm15U2lkZU5hdiB7XG4gIGhlaWdodDogYXV0bztcbiAgLyogMTAwJSBGdWxsLWhlaWdodCAqL1xuICB3aWR0aDogMTAwJTtcbiAgLyogMCB3aWR0aCAtIGNoYW5nZSB0aGlzIHdpdGggSmF2YVNjcmlwdCAqL1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8qIFN0YXkgaW4gcGxhY2UgKi9cbiAgei1pbmRleDogMTAwMDA7XG4gIC8qIFN0YXkgb24gdG9wICovXG4gIHRvcDogODRweDtcbiAgcmlnaHQ6IDEwMCU7XG4gIG92ZXJmbG93LXg6IGF1dG87XG4gIC8qIERpc2FibGUgaG9yaXpvbnRhbCBzY3JvbGwgKi9cbiAgdHJhbnNpdGlvbjogMC40cztcbiAgLyogMC41IHNlY29uZCB0cmFuc2l0aW9uIGVmZmVjdCB0byBzbGlkZSBpbiB0aGUgc2lkZW5hdiAqL1xuICAtd2Via2l0LXRyYW5zaXRpb246IDAuNHM7XG4gIC1tb3otdHJhbnNpdGlvbjogMC40cztcbiAgLW8tdHJhbnNpdGlvbjogMC40cztcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZGRkZGQ7XG4gIG1hcmdpbi1ib3R0b206IDU0cHg7XG4gIC8qIG1hcmdpbiBmb3IgZm9vdGVyIGhlaWdodCAqL1xufVxuXG4ubXlTaWRlTmF2IHtcbiAgbWFyZ2luLXRvcDogLTFweDtcbn1cblxuLm15U2lkZU5hdiB7XG4gIHRvcDogNTBweDtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSkge1xuICAubXlTaWRlTmF2IHtcbiAgICB0b3A6IDQ5cHg7XG4gICAgLyogZml4ZWQgaGVhZGVyIGZvciBpb3MgdmVyc2lvbiAqL1xuICB9XG59XG4vKiBpcGhvbmUgWCBhbmQgaXBob25lIFhzKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDM3NXB4KSBhbmQgKG1heC1kZXZpY2UtaGVpZ2h0OiA4MTJweCkgYW5kICgtd2Via2l0LWRldmljZS1waXhlbC1yYXRpbzogMykgYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpIHtcbiAgLm15U2lkZU5hdiB7XG4gICAgdG9wOiA4NHB4O1xuICB9XG59XG4vKiBpcGhvbmUgWHIgVEVTVCovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiAzNzVweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogODEycHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KSB7XG4gIC5teVNpZGVOYXYge1xuICAgIHRvcDogODRweDtcbiAgfVxufVxuLyogaXBob25lIFhyICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiA0MTRweCkgYW5kIChtYXgtZGV2aWNlLWhlaWdodDogODk2cHgpIGFuZCAoLXdlYmtpdC1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KSB7XG4gIC5teVNpZGVOYXYge1xuICAgIHRvcDogODRweDtcbiAgfVxufVxuLyogaXBob25lIFhzIE1heCAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogNDE0cHgpIGFuZCAobWF4LWRldmljZS1oZWlnaHQ6IDg5NnB4KSBhbmQgKC13ZWJraXQtZGV2aWNlLXBpeGVsLXJhdGlvOiAzKSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkge1xuICAubXlTaWRlTmF2IHtcbiAgICB0b3A6IDg0cHg7XG4gIH1cbn1cbi5hbmQge1xuICBtYXJnaW4tdG9wOiAtMzVweCAhaW1wb3J0YW50O1xufVxuXG4udGhlUmVkIHtcbiAgY29sb3I6ICNlZjM4MjkgIWltcG9ydGFudDtcbn1cblxuLnRoZVdoaXRlIHtcbiAgY29sb3I6ICNmY2Y3ZjcgIWltcG9ydGFudDtcbn1cblxuLm1lbnVJc09wZW4ge1xuICByaWdodDogMDtcbn1cblxuLm1lbnVJc0Nsb3NlZCB7XG4gIHJpZ2h0OiAxMDAlO1xufVxuXG4ubS1tZW51aW4sXG4ubS1tZW51YmFyIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNmZmZmZmY7XG4gIGZsb2F0OiBsZWZ0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGZvbnQtc2l6ZTogMTkuMnB4O1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogMCAyMHB4O1xuICBmb250LWZhbWlseTogXCJSb2JvdG9cIiwgc2Fucy1zZXJpZjtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWYzODI5O1xufVxuXG4ubS1tZW51aW4ge1xuICBjb2xvcjogIzIyMjIyMjtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2ZmZmZmZjtcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNkZGRkZGQ7XG59XG5cbi5tLW1lbnVpbi5sYXN0bGluayB7XG4gIGJvcmRlcjogMDtcbn1cblxuYS5tLW1lbnVpbiB7XG4gIGNvbG9yOiAjMjIyMjIyO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbmEubS1tZW51YmFyIHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuLm0tbWVudWluLFxuLm0tbWVudWJhciB7XG4gIHBhZGRpbmctcmlnaHQ6IDQwcHg7XG59XG5cbi5tLW1lbnViYXIgaSB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAyMHB4O1xuICB3aWR0aDogMjBweDtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiA3cHg7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBtYXJnaW46IGF1dG87XG59XG5cbi5tLW1lbnVpbiBpIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMTZweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDE2cHg7XG4gIHdpZHRoOiAxNnB4O1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDdweDtcbiAgY29sb3I6ICMyMjIyMjI7XG4gIG1hcmdpbjogYXV0bztcbn1cblxuLm0tbWVudWJhciBpLmZhLW1pbnVzLFxuLm0tbWVudWJhciBpLmZhLXBsdXMge1xuICByaWdodDogMTRweDtcbn1cblxuLm0tbWVudWluIGkuZmEtdGltZXMtY2lyY2xlIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsaW5lLWhlaWdodDogMThweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDE4cHg7XG4gIHdpZHRoOiAxOHB4O1xuICByaWdodDogMTFweDtcbiAgY29sb3I6ICNiNzIwMmU7XG59XG5cbi5tLW1lbnViYXIub3Blbi1pbnMgaS5mYS1wbHVzLFxuLm0tbWVudWJhciBpLm1pbnVzIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuLm0tbWVudWJhci5vcGVuLWlucyBpLmZhLW1pbnVzIHtcbiAgZGlzcGxheTogaW5saW5lO1xufVxuXG4ubS1tZW51YmFyIGkuZmEtdGltZXMtY2lyY2xlIHtcbiAgcmlnaHQ6IDEzcHg7XG59XG5cbi5tLW1lbnViYXIuaW1wb3J0YW50IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2I3MjAyZTtcbn1cblxuLm1jbG9zZXgge1xuICBmb250LXNpemU6IDIycHg7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICBoZWlnaHQ6IDIycHg7XG4gIHdpZHRoOiAyMnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICByaWdodDogN3B4O1xuICBtYXJnaW46IGF1dG87XG4gIGNvbG9yOiAjYWJhZWFkO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cbi5tYy1sb2dvLm1jZW50ZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogMTIzcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/_shared/components/menu/menu.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/_shared/components/menu/menu.component.ts ***!
  \*****************************************************************/
/*! exports provided: MenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuComponent", function() { return MenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _models_access_filter__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../models/access.filter */ "./src/app/models/access.filter.ts");










let MenuComponent = class MenuComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_3__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.router = router;
        this.access = [];
        this.close = true;
        this.isGuest = false;
    }
    ngOnInit() {
        this.access = this.cacheStorage().access;
        this.utils.openMenu.subscribe(open => {
            this.close = !open;
            if (!open) {
                this.selectedSection = undefined;
            }
        });
        this.isGuest = this.cacheStorage().isGuest;
    }
    going(page) {
        if (this.isGuest && !page.allowAsGuest) {
            this.utils.openMenu.emit(false);
            this.showAlertAccessLimited();
        }
        else {
            if (page.accessID === 20) {
                this.utils.openMenu.emit(false);
                this.openStore();
            }
            else if (page.accessID === 8) {
                this.utils.confirmCloseSession();
            }
            else {
                if (!this.isGuest && !this.services.isAccountDetailsUpdated() && this.pageIsNeedingSubscribers(page)) {
                    const account = this.cacheStorage().accountInfo.bANField;
                    const subscriber = this.cacheStorage().accountInfo.defaultSubscriberField;
                    this.showProgress();
                    this.services.loadAccount(String(account), subscriber, this.cacheStorage().tokenSession, this.cacheStorage().isGuest)
                        .then(() => {
                        this.dismissProgress();
                        this.services.setAccountDetailsUpdate();
                        this.filterAndGo(page);
                    });
                }
                else {
                    this.filterAndGo(page);
                }
            }
        }
    }
    pageIsNeedingSubscribers(page) {
        for (const route of _models_access_filter__WEBPACK_IMPORTED_MODULE_9__["AccessFilter"].routesToGo) {
            if (page.accessID === route.accessID && route.path !== '') {
                if (route.needSubscribers) {
                    return true;
                }
            }
        }
        return false;
    }
    filterAndGo(page) {
        _models_access_filter__WEBPACK_IMPORTED_MODULE_9__["AccessFilter"].routesToGo.forEach(route => {
            if (page.accessID === route.accessID && route.path !== '') {
                this.goPage('module/' + route.path);
                return;
            }
        });
    }
    showingMenu(index) {
        if (this.selectedSection !== index) {
            this.selectedSection = index;
        }
        else {
            this.selectedSection = undefined;
        }
    }
    isActive(page) {
        return _models_access_filter__WEBPACK_IMPORTED_MODULE_9__["AccessFilter"].isRouteActive(page.accessID, this.router.url);
    }
};
MenuComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_2__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] }
];
MenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-menu',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./menu.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/menu/menu.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./menu.component.scss */ "./src/app/pages/_shared/components/menu/menu.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], src_app_services_services_provider__WEBPACK_IMPORTED_MODULE_2__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], src_app_services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"]])
], MenuComponent);



/***/ }),

/***/ "./src/app/pages/_shared/components/popup/general-terms/popup-general-terms.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/_shared/components/popup/general-terms/popup-general-terms.component.scss ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".termgbg {\n  width: 100%;\n  height: auto;\n  float: left;\n  background-color: #f2f2f2;\n  border: 1px solid #cccccc;\n  padding: 16px;\n}\n\n.termr-t {\n  width: 100%;\n  height: 225px;\n  overflow-y: scroll;\n  float: left;\n  background-color: #ffffff;\n  padding: 14px;\n  border: 1px solid #cccccc;\n  text-align: justify;\n  font-size: 11px;\n  color: #222222;\n  font-family: \"Roboto\", sans-serif;\n  font-weight: 400;\n}\n\n.termr-t, .text-terms {\n  white-space: pre-line;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZG1pbi9Eb2N1bWVudHMvRSRHUy9SRUJSTkFESU5HL1Jlc3BhbGRvcy9pb25pYy9taWNsYXJvMy1pb25pYy12ZXJzaW9uL3NyYy9hcHAvcGFnZXMvX3NoYXJlZC9jb21wb25lbnRzL3BvcHVwL2dlbmVyYWwtdGVybXMvcG9wdXAtZ2VuZXJhbC10ZXJtcy5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcGFnZXMvX3NoYXJlZC9jb21wb25lbnRzL3BvcHVwL2dlbmVyYWwtdGVybXMvcG9wdXAtZ2VuZXJhbC10ZXJtcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0FDQ0Y7O0FERUE7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGlDQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURFQTtFQUNFLHFCQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9fc2hhcmVkL2NvbXBvbmVudHMvcG9wdXAvZ2VuZXJhbC10ZXJtcy9wb3B1cC1nZW5lcmFsLXRlcm1zLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRlcm1nYmcge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBmbG9hdDogbGVmdDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjY2NjYztcbiAgcGFkZGluZzogMTZweDtcbn1cblxuLnRlcm1yLXQge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyMjVweDtcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xuICBmbG9hdDogbGVmdDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogMTRweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjY2NjYztcbiAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBjb2xvcjogIzIyMjIyMjtcbiAgZm9udC1mYW1pbHk6ICdSb2JvdG8nLCBzYW5zLXNlcmlmO1xuICBmb250LXdlaWdodDogNDAwO1xufVxuXG4udGVybXItdCwgLnRleHQtdGVybXMge1xuICB3aGl0ZS1zcGFjZTogcHJlLWxpbmU7XG59IiwiLnRlcm1nYmcge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBmbG9hdDogbGVmdDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjY2NjYztcbiAgcGFkZGluZzogMTZweDtcbn1cblxuLnRlcm1yLXQge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyMjVweDtcbiAgb3ZlcmZsb3cteTogc2Nyb2xsO1xuICBmbG9hdDogbGVmdDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgcGFkZGluZzogMTRweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjY2NjYztcbiAgdGV4dC1hbGlnbjoganVzdGlmeTtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBjb2xvcjogIzIyMjIyMjtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG59XG5cbi50ZXJtci10LCAudGV4dC10ZXJtcyB7XG4gIHdoaXRlLXNwYWNlOiBwcmUtbGluZTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/_shared/components/popup/general-terms/popup-general-terms.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/_shared/components/popup/general-terms/popup-general-terms.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: PopupGeneralTermsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopupGeneralTermsComponent", function() { return PopupGeneralTermsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../base.page */ "./src/app/pages/base.page.ts");









let PopupGeneralTermsComponent = class PopupGeneralTermsComponent extends _base_page__WEBPACK_IMPORTED_MODULE_8__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.close = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    closeTerms() {
        this.close.emit();
    }
};
PopupGeneralTermsComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_5__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_2__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__["IntentProvider"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], PopupGeneralTermsComponent.prototype, "close", void 0);
PopupGeneralTermsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-popup-general-terms',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./popup-general-terms.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/popup/general-terms/popup-general-terms.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./popup-general-terms.component.scss */ "./src/app/pages/_shared/components/popup/general-terms/popup-general-terms.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_5__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_2__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_7__["IntentProvider"]])
], PopupGeneralTermsComponent);



/***/ }),

/***/ "./src/app/pages/_shared/components/popup/gift-received/gift-received.component.scss":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/_shared/components/popup/gift-received/gift-received.component.scss ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".termr-t, .text-terms {\n  white-space: pre-line;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZG1pbi9Eb2N1bWVudHMvRSRHUy9SRUJSTkFESU5HL1Jlc3BhbGRvcy9pb25pYy9taWNsYXJvMy1pb25pYy12ZXJzaW9uL3NyYy9hcHAvcGFnZXMvX3NoYXJlZC9jb21wb25lbnRzL3BvcHVwL2dpZnQtcmVjZWl2ZWQvZ2lmdC1yZWNlaXZlZC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcGFnZXMvX3NoYXJlZC9jb21wb25lbnRzL3BvcHVwL2dpZnQtcmVjZWl2ZWQvZ2lmdC1yZWNlaXZlZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9fc2hhcmVkL2NvbXBvbmVudHMvcG9wdXAvZ2lmdC1yZWNlaXZlZC9naWZ0LXJlY2VpdmVkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRlcm1yLXQsIC50ZXh0LXRlcm1zIHtcbiAgICB3aGl0ZS1zcGFjZTogcHJlLWxpbmU7XG59IiwiLnRlcm1yLXQsIC50ZXh0LXRlcm1zIHtcbiAgd2hpdGUtc3BhY2U6IHByZS1saW5lO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/_shared/components/popup/gift-received/gift-received.component.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/_shared/components/popup/gift-received/gift-received.component.ts ***!
  \*****************************************************************************************/
/*! exports provided: GiftReceivedComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GiftReceivedComponent", function() { return GiftReceivedComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let GiftReceivedComponent = class GiftReceivedComponent {
    constructor() {
        this.product = {};
        this.close = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    closePopup() {
        this.close.emit();
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], GiftReceivedComponent.prototype, "product", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], GiftReceivedComponent.prototype, "close", void 0);
GiftReceivedComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-popup-gift-received',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./gift-received.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/_shared/components/popup/gift-received/gift-received.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./gift-received.component.scss */ "./src/app/pages/_shared/components/popup/gift-received/gift-received.component.scss")).default]
    })
], GiftReceivedComponent);



/***/ }),

/***/ "./src/app/pages/_shared/shared.module.ts":
/*!************************************************!*\
  !*** ./src/app/pages/_shared/shared.module.ts ***!
  \************************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var src_app_pages_shared_components_loading_loading_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/pages/_shared/components/loading/loading.component */ "./src/app/pages/_shared/components/loading/loading.component.ts");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/pages/_shared/components/header/header.component.ts");
/* harmony import */ var _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/footer/footer.component */ "./src/app/pages/_shared/components/footer/footer.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _components_menu_menu_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/menu/menu.component */ "./src/app/pages/_shared/components/menu/menu.component.ts");
/* harmony import */ var _components_account_select_account_select_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/account-select/account-select.component */ "./src/app/pages/_shared/components/account-select/account-select.component.ts");
/* harmony import */ var _components_header_static_header_static_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/header-static/header-static.component */ "./src/app/pages/_shared/components/header-static/header-static.component.ts");
/* harmony import */ var _components_popup_gift_received_gift_received_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/popup/gift-received/gift-received.component */ "./src/app/pages/_shared/components/popup/gift-received/gift-received.component.ts");
/* harmony import */ var _components_popup_general_terms_popup_general_terms_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/popup/general-terms/popup-general-terms.component */ "./src/app/pages/_shared/components/popup/general-terms/popup-general-terms.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");














let SharedModule = class SharedModule {
};
SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            src_app_pages_shared_components_loading_loading_component__WEBPACK_IMPORTED_MODULE_3__["LoadingComponent"],
            _components_header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"],
            _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_5__["FooterComponent"],
            _components_menu_menu_component__WEBPACK_IMPORTED_MODULE_8__["MenuComponent"],
            _components_account_select_account_select_component__WEBPACK_IMPORTED_MODULE_9__["AccountSelectComponent"],
            _components_header_static_header_static_component__WEBPACK_IMPORTED_MODULE_10__["HeaderStaticComponent"],
            _components_popup_gift_received_gift_received_component__WEBPACK_IMPORTED_MODULE_11__["GiftReceivedComponent"],
            _components_popup_general_terms_popup_general_terms_component__WEBPACK_IMPORTED_MODULE_12__["PopupGeneralTermsComponent"]
        ],
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_13__["IonicModule"]
        ],
        exports: [
            src_app_pages_shared_components_loading_loading_component__WEBPACK_IMPORTED_MODULE_3__["LoadingComponent"],
            _components_header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"],
            _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_5__["FooterComponent"],
            _components_menu_menu_component__WEBPACK_IMPORTED_MODULE_8__["MenuComponent"],
            _components_account_select_account_select_component__WEBPACK_IMPORTED_MODULE_9__["AccountSelectComponent"],
            _components_header_static_header_static_component__WEBPACK_IMPORTED_MODULE_10__["HeaderStaticComponent"],
            _components_popup_gift_received_gift_received_component__WEBPACK_IMPORTED_MODULE_11__["GiftReceivedComponent"],
            _components_popup_general_terms_popup_general_terms_component__WEBPACK_IMPORTED_MODULE_12__["PopupGeneralTermsComponent"]
        ]
    })
], SharedModule);



/***/ }),

/***/ "./src/app/pages/base.page.ts":
/*!************************************!*\
  !*** ./src/app/pages/base.page.ts ***!
  \************************************/
/*! exports provided: BasePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BasePage", function() { return BasePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _utils_const_pages__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils/const/pages */ "./src/app/utils/const/pages.ts");




class BasePage {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        this.router = router;
        this.storage = storage;
        this.modelsServices = modelsServices;
        this.alertController = alertController;
        this.utilsService = utilsService;
        this.userStorage = userStorage;
        this.visited = [];
        this.services = modelsServices;
        this.utils = utilsService;
        if (this.utilsService.isMenuOpen) {
            this.utilsService.openMenu.emit(false);
        }
    }
    getAlert() {
        return this.alertController;
    }
    cacheStorage() {
        return this.userStorage;
    }
    goPage(page) {
        this.utils.goTo(page);
    }
    goLoginPage(guest = false) {
        if (guest) {
            this.utils.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_3__["pages"].GUEST);
        }
        else {
            this.utils.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_3__["pages"].LOGIN);
        }
    }
    goHomePage() {
        if (this.cacheStorage().isBusiness()) {
            this.clearStore();
            this.utils.goTo('module/consumption');
        }
        else {
            this.utils.goTo('home/dashboard');
        }
    }
    goBack() {
        this.utils.back();
    }
    openStore() {
        this.utils.registerScreen('store');
        const url = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].STORE_URL;
        this.openExternalBrowser(url);
    }
    /**
     * openExternalBrowser:
     *   to open navigation web in a external browser like safari or chrome
     *   this method is called immediately
     */
    openExternalBrowser(url) {
        this.utils.browserProvider.openExternalBrowser(url);
    }
    store(key, value) {
        return new Promise(resolve => {
            this.utils.storage.set(key, value).then(() => {
                console.log('stored key ' + key);
                resolve();
            });
        });
    }
    fetch(key) {
        return this.utils.storage.get(key);
    }
    clearStore() {
        return this.utils.storage.clear();
    }
    showProgress(message) {
        this.utilsService.showLoader.emit({ show: true, message });
    }
    dismissProgress() {
        this.utilsService.showLoader.emit({ show: false, message: undefined });
    }
    selectAccount(account, fromLogin = false) {
        this.utils.resetTimer.emit(true);
        this.showProgress();
        const authenticateInfo = this.userStorage.loginData;
        this.userStorage.prepaidCustomerId = undefined;
        this.userStorage.hasConfirmDue = false;
        this.userStorage.accountListRefer = undefined;
        this.services.loadAccount(account.account, account.subsriberByDefault, this.userStorage.tokenSession, this.userStorage.isGuest)
            .then(() => {
            if (_utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].isPrepaid(account.accountType, account.accountSubType)) {
                this.cacheStorage().prepaidSelectedSubscriber = account.subsriberByDefault;
            }
            this.dismissProgress();
            this.continueSelectAccount(authenticateInfo, fromLogin);
        }, error => {
            this.dismissProgress();
            this.showError(error.message).then();
        });
    }
    continueSelectAccount(authenticateInfo, fromLogin) {
        if (authenticateInfo.requiredAccountUpdate && !this.userStorage.isGuest) {
            this.goPage('update/username');
        }
        else if (authenticateInfo.requiredQuestions && !this.userStorage.isGuest) {
            this.showConfirmCustom('', 'Por tu seguridad, debes configurar tus Preguntas de Seguridad.', 'Configurar ahora', 'Configurar luego', () => {
                this.goPage('update/questions');
            }, () => {
                if (fromLogin && this.utils.biometricOptions.activated) {
                    this.goPage('touch');
                }
                else {
                    this.goHomePage();
                }
            }).then();
        }
        else {
            if (fromLogin && this.utils.biometricOptions.activated) {
                this.goPage('touch');
            }
            else {
                this.goHomePage();
            }
        }
    }
    reloadCurrentAccount() {
        const account = this.userStorage.accountInfo.bANField;
        const subscriber = this.userStorage.accountInfo.defaultSubscriberField;
        return this.services.loadAccount(String(account), subscriber, this.userStorage.tokenSession, this.userStorage.isGuest);
    }
    goPageNoAssociated() {
        this.goPage('/home/no-associated');
    }
    showError(message, onDismiss) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Aviso',
                message,
                buttons: [{
                        text: 'Ok',
                        handler: () => {
                            if (onDismiss) {
                                onDismiss();
                            }
                        }
                    }],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showAlert(message, onDismiss, okText) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                message,
                buttons: [{
                        text: okText === undefined ? 'Ok' : okText,
                        handler: () => {
                            if (onDismiss) {
                                onDismiss();
                            }
                        }
                    }],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showConfirm(header, message, onAccept, onCancel) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header,
                message,
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        handler: () => {
                            if (onCancel) {
                                onCancel();
                            }
                        }
                    },
                    {
                        text: 'Aceptar',
                        handler: () => {
                            if (onAccept) {
                                onAccept();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showConfirmCustom(header, message, acceptButton, cancelButton, onAccept, onCancel) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header,
                message,
                buttons: [
                    {
                        text: cancelButton,
                        role: 'cancel',
                        handler: () => {
                            if (onCancel) {
                                onCancel();
                            }
                        }
                    },
                    {
                        text: acceptButton,
                        handler: () => {
                            if (onAccept) {
                                onAccept();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showChat(message, onChat, onDismiss) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'chat-alert',
                message: '        <div class="basicrow f-black f-mildt roboto-m">\n' +
                    '            <img style="width: 40px; height: 40px;" src="assets/images/miclaro-icon.png" alt="">\n' +
                    '            Bienvenido a Mi <b>Claro Asistencia</b>\n' +
                    '        </div>\n' +
                    '\n' +
                    '        <div class="basicrow m-top-i">\n' +
                    '            <div class="logline full"></div>\n' +
                    '        </div>\n' +
                    '\n' +
                    '        <div class="basicrow f-reg f-black roboto-r m-top-i text-justify" id="chat-error-text">\n' +
                    '            <b>Incidencia:</b> ' + message + '\n' +
                    '        </div>\n' +
                    '\n' +
                    '        <div class="basicrow m-top-i">\n' +
                    '            <div class="logline full"></div>\n' +
                    '        </div>\n' +
                    '\n' +
                    '        <div class="basicrow f-reg f-black roboto-r m-top-ii text-justify">\n' +
                    '            ¿Desea que uno de nuestros agentes de servicio al cliente te asista en línea a través de nuestro Chat?\n' +
                    '        </div>',
                buttons: [
                    {
                        text: 'Conectar Via Chat',
                        handler: () => {
                            if (onChat) {
                                onChat();
                            }
                        }
                    },
                    {
                        text: 'No, Gracias',
                        role: 'cancel',
                        handler: () => {
                            if (onDismiss) {
                                onDismiss();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showAlertSessionExpired(onDismiss) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Sesión Expirada',
                message: 'Estimado cliente su sesión ha expirado.',
                buttons: [{
                        text: 'Ok',
                        handler: () => {
                            if (onDismiss) {
                                onDismiss();
                            }
                            location.reload();
                        }
                    }],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showAlertPleaseLogin(onDismiss) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const self = this;
            const alert = yield this.alertController.create({
                header: 'Acceso Restringido',
                message: 'Estimado cliente, para ingresar a este y otros modulos, debes iniciar sesión.',
                buttons: [
                    {
                        text: 'Ir al inicio de sesión',
                        handler: () => {
                            self.goLoginPage();
                        }
                    },
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        handler: () => {
                            if (onDismiss) {
                                onDismiss();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showAlertOnlyOnWeb(onDismiss) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const self = this;
            const alert = yield this.alertController.create({
                header: 'Acceso Restringido',
                message: 'Estimado cliente, este servicio solo se encuentra disponible en  <b>Mi Claro Web</b>.',
                buttons: [
                    {
                        text: 'Ir a Mi Claro Web',
                        handler: () => {
                            self.openExternalBrowser('https://miclaro.claropr.com/');
                        }
                    },
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        handler: () => {
                            if (onDismiss) {
                                onDismiss();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showAlertAccessLimited(onOpenRegister, onOpenLogin, onDismiss) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                message: 'Actualmente, estas en modo de usuario invitado. Si eres el dueño de la cuenta, debes autenticarte y/o registrarte para esta y otras secciones transacciones solo disponibles para el administrador.',
                buttons: [
                    {
                        text: 'Ir a registro',
                        handler: () => {
                            if (onOpenRegister) {
                                onOpenRegister();
                            }
                            this.goPage('register/step1');
                        }
                    },
                    {
                        text: 'Autenticarse',
                        handler: () => {
                            if (onOpenLogin) {
                                onOpenLogin();
                            }
                            this.goPage('login');
                        }
                    },
                    {
                        text: 'Continuar como invitado',
                        role: 'cancel',
                        handler: () => {
                            if (onDismiss) {
                                onDismiss();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    redirectUserToModule(url, browser) {
        browser.close(); // Close el browser
        console.log(`CHAT - redirect to: ${url}`);
        if (url === 'https://tienda.claropr.com/' || url === 'https://miclaro.claropr.com/') {
            this.openExternalBrowser(url);
            return;
        }
        const isGuest = this.cacheStorage().isGuest;
        const isLogged = this.cacheStorage().logged;
        const typeBan = {
            postpaid: this.cacheStorage().isPostpaidAccount,
            prepaid: this.cacheStorage().isPrepaidAccount,
            telephony: this.cacheStorage().isTelephonyAccount
        };
        /**
         * Go to accounts manage
         */
        if (url.endsWith('account_management/1')) {
            if (isLogged) {
                if (!isGuest) {
                    this.goToPageModule('add-account');
                }
                else {
                    this.showAlertAccessLimited();
                }
            }
            else {
                this.showAlertPleaseLogin();
            }
        }
        else 
        /**
         * Go to payment history
         */
        if (url.endsWith('bills-history')) {
            if (isLogged) {
                if (!isGuest) {
                    if (!typeBan.prepaid) {
                        this.goToPageModule('invoice-summary');
                    }
                    else {
                        this.showAlert('Estimado cliente, este servicio no se encuentra disponible para clientes prepago');
                    }
                }
                else {
                    this.showAlertAccessLimited();
                }
            }
            else {
                this.showAlertPleaseLogin();
            }
        }
        else 
        /**
         * Go to change plan
         */
        if (url.endsWith('changeplan')) {
            if (isLogged) {
                if (!isGuest) {
                    this.goToPageModule('change-plan');
                }
                else {
                    this.showAlertAccessLimited();
                }
            }
            else {
                this.showAlertPleaseLogin();
            }
        }
        else 
        /**
         * Go to claro club
         */
        if (url.endsWith('club')) {
            if (isLogged) {
                this.goToPageModule('club/home');
            }
            else {
                this.showAlertPleaseLogin();
            }
        }
        else 
        /**
         * Go to purchase additional data
         */
        if (url.endsWith('datapurchase')) {
            if (isLogged) {
                if (!isGuest) {
                    if (!typeBan.prepaid) {
                        this.goToPageModule('data-plan');
                    }
                    else {
                        this.showAlert('Estimado cliente, este servicio no se encuentra disponible para clientes prepago');
                    }
                }
                else {
                    this.showAlertAccessLimited();
                }
            }
            else {
                this.showAlertPleaseLogin();
            }
        }
        else 
        /**
         * Go to dashboard
         */
        if (url.endsWith('home')) {
            if (isLogged) {
                this.goHomePage();
            }
            else {
                this.showAlertPleaseLogin();
            }
        }
        else 
        /**
         * Go to profile manage
         */
        if (url.endsWith('modifyaccounts/1')) {
            if (isLogged) {
                if (!isGuest) {
                    this.goToPageModule('profile');
                }
                else {
                    this.showAlertAccessLimited();
                }
            }
            else {
                this.showAlertPleaseLogin();
            }
        }
        else 
        /**
         * Go to netflix
         */
        if (url.endsWith('netflix')) {
            if (!_utils_utils__WEBPACK_IMPORTED_MODULE_1__["Utils"].getPlatformInfo().ios) {
                if (isLogged) {
                    if (!isGuest) {
                        this.goToPageModule('netflix');
                    }
                    else {
                        this.showAlertAccessLimited();
                    }
                }
                else {
                    this.showAlertPleaseLogin();
                }
            }
            else {
                this.showAlertOnlyOnWeb();
            }
        }
        else 
        /**
         * Go to recharge prepaid
         */
        if (url.includes('prepaidrecharge')) {
            if (isLogged) {
                if (typeBan.prepaid) {
                    this.goToPageModule('recharge');
                }
                else {
                    this.showAlert('Estimado cliente, este servicio solo se encuentra disponible para clientes prepago');
                }
            }
            else {
                this.showAlertPleaseLogin();
            }
        }
        else 
        /**
         * Go to referred system
         */
        if (url.endsWith('referred')) {
            if (isLogged) {
                if (!isGuest) {
                    if (!typeBan.prepaid) {
                        this.goToPageModule('refer/home');
                    }
                    else {
                        this.showAlert('Estimado cliente, este servicio no se encuentra disponible para clientes prepago');
                    }
                }
                else {
                    this.showAlertAccessLimited();
                }
            }
            else {
                this.showAlertPleaseLogin();
            }
        }
        else 
        /**
         * Go to device info
         */
        if (url.endsWith('servicesandequipment')) {
            if (isLogged) {
                if (!isGuest) {
                    if (!typeBan.prepaid) {
                        this.goToPageModule('device');
                    }
                    else {
                        this.showAlert('Estimado cliente, este servicio no se encuentra disponible para clientes prepago');
                    }
                }
                else {
                    this.showAlertAccessLimited();
                }
            }
            else {
                this.showAlertPleaseLogin();
            }
        }
        else 
        /**
         * Go to consumption
         */
        if (url.endsWith('usage')) {
            if (isLogged) {
                if (typeBan.prepaid) {
                    this.goToPageModule('consumption/prepaid');
                }
                else {
                    this.goToPageModule('consumption');
                }
            }
            else {
                this.showAlertPleaseLogin();
            }
        }
        /**
         * if url dont have any recognition
         */
        else {
            this.showAlert('Estimado cliente, el modulo al cual desea acceder no se encuentra disponible en este momento.');
        }
    }
    goToPageModule(path) {
        this.goPage(`module/${path}`);
    }
    appReset() {
        this.userStorage.redirectData = undefined;
        this.userStorage.isAppReady = false;
    }
    showAlertRateApp(later, evaluate, noMore) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Califica nuestra aplicación',
                message: '¿Estás disfrutando de Mi Claro PR? Tómese un momento para calificarla en la tienda de aplicaciones.',
                buttons: [
                    {
                        text: 'Calificar ahora',
                        cssClass: 'primary-button-rate-dialog',
                        handler: () => {
                            if (evaluate) {
                                evaluate();
                            }
                        }
                    },
                    {
                        text: 'Luego',
                        cssClass: 'secondary-button-rate-dialog',
                        handler: () => {
                            if (later()) {
                                later();
                            }
                        }
                    },
                    {
                        text: 'No volver a mostrar',
                        cssClass: 'secondary-button-rate-dialog',
                        handler: () => {
                            if (noMore) {
                                noMore();
                            }
                        }
                    },
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
}


/***/ }),

/***/ "./src/app/pages/dummy/dummy.component.scss":
/*!**************************************************!*\
  !*** ./src/app/pages/dummy/dummy.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2R1bW15L2R1bW15LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/dummy/dummy.component.ts":
/*!************************************************!*\
  !*** ./src/app/pages/dummy/dummy.component.ts ***!
  \************************************************/
/*! exports provided: DummyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DummyComponent", function() { return DummyComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let DummyComponent = class DummyComponent {
    constructor() { }
    ngOnInit() { }
};
DummyComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-dummy',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./dummy.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dummy/dummy.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./dummy.component.scss */ "./src/app/pages/dummy/dummy.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], DummyComponent);



/***/ }),

/***/ "./src/app/pages/login/guest/guest.component.scss":
/*!********************************************************!*\
  !*** ./src/app/pages/login/guest/guest.component.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xvZ2luL2d1ZXN0L2d1ZXN0LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/login/guest/guest.component.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/login/guest/guest.component.ts ***!
  \******************************************************/
/*! exports provided: GuestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuestComponent", function() { return GuestComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _services_redirect_provider__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../services/redirect.provider */ "./src/app/services/redirect.provider.ts");











let GuestComponent = class GuestComponent extends _base_page__WEBPACK_IMPORTED_MODULE_6__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage, redirectProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.redirectProvider = redirectProvider;
        this.utils.registerScreen('guest');
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const isLogged = yield this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.IS_LOGGED);
            const isGuest = yield this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.IS_GUEST);
            const subscriber = yield this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.SUBSCRIBER);
            if (isLogged && isGuest && subscriber) {
                this.number = subscriber;
                this.login();
            }
            else {
                this.redirectProvider.checkIfRedirectIsWaiting();
            }
        });
    }
    login() {
        if (String(this.number).length !== 10) {
            this.showError('Debe ingresar un número de suscriptor válido.').then();
        }
        else {
            this.showProgress();
            this.services.loginGuest(String(this.number)).then((success) => {
                this.cacheStorage().loginData = success;
                this.cacheStorage().loginData.enterAsGuest = true;
                if (success.requiredPasswordReset) {
                    this.dismissProgress();
                    this.goPage('recover/step5');
                }
                else {
                    this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.SUBSCRIBER, String(this.number));
                    this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.IS_LOGGED, true);
                    this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.IS_GUEST, true);
                    this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.BIOMETRIC, false);
                    this.selectAccount(this.cacheStorage().getAccountByBan(success.account));
                    this.utils.storeDataForWidgets(success.account, success.subscriber, success.productType);
                }
            }, error => {
                this.dismissProgress();
                if (error.desc && error.desc.includes('device token incorrecto')) {
                    this.update();
                }
                else {
                    this.validateUser();
                }
            });
        }
    }
    validateUser() {
        this.showProgress();
        this.services.validateUser(String(this.number)).then((success) => {
            this.dismissProgress();
            if (!success.AccountExist) {
                this.showError('El número de teléfono ingresado no se encuentra registrado en nuestros sistemas, ' +
                    'su formato es incorrecto o no pertenece a nuestra red. Por favor intente nuevamente.').then();
            }
            else {
                if (success.userExist) {
                    this.update();
                }
                else {
                    this.register();
                }
            }
        }, error => {
            this.dismissProgress();
            this.showError(error.message).then();
        });
    }
    register() {
        this.showProgress();
        this.services.registerGuest(String(this.number)).then((success) => {
            this.dismissProgress();
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].REGISTER.SUBSCRIBER, String(this.number));
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].REGISTER.TOKEN, success.token);
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].REGISTER.IS_GUEST_UPDATE, false);
            this.goPage('/register/guest');
        }, error => {
            this.dismissProgress();
            this.showError(error.message).then();
        });
    }
    update() {
        this.showProgress();
        this.services.resendGuestCode(String(this.number)).then((success) => {
            this.dismissProgress();
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].REGISTER.SUBSCRIBER, String(this.number));
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].REGISTER.TOKEN, success.token);
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].REGISTER.IS_GUEST_UPDATE, true);
            this.goPage('/register/guest');
        }, error => {
            this.dismissProgress();
            this.showError(error.message).then();
        });
    }
};
GuestComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] },
    { type: _services_redirect_provider__WEBPACK_IMPORTED_MODULE_10__["RedirectProvider"] }
];
GuestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login-guest',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./guest.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/guest/guest.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./guest.component.scss */ "./src/app/pages/login/guest/guest.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"], _services_redirect_provider__WEBPACK_IMPORTED_MODULE_10__["RedirectProvider"]])
], GuestComponent);



/***/ }),

/***/ "./src/app/pages/login/login/login.component.scss":
/*!********************************************************!*\
  !*** ./src/app/pages/login/login/login.component.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xvZ2luL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/login/login/login.component.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/login/login/login.component.ts ***!
  \******************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_native_keychain_touch_id_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/keychain-touch-id/ngx */ "./node_modules/@ionic-native/keychain-touch-id/ngx/index.js");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var _services_redirect_provider__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../services/redirect.provider */ "./src/app/services/redirect.provider.ts");
/* harmony import */ var _services_storage_provider__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../../services/storage.provider */ "./src/app/services/storage.provider.ts");
/* harmony import */ var _services_browser_provider__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../../services/browser.provider */ "./src/app/services/browser.provider.ts");
















const DEFAULT_LOGIN_OPTIONS = {
    isLogged: false,
    keep: false,
    tryAgain: false,
    biometric: false,
    username: undefined,
    password: undefined
};
let LoginComponent = class LoginComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_3__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage, biometric, redirectProvider, storageProvider, zone, browserProvider) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.biometric = biometric;
        this.redirectProvider = redirectProvider;
        this.storageProvider = storageProvider;
        this.zone = zone;
        this.browserProvider = browserProvider;
        this.username = '';
        this.password = '';
        this.biometricAvailable = true;
        this.keepAuthActivated = false;
        this.biometricActivated = false;
        this.showKeepAuthenticate = true;
        this.showBiometricLogin = true;
        this.biometricAccepted = false;
        this.isTouchBiometric = true;
        this.ios = _utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPlatformInfo().ios;
        this.utils.registerScreen('login');
    }
    ngOnInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.zone.runOutsideAngular(() => {
                document.getElementById('password').addEventListener('change', (event) => {
                    this.password = event.target['value'];
                });
                document.getElementById('username').addEventListener('change', (event) => {
                    this.username = event.target['value'];
                });
            });
            this.loginStore = Object.assign({}, DEFAULT_LOGIN_OPTIONS);
            const isLogged = yield this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.IS_LOGGED);
            if (isLogged !== undefined && isLogged !== null) {
                this.loginStore.isLogged = isLogged;
            }
            const keep = yield this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.KEEP);
            if (keep !== undefined && keep !== null) {
                this.loginStore.keep = keep;
            }
            const tryAgain = yield this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.TRY_IN);
            if (tryAgain !== undefined && tryAgain !== null) {
                this.loginStore.tryAgain = tryAgain;
            }
            const biometric = yield this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.BIOMETRIC);
            if (biometric !== undefined && biometric !== null) {
                this.loginStore.biometric = biometric;
            }
            const username = yield this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.USERNAME);
            if (username !== undefined && username !== null) {
                this.loginStore.username = username;
            }
            const password = yield this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.PASSWORD);
            if (password !== undefined && password !== null) {
                this.loginStore.password = password;
            }
            this.biometricAvailable = this.utils.biometricOptions.available;
            this.isTouchBiometric = this.utils.biometricOptions.type === 'touch';
            yield this.validateKeepData();
        });
    }
    validateKeepData() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.loginStore.isLogged) {
                if (this.loginStore.keep || this.loginStore.tryAgain) {
                    this.username = this.loginStore.username;
                    this.password = this.loginStore.password;
                    this.keepAuthActivated = true;
                    this.showBiometricLogin = false;
                    this.showKeepAuthenticate = true;
                    yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.TRY_IN, false);
                    this.login();
                }
                else {
                    if (this.loginStore.biometric && this.utils.biometricOptions.available) {
                        this.verifyBiometricIdentity();
                    }
                }
            }
            else {
                this.redirectProvider.checkIfRedirectIsWaiting();
            }
        });
    }
    verifyBiometricIdentity() {
        let text = '';
        let textError = '';
        if (this.utils.biometricOptions.type === 'face') {
            text = 'Ponga su rostro por favor';
            textError = 'Por favor coloque el rostro que usa para desbloquear su dispositivo, ' +
                'de caso contrario seleccione cancelar para desactivar el Face ID.';
        }
        else {
            text = 'Ponga su huella dactilar por favor';
            textError = `Por favor coloque la huella que usa para desbloquear su dispositivo, ' +
                'de caso contrario seleccione cancelar para desactivar el ${this.ios ? 'Touch ID' : 'Fingerprint'}.`;
        }
        this.biometric.verify(_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].BIOMETRIC_SECRET_KEY, text).then((res) => {
            this.username = this.loginStore.username;
            this.password = this.loginStore.password;
            this.biometricActivated = true;
            this.biometricAccepted = true;
            this.onMarkBiometric();
            this.login();
        }, (error) => {
            this.biometricAccepted = false;
            this.showConfirmCustom('', textError, 'Volver a Intentar', 'Cancelar', () => {
                this.verifyBiometricIdentity();
            }, () => {
                this.clearStore().then(() => {
                    this.loginStore = DEFAULT_LOGIN_OPTIONS;
                });
            });
        });
    }
    identityEdited() {
        if (this.biometricAccepted) {
            this.biometricActivated = false;
            this.biometricAccepted = false;
        }
    }
    login() {
        this.username = this.username.trim();
        this.password = this.password.trim();
        this.utils.hideKeyboard();
        if (!this.username || !this.password) {
            this.showError('Debe ingresar su usuario y contraseña.').then();
            return;
        }
        this.showProgress();
        this.services.login(this.username, this.password).then((success) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.checkRateApp();
            if (!_utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].isBusinessAccount(success.accountType)) {
                this.cacheStorage().loginData = success;
                if (success.requiredPasswordReset) {
                    yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].RECOVER_PASSWORD.TOKEN, success.token);
                    this.dismissProgress();
                    this.goPage('recover/step5');
                }
                else {
                    const isGuest = yield this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.IS_GUEST);
                    yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.USERNAME, this.username);
                    yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.PASSWORD, this.password);
                    yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.KEEP, this.keepAuthActivated);
                    yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.BIOMETRIC, false);
                    if (this.biometricAccepted) {
                        yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.BIOMETRIC, true);
                    }
                    yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.IS_LOGGED, true);
                    if (isGuest && !this.keepAuthActivated) {
                        yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.IS_GUEST, true);
                    }
                    else {
                        yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.IS_GUEST, false);
                    }
                    this.utils.biometricOptions.activated = this.biometricActivated;
                    this.utils.biometricOptions.password = this.password;
                    this.selectAccount(this.cacheStorage().getAccountByBan(success.account), !this.biometricAccepted);
                    if (this.keepAuthActivated || this.biometricActivated) {
                        this.utils.storeDataForWidgets(success.account, success.subscriber, success.productType);
                    }
                }
            }
            else {
                this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.USERNAME, this.username);
                this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.PASSWORD, this.password);
                this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.IS_LOGGED, true);
                this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.IS_GUEST, false);
                this.browserProvider.openExternalBrowser(`${_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].WEB_PAGE_BUSINESS}/login?token=${success.token}`);
                this.dismissProgress();
            }
        }), (error) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const self = this;
            if (error.num === 100 && _utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].isBusinessAccount(error.accountType)) {
                this.browserProvider.openExternalBrowser(`${_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].WEB_PAGE_BUSINESS}/login?token=${error.token}`);
                this.dismissProgress();
            }
            else if (error.num === 30 || error.num === 32) {
                this.dismissProgress();
                this.showChat(error.message, () => {
                    this.redirectProvider.openChat();
                }).then();
            }
            else if (error.num === 41) {
                this.dismissProgress();
                yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].UPDATE_REGISTRATION.TOKEN, error.token);
                yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].UPDATE_REGISTRATION.USERNAME_USED, this.username);
                yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].UPDATE_REGISTRATION.PASSWORD_USED, this.password);
                yield this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].UPDATE_REGISTRATION.KEEP_AUTH, this.keepAuthActivated);
                this.goPage('update/step1');
            }
            else {
                this.dismissProgress();
                if (error.message.includes('credenciales de acceso incorrectas')) {
                    yield this.clearStore();
                }
                this.showError(error.message).then();
            }
        }));
    }
    onKeepAuthenticated() {
        if (this.keepAuthActivated) {
            this.showConfirm('Mantener Autenticado', 'Usted permanecera conectado hasta que cierre la sesión.', () => {
                this.keepAuthActivated = true;
                this.showBiometricLogin = false;
                this.showKeepAuthenticate = true;
            }, () => {
                this.keepAuthActivated = false;
                this.showBiometricLogin = true;
                this.showKeepAuthenticate = true;
            }).then();
        }
        else {
            this.showBiometricLogin = true;
            this.showKeepAuthenticate = true;
        }
    }
    onMarkBiometric() {
        if (this.biometricActivated) {
            this.showBiometricLogin = true;
            this.showKeepAuthenticate = false;
        }
        else {
            this.showBiometricLogin = true;
            this.showKeepAuthenticate = true;
        }
    }
    checkRateApp() {
        this.storageProvider.isRated()
            .then(isRated => {
            if (!isRated) {
                this.storageProvider.getOpenAppTimes()
                    .then(times => {
                    if (times >= 2) {
                        this.showAlertRateApp(() => { }, () => {
                            this.storageProvider.setRated(true).then();
                            this.openAppStore();
                        }, () => {
                            this.storageProvider.setRated(true).then();
                        });
                        this.storageProvider.setOpenAppTimes(0).then();
                    }
                    else {
                        this.storageProvider.increaseOpenAppTimes().then();
                    }
                });
            }
        });
    }
    openAppStore() {
        if (_utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPlatformInfo().android) {
            this.openExternalBrowser(`market://details?id=${_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].PACKET_NAME}`);
        }
        else if (_utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPlatformInfo().ios) {
            this.openExternalBrowser('https://apps.apple.com/us/app/mi-claro-pr/id775322054');
        }
        else {
            this.openExternalBrowser('https://play.google.com/store/apps/details?id=com.todoclaro.miclaroapp');
        }
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_2__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] },
    { type: _ionic_native_keychain_touch_id_ngx__WEBPACK_IMPORTED_MODULE_11__["KeychainTouchId"] },
    { type: _services_redirect_provider__WEBPACK_IMPORTED_MODULE_13__["RedirectProvider"] },
    { type: _services_storage_provider__WEBPACK_IMPORTED_MODULE_14__["StorageProvider"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _services_browser_provider__WEBPACK_IMPORTED_MODULE_15__["BrowserProvider"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login/login.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.component.scss */ "./src/app/pages/login/login/login.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_2__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"], _ionic_native_keychain_touch_id_ngx__WEBPACK_IMPORTED_MODULE_11__["KeychainTouchId"],
        _services_redirect_provider__WEBPACK_IMPORTED_MODULE_13__["RedirectProvider"], _services_storage_provider__WEBPACK_IMPORTED_MODULE_14__["StorageProvider"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"], _services_browser_provider__WEBPACK_IMPORTED_MODULE_15__["BrowserProvider"]])
], LoginComponent);



/***/ }),

/***/ "./src/app/pages/login/touch/touch.component.scss":
/*!********************************************************!*\
  !*** ./src/app/pages/login/touch/touch.component.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xvZ2luL3RvdWNoL3RvdWNoLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/login/touch/touch.component.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/login/touch/touch.component.ts ***!
  \******************************************************/
/*! exports provided: TouchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TouchComponent", function() { return TouchComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/pages/base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_native_keychain_touch_id_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/keychain-touch-id/ngx */ "./node_modules/@ionic-native/keychain-touch-id/ngx/index.js");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../utils/utils */ "./src/app/utils/utils.ts");













let TouchComponent = class TouchComponent extends src_app_pages_base_page__WEBPACK_IMPORTED_MODULE_3__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage, biometric) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.biometric = biometric;
        this.biometricIsTouch = true;
        this.ios = _utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPlatformInfo().ios;
        this.utils.registerScreen('biometric_configuration');
    }
    ngOnInit() {
        this.username = this.cacheStorage().loginData.username;
        this.biometricIsTouch = this.utils.biometricOptions.type !== 'face';
    }
    cancel() {
        this.goHomePage();
    }
    configure() {
        this.biometric.save(_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].BIOMETRIC_SECRET_KEY, this.utils.biometricOptions.password).then((res) => {
            if (this.utils.biometricOptions.ios) {
                this.checkBiometric();
            }
            else {
                this.completeConfiguration();
            }
        }, (error) => {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.BIOMETRIC, false);
            console.log(error);
        });
    }
    checkBiometric() {
        let text = '';
        if (this.utils.biometricOptions.type === 'face') {
            text = 'Ponga su rostro por favor';
        }
        else {
            text = 'Ponga su huella dactilar por favor';
        }
        this.biometric.verify(_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].BIOMETRIC_SECRET_KEY, text).then((res) => {
            this.completeConfiguration();
        }, (error) => {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.BIOMETRIC, false);
            console.log(error);
        });
    }
    completeConfiguration() {
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].LOGIN.BIOMETRIC, true);
        this.goHomePage();
    }
};
TouchComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_2__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] },
    { type: _ionic_native_keychain_touch_id_ngx__WEBPACK_IMPORTED_MODULE_11__["KeychainTouchId"] }
];
TouchComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-touch',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./touch.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/touch/touch.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./touch.component.scss */ "./src/app/pages/login/touch/touch.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_2__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"],
        _ionic_native_keychain_touch_id_ngx__WEBPACK_IMPORTED_MODULE_11__["KeychainTouchId"]])
], TouchComponent);



/***/ }),

/***/ "./src/app/pages/update-app/update-app.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/update-app/update-app.component.ts ***!
  \**********************************************************/
/*! exports provided: UpdateAppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateAppComponent", function() { return UpdateAppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../utils/utils */ "./src/app/utils/utils.ts");











let UpdateAppComponent = class UpdateAppComponent extends _base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }
    ngOnInit() {
    }
    openAppStore() {
        if (_utils_utils__WEBPACK_IMPORTED_MODULE_10__["Utils"].getPlatformInfo().android) {
            this.openExternalBrowser(`market://details?id=${_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].PACKET_NAME}`);
        }
        else if (_utils_utils__WEBPACK_IMPORTED_MODULE_10__["Utils"].getPlatformInfo().ios) {
            this.openExternalBrowser('https://apps.apple.com/us/app/mi-claro-pr/id775322054');
        }
        else {
            this.openExternalBrowser('https://play.google.com/store/apps/details?id=com.todoclaro.miclaroapp');
        }
    }
};
UpdateAppComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_5__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] }
];
UpdateAppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-update',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./update-app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/update-app/update-app.component.html")).default
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_5__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"]])
], UpdateAppComponent);



/***/ }),

/***/ "./src/app/services/alert.provider.ts":
/*!********************************************!*\
  !*** ./src/app/services/alert.provider.ts ***!
  \********************************************/
/*! exports provided: AlertProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertProvider", function() { return AlertProvider; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let AlertProvider = class AlertProvider {
    constructor(alertController) {
        this.alertController = alertController;
    }
    showError(message, onDismiss) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Aviso',
                message,
                buttons: [{
                        text: 'Ok',
                        handler: () => {
                            if (onDismiss) {
                                onDismiss();
                            }
                        }
                    }],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showAlert(message, onDismiss, okText) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                message,
                buttons: [{
                        text: okText === undefined ? 'Ok' : okText,
                        handler: () => {
                            if (onDismiss) {
                                onDismiss();
                            }
                        }
                    }],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showConfirm(header, message, onAccept, onCancel) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header,
                message,
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        handler: () => {
                            if (onCancel) {
                                onCancel();
                            }
                        }
                    },
                    {
                        text: 'Aceptar',
                        handler: () => {
                            if (onAccept) {
                                onAccept();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showConfirmCustom(header, message, acceptButton, cancelButton, onAccept, onCancel) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header,
                message,
                buttons: [
                    {
                        text: cancelButton,
                        role: 'cancel',
                        handler: () => {
                            if (onCancel) {
                                onCancel();
                            }
                        }
                    },
                    {
                        text: acceptButton,
                        handler: () => {
                            if (onAccept) {
                                onAccept();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showChat(message, onChat, onDismiss) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'chat-alert',
                message: '        <div class="basicrow f-black f-mildt roboto-m">\n' +
                    '            <img style="width: 40px; height: 40px;" src="assets/images/miclaro-icon.png" alt="">\n' +
                    '            Bienvenido a Mi <b>Claro Asistencia</b>\n' +
                    '        </div>\n' +
                    '\n' +
                    '        <div class="basicrow m-top-i">\n' +
                    '            <div class="logline full"></div>\n' +
                    '        </div>\n' +
                    '\n' +
                    '        <div class="basicrow f-reg f-black roboto-r m-top-i text-justify" id="chat-error-text">\n' +
                    '            <b>Incidencia:</b> ' + message + '\n' +
                    '        </div>\n' +
                    '\n' +
                    '        <div class="basicrow m-top-i">\n' +
                    '            <div class="logline full"></div>\n' +
                    '        </div>\n' +
                    '\n' +
                    '        <div class="basicrow f-reg f-black roboto-r m-top-ii text-justify">\n' +
                    '            ¿Desea que uno de nuestros agentes de servicio al cliente te asista en línea a través de nuestro Chat?\n' +
                    '        </div>',
                buttons: [
                    {
                        text: 'Conectar Via Chat',
                        handler: () => {
                            if (onChat) {
                                onChat();
                            }
                        }
                    },
                    {
                        text: 'No, Gracias',
                        role: 'cancel',
                        handler: () => {
                            if (onDismiss) {
                                onDismiss();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showAlertSessionExpired(onDismiss) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Sesión Expirada',
                message: 'Estimado cliente su sesión ha expirado.',
                buttons: [{
                        text: 'Ok',
                        handler: () => {
                            if (onDismiss) {
                                onDismiss();
                            }
                            location.reload();
                        }
                    }],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showAlertPleaseLoginOrRegister(onOpenRegisterPage, onCancel) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const self = this;
            const alert = yield this.alertController.create({
                header: 'Acceso Restringido',
                message: 'Estimado cliente, para acceder a este modulo debe ingresar con su usuario y contraseña, si no tiene presione en la opción <b>&#191;No tienes cuenta&#63; Reg&iacute;strate</b> para crear sus datos de acceso.',
                buttons: [
                    {
                        text: 'Registrarse',
                        handler: () => {
                            onOpenRegisterPage();
                        }
                    },
                    {
                        text: 'Cerrar',
                        role: 'cancel',
                        handler: () => {
                            if (onCancel) {
                                onCancel();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showAlertOnlyOnWeb(onOpenWeb, onCancel) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const self = this;
            const alert = yield this.alertController.create({
                header: 'Acceso Restringido',
                message: 'Estimado cliente, este servicio solo se encuentra disponible en  <b>Mi Claro Web</b>.',
                buttons: [
                    {
                        text: 'Ir a Mi Claro Web',
                        handler: () => {
                            onOpenWeb('https://miclaro.claropr.com/');
                        }
                    },
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        handler: () => {
                            if (onCancel) {
                                onCancel();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showAlertAccessLimited(onOpenRegisterPage, onOpenLoginPage, onDismiss) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                message: 'Actualmente, estas en modo de usuario invitado. Si eres el dueño de la cuenta, ' +
                    'debes autenticarte y/o registrarte para esta y otras secciones transacciones solo ' +
                    'disponibles para el administrador.',
                buttons: [
                    {
                        text: 'Ir a registro',
                        handler: () => {
                            onOpenRegisterPage();
                        }
                    },
                    {
                        text: 'Autenticarse',
                        handler: () => {
                            onOpenLoginPage();
                        }
                    },
                    {
                        text: 'Continuar como invitado',
                        role: 'cancel',
                        handler: () => {
                            if (onDismiss) {
                                onDismiss();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    showAlertGoWebOrKeepInApp(openInExternalBrowser, openInApp) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const self = this;
            const alert = yield this.alertController.create({
                message: '¿Desea abrir este enlace en su navegador web o mantener en la app?.',
                buttons: [
                    {
                        text: 'Abrir en el NAVEGADOR',
                        handler: () => {
                            openInExternalBrowser();
                        }
                    },
                    {
                        text: 'Mantener en el APP',
                        role: 'cancel',
                        handler: () => {
                            if (openInApp) {
                                openInApp();
                            }
                        }
                    }
                ],
                backdropDismiss: false
            });
            yield alert.present();
        });
    }
    alertShouldCloseSession(onAccept, onCancel) {
        this.showConfirmCustom('', 'Estimado cliente, para acceder a esta opcion debe cerrar su sesión.', 'Cerra Sesión', 'Cancelar', onAccept, onCancel);
    }
};
AlertProvider.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
];
AlertProvider = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
], AlertProvider);



/***/ }),

/***/ "./src/app/services/browser.provider.ts":
/*!**********************************************!*\
  !*** ./src/app/services/browser.provider.ts ***!
  \**********************************************/
/*! exports provided: BrowserProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrowserProvider", function() { return BrowserProvider; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");




let BrowserProvider = class BrowserProvider {
    constructor(iab) {
        this.iab = iab;
    }
    /**
     * openExternalBrowser:
     *   to open navigation web in a external browser like safari or chrome
     *   this method is called immediately
     */
    openExternalBrowser(url) {
        console.log(`opening external link: ${url}`);
        this.iab.create(url, '_system', {});
    }
    /**
     * openChatBrowser:
     *   to open navigation web in app, promise is called after 'in app web' is closed
     *   this is called in async task
     */
    openChatBrowser(url) {
        let isOpen = false;
        return new Promise((resolve, reject) => {
            const options = {
                location: 'no',
                // @ts-ignore
                fullscreen: 'no',
                hidden: 'yes',
                toolbar: 'no'
            };
            const browser = this.iab.create(url, '_blank', options);
            if (!_utils_utils__WEBPACK_IMPORTED_MODULE_2__["Utils"].getPlatformInfo().desktop) {
                let redirectLink;
                browser.on('loaderror').subscribe((res) => {
                    console.log(res);
                    reject('En este momento no podemos acceder a la pagina solicitada.');
                    browser.close();
                });
                browser.on('loadstart').subscribe((res) => {
                    if (isOpen) {
                        if (res.url.trim() === url.trim()) {
                            redirectLink = undefined;
                            browser.close();
                        }
                        else {
                            redirectLink = res.url;
                            browser.close();
                        }
                    }
                });
                browser.on('loadstop').subscribe((res) => {
                    console.log(res);
                    if (!isOpen) {
                        browser.show();
                    }
                    isOpen = true;
                });
                browser.on('exit').subscribe((res) => {
                    console.log(res);
                    resolve(redirectLink);
                });
            }
            else {
                browser.show();
                resolve();
            }
        });
    }
    /**
     * openPaymentBrowser:
     *   to open navigation web in app, promise is called after 'in app web' is closed
     *   this is called in async task
     */
    openPaymentBrowser(url) {
        let paymentCompleted = false;
        let paymentDone = false;
        let isOpen = false;
        return new Promise((resolve, reject) => {
            const options = {
                location: 'yes',
                hidden: 'yes',
                // @ts-ignore
                fullscreen: 'no',
                toolbarcolor: '#ef3829',
                zoom: 'no',
                closebuttoncolor: '#ffffff',
                closebuttoncaption: 'CERRAR',
                // Only IOS
                presentationstyle: 'fullscreen',
                toolbarposition: 'top',
                hideurlbar: 'yes',
                toolbar: 'yes',
                hidenavigationbuttons: 'yes'
            };
            if (!_utils_utils__WEBPACK_IMPORTED_MODULE_2__["Utils"].getPlatformInfo().android) {
                options.location = 'no';
                options.lefttoright = 'yes';
            }
            const browser = this.iab.create(url, '_blank', options);
            if (!_utils_utils__WEBPACK_IMPORTED_MODULE_2__["Utils"].getPlatformInfo().desktop) {
                browser.on('loaderror').subscribe((res) => {
                    console.log(res);
                    if (!paymentCompleted) {
                        paymentCompleted = true;
                        reject('En este momento no podemos acceder a la pagina solicitada.');
                        browser.close();
                    }
                });
                browser.on('loadstart').subscribe((res) => {
                    console.log(res);
                    if (res.url === 'https://checkout.evertecinc.com/Close.aspx' ||
                        res.url === 'http://miclaroreferals.claroinfo.com/Close-Payments/mmpClosingPyamnet.aspx' ||
                        (res.url === 'https://miclaro.claropr.com/')) {
                        paymentCompleted = true;
                        paymentDone = true;
                        browser.close();
                    }
                    if (res.url.includes('Payment/Confirmation')) {
                        paymentDone = true;
                    }
                });
                browser.on('loadstop').subscribe((res) => {
                    console.log(res);
                    if (!paymentCompleted) {
                        browser.show();
                    }
                    isOpen = true;
                });
                browser.on('exit').subscribe((res) => {
                    console.log(res);
                    resolve(paymentDone);
                });
            }
            else {
                browser.show();
                resolve();
            }
        });
    }
};
BrowserProvider.ctorParameters = () => [
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__["InAppBrowser"] }
];
BrowserProvider = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__["InAppBrowser"]])
], BrowserProvider);



/***/ }),

/***/ "./src/app/services/intent.provider.ts":
/*!*********************************************!*\
  !*** ./src/app/services/intent.provider.ts ***!
  \*********************************************/
/*! exports provided: IntentProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IntentProvider", function() { return IntentProvider; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/utils */ "./src/app/utils/utils.ts");



let IntentProvider = class IntentProvider {
    get isAppReady() {
        return this._isAppReady;
    }
    set isAppReady(value) {
        this._isAppReady = value;
    }
    /* Authenticate Details */
    get loginData() {
        return this._loginData;
    }
    set loginData(value) {
        this._loginData = value;
    }
    get accountList() {
        if (this.loginData) {
            if (this.isGuest) {
                const accounts = [];
                accounts.push(this.getGuestAccount());
                return accounts;
            }
            else {
                return this._loginData.accounts.AccountList;
            }
        }
        else {
            console.log('No authenticated data found');
            return [];
        }
    }
    get tokenSession() {
        if (this._loginData) {
            return this._loginData.token;
        }
        else {
            console.log('No authenticated data found');
            return '';
        }
    }
    get isGuest() {
        if (this._loginData) {
            return this._loginData.enterAsGuest;
        }
        else {
            console.log('No authenticated data found');
            return null;
        }
    }
    get postpaidAccounts() {
        if (this._loginData) {
            const accounts = [];
            const allAccounts = this.accountList;
            allAccounts.forEach((account) => {
                if (_utils_utils__WEBPACK_IMPORTED_MODULE_2__["Utils"].isPostpaid(account.accountType, account.accountSubType, account.productType)) {
                    accounts.push(account);
                }
            });
            return accounts;
        }
        else {
            console.log('No authenticated data found');
            return null;
        }
    }
    get prepaidAccounts() {
        if (this._loginData) {
            const accounts = [];
            const allAccounts = this.accountList;
            allAccounts.forEach((account) => {
                if (_utils_utils__WEBPACK_IMPORTED_MODULE_2__["Utils"].isPrepaid(account.accountType, account.accountSubType)) {
                    accounts.push(account);
                }
            });
            return accounts;
        }
        else {
            console.log('No authenticated data found');
            return null;
        }
    }
    get telephonyAccounts() {
        if (this._loginData) {
            const accounts = [];
            const allAccounts = this.accountList;
            allAccounts.forEach((account) => {
                if (_utils_utils__WEBPACK_IMPORTED_MODULE_2__["Utils"].isTelephony(account.accountType, account.accountSubType, account.productType)) {
                    accounts.push(account);
                }
            });
            return accounts;
        }
        else {
            console.log('No authenticated data found');
            return null;
        }
    }
    getAccountByBan(ban) {
        if (this._loginData) {
            if (this.isGuest) {
                return this.getGuestAccount();
            }
            else {
                return this.accountList.find(x => x.account === ban);
            }
        }
        else {
            console.log('No authenticated data found');
            return null;
        }
    }
    getGuestAccount() {
        const account = {
            account: this._loginData.account,
            accountType: this._loginData.accountType,
            accountSubType: this._loginData.accountSubType,
            subsriberByDefault: this._loginData.subscriber,
            productType: this._loginData.productType
        };
        return account;
    }
    /* Account details vars */
    set accountDetails(value) {
        this._accountDetails = value;
    }
    get accountDetails() {
        return this._accountDetails;
    }
    get accountInfo() {
        if (this._accountDetails) {
            return this._accountDetails.AccounInfo;
        }
        else {
            return null;
        }
    }
    get notifications() {
        return this._accountDetails.Messages.MessagesList;
    }
    get countPendingNotifications() {
        return this._accountDetails.Messages.newMessageCounter;
    }
    get subscriberList() {
        return this._accountDetails.SubscriberInfo;
    }
    get getQualification() {
        return this._accountDetails.qualification;
    }
    get isPostpaidAccount() {
        if (!this.accountInfo) {
            return false;
        }
        const account = this
            .getAccountByBan(String(this.accountInfo.bANField));
        return _utils_utils__WEBPACK_IMPORTED_MODULE_2__["Utils"].isPostpaid(account.accountType, account.accountSubType, account.productType);
    }
    get isPrepaidAccount() {
        if (!this.accountInfo) {
            return false;
        }
        const account = this
            .getAccountByBan(String(this.accountInfo.bANField));
        return _utils_utils__WEBPACK_IMPORTED_MODULE_2__["Utils"].isPrepaid(account.accountType, account.accountSubType);
    }
    get isTelephonyAccount() {
        if (!this.accountInfo) {
            return false;
        }
        const account = this
            .getAccountByBan(String(this.accountInfo.bANField));
        return _utils_utils__WEBPACK_IMPORTED_MODULE_2__["Utils"].isTelephony(account.accountType, account.accountSubType, account.productType);
    }
    get isByop() {
        if (!this.accountInfo) {
            return false;
        }
        const account = this
            .getAccountByBan(String(this.accountInfo.bANField));
        return _utils_utils__WEBPACK_IMPORTED_MODULE_2__["Utils"].isByop(account.accountType, account.accountSubType, account.productType);
    }
    /* User access vars */
    set access(value) {
        this._access = value;
    }
    get access() {
        return this._access;
    }
    getAccessPageByID(id) {
        let aPage = null;
        this.access.forEach((section) => {
            section.Pages.forEach((page) => {
                if (page.accessID === id) {
                    aPage = page;
                    return aPage;
                }
            });
        });
        return aPage;
    }
    get tab() {
        return this._tab;
    }
    set tab(value) {
        this._tab = value;
    }
    get logged() {
        if (this._logged === undefined) {
            return false;
        }
        return this._logged;
    }
    set logged(value) {
        this._logged = value;
    }
    get selectedSubscriber() {
        return this._selectedSubscriber;
    }
    set selectedSubscriber(value) {
        this._selectedSubscriber = value;
    }
    get selectedPlan() {
        return this._selectedPlan;
    }
    set selectedPlan(value) {
        this._selectedPlan = value;
    }
    get selectedPacket() {
        return this._selectedPacket;
    }
    set selectedPacket(value) {
        this._selectedPacket = value;
    }
    get orderInfo() {
        return this._orderInfo;
    }
    set orderInfo(value) {
        this._orderInfo = value;
    }
    get payment() {
        return this._payment;
    }
    set payment(value) {
        this._payment = value;
    }
    get prepaidCustomerId() {
        return this._prepaidCustomerId;
    }
    set prepaidCustomerId(value) {
        this._prepaidCustomerId = value;
    }
    get prepaidSelectedSubscriber() {
        return this._prepaidSelectedSubscriber;
    }
    set prepaidSelectedSubscriber(value) {
        this._prepaidSelectedSubscriber = value;
    }
    getFullCurrentSelectedSubscriber() {
        if (this._loginData) {
            return this.subscriberList.find(x => x.subscriberNumberField === this.prepaidSelectedSubscriber);
        }
        else {
            console.log('No authenticated data found');
            return null;
        }
    }
    get PaymentProcess() {
        return this._PaymentProcess;
    }
    set PaymentProcess(value) {
        this._PaymentProcess = value;
    }
    get hasConfirmDue() {
        return this._hasConfirmDue;
    }
    set hasConfirmDue(value) {
        this._hasConfirmDue = value;
    }
    get accountListRefer() {
        return this._accountListRefer;
    }
    set accountListRefer(value) {
        this._accountListRefer = value;
    }
    containsSubscriber(subscriber) {
        if (this._loginData) {
            const object = this.accountDetails.SubscriberInfo
                .find(x => x.subscriberNumberField === subscriber);
            return !!object;
        }
        else {
            console.log('No authenticated data found');
            return false;
        }
    }
    isBusiness() {
        if (this._loginData) {
            return _utils_utils__WEBPACK_IMPORTED_MODULE_2__["Utils"].isBusinessAccount(this._loginData.accountType);
        }
        else {
            console.log('No authenticated data found');
            return false;
        }
    }
    get redirectData() {
        return this._redirectData;
    }
    set redirectData(value) {
        this._redirectData = value;
    }
};
IntentProvider = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], IntentProvider);



/***/ }),

/***/ "./src/app/services/redirect.provider.ts":
/*!***********************************************!*\
  !*** ./src/app/services/redirect.provider.ts ***!
  \***********************************************/
/*! exports provided: RedirectProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RedirectProvider", function() { return RedirectProvider; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _utils_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _intent_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _browser_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./browser.provider */ "./src/app/services/browser.provider.ts");
/* harmony import */ var _utils_const_pages__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utils/const/pages */ "./src/app/utils/const/pages.ts");
/* harmony import */ var _alert_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./alert.provider */ "./src/app/services/alert.provider.ts");
/* harmony import */ var _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../utils/const/redirect-path.enum */ "./src/app/utils/const/redirect-path.enum.ts");
/* harmony import */ var _utils_const_appConstants__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../utils/const/appConstants */ "./src/app/utils/const/appConstants.ts");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");












let RedirectProvider = class RedirectProvider {
    constructor(router, alertProvider, utilsService, userStorage, modelsServices, browserProvider) {
        this.router = router;
        this.alertProvider = alertProvider;
        this.utilsService = utilsService;
        this.userStorage = userStorage;
        this.modelsServices = modelsServices;
        this.browserProvider = browserProvider;
    }
    /**
     * This method check if a redirect is waiting and then sends user to that module
     */
    checkIfRedirectIsWaiting() {
        if (this.userStorage.redirectData) {
            this.redirect(this.userStorage.redirectData.url);
        }
    }
    redirect(url, fromChat = false) {
        const host = url.replace(_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_9__["APP"].SCHEME, '');
        console.log(`postponing redirect to: ${host}`);
        this.userStorage.redirectData = { url };
        if (this.userStorage.isAppReady) {
            this.manageRedirects(host, fromChat);
        }
    }
    manageRedirects(path, fromChat = false) {
        if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].LOGIN) {
            // GO TO LOGIN
            this.userStorage.redirectData = undefined;
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].CHAT || path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].WIDGET_CHAT) {
            this.redirectToChat();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].HOME) {
            // GO HOME
            if (fromChat) {
                this.redirectToHomeFromChat();
            }
            else {
                this.redirectToHome();
            }
            this.userStorage.redirectData = undefined;
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].INVOICE_SUMMARY || path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].WIDGET_PAY || path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].INVOICE_SUMMARY_BILLS) {
            this.redirectToInvoiceSummary();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].INVOICE_DOWNLOAD) {
            // Descarga tu Factura / postpago - fijo
            this.redirectToInvoiceDownload();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].PAYMENT_HISTORY) {
            // Historial de pagos / postpago - fijo
            this.redirectToInvoiceHistory();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].ELECTRONIC_BILL) {
            // Factura electronica / postpago - fijo
            this.redirectToElectronicBill();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].DIRECT_DEBIT) {
            // Debito Directo / postpago
            this.redirectToDirectDebit();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].CONSUMPTION) {
            // Mi consumo
            this.redirectToConsumption();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].DEVICE) {
            // Mis Equipos y Servicios / postpago
            this.redirectToServicesAndEquipment();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].CHANGE_PLAN) {
            // Cambio de plan
            this.redirectToChangePlan();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].NETFLIX) {
            // Netflix / postpago - Fijo (Android)
            this.redirectToNetflix();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].REFERRED) {
            // Programa Refiere y Gana / postpago - Fijo
            this.redirectToReferrer();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].CLUB) {
            // claro Club / postpago
            this.redirectToClub();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].PURCHASES) {
            // Compras / postpago
            this.redirectToPurchases();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].DATA_PLAN) {
            // Paquetes Adicionales de Datos / postpago
            this.redirectToDataPlan();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].SHOPPING_HISTORY) {
            // Historial de Compras / postpago
            this.redirectToShoppingHistory();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].GIFT_ONE_GB) {
            // Regala 1 GB a un Pospago / postpago
            this.redirectToGiftOneGB();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].GIFT_RECHARGE) {
            // Regala una Recarga Prepago / postpago
            this.redirectToGiftRecharge();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].NOTIFICATIONS) {
            // Notifications
            this.redirectToNotifications();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].PROFILE_1 || path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].WIDGET_MY_ACCOUNT) {
            // Mi Perfil - Información Personal
            this.redirectToProfile(1);
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].PROFILE_2) {
            // Mi Perfil - Actualiza Tu Correo
            this.redirectToProfile(2);
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].PROFILE_3) {
            // Mi Perfil - Cambiar Usuario y Contraseña
            this.redirectToProfile(3);
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].PROFILE_4) {
            // Mi Perfil - Dirección Postal
            this.redirectToProfile(4);
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].PROFILE_5) {
            // Mi Perfil - Preguntas de Seguridad
            this.redirectToProfile(5);
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].MY_ACCOUNTS) {
            // Mis cuentas
            this.redirectToAccountsManage();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].ADD_ACCOUNT) {
            // Mis cuentas
            this.redirectToAccountsManage(1);
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].RECHARGE) {
            // Recarga / prepago
            this.redirectToPrepaidRecharge();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].TRANSACTIONS) {
            // Transacciones / prepago
            this.redirectToPrepaidTransactions();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].FAULT_REPORT) {
            // Reporte de Averias / Fijo
            this.redirectToFaultReport();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].SUPPORT) {
            // SUPPORT
            this.redirectToSupport();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].FORGOT_PASSWORD) {
            // SUPPORT
            this.redirectToRecoverPassword();
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].REGISTER) {
            // SUPPORT
            this.redirectToRegister();
        }
        else if (path.includes('transit')) {
            this.redirectTransit(path);
        }
        else if (path === _utils_const_redirect_path_enum__WEBPACK_IMPORTED_MODULE_8__["RedirectPathEnum"].FREQUENT_QUESTIONS) {
            this.redirectFaq();
        }
    }
    redirectTransit(path) {
        let url = path.replace('transit?url=', '');
        if (this.userStorage.logged) {
            url = `${url}?token=${this.userStorage.tokenSession}`;
        }
        this.browserProvider.openExternalBrowser(url);
        this.userStorage.redirectData = undefined;
    }
    redirectToHome() {
        if (this.userStorage.logged) {
            this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].HOME);
            this.userStorage.redirectData = undefined;
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToHomeFromChat() {
        this.alertProvider.showAlertGoWebOrKeepInApp(() => {
            this.browserProvider.openExternalBrowser(_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_9__["APP"].MI_CLARO_WEB);
            this.userStorage.redirectData = undefined;
        }, () => {
            if (this.userStorage.logged) {
                this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].HOME);
                this.userStorage.redirectData = undefined;
            }
            else {
                this.alertPleaseLoginOrRegister();
            }
        });
    }
    redirectToInvoiceSummary() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].INVOICE_SUMMARY);
            }
            else {
                this.alertNotValidForPrepaid();
            }
            this.userStorage.redirectData = undefined;
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToInvoiceDownload() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].INVOICE_DOWNLOAD);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertNotValidForPrepaid();
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToInvoiceHistory() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].INVOICE_HISTORY);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertNotValidForPrepaid();
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToElectronicBill() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].ELECTRONIC_BILL);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertNotValidForPrepaid();
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToDirectDebit() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].DIRECT_DEBIT);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertValidOnlyForPostpaid();
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToConsumption() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPrepaidAccount) {
                this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].CONSUMPTION_PREPAID);
            }
            else {
                this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].CONSUMPTION);
            }
            this.userStorage.redirectData = undefined;
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToServicesAndEquipment() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].SERVICES_AND_EQUIPMENT);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertNotValidForPrepaid();
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToChangePlan() {
        if (this.userStorage.logged) {
            if (this.userStorage.isGuest) {
                this.alertAccessLimited();
            }
            else {
                this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].CHANGE_PLAN);
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToNetflix() {
        if (_utils_utils__WEBPACK_IMPORTED_MODULE_10__["Utils"].getPlatformInfo().ios) {
            this.alertProvider.showAlertOnlyOnWeb(url => {
                this.browserProvider.openExternalBrowser(url);
            });
        }
        else {
            if (this.userStorage.logged) {
                if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                    if (this.userStorage.isGuest) {
                        this.alertAccessLimited();
                    }
                    else {
                        this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].NETFLIX);
                        this.userStorage.redirectData = undefined;
                    }
                }
                else {
                    this.alertNotValidForPrepaid();
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertPleaseLoginOrRegister();
            }
        }
    }
    redirectToReferrer() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].REFERRER);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertNotValidForPrepaid();
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToClub() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].CLUB);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertValidOnlyForPostpaid();
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToPurchases() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].PURCHASES);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertValidOnlyForPostpaid();
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToDataPlan() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount || this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].DATA_PLAN);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertNotValidForPrepaid();
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToShoppingHistory() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].SHOPPING_HISTORY);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertValidOnlyForPostpaid();
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToGiftOneGB() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].GIFT_ONE_GB);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertValidOnlyForPostpaid();
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToGiftRecharge() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPostpaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].GIFT_RECHARGE);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertValidOnlyForPostpaid();
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToNotifications() {
        if (this.userStorage.logged) {
            this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].NOTIFICATIONS);
            this.userStorage.redirectData = undefined;
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToProfile(index) {
        if (this.userStorage.logged) {
            if (this.userStorage.isGuest) {
                this.alertAccessLimited();
            }
            else {
                switch (index) {
                    case 1:
                        this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].PROFILE_NAME);
                        break;
                    case 2:
                        this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].PROFILE_EMAIL);
                        break;
                    case 3:
                        this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].PROFILE_PASSWORD);
                        break;
                    case 4:
                        this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].PROFILE_POSTAL);
                        break;
                    case 5:
                        this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].PROFILE_QUESTIONS);
                        break;
                    default:
                        this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].PROFILE);
                        break;
                }
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToAccountsManage(index) {
        if (this.userStorage.logged) {
            if (this.userStorage.isGuest) {
                this.alertAccessLimited();
            }
            else {
                if (index === 1) {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].ADD_ACCOUNTS);
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].ACCOUNTS_MANAGE);
                }
                this.userStorage.redirectData = undefined;
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToPrepaidRecharge() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPrepaidAccount) {
                this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].RECHARGE);
                this.userStorage.redirectData = undefined;
            }
            else {
                this.alertValidOnlyForPrepaid();
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToPrepaidTransactions() {
        if (this.userStorage.logged) {
            if (this.userStorage.isPrepaidAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].TRANSACTIONS);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertValidOnlyForPrepaid();
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToFaultReport() {
        if (this.userStorage.logged) {
            if (this.userStorage.isTelephonyAccount) {
                if (this.userStorage.isGuest) {
                    this.alertAccessLimited();
                }
                else {
                    this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].FAULT_REPORT);
                    this.userStorage.redirectData = undefined;
                }
            }
            else {
                this.alertValidOnlyForTelephony();
            }
        }
        else {
            this.alertPleaseLoginOrRegister();
        }
    }
    redirectToSupport() {
        this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].SUPPORT);
        this.userStorage.redirectData = undefined;
    }
    redirectToRecoverPassword() {
        if (this.userStorage.logged) {
            this.confirmCloseSession();
        }
        else {
            this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].RECOVER_PASSWORD);
            this.userStorage.redirectData = undefined;
        }
    }
    redirectToRegister() {
        if (this.userStorage.logged) {
            this.confirmCloseSession();
        }
        else {
            this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].REGISTER);
            this.userStorage.redirectData = undefined;
        }
    }
    redirectFaq() {
        this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].FAQ);
        this.userStorage.redirectData = undefined;
    }
    redirectToChat() {
        this.openChat();
        this.userStorage.redirectData = undefined;
    }
    showProgress(message) {
        this.utilsService.showLoader.emit({ show: true, message });
    }
    dismissProgress() {
        this.utilsService.showLoader.emit({ show: false, message: undefined });
    }
    alertNotValidForPrepaid() {
        this.alertProvider.showAlert('Estimado cliente, este servicio no se encuentra disponible para clientes prepago');
    }
    alertValidOnlyForPostpaid() {
        this.alertProvider.showAlert('Estimado cliente, este servicio se encuentra disponible solo para clientes postpago');
    }
    alertValidOnlyForPrepaid() {
        this.alertProvider.showAlert('Estimado cliente, este servicio se encuentra disponible solo para clientes prepago');
    }
    alertValidOnlyForTelephony() {
        this.alertProvider.showAlert('Estimado cliente, este servicio se encuentra disponible solo para clientes de telefonia fija');
    }
    alertPleaseLoginOrRegister() {
        if (this.router.isActive('/login', false)) {
            this.alertProvider.showAlert('Estimado cliente, para acceder a este modulo debe ingresar con su usuario y contraseña, si no tiene presione en la opción <b>&#191;No tienes cuenta&#63; Reg&iacute;strate</b> para crear sus datos de acceso.');
        }
        else {
            this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].LOGIN);
        }
    }
    confirmCloseSession() {
        this.alertProvider.alertShouldCloseSession(() => {
            this.utilsService.confirmCloseSession(() => {
                // NOTHING TO DO HERE
            }, () => {
                this.userStorage.redirectData = undefined;
            });
        }, () => {
            this.userStorage.redirectData = undefined;
        });
    }
    alertAccessLimited() {
        this.alertProvider.showAlertAccessLimited(() => {
            this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].REGISTER);
        }, () => {
            this.utilsService.goTo(_utils_const_pages__WEBPACK_IMPORTED_MODULE_6__["pages"].LOGIN);
        });
    }
    openChat() {
        let tokenSession = '';
        let tokenSSO = '';
        if (this.userStorage.logged) {
            tokenSession = this.userStorage.loginData.token;
            tokenSSO = this.userStorage.loginData.SSOAccessToken;
        }
        this.showProgress();
        this.modelsServices.chat(tokenSession, tokenSSO)
            .then(response => {
            this.utilsService.registerScreen('chat');
            this.browserProvider.openChatBrowser(response.url).then((redirectLink) => {
                this.dismissProgress();
                if (redirectLink) {
                    this.alertProvider.showConfirm('Estas siendo rediregido a:', redirectLink, () => {
                        this.redirectFromChat(redirectLink);
                    });
                }
            }).catch(err => {
                this.dismissProgress();
                this.alertProvider.showError(err);
            });
        }, error => {
            this.dismissProgress();
            this.alertProvider.showAlert(error.message);
        });
    }
    redirectFromChat(url) {
        const host = url.replace(_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_9__["APP"].MI_CLARO_WEB, '').trim();
        // IF contains web link and any extra path
        if (url.includes(_utils_const_appConstants__WEBPACK_IMPORTED_MODULE_9__["APP"].MI_CLARO_WEB) && host.length > 0) {
            this.redirect(host, true);
        }
        else { // IF contains web link or any other link
            this.browserProvider.openExternalBrowser(url);
        }
    }
};
RedirectProvider.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_11__["Router"] },
    { type: _alert_provider__WEBPACK_IMPORTED_MODULE_7__["AlertProvider"] },
    { type: _utils_service__WEBPACK_IMPORTED_MODULE_2__["UtilsService"] },
    { type: _intent_provider__WEBPACK_IMPORTED_MODULE_3__["IntentProvider"] },
    { type: _services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _browser_provider__WEBPACK_IMPORTED_MODULE_5__["BrowserProvider"] }
];
RedirectProvider = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_11__["Router"],
        _alert_provider__WEBPACK_IMPORTED_MODULE_7__["AlertProvider"],
        _utils_service__WEBPACK_IMPORTED_MODULE_2__["UtilsService"],
        _intent_provider__WEBPACK_IMPORTED_MODULE_3__["IntentProvider"],
        _services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _browser_provider__WEBPACK_IMPORTED_MODULE_5__["BrowserProvider"]])
], RedirectProvider);



/***/ }),

/***/ "./src/app/services/services.provider.ts":
/*!***********************************************!*\
  !*** ./src/app/services/services.provider.ts ***!
  \***********************************************/
/*! exports provided: ServicesProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicesProvider", function() { return ServicesProvider; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _utils_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/device/ngx */ "./node_modules/@ionic-native/device/ngx/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _models_access_filter__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../models/access.filter */ "./src/app/models/access.filter.ts");
/* harmony import */ var _intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/app-version/ngx */ "./node_modules/@ionic-native/app-version/ngx/index.js");












let ServicesProvider = class ServicesProvider {
    constructor(appVersion, util, userStorage, device, googleMaps, geolocation) {
        this.appVersion = appVersion;
        this.util = util;
        this.userStorage = userStorage;
        this.device = device;
        this.googleMaps = googleMaps;
        this.geolocation = geolocation;
        this.location = {
            latitude: '',
            longitude: ''
        };
        this.temporaryStorage = {
            productTypeField: '',
            ticket: '',
            selectedSubscriber: '',
            faultSelectedType: 1,
            faultSelectedLine: 1,
            contact1: '',
            contact2: '',
            description: '',
            success_report_title: '',
            store: {},
            storeList: [],
            storeType: 0,
            coords: {},
            referrerMember: {
                CountAvialable: 0,
                CountExpired: 0,
                CountPending: 0,
                CountRedeems: 0,
                CountReferrs: 0,
                TotalAvailable: 0,
                TotalCredits: 0,
                TotalRedeem: 0,
                TotalReferer: 0,
                account: '',
                accountName: '',
                memberID: 0
            },
            referData: {
                account: '',
                balance: '',
                paperless: true,
                registerUpdated: true,
                solvent: true,
                subscriber: ''
            }
        };
        this.firstGetAccountDetailsLoad = true;
        this.accountDetailsIsUpdated = false;
        this.SO = device.platform;
        this.soVersion = device.version;
        this.UUID = device.uuid;
        this.model = device.model;
        if (_utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].getPlatformInfo().desktop) {
            /***** Only For Web Test *****/
            this.UUID = 'test-uuid-greenSize';
            this.model = 'test-ionic';
            this.version = 'test';
            /***** Only For Web Test *****/
        }
        else {
            this.appVersion.getVersionNumber().then((result) => {
                this.version = result;
            });
        }
    }
    isAccountDetailsUpdated() {
        return this.accountDetailsIsUpdated;
    }
    setAccountDetailsUpdate() {
        this.accountDetailsIsUpdated = true;
    }
    myPosition() {
        return new Promise((resolve, reject) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            try {
                const resp = yield this.geolocation.getCurrentPosition();
                this.location.latitude = resp.coords.latitude.toString();
                this.location.longitude = resp.coords.longitude.toString();
                console.log(this.location);
                resolve(this.location);
            }
            catch (error) {
                console.log('Error getting location', error);
                reject(error);
            }
        }));
    }
    // todo: validate if the method is used
    loadMap(coords) {
        this.mapElement = document.getElementById('store-map');
        const mapOption = {
            camera: {
                target: {
                    lat: coords.latitude,
                    Ing: coords.longitude
                },
                zoom: 18,
                tilt: 30
            }
        };
        this.map = this.googleMaps.create(this.mapElement, mapOption);
        this.map.one(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["GoogleMapsEvent"].MAP_READY).then(() => {
            console.log('Mapa Cargado');
        });
    }
    getToken() {
        const token = localStorage.getItem('token');
        this.token = (token !== 'undefined') ? token : null;
        return this.token;
    }
    login(username, password) {
        const method = 'authenticate';
        const parameters = JSON.stringify({
            Username: btoa(username),
            Password: btoa(password),
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }
    loadAccount(account, subscriber, token, isGuest) {
        return new rxjs__WEBPACK_IMPORTED_MODULE_4__["Observable"](observer => {
            this.getAccountDetailsBestOption(account, subscriber, token).then((details) => {
                details.AccounInfo.defaultSubscriberField = subscriber;
                details.AccounInfo.defaultSubscriberObject =
                    _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].getSubscriberByNumber(subscriber, details.SubscriberInfo);
                this.data = details;
                if (!this.firstGetAccountDetailsLoad) {
                    this.setAccountDetailsUpdate();
                }
                this.firstGetAccountDetailsLoad = false;
                this.getAccess(account, subscriber, token).then((access) => {
                    this.userStorage.accountDetails = details;
                    this.userStorage.access = _models_access_filter__WEBPACK_IMPORTED_MODULE_7__["AccessFilter"].do(access.Sections, isGuest, this.userStorage.accountInfo.accountTypeField, this.userStorage.accountInfo.accountSubtypeField, this.userStorage.accountInfo.defaultSubscriberObject.productTypeField);
                    this.userStorage.logged = true;
                    observer.next(details);
                    observer.complete();
                }, error => {
                    observer.error(error);
                    observer.complete();
                });
            }, error => {
                observer.error(error);
                observer.complete();
            });
        }).toPromise();
    }
    getAccountDetailsBestOption(account, subscriber, token) {
        if (this.firstGetAccountDetailsLoad) {
            return this.getAccountDetailsHome(account, subscriber, token);
        }
        return this.getAccountDetails(account, subscriber, token);
    }
    getAccountDetails(account, subscriber, token) {
        const method = 'getaccountdetails';
        const parameters = JSON.stringify({
            subscriber: btoa(subscriber),
            account: btoa(account),
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getAccountDetailsHome(account, subscriber, token) {
        const method = 'getaccountdetailshome';
        const parameters = JSON.stringify({
            subscriber,
            account,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getAccess(account, subscriber, token) {
        const method = 'getaccess';
        const parameters = JSON.stringify({
            subscriber,
            account,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getChallengeQuestions(subscriber) {
        const method = 'getChallengeQuestions';
        const parameters = JSON.stringify({
            subscriber,
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getChallengeQuestions2(token) {
        const method = 'getChallengeQuestions2';
        const parameters = JSON.stringify({
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    answerSecurityQuestions(subscriber, questions) {
        const method = 'getpasswordrecovery';
        const parameters = JSON.stringify({
            subscriber,
            ResponseList: questions,
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }
    recoveryPasswordBySubscriber(subscriber, sms, email) {
        const method = 'getPasswordRecoveryBySubscriber';
        const parameters = JSON.stringify({
            subscriber,
            sms,
            email,
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }
    passwordUpdate(currentPassword, newPassword, token) {
        const method = 'setPasswordUpdate';
        const parameters = JSON.stringify({
            currentPassword,
            newPassword,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    validateSubscriber(subscriber) {
        const method = 'validateSubscriber';
        const parameters = JSON.stringify({
            subscriber,
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }
    validateSSNAndEmail(subscriber, code, ssn, email) {
        const method = 'registerUserValidateSSNAndEmail';
        const parameters = JSON.stringify({
            Subscriber: subscriber,
            SSN: ssn,
            Code: code,
            Email: email,
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }
    validatePassword(subscriber, code, ssn, email, password) {
        const method = 'registerUser';
        const parameters = JSON.stringify({
            Subscriber: subscriber,
            SSN: ssn,
            Code: code,
            Email: email,
            Password: password,
            source: 'mobile',
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }
    loginGuest(subscriber) {
        const method = 'authenticateAsGuest';
        const parameters = JSON.stringify({
            subscriber,
            device: this.model,
            deviceToken: this.UUID,
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }
    validateUser(subscriber) {
        const method = 'validateUser';
        const parameters = JSON.stringify({
            subscriber,
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }
    registerGuest(subscriber) {
        const method = 'registerGuestUser';
        const parameters = JSON.stringify({
            subscriber,
            device: this.model,
            deviceToken: this.UUID,
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }
    validateGuest(subscriber, code, token) {
        const method = 'validateGuestAccount';
        const parameters = JSON.stringify({
            subscriber,
            code,
            device: this.model,
            deviceToken: this.UUID,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    resendGuestCode(subscriber) {
        const method = 'updateUserGetCode';
        const parameters = JSON.stringify({
            subscriber,
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updateGuest(subscriber, code) {
        const method = 'updateUserSetToken';
        const parameters = JSON.stringify({
            subscriber,
            code,
            device: this.model,
            deviceToken: this.UUID,
            method
        });
        return this.util.serverListener(parameters).toPromise();
    }
    validateSubscriberUpdate(subscriber, token) {
        const method = 'validateSubscriberUpdate';
        const parameters = JSON.stringify({
            subscriber,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updateValidateAccount(subscriber, code, ssn, token) {
        const method = 'updAccountValidateCode';
        const parameters = JSON.stringify({
            subscriber: subscriber + '',
            code,
            ssn,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updateUsername(email, token) {
        const method = 'changeUser';
        const parameters = JSON.stringify({
            email,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updatePassword(newPassword, oldPassword, account, token) {
        const method = 'changePassword';
        const parameters = JSON.stringify({
            ban: account,
            newPassword,
            password: oldPassword,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getSecurityQuestions(token) {
        const method = 'getquestions';
        const parameters = JSON.stringify({
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    setChallengeQuestions(questionId, response, token) {
        const method = 'setchallengequestions';
        const parameters = JSON.stringify({
            questionID: questionId,
            response,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getPlans(creditClass, customerSubType, customerType, price, soc, technology, token) {
        const method = 'getPlanes2';
        const parameters = JSON.stringify({
            creditClass,
            customerSubType,
            customerType,
            price,
            soc,
            tecnology: technology,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getPlansPrepaid(account, subscriber, customerType, technology, token) {
        const method = 'getChangePlanPrepago';
        const parameters = JSON.stringify({
            account,
            suscriber: subscriber,
            accountType: customerType,
            tech: technology,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getPlansPrepaid2(creditClass, productType, customerSubType, customerType, price, soc, tecnology, token) {
        const method = 'getPlans';
        const parameters = JSON.stringify({
            creditClass,
            productType,
            customerSubType,
            customerType,
            price,
            soc,
            tecnology,
            source: 'MICLARO',
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getPlansDSL(subscriber, token) {
        const method = 'DSLCatalog';
        const parameters = JSON.stringify({
            phoneNumber: subscriber,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updateSubscriberDSLPlan(productId, oldSocPrice, contract, alphaCodeContract, productType, subscriber, account, token) {
        const method = 'adaDslPackageChange';
        const parameters = JSON.stringify({
            ProductType: productType,
            alphaCodeContract,
            contract,
            dslBan: account,
            dslPhoneNumber: subscriber,
            oldSocPrice,
            productId,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updateSubscriberPlan(newSoc, oldSoc, productType, subscriber, token) {
        const method = 'updateSubscriberPricePlanSocs';
        const parameters = JSON.stringify({
            NewSocCode: newSoc,
            OldSocCode: oldSoc,
            mProductType: productType,
            mSubscriberNo: subscriber,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updateSubscriberPlanNextCycle(newSoc, oldSoc, productType, subscriber, account, token) {
        const method = 'updateSubscriberPricePlanSocsNextCicle';
        const parameters = JSON.stringify({
            BAN: account,
            NewSocCode: newSoc,
            OldSocCode: oldSoc,
            mProductType: productType,
            mSubscriberNo: subscriber,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updatePrepaidSubscriberPlan(amount, planName, rechargeMinutes, newSoc, currentSoc, accountType, accountSubType, account, subscriber, token) {
        const method = 'changePrepaidAccount';
        const parameters = JSON.stringify({
            suscriber: subscriber,
            amount,
            ban: account,
            currentAccountSubType: accountSubType,
            currentAccountType: accountType,
            currentSoc,
            newAccountSubType: accountSubType,
            newAccountType: accountType,
            newSoc,
            planName,
            rechargeMinutes,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updatePrepaidSubscriberPlan2(accountSubType, accountType, productType, effectiveDate, oldSocCode, socCode, socId, subscriberId, token) {
        const method = 'changePrepaidPlan';
        const parameters = JSON.stringify({
            accountSubType,
            accountType,
            effectiveDate,
            oldSocCode,
            socCode,
            productType,
            socId,
            subscriberId,
            source: 'MICLARO',
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getDataPackets(groupID, transactionId, subscriber, token, offerID) {
        const method = 'GetOffersToSubscriber';
        const parameters = {
            OfferGroup: '',
            SubscriberId: subscriber,
            TransactionId: transactionId,
            method,
            token
        };
        if (offerID) {
            parameters.offerID = offerID;
        }
        return this.util.serverListener(JSON.stringify(parameters)).toPromise();
    }
    getReadSubscriber(subscriber, transactionId, token) {
        const method = 'GetReadSubscriber';
        const parameters = JSON.stringify({
            IdSubscriber: subscriber,
            TransactionId: transactionId,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    validateCreditLimit(account, accountTotalRent, productPrice, token) {
        const method = 'ValidateCreditLimit';
        const parameters = JSON.stringify({
            Ban: btoa(account),
            ProductPrice: btoa(productPrice),
            AccountTotalRent: btoa(accountTotalRent),
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    addOfferToSubscriber(transactionId, subscriberId, offerId, charge, cycle, paymentId, username, token) {
        const method = 'AddOffersToSubscriber';
        const parameters = JSON.stringify({
            TransactionId: transactionId,
            SubscriberId: subscriberId,
            OfferId: offerId,
            Charge: charge,
            Cicle: cycle,
            paymentID: paymentId,
            UserID: username,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getUserProfile(account, token) {
        const method = 'GetPersonalData';
        const parameters = JSON.stringify({
            BAN: account,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updatePersonalData(account, email, phone, phoneF, token) {
        const method = 'UpdatePersonalData';
        const parameters = JSON.stringify({
            BAN: btoa(account),
            Email: btoa(email),
            PhoneNumber: btoa(phone),
            PhoneNumber2: btoa(phoneF),
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updatePersonalAddress(account, address1, address2, city, zipCode, token) {
        const method = 'UpdatePersonalDir';
        const parameters = JSON.stringify({
            BAN: account,
            AddressDet: address1,
            AddressDet2: address2,
            City: city,
            zip: zipCode,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    // Account Models (7):
    addAccount(accountNumber, ssn, defaultAccount, token) {
        const method = 'addAccounts';
        const parameters = JSON.stringify({
            account: accountNumber,
            ssn,
            setAsDefaultAccount: defaultAccount,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    setDefaultAccount(accountNumber, subscriber, accountType, accountSubType, productType, token) {
        const method = 'SetAsDefaultAccount';
        const parameters = JSON.stringify({
            account: accountNumber,
            subscriber,
            accountType,
            accountSubType,
            productType,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getAccounts(token) {
        const method = 'getAcounts';
        const parameters = JSON.stringify({
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    deleteAccount(accountNumber, token) {
        const method = 'deleteAccount2';
        const parameters = JSON.stringify({
            account: accountNumber,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getHistoryOrders(account, token) {
        // const token = token
        const method = 'accountPackagesInfo';
        const parameters = JSON.stringify({
            Ban: account,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    subscribeNetflix(account, subscriber, token) {
        const tokenSession = token;
        const method = 'suscribirNeflix';
        const parameters = JSON.stringify({
            account,
            subscriber,
            customerType: '',
            mdeviceSerialNumber: '',
            moperatorUrlError: '',
            mpromotionId: '',
            msalesChannel: '',
            productId: '',
            subProductId: '',
            method,
            token: tokenSession,
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getInstallments(account, subscriber, productType, query, value, token) {
        const method = 'getInstallments';
        const parameters = JSON.stringify({
            userId: '',
            appKey: this.getPaymentId(),
            authToken: this.getPaymentToken(),
            appId: this.getPaymentId(),
            accountNumber: account,
            subscriberNumber: subscriber,
            queryType: query,
            valueRequested: value,
            productType,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    resendCode() {
        const tokenSession = this.getToken();
        const method = 'resendCode';
        const parameters = JSON.stringify({
            method,
            token: tokenSession,
        });
        return this.util.serverListener(parameters).toPromise();
    }
    // Customer model (15):
    validateAccount(account, accountType, accountSubType, subscriber) {
        const tokenSession = this.getToken();
        const method = 'validateAccount';
        const parameters = JSON.stringify({
            suscriber: subscriber,
            ban: account,
            currentAccountSubType: accountSubType,
            currentAccountType: accountType,
            newAccountSubType: accountSubType,
            newAccountType: accountType,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    accountDetails(subscriber, account) {
        const tokenSession = this.getToken();
        const method = 'getaccountdetails';
        const parameters = JSON.stringify({
            subscriber: btoa(subscriber),
            account: btoa(account),
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    userAccess(subscriber, account) {
        const tokenSession = this.getToken();
        const method = 'getaccess';
        const parameters = JSON.stringify({
            subscriber,
            account,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updateToken(account, subscriber, token) {
        const method = 'updateToken';
        const parameters = JSON.stringify({
            subscriber,
            account,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    /*
    passwordUpdate(currentPassword, newPassword, successCB, errorCB){

        const tokenSession = app.utils.Storage.getSessionItem('token');

        const method = 'setPasswordUpdate';

        const parameters = JSON.stringify({
            currentPassword: currentPassword,
            newPassword: btoa(newPassword),
            method: method,
            token: tokenSession
        });

          return this.util.serverListener(parameters).toPromise();
    } */
    getBan(account) {
        const tokenSession = this.getToken();
        const parameters = JSON.stringify({
            BAN: account,
            method: 'getBan',
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updateBillParameters(account, token) {
        const parameters = JSON.stringify({
            Ban: String(account),
            method: 'updateBillParameters',
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getSubscriber(subscriber, token) {
        const parameters = JSON.stringify({
            Subscriber: subscriber,
            method: 'GetSubscriber',
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getMember(account, token) {
        const tokenSession = token;
        const parameters = JSON.stringify({
            account,
            method: 'getMember',
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    addMember(data, token) {
        const tokenSession = token;
        data.token = tokenSession;
        data.method = 'addmember';
        const parameters = JSON.stringify(data);
        return this.util.serverListener(parameters).toPromise();
    }
    sendGift1GB(data, token) {
        const tokenSession = token;
        const method = 'AddGift1GB';
        data.token = tokenSession; // ????
        data.method = method; // ??????
        const parameters = JSON.stringify(data);
        return this.util.serverListener(parameters).toPromise();
    }
    sendGiftRecharge(data, token) {
        const tokenSession = token;
        const method = 'giftRecharge';
        data.token = tokenSession; // ?????
        data.method = method; // ????
        const parameters = JSON.stringify(data);
        return this.util.serverListener(parameters).toPromise();
    }
    getGift1GBSend(account, token) {
        const parameters = JSON.stringify({
            BAN: account,
            method: 'GetGift1GBSend',
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getGift1GBByGUI(account, gui, token) {
        const parameters = JSON.stringify({
            BAN: account,
            GUI: gui,
            method: 'GetGift1GBByGUI',
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    // payment model(13):
    doPayment(account, amount, token) {
        const tokenSession = token;
        const method = 'DoPayment';
        const parameters = JSON.stringify({
            Account: account,
            Amount: amount,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    paymentHistory(account, year, token) {
        const tokenSession = token;
        const method = 'PaymentHistory';
        const parameters = JSON.stringify({
            Ban: account,
            year,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getInvoiceHistory(account, year, token) {
        const method = 'GetHistoricoFacturas2';
        const format = 'pdf';
        const parameters = JSON.stringify({
            Ban: account,
            year,
            format,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    prepaidHistory(account, subscriber, idCustomerCard, index, year, month, status, type, token) {
        const method = 'refillHistory';
        const parameters = JSON.stringify({
            account,
            suscriber: subscriber,
            idCustomerCard,
            index,
            year,
            month,
            status,
            type,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getPrepaidTransactions(subscriber, token) {
        const method = 'getPrepaidInfo';
        const parameters = JSON.stringify({
            subscriberId: subscriber,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getDirectDebitInfo(account, token) {
        const tokenSession = token;
        const method = 'getDirectDebitInfo2';
        const parameters = JSON.stringify({
            accountNumber: account,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updateDirectDebit(data, token) {
        const tokenSession = token;
        const method = 'updateDirectDebit2';
        data.token = tokenSession;
        data.method = method;
        const parameters = JSON.stringify(data);
        return this.util.serverListener(parameters).toPromise();
    }
    makePayment(data, token) {
        const method = 'makePayment';
        data.token = token;
        data.method = method;
        const parameters = JSON.stringify(data);
        return this.util.serverListener(parameters).toPromise();
    }
    makePaymentRecharge(data, subscriber) {
        const tokenSession = this.getToken();
        const method = 'prepaidPayment';
        data.suscriber = subscriber;
        data.token = tokenSession;
        data.method = method;
        const parameters = JSON.stringify(data);
        return this.util.serverListener(parameters).toPromise();
    }
    makePaymentRechargeAth(data, subscriber) {
        const tokenSession = this.getToken();
        const method = 'prepaidPaymentATH';
        data.suscriber = subscriber;
        data.token = tokenSession;
        data.method = method;
        const parameters = JSON.stringify(data);
        return this.util.serverListener(parameters).toPromise();
    }
    doRecharge(data, subscriber) {
        const tokenSession = this.getToken();
        const method = 'minutesRecharge';
        data.suscriber = subscriber;
        data.token = tokenSession;
        data.method = method;
        const parameters = JSON.stringify(data);
        return this.util.serverListener(parameters).toPromise();
    }
    listProductService(subscriber, idProduct, token) {
        const method = 'productserviceList';
        const parameters = JSON.stringify({
            suscriber: subscriber,
            idProductType: idProduct,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    listPrepaidAddress(subscriber, token) {
        const method = 'prepaidAddress';
        const parameters = JSON.stringify({
            suscriber: subscriber,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    listPrepaidPaymentsType(subscriber, token) {
        const method = 'listTypesPayments';
        const parameters = JSON.stringify({
            suscriber: subscriber,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    // Referrer model (7):
    getHobbies(account, token) {
        const tokenSession = token;
        const method = 'gethobbies';
        const parameters = JSON.stringify({
            account,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getValidateReferrer(account, subscriber, token) {
        const tokenSession = token;
        const method = 'getValidateReferrer';
        const parameters = JSON.stringify({
            account,
            subscriber,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getSharingMediaByUser(memberID, token) {
        const tokenSession = token;
        const method = 'getSharingMediaByUser';
        const parameters = JSON.stringify({
            method,
            token: tokenSession,
            memberID,
            campaignID: 1,
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getCredits(account, token) {
        const tokenSession = token;
        const method = 'getCreditsByAccount';
        const parameters = JSON.stringify({
            Account: account,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
        // $.getJSON("js/pojo/get-credits.json", function(json) {
        //     var data = json;
        //     successCB(data);
        // });
    }
    applyCredits(account, subscriber, amount, token) {
        const method = 'RedeemCuponsByAccount';
        const parameters = JSON.stringify({
            account,
            subscriber,
            total: amount,
            applyDiscountID: 1,
            strcomentario: 'dashboard',
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getReferrerAccountsAllStatus(account, token) {
        const tokenSession = token;
        const method = 'getReferrAccountsAllStatus';
        const parameters = JSON.stringify({
            account,
            subscriber: '',
            campaignID: 1,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    sharedCoupons(memberID, account, subscriber, emails, userLink, token) {
        const tokenSession = token;
        const method = 'sharedCupons';
        const parameters = JSON.stringify({
            account: account + '',
            subscriber: subscriber + '',
            memberID: memberID + '',
            email: emails,
            link: userLink + '',
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getStores(storeType, userLocation, distanceMeasure, url) {
        const parameters = {
            type: storeType
        };
        if (distanceMeasure !== '') {
            parameters.latitude = userLocation.latitude;
            parameters.longitude = userLocation.longitude;
            parameters.measureUnit = distanceMeasure;
        }
        return this.util.helpListener(parameters, url).toPromise();
    }
    // Model User (14):
    getVerifyEmail(email, token) {
        const method = 'getVerifyEmail';
        const parameters = JSON.stringify({
            email,
            method,
            token
        });
        return this.util.serverListener(parameters).toPromise();
    }
    getPersonalAlertsStatus(accountNumber, subscriber, token) {
        const tokenSession = token;
        const method = 'GetPersonalAlertsAndNTCStatus';
        const parameters = JSON.stringify({
            BAN: accountNumber,
            Subscriber: subscriber,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updateNotToCall(accountNumber, subscriber, action, token) {
        const tokenSession = token;
        const method = 'NotToCall';
        const parameters = JSON.stringify({
            BAN: accountNumber,
            Subscriber: subscriber,
            Action: action,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updateAlerts(accountNumber, subscriber, alerts, token) {
        const tokenSession = token;
        const method = 'UpdateAlerts';
        const parameters = JSON.stringify({
            BAN: accountNumber,
            Subscriber: subscriber,
            alertList: alerts,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    updateNotification(idMessage, accountNumber, token) {
        const tokenSession = token;
        const method = 'updMessageStatus';
        const parameters = JSON.stringify({
            id_message: idMessage,
            account: accountNumber,
            method,
            token: tokenSession
        });
        return this.util.serverListener(parameters).toPromise();
    }
    // help:
    sendHelpMessage(userEmail, message, url) {
        const parameters = {
            userMail: userEmail,
            info: {
                userAgent: this.SO,
                // versionCode: app.build, ??
                // versionName: app.soVersion, ??
                versionCode: null,
                versionName: null,
                soVersion: this.soVersion,
                userId: this.UUID
            },
            message
        };
        return this.util.helpListener(parameters, url).toPromise();
    }
    subscriptionAutomaticRenewalAdd(data, token) {
        const method = 'subscriptionAutomaticRenewalAdd';
        const parameters = {
            SubscriberId: data.subcriberId,
            OfferID: data.OfferId,
            BaseOfferId: data.BaseOfferID,
            method,
            token
        };
        return this.util.serverListener(parameters).toPromise();
    }
    subscriptionAutomaticRenewalRemove(data, token) {
        const method = 'subscriptionAutomaticRenewalRemove';
        const parameters = {
            SubscriberId: data.subcriberId,
            OfferID: data.OfferId,
            BaseOfferId: data.BaseOfferID,
            method,
            token
        };
        return this.util.serverListener(parameters).toPromise();
    }
    checkWarrantyStatus(subscriberId, token) {
        const method = 'warrantyStatus';
        const parameters = {
            subscriber: subscriberId,
            method,
        };
        // TODO, para caso de prueba
        // return new Promise((resolve, reject) => {
        //     setTimeout(() => {
        //         const object = JSON.parse('{"statusDescription":"Equipo en tránsito al taller del Centro de Atención.","warrantyReclamationDate":1583158271000}');
        //         resolve(object);
        //     }, 3000);
        // });
        return this.util.serverListener(parameters).toPromise();
    }
    checkIfTicketExists(subscriber) {
        const method = 'CheckIfTicketExists';
        const formData = new FormData();
        formData.append('telNum', subscriber);
        return this.util.faultReportServerListener(formData, _environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].FAULT_REPORT_CHECK_URL).toPromise();
    }
    setRaiseIssueTicket(data) {
        const method = 'RaiseIssueTicket'; // ??????
        const formData = new FormData();
        formData.append('SubscriberNumber', data.SubscriberNumber);
        formData.append('Ban', data.Ban);
        formData.append('ContactNumber1', data.ContactNumber1);
        formData.append('ContactNumber2', data.ContactNumber2);
        formData.append('OutageType', data.OutageType);
        formData.append('OperatorID', data.OperatorID);
        formData.append('CustomerReport', data.CustomerReport);
        formData.append('Remark', data.Remark);
        formData.append('ProductType', data.ProductType);
        return this.util.faultReportServerListener(formData, _environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].FAULT_REPORT_CREATE_URL).toPromise();
        // return new Promise((resolve, reject) => {
        //     setTimeout(() => {
        //         const object = JSON.parse('{"commitmenrDate":"20181213","createTicket":"Y",' +
        //             '"errorCode":"0","errorMessage":"Success","memoSIF":"SIF Memo sended","q_id":"27","tt_id":"1234567"}');
        //         resolve(object);
        //     }, 3000);
        // });
    }
    checkAppVersion(so, version) {
        const url = `${_environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].API_URL}app/getVersion`;
        const parameters = JSON.stringify({
            appId: _environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].ID,
            version: version,
            osType: so,
        });
        return this.util.helpListener(parameters, url).toPromise();
    }
    // services for new payments method
    getPaymentOptions(productId, merchantCodeId) {
        const method = 'getPaymentOptions';
        const parameters = {
            appId: this.getPaymentId(),
            productId,
            merchantCodeId: merchantCodeId ? merchantCodeId : '',
            appKey: this.getPaymentId(),
            authToken: this.getPaymentToken(),
            method
        };
        return this.util.serverListener(parameters).toPromise();
    }
    initiatePaymentProcess(productId, paymentOptionId, paymentAmount, customerEmail, subscriberNumber, subscriberAccountNumber, subscriberFullName, transactionDescription, merchantCodeId, locationId, invoiceNumber) {
        const method = 'initiatePaymentProcess';
        const parameters = {
            appId: this.getPaymentId(),
            productId,
            paymentOptionId,
            paymentAmount,
            customerEmail,
            subscriberNumber,
            subscriberAccountNumber,
            subscriberFullName,
            subscriberAddress1: '',
            subscriberAddress2: '',
            subscriberCity: '',
            subscriberState: '',
            subscriberZipCode: '',
            transactionDescription,
            appKey: this.getPaymentId(),
            authToken: this.getPaymentToken(),
            method
        };
        if (merchantCodeId) {
            parameters.merchantCodeId = merchantCodeId;
        }
        if (locationId) {
            parameters.locationId = locationId;
        }
        if (invoiceNumber) {
            parameters.invoiceNumber = invoiceNumber;
        }
        return this.util.serverListener(parameters).toPromise();
    }
    verifyPayment(paymentToken) {
        const method = 'verifyPaymentStatus';
        const parameters = {
            paymentToken,
            appKey: this.getPaymentId(),
            authToken: this.getPaymentToken(),
            method
        };
        return this.util.serverListener(parameters).toPromise();
    }
    getPaymentId() {
        if (_utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].getPlatformInfo().ios) {
            return _environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].PAYMENT_ID_IOS;
        }
        else {
            return /*environment.PAYMENT_ID_ANDROID*/ 'MICLAROWEB';
        }
    }
    getPaymentToken() {
        if (_utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].getPlatformInfo().ios) {
            return _environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].PAYMENT_TOKEN_IOS;
        }
        else {
            return _environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].PAYMENT_TOKEN_ANDROID;
        }
    }
    registerUserDevice(userName, subscriber, pnToken, successCB, errorCB) {
        const method = 'device/register';
        const parameters = {
            userName,
            suscriber: subscriber,
            type: this.SO,
            pnToken,
            uuid: this.UUID
        };
        return this.util.serverListener(parameters).toPromise();
    }
    chat(tokenSession = '', tokenSSO = '') {
        const method = 'chatbot';
        const parameters = {
            tokenSession,
            tokenSSO,
            region: '',
            category: '',
            id: '',
            origin: '',
            customerName: '',
            accountNumber: '',
            email: '',
            appType: _utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].getPlatformInfo().ios ? 'App iOS' : 'App Android',
            cedula: '',
            method
        };
        return this.util.serverListener(parameters).toPromise();
    }
};
ServicesProvider.ctorParameters = () => [
    { type: _ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_11__["AppVersion"] },
    { type: _utils_service__WEBPACK_IMPORTED_MODULE_2__["UtilsService"] },
    { type: _intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] },
    { type: _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_3__["Device"] },
    { type: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["GoogleMaps"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_6__["Geolocation"] }
];
ServicesProvider = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_app_version_ngx__WEBPACK_IMPORTED_MODULE_11__["AppVersion"],
        _utils_service__WEBPACK_IMPORTED_MODULE_2__["UtilsService"],
        _intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"],
        _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_3__["Device"],
        _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_5__["GoogleMaps"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_6__["Geolocation"]])
], ServicesProvider);



/***/ }),

/***/ "./src/app/services/storage.provider.ts":
/*!**********************************************!*\
  !*** ./src/app/services/storage.provider.ts ***!
  \**********************************************/
/*! exports provided: StorageProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageProvider", function() { return StorageProvider; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils/const/keys */ "./src/app/utils/const/keys.ts");




let StorageProvider = class StorageProvider {
    constructor(storage) {
        this.storage = storage;
    }
    isLogged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const isLogged = yield this.storage.get(_utils_const_keys__WEBPACK_IMPORTED_MODULE_3__["keys"].LOGIN.IS_LOGGED).then();
            return !!isLogged;
        });
    }
    setLogged(logged) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.storage.set(_utils_const_keys__WEBPACK_IMPORTED_MODULE_3__["keys"].LOGIN.IS_LOGGED, logged).then();
        });
    }
    isGuest() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const isLogged = yield this.storage.get(_utils_const_keys__WEBPACK_IMPORTED_MODULE_3__["keys"].LOGIN.IS_GUEST).then();
            return !!isLogged;
        });
    }
    setGuest(guest) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.storage.set(_utils_const_keys__WEBPACK_IMPORTED_MODULE_3__["keys"].LOGIN.IS_GUEST, guest).then();
        });
    }
    isRated() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const isRated = yield this.storage.get(_utils_const_keys__WEBPACK_IMPORTED_MODULE_3__["keys"].APP.IS_RATED).then();
            return !!isRated;
        });
    }
    setRated(isRated) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.storage.set(_utils_const_keys__WEBPACK_IMPORTED_MODULE_3__["keys"].APP.IS_RATED, isRated).then();
        });
    }
    getOpenAppTimes() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const times = yield this.storage.get(_utils_const_keys__WEBPACK_IMPORTED_MODULE_3__["keys"].APP.OPEN_APP_TIMES).then();
            // tslint:disable-next-line:no-bitwise
            return times | 0;
        });
    }
    setOpenAppTimes(times) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.storage.set(_utils_const_keys__WEBPACK_IMPORTED_MODULE_3__["keys"].APP.OPEN_APP_TIMES, times).then();
        });
    }
    increaseOpenAppTimes() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const times = yield this.storage.get(_utils_const_keys__WEBPACK_IMPORTED_MODULE_3__["keys"].APP.OPEN_APP_TIMES).then();
            // tslint:disable-next-line:no-bitwise
            let count = times | 0;
            count++;
            yield this.setOpenAppTimes(count);
        });
    }
};
StorageProvider.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] }
];
StorageProvider = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]])
], StorageProvider);



/***/ }),

/***/ "./src/app/services/utils.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/utils.service.ts ***!
  \*******************************************/
/*! exports provided: UtilsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UtilsService", function() { return UtilsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/firebase-x/ngx */ "./node_modules/@ionic-native/firebase-x/ngx/index.js");
/* harmony import */ var _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/keyboard/ngx */ "./node_modules/@ionic-native/keyboard/ngx/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _ionic_native_app_preferences_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/app-preferences/ngx */ "./node_modules/@ionic-native/app-preferences/ngx/index.js");
/* harmony import */ var _browser_provider__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./browser.provider */ "./src/app/services/browser.provider.ts");

var UtilsService_1;
















const PREF_URL = 'api-url';
const PREF_TOKEN = 'login';
const PREF_PRODUCT = 'productType';
let UtilsService = UtilsService_1 = class UtilsService {
    constructor(storage, userStorage, httpClient, navCtrl, alertController, firebase, keyboard, appPreferences, browserProvider) {
        this.storage = storage;
        this.userStorage = userStorage;
        this.httpClient = httpClient;
        this.navCtrl = navCtrl;
        this.alertController = alertController;
        this.firebase = firebase;
        this.keyboard = keyboard;
        this.appPreferences = appPreferences;
        this.browserProvider = browserProvider;
        this.showLoader = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.resetTimer = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.openMenu = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.isMenuOpen = false;
        this.help = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].HELP_URL;
        this.chat = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].CHAT_URL;
        this.sailed = ['login'];
        this.moment = moment__WEBPACK_IMPORTED_MODULE_13__;
        this.biometricOptions = {
            available: false,
            ios: _utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPlatformInfo().ios,
            android: _utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPlatformInfo().android,
            type: 'touch',
            activated: false,
            password: 'none'
        };
        this.openMenu.subscribe(open => {
            this.isMenuOpen = open;
        });
    }
    /**
     * Save page name on firebase for analytics only if is mobile
     */
    registerScreen(pageName) {
        if (!_utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPlatformInfo().desktop) {
            this.firebase.setScreenName(pageName).then();
            this.firebase.logEvent(pageName, {}).then();
        }
    }
    hideKeyboard() {
        if (!_utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPlatformInfo().desktop) {
            this.keyboard.hide();
        }
    }
    goTo(page) {
        const currentPage = this.sailed[this.sailed.length - 1];
        if (currentPage.includes('profile') && page.includes('profile')) {
            this.sailed.pop();
        }
        if (page === 'login') {
            this.navCtrl.navigateRoot('login').then(() => {
                this.sailed = ['login'];
            });
        }
        else if (page === 'home/dashboard' && this.sailed[this.sailed.length - 1] === 'home/dashboard') {
            this.navCtrl.navigateForward('dummy', { replaceUrl: true, animated: false })
                .then(() => {
                this.showPage(page, false);
            });
        }
        else {
            this.showPage(page, false);
        }
    }
    back() {
        const currentPage = this.sailed[this.sailed.length - 1];
        if (currentPage === 'login' || currentPage === 'update-app') {
            // @ts-ignore
            navigator['app'].exitApp();
        }
        else if (currentPage === 'home/dashboard') {
            this.confirmExitApp();
        }
        else if (currentPage === 'update/username') {
            this.confirmCloseSession();
        }
        else {
            this.sailed.pop();
            const page = this.sailed.pop();
            // page where can't navigate
            if (page === 'module/change-plan/confirm' ||
                page === 'module/change-plan/success') {
                this.sailed.push(page);
                this.back();
                return;
            }
            this.showPage(page, true);
        }
    }
    confirmExitApp() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Confirmación',
                message: '¿Esta seguro que desea salir de la aplicación?',
                buttons: [{
                        text: 'Cancelar',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => { }
                    }, {
                        text: 'Si, Salir',
                        handler: () => {
                            navigator['app'].exitApp();
                        }
                    }]
            });
            yield alert.present();
        });
    }
    confirmCloseSession(onClose, onCancel) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Salir',
                message: '¿Esta seguro que desea cerrar la sesión?',
                buttons: [{
                        text: 'No',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            if (onCancel) {
                                onCancel();
                            }
                        }
                    }, {
                        text: 'Si',
                        handler: () => {
                            if (onClose) {
                                onClose();
                            }
                            this.firebase.setScreenName('logout button');
                            this.clearStore().then(() => {
                                this.userStorage.logged = false;
                                this.deleteDataForWidgets();
                                this.goTo('login');
                            });
                        }
                    }]
            });
            yield alert.present();
        });
    }
    clearStore() {
        return new Promise(resolve => {
            this.storage.remove(_utils_const_keys__WEBPACK_IMPORTED_MODULE_14__["keys"].LOGIN.IS_LOGGED).then(() => {
                this.storage.clear().then(() => {
                    resolve();
                });
            });
        });
    }
    showPage(page, back) {
        this.navCtrl.navigateForward(page, { replaceUrl: true, animationDirection: back ? 'back' : 'forward' })
            .then(() => {
            if (page === 'home/dashboard') {
                this.sailed = ['home/dashboard'];
            }
            else {
                if (page !== this.sailed[this.sailed.length - 1]) { // only if don't contain same page
                    this.sailed.push(page);
                }
            }
        });
    }
    serverListener(data) {
        return this.httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].PROCESS_URL, data, {
            headers: this.getHeaders()
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["timeout"])(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].TIMEOUT), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mergeMap"])(res => {
            return this.handlerResponse(res);
        }));
    }
    handlerResponse(response) {
        return new rxjs__WEBPACK_IMPORTED_MODULE_5__["Observable"](observer => {
            if (response.hasError || response.HasError || response.hasErrorField) {
                let message = UtilsService_1.errorGeneral;
                if (response.errorDisplay) {
                    message = response.errorDisplay;
                }
                else {
                    if (response.errorDesc) {
                        message = response.errorDesc;
                    }
                    else {
                        if (response.ErrorDesc) {
                            message = response.ErrorDesc;
                        }
                    }
                }
                if (message === 'Display Messsage 7') {
                    message = response.message;
                }
                observer.error({
                    message,
                    num: response.errorNum,
                    desc: response.errorDesc,
                    withStatus200: true,
                    token: response.token,
                    account: response.account,
                    accountType: response.accountType
                });
            }
            else {
                observer.next(response);
                observer.complete();
            }
        });
    }
    handleError(error) {
        let errorMessage = '';
        if (error.status === 404 || error.status === 0) {
            errorMessage = UtilsService_1.error404;
        }
        else {
            errorMessage = UtilsService_1.errorGeneral;
        }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["throwError"])({
            message: errorMessage,
            withStatus200: false,
            num: 0
        });
    }
    helpListener(data, url) {
        return this.httpClient.post(url, data, {
            headers: this.getHelpHeaders()
        })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["timeout"])(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].TIMEOUT), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mergeMap"])(res => {
            return this.handlerResponse(res);
        }));
    }
    getHelpHeaders() {
        let header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        header = header.append('api-key', '6af3982a-ce65-41a0-93d9-52bd172685cd'); // pones la key esta en las const
        header = header.append('content-type', 'application/json; charset=utf-8');
        return header;
    }
    getHeaders() {
        let header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        header = header.append('content-type', 'application/json; charset=utf-8');
        return header;
    }
    faultReportServerListener(data, url) {
        return this.httpClient.post(url, data, {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["timeout"])(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].TIMEOUT), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["mergeMap"])(res => {
            return this.handlerResponse(res);
        }));
    }
    /**
     * only call on android or ios
     */
    storeDataForWidgets(account, subscriber, productType) {
        if (!_utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPlatformInfo().desktop) {
            const token = account + '||' + subscriber + '||' + this.moment().format('DD-MM-YYYY HH:mm:ss');
            const ENCRYPTION_KEY = 'qcPQK9012G3G7DCt';
            const ENCRYPTION_IV = '4W4NtvbLf85vUTZ3';
            const cipherText = crypto_js__WEBPACK_IMPORTED_MODULE_11__["AES"].encrypt(token, crypto_js__WEBPACK_IMPORTED_MODULE_11__["enc"].Utf8.parse(ENCRYPTION_KEY), {
                iv: crypto_js__WEBPACK_IMPORTED_MODULE_11__["enc"].Utf8.parse(ENCRYPTION_IV),
                mode: crypto_js__WEBPACK_IMPORTED_MODULE_11__["mode"].CBC,
                padding: crypto_js__WEBPACK_IMPORTED_MODULE_11__["pad"].Pkcs7
            });
            const tokenEncrypted = cipherText.toString();
            const url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].API_URL + 'widgets/getInvoiceInfo';
            const pref = _utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPlatformInfo().ios ? this.appPreferences.suite(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].GROUP_ID) : this.appPreferences;
            console.log('GROUP_ID: ' + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].GROUP_ID);
            const storeUrl = pref.store(PREF_URL, url).then(() => {
                console.log('Shared: ' + PREF_URL + ' ' + url);
            });
            const storeToken = pref.store(PREF_TOKEN, tokenEncrypted).then(() => {
                console.log('Shared: ' + PREF_TOKEN + ' ' + tokenEncrypted);
            });
            const storeType = pref.store(PREF_PRODUCT, productType).then(() => {
                console.log('Shared: ' + PREF_PRODUCT + ' ' + productType);
            });
            Promise.all([storeUrl, storeToken, storeType]).then(() => {
                console.log('Shared preferences saved');
            });
        }
    }
    deleteDataForWidgets() {
        if (!_utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPlatformInfo().desktop) {
            const pref = _utils_utils__WEBPACK_IMPORTED_MODULE_12__["Utils"].getPlatformInfo().ios ? this.appPreferences.suite(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].GROUP_ID) : this.appPreferences;
            const storeUrl = pref.remove(PREF_URL).then(() => {
                console.log('Removed: ' + PREF_URL);
            });
            const storeToken = pref.remove(PREF_TOKEN).then(() => {
                console.log('Removed: ' + PREF_TOKEN);
            });
            const storeType = pref.remove(PREF_PRODUCT).then(() => {
                console.log('Removed: ' + PREF_PRODUCT);
            });
            Promise.all([storeUrl, storeToken, storeType]).then(() => {
                console.log('Shared preferences removed');
            });
        }
    }
};
UtilsService.errorGeneral = 'Disculpe, actualmente estamos presentando inconveniente, por favor intente mas tarde.';
UtilsService.error404 = 'Disculpe, no fue posible establecer la comunicación.';
UtilsService.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: _intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_9__["FirebaseX"] },
    { type: _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_10__["Keyboard"] },
    { type: _ionic_native_app_preferences_ngx__WEBPACK_IMPORTED_MODULE_15__["AppPreferences"] },
    { type: _browser_provider__WEBPACK_IMPORTED_MODULE_16__["BrowserProvider"] }
];
UtilsService = UtilsService_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"],
        _intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"],
        _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_9__["FirebaseX"],
        _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_10__["Keyboard"],
        _ionic_native_app_preferences_ngx__WEBPACK_IMPORTED_MODULE_15__["AppPreferences"],
        _browser_provider__WEBPACK_IMPORTED_MODULE_16__["BrowserProvider"]])
], UtilsService);



/***/ }),

/***/ "./src/app/utils/_helpers/response.interceptor.ts":
/*!********************************************************!*\
  !*** ./src/app/utils/_helpers/response.interceptor.ts ***!
  \********************************************************/
/*! exports provided: ResponseInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResponseInterceptor", function() { return ResponseInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ResponseInterceptor = class ResponseInterceptor {
    constructor() { }
    intercept(request, next) {
        return next.handle(request).pipe();
    }
};
ResponseInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ResponseInterceptor);



/***/ }),

/***/ "./src/app/utils/const/appConstants.ts":
/*!*********************************************!*\
  !*** ./src/app/utils/const/appConstants.ts ***!
  \*********************************************/
/*! exports provided: APP */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APP", function() { return APP; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

const APP = {
    id: '775322054',
    country: 'pr',
    // os: 'ANDROID',
    // device: 'android',
    version: null,
    build: null,
    uuid: null,
    connectionType: '',
    registrationId: null,
    rate: null,
    enableAppRate: true,
    enableCheckAppVersion: true,
    senderId: '680347282653',
    distanceMeasure: 'mi',
    sessionPasswordTime: 10,
    packageName: 'com.todoclaro.miclaroappdev',
    pushNotification: null,
    PRODUCT_ID_INVOICE_PAYMENTS: '1',
    PRODUCT_ID_RECHARGE_PREPAID: '2',
    PRODUCT_ID_PURCHASE_DATA: '3',
    PRODUCT_ID_INSTALLMENTS: '5',
    PAYMENT_STATUS_STORED: 'payment_status_token',
    BANK_PAYMENT_SUCCESS: 'BANK PAYMENT SUCCESS',
    PROVISIONING_STATUS: {
        NOT_PROVISIONED: 'NOT PROVISIONED',
        PROVISIONED: 'PROVISIONED',
    },
    SCHEME: 'miclaropr://',
    MI_CLARO_WEB: 'https://miclaro.claropr.com/',
    MI_CLARO_STORE: 'https://tienda.claropr.com/'
};


/***/ }),

/***/ "./src/app/utils/const/keys.ts":
/*!*************************************!*\
  !*** ./src/app/utils/const/keys.ts ***!
  \*************************************/
/*! exports provided: keys */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "keys", function() { return keys; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

const keys = {
    SESSION_TOKEN: 'session-token',
    RECOVER_PASSWORD: {
        IS_TELEPHONY: 'recover-password-is-telephony',
        QUESTION_LIST: 'recover-password-question-list',
        SUBSCRIBER: 'recover-password-subscriber',
        EMAIL: 'recover-password-email',
        TEMPORARY_PASSWORD: 'recover-password-new-password',
        TOKEN: 'recover-password-session-token'
    },
    REGISTER: {
        IS_POSTPAID: 'register-is-postpaid',
        IS_PREPAID: 'register-is-prepaid',
        SUBSCRIBER: 'register-subscriber',
        SSN: 'register-ssn',
        CODE: 'register-code',
        EMAIL: 'register-email',
        TOKEN: 'register-session-token',
        IS_GUEST_UPDATE: 'register-is-guest-update'
    },
    UPDATE_REGISTRATION: {
        USERNAME_USED: 'update-registration-username-used',
        PASSWORD_USED: 'update-registration-password-used',
        KEEP_AUTH: 'update-registration-keep',
        SUBSCRIBER: 'update-registration-subscriber',
        PRODUCT_TYPE_G: 'update-registration-is-type-G',
        IS_TELEPHONY: 'update-registration-is-telephony',
        SSN: 'update-registration-ssn',
        CODE: 'update-registration-code',
        TOKEN: 'update-registration-session-token',
    },
    LOGIN: {
        DATA: 'login-full-data',
        TRY_IN: 'login-try-in-now',
        USERNAME: 'login-username',
        SUBSCRIBER: 'login-subscriber',
        PASSWORD: 'login-password',
        KEEP: 'login-keep-authenticated',
        BIOMETRIC: 'login-biometric-authenticated',
        IS_LOGGED: 'login-is-logged',
        IS_GUEST: 'login-is-guest'
    },
    LIST: {
        STORES: 'stores',
        STORE_TYPE: 'active-store-type',
        STORE: 'store',
        SELECTED_STORE: 'selected-store'
    },
    /**
     *  Necesitaba estas constantes en el help sectión
     *  despues se coordina  la verdadera estructura del
     * ACCOUNT_DETAILS:
     *
     */
    ACCOUNT_DETAILS: {
        SELECTED_ACCOUNT: 'selected-account',
        ACCOUNT_INFO: 'account-info',
        ACCOUNT_LIST_SUBSCRIBER: 'accounts-list-subscribers',
        ACCOUNT_LIST_SUBSCRIBER_IS_LOADED: 'accounts-subscribers-is-loaded'
    },
    HISTORY: {
        VISITED: 'visited'
    },
    DEBIT_DIRECT: {
        PAY_DIRECT_DEBIT: 'pay-debit-direct'
    },
    NOTIFICATION: {
        NOTIFICATION_LIST: 'notifications'
    },
    ORDERS: {
        IS_LOADED: 'account-orders-is-loaded',
        LIST: 'account-list-orders'
    },
    NETFLIX: {
        SUBSCRIBER_LOADED: 'netflix-accounts-subscribers-is-loaded',
        NETFLIX_SUBSCRIBER: 'netflix-accounts-list-subscribers',
        ACCOUNT: 'netflix-subscription-account-number',
        SUBSCRIBER_NUMBER: 'netflix-subscription-subscriber-number'
    },
    REFER: {
        ACCOUNT_LIST_REFER: 'accounts-list-refer',
        ACCOUNT_REFER_IS_LOADED: 'accounts-refer-is-loaded',
        REFER_DATA: 'referrer-data',
        REFERRER_MEMBER: 'referrer-member',
        HOBIES_LIST: 'hobbiesList',
        REFERRER_VALID_MEMBER: 'referred-valid-member-id'
    },
    PAYMENT: {
        PAYMENT_DETAILS: 'payment-data_details',
        PAYMENT_AMOUNT: 'payment-data_amount',
        PAYMENT_SUBSCRIBER: 'payment-data_subscriber',
        PAYMENT_SUBSCRIBER_TYPE: 'payment-data_subscriber_type',
        PAYMENT_ACCOUNT: 'payment-data_account',
        PAYMENT_EMAIL: 'payment-data_email',
        PAYMENT_DESCRIPTION: 'payment-data_description',
        PAYMENT_TYPE: 'payment-data_type',
        PAYMENT_GIFT_1GB: 'payment-data_gift-1GB-data',
        PAYMENT_GIFT_RECHARGE: 'payment-data_gift-recharge-data',
        GIFT_SENT_TEXT: 'gift-send-text'
    },
    GIFT: {
        DATA_GIFT: 'data-gift'
    },
    APP: {
        OUTDATED_APP: 'outdated-app',
        IS_RATED: 'app-is-done-rated',
        OPEN_APP_TIMES: 'open_app_times'
    }
};


/***/ }),

/***/ "./src/app/utils/const/pages.ts":
/*!**************************************!*\
  !*** ./src/app/utils/const/pages.ts ***!
  \**************************************/
/*! exports provided: pages */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pages", function() { return pages; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

const pages = {
    DUMMY: 'dummy',
    GUEST: 'guest',
    LOGIN: 'login',
    HOME: 'home/dashboard',
    REGISTER: 'register/step1',
    UPDATE_APP: 'update-app',
    INVOICE_SUMMARY: 'module/invoice-summary',
    INVOICE_DOWNLOAD: 'module/invoice-download',
    INVOICE_HISTORY: 'module/invoice-history',
    ELECTRONIC_BILL: 'module/electronic-bill',
    DIRECT_DEBIT: 'module/direct-debit',
    CONSUMPTION: 'module/consumption',
    CONSUMPTION_PREPAID: 'module/consumption-prepaid',
    SERVICES_AND_EQUIPMENT: 'module/device',
    CHANGE_PLAN: 'module/change-plan',
    NETFLIX: 'module/netflix',
    REFERRER: 'module/refer/home',
    CLUB: 'module/club/home',
    PURCHASES: 'module/purchases',
    DATA_PLAN: 'module/data-plan',
    SHOPPING_HISTORY: 'module/shopping-history',
    GIFT_ONE_GB: 'module/gift/one-gb',
    GIFT_RECHARGE: 'module/gift/recharge',
    NOTIFICATIONS: 'notifications',
    PROFILE: 'module/profile',
    PROFILE_NAME: 'module/profile/name',
    PROFILE_EMAIL: 'module/profile/email',
    PROFILE_PASSWORD: 'module/profile/password',
    PROFILE_POSTAL: 'module/profile/postal',
    PROFILE_QUESTIONS: 'module/profile/questions',
    ACCOUNTS_MANAGE: 'module/add-account',
    ADD_ACCOUNTS: 'module/add-account/1',
    RECHARGE: 'module/recharge',
    TRANSACTIONS: 'module/recharge/history',
    FAULT_REPORT: 'module/fault/step1',
    SUPPORT: 'help/home',
    FAQ: 'help/faq',
    RECOVER_PASSWORD: 'recover/step1'
};


/***/ }),

/***/ "./src/app/utils/const/redirect-path.enum.ts":
/*!***************************************************!*\
  !*** ./src/app/utils/const/redirect-path.enum.ts ***!
  \***************************************************/
/*! exports provided: RedirectPathEnum */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RedirectPathEnum", function() { return RedirectPathEnum; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var RedirectPathEnum;
(function (RedirectPathEnum) {
    RedirectPathEnum["LOGIN"] = "login";
    RedirectPathEnum["CHAT"] = "chat";
    RedirectPathEnum["HOME"] = "home";
    RedirectPathEnum["INVOICE_SUMMARY"] = "billsmanagement";
    RedirectPathEnum["INVOICE_SUMMARY_BILLS"] = "bills-history";
    RedirectPathEnum["INVOICE_DOWNLOAD"] = "billshistory";
    RedirectPathEnum["PAYMENT_HISTORY"] = "paymenthistory";
    RedirectPathEnum["ELECTRONIC_BILL"] = "paperless";
    RedirectPathEnum["DIRECT_DEBIT"] = "direct-debit";
    RedirectPathEnum["CONSUMPTION"] = "usage";
    RedirectPathEnum["DEVICE"] = "servicesandequipment";
    RedirectPathEnum["CHANGE_PLAN"] = "changeplan";
    RedirectPathEnum["NETFLIX"] = "netflix";
    RedirectPathEnum["REFERRED"] = "referred";
    RedirectPathEnum["CLUB"] = "club";
    RedirectPathEnum["PURCHASES"] = "purchases";
    RedirectPathEnum["DATA_PLAN"] = "datapurchase";
    RedirectPathEnum["SHOPPING_HISTORY"] = "myorders";
    RedirectPathEnum["GIFT_ONE_GB"] = "giveone";
    RedirectPathEnum["GIFT_RECHARGE"] = "giverecharge";
    RedirectPathEnum["NOTIFICATIONS"] = "notification/-1";
    RedirectPathEnum["PROFILE_1"] = "profile/1";
    RedirectPathEnum["PROFILE_2"] = "profile/2";
    RedirectPathEnum["PROFILE_3"] = "profile/3";
    RedirectPathEnum["PROFILE_4"] = "profile/4";
    RedirectPathEnum["PROFILE_5"] = "profile/5";
    RedirectPathEnum["MY_ACCOUNTS"] = "modifyaccounts";
    RedirectPathEnum["ADD_ACCOUNT"] = "modifyaccounts/1";
    RedirectPathEnum["RECHARGE"] = "recharge";
    RedirectPathEnum["TRANSACTIONS"] = "transactions";
    RedirectPathEnum["FAULT_REPORT"] = "faultreport";
    RedirectPathEnum["SUPPORT"] = "support";
    RedirectPathEnum["FORGOT_PASSWORD"] = "forgotpassword";
    RedirectPathEnum["REGISTER"] = "register";
    RedirectPathEnum["FREQUENT_QUESTIONS"] = "frequentquestions";
    RedirectPathEnum["WIDGET_MY_ACCOUNT"] = "account";
    RedirectPathEnum["WIDGET_PAY"] = "invoice";
    RedirectPathEnum["WIDGET_CHAT"] = "chat";
})(RedirectPathEnum || (RedirectPathEnum = {}));


/***/ }),

/***/ "./src/app/utils/utils.ts":
/*!********************************!*\
  !*** ./src/app/utils/utils.ts ***!
  \********************************/
/*! exports provided: Utils */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Utils", function() { return Utils; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");

var Utils_1;


/* this class should be used only for static methods */
let Utils = Utils_1 = class Utils {
    static validateEmail(email) {
        const expr = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return expr.test(String(email).toLowerCase());
    }
    static isPostpaid(accountType, accountSubType, productType) {
        return (accountType === 'I2' && accountSubType === '4') ||
            (accountType === 'I3' && accountSubType === '4') ||
            (accountType === 'I' && accountSubType === 'R') ||
            (accountType === 'I' && accountSubType === '4') ||
            (accountType === 'I' && accountSubType === 'E') ||
            (accountType === 'I' && accountSubType === 'S' && productType === 'G');
    }
    static isPrepaid(accountType, accountSubType) {
        return (accountType === 'I' && accountSubType === 'P') ||
            (accountType === 'I3' && accountSubType === 'P');
    }
    static isTelephony(accountType, accountSubType, productType) {
        return (accountType === 'I' && accountSubType === 'W') ||
            (accountType === 'I' && accountSubType === 'S' && productType === 'O') ||
            (accountType === 'I' && accountSubType === 'S' && productType === 'V') ||
            (accountType === 'F' && accountSubType === '4');
    }
    static isByop(accountType, accountSubType, productType) {
        return (accountType === 'I3' && accountSubType === '4') ||
            (accountType === 'I3' && accountSubType === 'p');
    }
    static isBusinessAccount(accountType) {
        return accountType === 'B' || accountType === 'G';
    }
    static objectTypeOfSubscriber(accountType, accountSubType, productType) {
        return {
            postpaid: Utils_1.isPostpaid(accountType, accountSubType, productType),
            prepaid: Utils_1.isPrepaid(accountType, accountSubType),
            telephony: Utils_1.isTelephony(accountType, accountSubType, productType),
            byop: Utils_1.isByop(accountType, accountSubType, productType)
        };
    }
    static objectTypeOfAccount(accountType, accountSubType, productType) {
        return this.objectTypeOfSubscriber(accountType, accountSubType, productType);
    }
    /*
     * G or C = WLS, Others = WRL
     */
    static subscriberIsMobile(productType) {
        return (productType === 'C' || productType === 'G');
    }
    static isDLS(accountSubtype, accountType) {
        return (accountSubtype === 'W' && accountType === 'I');
    }
    static formatAmount(amount) {
        if (amount === undefined || amount === null || amount === '' || amount === 'not_set') {
            amount = '0.00';
        }
        amount = parseFloat(String(amount).replace('$', '')).toFixed(2);
        return amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }
    static typeOfTelephony(productType) {
        // O = wireline - fijo, I = IPTV - fijo, V = VOIP - fijo, N = ISP -fijo, J = DTH - fijo, S = DISH nuevo - fijo
        // C = cellular - móvil, G = GSM - móvil, P & K móvil if business
        let type;
        if (productType === 'O' || productType === 'I' || productType === 'V' || productType === 'N' || productType === 'J' || productType === 'S') {
            type = 'Fijo';
        }
        else if (productType === 'C' || productType === 'G' || productType === 'P' || productType === 'K') {
            type = 'Móvil';
        }
        return type;
    }
    static convertCase(str) {
        const lower = String(str).toLowerCase();
        return lower.replace(/(^| )(\w)/g, (x) => x.toUpperCase());
    }
    /*
    dateForTimePassword() {
        var now = new Date();
        now.setTime(now.getTime() + (1000 * 60 * app.sessionPasswordTime));
        return now;
    }
    */
    static transformAvailable(remaining) {
        const remainingMB = (remaining / 1024) / 1024;
        const remainingGB = ((remaining / 1024) / 1024) / 1024;
        let text = remainingMB.toFixed(2) + ' MB';
        if (remainingMB > 2048) {
            text = remainingGB.toFixed(2) + ' GB';
        }
        return text;
    }
    static formatSubscriber(subscriber) {
        let newNumber = '(' + splice(subscriber + '', 3, 0, ') ');
        newNumber = splice(newNumber, 9, 0, '-');
        return newNumber;
    }
    static parseDate(dateString) {
        let date;
        if (dateString) {
            date = new Date(dateString);
        }
        else {
            date = new Date();
        }
        const month = date.getMonth();
        const dayOfMonth = date.getDate();
        const dayOfWeek = date.getDay();
        const year = date.getFullYear();
        const daysList = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'];
        const monthList = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        return daysList[dayOfWeek] + ', ' + dayOfMonth + ' de ' + monthList[month] + ' de ' + year;
    }
    static getSubscriberByNumber(iNumber, subscribers) {
        let subscriber = null;
        subscribers.forEach((object) => {
            if (iNumber === object.subscriberNumberField) {
                subscriber = object;
            }
        });
        return subscriber;
    }
    static getCardMonths() {
        // Months
        const months = [];
        for (let j = 1; j <= 12; j++) {
            months.push((j < 10) ? '0' + j : j);
        }
        return months;
    }
    static getCardYears() {
        // Years
        const years = [];
        const today = Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["formatDate"])(new Date(), 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
        let now = Number(today.substring(6, 10));
        for (let i = 0; i < 10; i++) {
            years.push(now);
            now++;
        }
        return years;
    }
    static getLast10Years() {
        // Years
        const years = [];
        const today = Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["formatDate"])(new Date(), 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
        let now = Number(today.substring(6, 10));
        for (let i = 0; i < 10; i++) {
            years.push(now);
            now--;
        }
        return years;
    }
    static getCurrentMonth() {
        const today = Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["formatDate"])(new Date(), 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
        let month = today.substring(3, 5);
        if (month.length === 1) {
            month = '0' + month;
        }
        console.log(month);
        return month;
    }
    static getCurrentYear() {
        const today = Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["formatDate"])(new Date(), 'dd-MM-yyyy hh:mm:ss a', 'en-US', '+0530');
        return today.substring(6, 10);
    }
    static getPlatformInfo() {
        return {
            android: (navigator.userAgent.toLowerCase().indexOf('android') > -1),
            iphone: (navigator.userAgent.toLowerCase().indexOf('iphone') > -1),
            ipad: (navigator.userAgent.toLowerCase().indexOf('ipad') > -1),
            ios: (navigator.userAgent.toLowerCase().indexOf('iphone') > -1)
                || (navigator.userAgent.toLowerCase().indexOf('ipad') > -1),
            desktop: !window.hasOwnProperty('cordova')
        };
    }
    static formatStringDate(stringDate) {
        return new Date(stringDate).toLocaleString();
    }
    static formatStringDateForSIG(stringDate) {
        return Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["formatDate"])(new Date(stringDate), 'yyyy-MM-dd hh:mm', 'en-US');
    }
    static formatMMDDYYYYToYYYY_MM_DD(stringDate) {
        const text = stringDate.split('/');
        return `${text[2]}-${text[0]}-${text[1]}`;
    }
};
Utils = Utils_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
], Utils);

function splice(text, start, delCount, newSubStr) {
    return text.slice(0, start) + newSubStr + text.slice(start + Math.abs(delCount));
}


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false,
    // PROCESS_URL: 'http://rebranding.claroinfo.com/proccess/procesos-dev.aspx', // QA URL
    // PROCESS_URL: 'https://us-central1-usf-crud-firebase.cloudfunctions.net/rebranding', // MOCK SERVER
    // PROCESS_URL: 'https://miclaro.claropr.com/proccess/procesos-dev.aspx', // PROD
    // PROCESS_URL: 'http://miclaroreferals.claroinfo.com/proccess/procesos-mobile.aspx', // MOBILE (DEV)
    // PROCESS_URL: 'https://miclaro.claropr.com/proccess/procesos-mobile.aspx', // MOBILE (PROD)
    // PROCESS_URL: 'https://mobileappios.claropr.com/proccess/procesos-mobile.aspx', // MOBILE (DEV)
    PROCESS_URL: 'https://pciapp3.claropr.com/proccess/procesos-mobile-dev.aspx',
    // PROCESS_URL: 'https://pciapp3.claropr.com/proccess/procesos-mobile.aspx', // PROD 2020
    HELP_URL: 'http://soporteapps.speedymovil.com:8090/appFeedback/service/feedback/application',
    CHAT_URL: 'https://chat3.claropr.com/webapiserver/ECSApp/ChatWidget3/ChatPanel.aspx',
    API_URL: 'https://wsclarorprodnew.claropr.com/api-miclaro-services-prod-new/miclaro/',
    STORE_URL: 'https://tienda.claropr.com/',
    DISTANCE_MEASURE: 'mi',
    GATEWAY_APP_ID: 'At01bMi0aXhr6ktmTaow',
    GROUP_ID: 'group.com.claro.pr.MiClaro',
    // GROUP_ID: 'group.com.todoclaro.miclaroapp.test', // IOS DEV
    PACKET_NAME: 'com.todoclaro.miclaroapp',
    BIOMETRIC_SECRET_KEY: 'todoclaro-biometric',
    WEB_PAGE_BUSINESS: 'https://empresas.claropr.com',
    // WEB_PAGE_BUSINESS: 'http://empresas.claroinfo.com/', // DEV
    // FAULT_REPORT_CHECK_URL: 'https://registro.claropr.com/averias',
    FAULT_REPORT_CHECK_URL: 'https://miclaro.claropr.com/api/Authenticate/CheckIfTicketExists',
    // FAULT_REPORT_CREATE_URL: 'https://registro.claropr.com/Create',
    FAULT_REPORT_CREATE_URL: 'https://miclaro.claropr.com/api/Authenticate/RaiseIssueTicket',
    ID: '775322054',
    PAYMENT_ID_ANDROID: 'MICLAROANDROID',
    PAYMENT_TOKEN_ANDROID: '8JNJr579UI363751',
    PAYMENT_ID_IOS: 'MICLAROIOS',
    PAYMENT_TOKEN_IOS: 'd6pT5bnztZGrsv49',
    // TIME OUT for https requests
    TIMEOUT: 60 * 1000,
    siteKey: '6Ldo2yQaAAAAAGLxadch63xDKfiMwz0w4off74ry'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/admin/Documents/E$GS/REBRNADING/Respaldos/ionic/miclaro3-ionic-version/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map