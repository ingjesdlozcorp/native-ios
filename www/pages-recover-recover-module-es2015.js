(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-recover-recover-module"],{

/***/ "./node_modules/ng-recaptcha/fesm2015/ng-recaptcha.js":
/*!************************************************************!*\
  !*** ./node_modules/ng-recaptcha/fesm2015/ng-recaptcha.js ***!
  \************************************************************/
/*! exports provided: RECAPTCHA_BASE_URL, RECAPTCHA_LANGUAGE, RECAPTCHA_NONCE, RECAPTCHA_SETTINGS, RECAPTCHA_V3_SITE_KEY, ReCaptchaV3Service, RecaptchaComponent, RecaptchaFormsModule, RecaptchaLoaderService, RecaptchaModule, RecaptchaV3Module, RecaptchaValueAccessorDirective, ɵa */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RECAPTCHA_BASE_URL", function() { return RECAPTCHA_BASE_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RECAPTCHA_LANGUAGE", function() { return RECAPTCHA_LANGUAGE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RECAPTCHA_NONCE", function() { return RECAPTCHA_NONCE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RECAPTCHA_SETTINGS", function() { return RECAPTCHA_SETTINGS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RECAPTCHA_V3_SITE_KEY", function() { return RECAPTCHA_V3_SITE_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReCaptchaV3Service", function() { return ReCaptchaV3Service; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecaptchaComponent", function() { return RecaptchaComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecaptchaFormsModule", function() { return RecaptchaFormsModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecaptchaLoaderService", function() { return RecaptchaLoaderService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecaptchaModule", function() { return RecaptchaModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecaptchaV3Module", function() { return RecaptchaV3Module; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecaptchaValueAccessorDirective", function() { return RecaptchaValueAccessorDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return RecaptchaCommonModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");





function loadScript(renderMode, onLoaded, urlParams, url, nonce) {
    window.ng2recaptchaloaded = () => {
        onLoaded(grecaptcha);
    };
    const script = document.createElement("script");
    script.innerHTML = "";
    const baseUrl = url || "https://www.google.com/recaptcha/api.js";
    script.src = `${baseUrl}?render=${renderMode}&onload=ng2recaptchaloaded${urlParams}`;
    if (nonce) {
        script.nonce = nonce;
    }
    script.async = true;
    script.defer = true;
    document.head.appendChild(script);
}

const RECAPTCHA_LANGUAGE = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]("recaptcha-language");
const RECAPTCHA_BASE_URL = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]("recaptcha-base-url");
const RECAPTCHA_NONCE = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]("recaptcha-nonce-tag");
const RECAPTCHA_SETTINGS = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]("recaptcha-settings");
const RECAPTCHA_V3_SITE_KEY = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]("recaptcha-v3-site-key");

class RecaptchaLoaderService {
    constructor(
    // eslint-disable-next-line @typescript-eslint/ban-types
    platformId, language, baseUrl, nonce, v3SiteKey) {
        this.platformId = platformId;
        this.language = language;
        this.baseUrl = baseUrl;
        this.nonce = nonce;
        this.v3SiteKey = v3SiteKey;
        this.init();
        this.ready = Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(this.platformId)
            ? RecaptchaLoaderService.ready.asObservable()
            : Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])();
    }
    /** @internal */
    init() {
        if (RecaptchaLoaderService.ready) {
            return;
        }
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(this.platformId)) {
            const subject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
            RecaptchaLoaderService.ready = subject;
            const langParam = this.language ? "&hl=" + this.language : "";
            const renderMode = this.v3SiteKey || "explicit";
            loadScript(renderMode, (grecaptcha) => subject.next(grecaptcha), langParam, this.baseUrl, this.nonce);
        }
    }
}
/**
 * @internal
 * @nocollapse
 */
RecaptchaLoaderService.ready = null;
RecaptchaLoaderService.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] }
];
RecaptchaLoaderService.ctorParameters = () => [
    { type: Object, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"],] }] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [RECAPTCHA_LANGUAGE,] }] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [RECAPTCHA_BASE_URL,] }] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [RECAPTCHA_NONCE,] }] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [RECAPTCHA_V3_SITE_KEY,] }] }
];

let nextId = 0;
class RecaptchaComponent {
    constructor(elementRef, loader, zone, settings) {
        this.elementRef = elementRef;
        this.loader = loader;
        this.zone = zone;
        this.id = `ngrecaptcha-${nextId++}`;
        this.errorMode = "default";
        this.resolved = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        // The rename will happen a bit later
        // eslint-disable-next-line @angular-eslint/no-output-native
        this.error = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        if (settings) {
            this.siteKey = settings.siteKey;
            this.theme = settings.theme;
            this.type = settings.type;
            this.size = settings.size;
            this.badge = settings.badge;
        }
    }
    ngAfterViewInit() {
        this.subscription = this.loader.ready.subscribe((grecaptcha) => {
            if (grecaptcha != null && grecaptcha.render instanceof Function) {
                this.grecaptcha = grecaptcha;
                this.renderRecaptcha();
            }
        });
    }
    ngOnDestroy() {
        // reset the captcha to ensure it does not leave anything behind
        // after the component is no longer needed
        this.grecaptchaReset();
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    /**
     * Executes the invisible recaptcha.
     * Does nothing if component's size is not set to "invisible".
     */
    execute() {
        if (this.size !== "invisible") {
            return;
        }
        if (this.widget != null) {
            this.grecaptcha.execute(this.widget);
        }
        else {
            // delay execution of recaptcha until it actually renders
            this.executeRequested = true;
        }
    }
    reset() {
        if (this.widget != null) {
            if (this.grecaptcha.getResponse(this.widget)) {
                // Only emit an event in case if something would actually change.
                // That way we do not trigger "touching" of the control if someone does a "reset"
                // on a non-resolved captcha.
                this.resolved.emit(null);
            }
            this.grecaptchaReset();
        }
    }
    /** @internal */
    expired() {
        this.resolved.emit(null);
    }
    /** @internal */
    errored(args) {
        this.error.emit(args);
    }
    /** @internal */
    captchaResponseCallback(response) {
        this.resolved.emit(response);
    }
    /** @internal */
    grecaptchaReset() {
        if (this.widget != null) {
            this.zone.runOutsideAngular(() => this.grecaptcha.reset(this.widget));
        }
    }
    /** @internal */
    renderRecaptcha() {
        // This `any` can be removed after @types/grecaptcha get updated
        const renderOptions = {
            badge: this.badge,
            callback: (response) => {
                this.zone.run(() => this.captchaResponseCallback(response));
            },
            "expired-callback": () => {
                this.zone.run(() => this.expired());
            },
            sitekey: this.siteKey,
            size: this.size,
            tabindex: this.tabIndex,
            theme: this.theme,
            type: this.type,
        };
        if (this.errorMode === "handled") {
            renderOptions["error-callback"] = (...args) => {
                this.zone.run(() => this.errored(args));
            };
        }
        this.widget = this.grecaptcha.render(this.elementRef.nativeElement, renderOptions);
        if (this.executeRequested === true) {
            this.executeRequested = false;
            this.execute();
        }
    }
}
RecaptchaComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                exportAs: "reCaptcha",
                selector: "re-captcha",
                template: ``
            },] }
];
RecaptchaComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"] },
    { type: RecaptchaLoaderService },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [RECAPTCHA_SETTINGS,] }] }
];
RecaptchaComponent.propDecorators = {
    id: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"], args: ["attr.id",] }],
    siteKey: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    theme: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    type: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    size: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    tabIndex: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    badge: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    errorMode: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
    resolved: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
    error: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }]
};

class RecaptchaCommonModule {
}
RecaptchaCommonModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                declarations: [RecaptchaComponent],
                exports: [RecaptchaComponent],
            },] }
];

class RecaptchaModule {
}
RecaptchaModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                exports: [RecaptchaComponent],
                imports: [RecaptchaCommonModule],
                providers: [RecaptchaLoaderService],
            },] }
];

/**
 * The main service for working with reCAPTCHA v3 APIs.
 *
 * Use the `execute` method for executing a single action, and
 * `onExecute` observable for listening to all actions at once.
 */
class ReCaptchaV3Service {
    constructor(zone, siteKey, 
    // eslint-disable-next-line @typescript-eslint/ban-types
    platformId, baseUrl, nonce, language) {
        /** @internal */
        this.onLoadComplete = (grecaptcha) => {
            this.grecaptcha = grecaptcha;
            if (this.actionBacklog && this.actionBacklog.length > 0) {
                this.actionBacklog.forEach(([action, subject]) => this.executeActionWithSubject(action, subject));
                this.actionBacklog = undefined;
            }
        };
        this.zone = zone;
        this.isBrowser = Object(_angular_common__WEBPACK_IMPORTED_MODULE_1__["isPlatformBrowser"])(platformId);
        this.siteKey = siteKey;
        this.nonce = nonce;
        this.language = language;
        this.baseUrl = baseUrl;
        this.init();
    }
    get onExecute() {
        if (!this.onExecuteSubject) {
            this.onExecuteSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
            this.onExecuteObservable = this.onExecuteSubject.asObservable();
        }
        return this.onExecuteObservable;
    }
    get onExecuteError() {
        if (!this.onExecuteErrorSubject) {
            this.onExecuteErrorSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
            this.onExecuteErrorObservable = this.onExecuteErrorSubject.asObservable();
        }
        return this.onExecuteErrorObservable;
    }
    /**
     * Executes the provided `action` with reCAPTCHA v3 API.
     * Use the emitted token value for verification purposes on the backend.
     *
     * For more information about reCAPTCHA v3 actions and tokens refer to the official documentation at
     * https://developers.google.com/recaptcha/docs/v3.
     *
     * @param {string} action the action to execute
     * @returns {Observable<string>} an `Observable` that will emit the reCAPTCHA v3 string `token` value whenever ready.
     * The returned `Observable` completes immediately after emitting a value.
     */
    execute(action) {
        const subject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        if (this.isBrowser) {
            if (!this.grecaptcha) {
                // todo: add to array of later executions
                if (!this.actionBacklog) {
                    this.actionBacklog = [];
                }
                this.actionBacklog.push([action, subject]);
            }
            else {
                this.executeActionWithSubject(action, subject);
            }
        }
        return subject.asObservable();
    }
    /** @internal */
    executeActionWithSubject(action, subject) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const onError = (error) => {
            this.zone.run(() => {
                subject.error(error);
                if (this.onExecuteErrorSubject) {
                    // We don't know any better at this point, unfortunately, so have to resort to `any`
                    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                    this.onExecuteErrorSubject.next({ action, error });
                }
            });
        };
        this.zone.runOutsideAngular(() => {
            try {
                this.grecaptcha
                    .execute(this.siteKey, { action })
                    .then((token) => {
                    this.zone.run(() => {
                        subject.next(token);
                        subject.complete();
                        if (this.onExecuteSubject) {
                            this.onExecuteSubject.next({ action, token });
                        }
                    });
                }, onError);
            }
            catch (e) {
                onError(e);
            }
        });
    }
    /** @internal */
    init() {
        if (this.isBrowser) {
            if ("grecaptcha" in window) {
                this.grecaptcha = grecaptcha;
            }
            else {
                const langParam = this.language ? "&hl=" + this.language : "";
                loadScript(this.siteKey, this.onLoadComplete, langParam, this.baseUrl, this.nonce);
            }
        }
    }
}
ReCaptchaV3Service.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] }
];
ReCaptchaV3Service.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [RECAPTCHA_V3_SITE_KEY,] }] },
    { type: Object, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"],] }] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [RECAPTCHA_BASE_URL,] }] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [RECAPTCHA_NONCE,] }] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [RECAPTCHA_LANGUAGE,] }] }
];

class RecaptchaV3Module {
}
RecaptchaV3Module.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                providers: [ReCaptchaV3Service],
            },] }
];

class RecaptchaValueAccessorDirective {
    constructor(host) {
        this.host = host;
    }
    writeValue(value) {
        if (!value) {
            this.host.reset();
        }
    }
    registerOnChange(fn) {
        this.onChange = fn;
    }
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    onResolve($event) {
        if (this.onChange) {
            this.onChange($event);
        }
        if (this.onTouched) {
            this.onTouched();
        }
    }
}
RecaptchaValueAccessorDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                providers: [
                    {
                        multi: true,
                        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NG_VALUE_ACCESSOR"],
                        // tslint:disable-next-line:no-forward-ref
                        useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(() => RecaptchaValueAccessorDirective),
                    },
                ],
                // tslint:disable-next-line:directive-selector
                selector: "re-captcha[formControlName],re-captcha[formControl],re-captcha[ngModel]",
            },] }
];
RecaptchaValueAccessorDirective.ctorParameters = () => [
    { type: RecaptchaComponent }
];
RecaptchaValueAccessorDirective.propDecorators = {
    onResolve: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ["resolved", ["$event"],] }]
};

class RecaptchaFormsModule {
}
RecaptchaFormsModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                declarations: [RecaptchaValueAccessorDirective],
                exports: [RecaptchaValueAccessorDirective],
                imports: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], RecaptchaCommonModule],
            },] }
];

/**
 * Generated bundle index. Do not edit.
 */


//# sourceMappingURL=ng-recaptcha.js.map


/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/recover.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/recover.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <router-outlet></router-outlet>\n\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step1/step1.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step1/step1.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                <span>\n                                    &#191;Olvidaste tu contrase&ntilde;a&#63;\n                                </span>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"ok\">\n                        <div class=\"basicrow m-top-i\">\n                            <div class=\"basicrow\">\n                                <input type=\"text\"\n                                    id=\"username\"\n                                    name=\"username\"\n                                    [(ngModel)]=\"username\"\n                                    placeholder=\"Correo o N&uacute;mero de Tel&eacute;fono\"\n                                    class=\"inp-f\"\n                                    autocapitalize=\"off\">\n                            </div>\n                        </div>\n\n                        <div class=\"basicrow m-top-i\">\n\n                            <form #captchaProtectedForm=\"ngForm\" class=\"captcha\">\n                                <re-captcha\n                                    #captchaRef=\"reCaptcha\"\n                                    [(ngModel)]=\"formModel.captcha\"\n                                    name=\"captcha\"\n                                    required\n                                    [siteKey]=\"siteKey\"\n                                    #captchaControl=\"ngModel\"\n                                ></re-captcha>\n                              </form>\n\n                        </div>\n\n\n                        <div class=\"basicrow m-top-i\">\n                            <div [className]=\"(formModel && formModel.captcha) ? 'btns vcenter red rippleR' : 'btns vcenter gray'\" (click)=\"nextStep()\">\n                                <div class=\"tabcell\">\n                                    Continuar\n                                </div>\n                            </div>\n                        </div>\n\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step2/step2.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step2/step2.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                &#191;Olvidaste tu contrase&ntilde;a&#63;\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow f-reg f-black roboto-r m-top-i text-justify\">\n                    Escoge entre las opciones para recuperación de acceso a tu cuenta.\n                </div>\n\n                <div class=\"basicrow\">\n                    <div class=\"basicrow m-top-i text-left\">\n                        <label>\n                            <input [(ngModel)]=\"selected\" type=\"radio\" name=\"group\" [value]=\"1\" checked/>\n                            <span *ngIf=\"isTelephony\">&nbsp;V&iacute;a correo electr&oacute;nico</span>\n                            <span *ngIf=\"!isTelephony\">&nbsp;V&iacute;a SMS o correo electr&oacute;nico</span>\n                        </label>\n                    </div>\n\n                    <div class=\"basicrow m-top text-left\">\n                        <label>\n                            <input [(ngModel)]=\"selected\" type=\"radio\" name=\"group\" [value]=\"2\"/>\n                            <span>&nbsp;Contestar preguntas de Seguridad</span>\n                        </label>\n                    </div>\n                </div>\n\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"btns red vcenter rippleR\" (click)=\"nextStep()\">\n                        <div class=\"tabcell\">\n                            Continuar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step3/step3.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step3/step3.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                &#191;Olvidaste tu contrase&ntilde;a&#63;\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                    &nbsp;&nbsp;&nbsp;Por favor responde las preguntas de seguridad previamente configuradas en tu cuenta de acceso.\n                </div>\n\n                <div class=\"basicrow m-top-i text-left\">\n                    <label id=\"question1\" class=\"label-f\">{{firstQuestion}}</label>\n                    <div class=\"basicrow rel\">\n                        <input type=\"password\" placeholder=\"Respuesta\" class=\"inp-f\" autocapitalize=\"off\" [(ngModel)]=\"firstAnswer\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top text-left\">\n                    <label id=\"question2\" class=\"label-f\">{{secondQuestion}}</label>\n                    <div class=\"basicrow\">\n                        <input type=\"password\" placeholder=\"Respuesta\" class=\"inp-f\" autocapitalize=\"off\" [(ngModel)]=\"secondAnswer\">\n                    </div>\n                </div>\n\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"btns red vcenter rippleR\" (click)=\"nextStep()\">\n                        <div class=\"tabcell\">\n                            Recuperar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step4/step4.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step4/step4.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                &#191;Olvidaste tu contrase&ntilde;a&#63;\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow f-reg f-black roboto-r m-top-i text-justify\">\n                    Escoja d&oacute;nde deseas recibir la contrase&ntilde;a temporal.\n                </div>\n\n                <div class=\"basicrow f-reg f-black roboto-r\">\n                    <div class=\"row\">\n\n                        <div *ngIf=\"!isTelephony\" class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n                            <div [ngClass]=\"{'on': bySubscriber}\" class=\"optchoice roboto-r text-left\" (click)=\"byEmail = false; bySubscriber = true; $event.stopPropagation()\">\n                                <div class=\"checkdef\">\n                                    <input type=\"checkbox\" class=\"css-radio\" [(ngModel)]=\"bySubscriber\"/><label class=\"css-label3 radGroup1\"></label>\n                                </div>\n\n                                <div class=\"basicrow opttext\">\n                                    N&uacute;mero Registrado<br/>\n                                    <span class=\"roboto-b\">{{subscriberHidden}}</span>\n                                </div>\n                            </div>\n                        </div>\n\n                        <div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n                            <div [ngClass]=\"{'on': byEmail}\" class=\"optchoice roboto-r text-left\" (click)=\"byEmail = true; bySubscriber = false; $event.stopPropagation()\">\n                                <div class=\"checkdef\">\n                                    <input type=\"checkbox\" class=\"css-radio\" [(ngModel)]=\"byEmail\"/><label class=\"css-label3 radGroup1\"></label>\n                                </div>\n\n                                <div class=\"basicrow opttext\">\n                                    Email Registrado<br/>\n                                    <span class=\"roboto-b\">{{emailHidden}}</span>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div (click)=\"nextStep()\" [className]=\"byEmail || bySubscriber ? 'btns vcenter red rippleR' : 'btns vcenter gray'\">\n                        <div class=\"tabcell\">\n                            Recuperar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step5/step5.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step5/step5.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                &#191;Olvidaste tu contrase&ntilde;a&#63;\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                        Debe modificar la clave de acceso para poder ingresar a su cuenta Claro.\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"oldPassword\" placeholder=\"Contrase&ntilde;a temporal\" class=\"inp-f\" maxlength=\"15\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"password\" placeholder=\"Nueva Contrase&ntilde;a\" class=\"inp-f\" maxlength=\"15\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"passwordRepeat\" placeholder=\"Confirmar Contrase&ntilde;a\" class=\"inp-f\" maxlength=\"15\" (keyup.enter)=\"nextStep()\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow f-black m-top roboto-r\">\n                        <div class=\"basicrow f-lmini roboto-b\">\n                            Requerimientos de contrase&ntilde;a:\n                        </div>\n\n                        <div class=\"basicrow\">\n                            <ul class=\"redb\">\n                                <li [ngClass]=\"{'done': (password.length >= 8 && password.length <= 16)}\">\n                                    Debe tener entre 8 y 15 caracteres\n                                </li>\n\n                                <li [ngClass]=\"{'done': lowercaseValidator(password)}\">\n                                    Al menos una letra min&uacute;scula\n                                </li>\n\n                                <li [ngClass]=\"{'done': uppercaseValidator(password)}\">\n                                    Al menos una letra may&uacute;scula\n                                </li>\n\n                                <li [ngClass]=\"{'done': twoNumberValidator(password)}\">\n                                    Al menos 2 n&uacute;meros\n                                </li>\n\n                                <li [ngClass]=\"{'done': specialCharacterValidator(password)}\">\n                                    No est&aacute; permitido ning&uacute;n car&aacute;cter especial o no alfanum&eacute;rico\n                                </li>\n\n                                <li [ngClass]=\"{'done': password === passwordRepeat && password.length > 0}\">\n                                    Las contrase&ntilde;as deben coincidir\n                                </li>\n                            </ul>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div (click)=\"nextStep()\" [className]=\"\n                        (password.length >= 8 && password.length <= 16)\n                         && lowercaseValidator(password)\n                         && uppercaseValidator(password)\n                         && twoNumberValidator(password)\n                         && specialCharacterValidator(password)\n                         && (password === passwordRepeat && password.length > 0)\n                         ? 'btns vcenter red rippleR' : 'btns vcenter gray'\">\n                            <div class=\"tabcell\">\n                                Continuar\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow text-center m-top-i\">\n                        <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step6/step6.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step6/step6.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                &#191;Olvidaste tu contrase&ntilde;a&#63;\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                        La contraseña de acceso es la clave con la que podrás acceder a manejar tu cuenta de servicios en Claro.\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"password\" placeholder=\"Nueva Contrase&ntilde;a\" class=\"inp-f\" maxlength=\"15\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"passwordRepeat\" placeholder=\"Confirmar Contrase&ntilde;a\" class=\"inp-f\" maxlength=\"15\" (keyup.enter)=\"nextStep()\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow f-black m-top roboto-r\">\n                        <div class=\"basicrow f-lmini roboto-b\">\n                            Requerimientos de contrase&ntilde;a:\n                        </div>\n\n                        <div class=\"basicrow\">\n                            <ul class=\"redb\">\n                                <li [ngClass]=\"{'done': (password.length >= 8 && password.length <= 16)}\">\n                                    Debe tener entre 8 y 15 caracteres\n                                </li>\n\n                                <li [ngClass]=\"{'done': lowercaseValidator(password)}\">\n                                    Al menos una letra min&uacute;scula\n                                </li>\n\n                                <li [ngClass]=\"{'done': uppercaseValidator(password)}\">\n                                    Al menos una letra may&uacute;scula\n                                </li>\n\n                                <li [ngClass]=\"{'done': twoNumberValidator(password)}\">\n                                    Al menos 2 n&uacute;meros\n                                </li>\n\n                                <li [ngClass]=\"{'done': specialCharacterValidator(password)}\">\n                                    No est&aacute; permitido ning&uacute;n car&aacute;cter especial o no alfanum&eacute;rico\n                                </li>\n\n                                <li [ngClass]=\"{'done': password === passwordRepeat && password.length > 0}\">\n                                    Las contrase&ntilde;as deben coincidir\n                                </li>\n                            </ul>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div (click)=\"nextStep()\" [className]=\"\n                        (password.length >= 8 && password.length <= 16)\n                         && lowercaseValidator(password)\n                         && uppercaseValidator(password)\n                         && twoNumberValidator(password)\n                         && specialCharacterValidator(password)\n                         && (password === passwordRepeat && password.length > 0)\n                         ? 'btns vcenter red rippleR' : 'btns vcenter gray'\">\n                            <div class=\"tabcell\">\n                                Continuar\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow text-center m-top-i\">\n                        <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./src/app/pages/recover/recover.component.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/recover/recover.component.ts ***!
  \****************************************************/
/*! exports provided: RecoverComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecoverComponent", function() { return RecoverComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let RecoverComponent = class RecoverComponent {
    constructor() { }
    ngOnInit() {
    }
};
RecoverComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-recover',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./recover.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/recover.component.html")).default
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], RecoverComponent);



/***/ }),

/***/ "./src/app/pages/recover/recover.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/recover/recover.module.ts ***!
  \*************************************************/
/*! exports provided: routes, RecoverModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecoverModule", function() { return RecoverModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_shared/shared.module */ "./src/app/pages/_shared/shared.module.ts");
/* harmony import */ var _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./step1/step1.component */ "./src/app/pages/recover/step1/step1.component.ts");
/* harmony import */ var _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./step2/step2.component */ "./src/app/pages/recover/step2/step2.component.ts");
/* harmony import */ var _step3_step3_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./step3/step3.component */ "./src/app/pages/recover/step3/step3.component.ts");
/* harmony import */ var _step4_step4_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./step4/step4.component */ "./src/app/pages/recover/step4/step4.component.ts");
/* harmony import */ var _step5_step5_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./step5/step5.component */ "./src/app/pages/recover/step5/step5.component.ts");
/* harmony import */ var _step6_step6_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./step6/step6.component */ "./src/app/pages/recover/step6/step6.component.ts");
/* harmony import */ var _recover_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./recover.component */ "./src/app/pages/recover/recover.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var ngx_popper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-popper */ "./node_modules/ngx-popper/fesm2015/ngx-popper.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var ng_recaptcha__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ng-recaptcha */ "./node_modules/ng-recaptcha/fesm2015/ng-recaptcha.js");















const routes = [
    { path: 'step1', component: _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__["Step1Component"] },
    { path: 'step2', component: _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__["Step2Component"] },
    { path: 'step3', component: _step3_step3_component__WEBPACK_IMPORTED_MODULE_6__["Step3Component"] },
    { path: 'step4', component: _step4_step4_component__WEBPACK_IMPORTED_MODULE_7__["Step4Component"] },
    { path: 'step5', component: _step5_step5_component__WEBPACK_IMPORTED_MODULE_8__["Step5Component"] },
    { path: 'step6', component: _step6_step6_component__WEBPACK_IMPORTED_MODULE_9__["Step6Component"] }
];
let RecoverModule = class RecoverModule {
};
RecoverModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        providers: [
            {
                provide: ng_recaptcha__WEBPACK_IMPORTED_MODULE_14__["RECAPTCHA_LANGUAGE"],
                useValue: 'es',
            },
        ],
        declarations: [
            _recover_component__WEBPACK_IMPORTED_MODULE_10__["RecoverComponent"],
            _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__["Step1Component"],
            _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__["Step2Component"],
            _step3_step3_component__WEBPACK_IMPORTED_MODULE_6__["Step3Component"],
            _step4_step4_component__WEBPACK_IMPORTED_MODULE_7__["Step4Component"],
            _step5_step5_component__WEBPACK_IMPORTED_MODULE_8__["Step5Component"],
            _step6_step6_component__WEBPACK_IMPORTED_MODULE_9__["Step6Component"]
        ],
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_13__["RouterModule"].forChild(routes),
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ReactiveFormsModule"],
            ng_recaptcha__WEBPACK_IMPORTED_MODULE_14__["RecaptchaFormsModule"],
            ng_recaptcha__WEBPACK_IMPORTED_MODULE_14__["RecaptchaModule"],
            ngx_popper__WEBPACK_IMPORTED_MODULE_12__["NgxPopperModule"].forRoot({ placement: 'top', styles: { 'background-color': 'white' } })
        ]
    })
], RecoverModule);



/***/ }),

/***/ "./src/app/pages/recover/step1/step1.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/recover/step1/step1.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".error {\n  color: crimson;\n}\n\n.success {\n  color: green;\n}\n\n.captcha {\n  display: flex;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hZG1pbi9Eb2N1bWVudHMvRSRHUy9SRUJSTkFESU5HL1Jlc3BhbGRvcy9pb25pYy9taWNsYXJvMy1pb25pYy12ZXJzaW9uL3NyYy9hcHAvcGFnZXMvcmVjb3Zlci9zdGVwMS9zdGVwMS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcGFnZXMvcmVjb3Zlci9zdGVwMS9zdGVwMS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7QUNDSjs7QURDQTtFQUNJLFlBQUE7QUNFSjs7QURDQTtFQUNJLGFBQUE7RUFBZSx1QkFBQTtBQ0duQiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY292ZXIvc3RlcDEvc3RlcDEuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXJyb3Ige1xuICAgIGNvbG9yOiBjcmltc29uO1xufVxuLnN1Y2Nlc3Mge1xuICAgIGNvbG9yOiBncmVlbjtcbn1cblxuLmNhcHRjaGEge1xuICAgIGRpc3BsYXk6IGZsZXg7IGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuIiwiLmVycm9yIHtcbiAgY29sb3I6IGNyaW1zb247XG59XG5cbi5zdWNjZXNzIHtcbiAgY29sb3I6IGdyZWVuO1xufVxuXG4uY2FwdGNoYSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/recover/step1/step1.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/recover/step1/step1.component.ts ***!
  \********************************************************/
/*! exports provided: Step1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Step1Component", function() { return Step1Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");












let Step1Component = class Step1Component extends _base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage, fb) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.fb = fb;
        this.formModel = {};
        this.utils.registerScreen('forgot_password');
    }
    nextStep() {
        if (this.formModel.captcha) {
            this.goNext();
        }
        else {
            this.showAlert('Debe completar el captcha para continuar');
        }
    }
    goNext() {
        if (!this.username) {
            this.showError('El número de teléfono o correo ingresado no se encuentra registrado en nuestros sistemas, ' +
                'su formato es incorrecto o no pertenece a nuestra red. Por favor intente nuevamente.');
            this.formModel.captcha = '';
        }
        else {
            this.showProgress();
            this.services.getChallengeQuestions(this.username).then((success) => {
                this.dismissProgress();
                this.processResponse(success);
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
                this.formModel.captcha = '';
            });
        }
    }
    processResponse(response) {
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.SUBSCRIBER, response.subscriber);
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.EMAIL, response.email);
        if (response.accountType === 'I' &&
            response.accountSubType === 'W' &&
            response.productType === 'O') {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.IS_TELEPHONY, true);
        }
        else {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.IS_TELEPHONY, false);
        }
        if (response.ResponseList == null) {
            this.goPage('/recover/step4');
        }
        else {
            const questions = [];
            for (const object of response.ResponseList) {
                questions.push({
                    question: object.question,
                    questionID: object.questionID,
                    response: object.response
                });
            }
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.QUESTION_LIST, questions);
            this.goPage('/recover/step2');
        }
        this.formModel.captcha = '';
    }
    get siteKey() {
        return _environments_environment__WEBPACK_IMPORTED_MODULE_10__["environment"].siteKey;
    }
};
Step1Component.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormBuilder"] }
];
Step1Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'recover-step1',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./step1.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step1/step1.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./step1.component.scss */ "./src/app/pages/recover/step1/step1.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"], _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormBuilder"]])
], Step1Component);



/***/ }),

/***/ "./src/app/pages/recover/step2/step2.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/recover/step2/step2.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY292ZXIvc3RlcDIvc3RlcDIuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/recover/step2/step2.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/recover/step2/step2.component.ts ***!
  \********************************************************/
/*! exports provided: Step2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Step2Component", function() { return Step2Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");









let Step2Component = class Step2Component extends _base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.username = '';
        this.isTelephony = false;
        this.selected = 1;
    }
    ngOnInit() { }
    nextStep() {
        console.log(this.selected);
        if (this.selected === 1) {
            this.goPage('/recover/step4');
        }
        if (this.selected === 2) {
            this.goPage('/recover/step3');
        }
    }
};
Step2Component.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"] }
];
Step2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'recover-step2',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./step2.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step2/step2.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./step2.component.scss */ "./src/app/pages/recover/step2/step2.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_8__["IntentProvider"]])
], Step2Component);



/***/ }),

/***/ "./src/app/pages/recover/step3/step3.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/recover/step3/step3.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY292ZXIvc3RlcDMvc3RlcDMuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/recover/step3/step3.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/recover/step3/step3.component.ts ***!
  \********************************************************/
/*! exports provided: Step3Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Step3Component", function() { return Step3Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");










let Step3Component = class Step3Component extends _base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.firstQuestion = '¿Primera Pregunta?';
        this.secondQuestion = '¿Segunda Pregunta?';
        this.firstAnswer = '';
        this.secondAnswer = '';
        this.questions = [];
    }
    ngOnInit() {
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].RECOVER_PASSWORD.QUESTION_LIST).then(questions => {
            this.questions = questions;
            if (this.questions.length > 0) {
                this.setQuestions();
            }
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].RECOVER_PASSWORD.SUBSCRIBER).then(success => {
            this.subscriber = success;
        });
    }
    setQuestions() {
        let question1 = this.questions[0].question;
        let question2 = this.questions[1].question;
        // init validate if you have question marks
        if (!question1.includes('¿')) {
            question1 = '¿' + question1;
        }
        if (!question1.includes('?')) {
            question1 = question1 + '?';
        }
        if (!question2.includes('¿')) {
            question2 = '¿' + question2;
        }
        if (!question2.includes('?')) {
            question2 = question2 + '?';
        }
        this.firstQuestion = question1;
        this.secondQuestion = question2;
    }
    nextStep() {
        if (this.firstAnswer.length <= 0 || this.secondAnswer.length <= 0) {
            this.showError('Por favor, complete los campos para continuar.');
        }
        else {
            this.questions[0].response = this.firstAnswer;
            this.questions[1].response = this.secondAnswer;
            this.showProgress();
            this.services.answerSecurityQuestions(this.subscriber, this.questions).then((success) => {
                this.dismissProgress();
                this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].RECOVER_PASSWORD.TEMPORARY_PASSWORD, success.newpassword);
                this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_8__["keys"].RECOVER_PASSWORD.TOKEN, success.token);
                this.goPage('/recover/step6');
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
        }
    }
};
Step3Component.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
Step3Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'recover-step3',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./step3.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step3/step3.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./step3.component.scss */ "./src/app/pages/recover/step3/step3.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_7__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], Step3Component);



/***/ }),

/***/ "./src/app/pages/recover/step4/step4.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/recover/step4/step4.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY292ZXIvc3RlcDQvc3RlcDQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/recover/step4/step4.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/recover/step4/step4.component.ts ***!
  \********************************************************/
/*! exports provided: Step4Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Step4Component", function() { return Step4Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");










let Step4Component = class Step4Component extends _base_page__WEBPACK_IMPORTED_MODULE_5__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.isTelephony = true;
        this.subscriberHidden = '';
        this.subscriber = '';
        this.emailHidden = '';
        this.email = '';
        this.bySubscriber = false;
        this.byEmail = false;
    }
    ngOnInit() {
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.IS_TELEPHONY).then(success => {
            this.isTelephony = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.SUBSCRIBER).then(success => {
            this.subscriber = success;
            this.subscriberHidden = success.substr(success.length - 4);
            this.subscriberHidden = '********' + this.subscriberHidden;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.EMAIL).then(success => {
            this.email = success;
            const first2 = success.substring(0, 2);
            const emailCut = success.split('@');
            this.emailHidden = first2 + '******@' + emailCut[1];
        });
    }
    nextStep() {
        if (!this.bySubscriber && !this.byEmail) {
            return;
        }
        else {
            this.showProgress();
            this.services.recoveryPasswordBySubscriber(this.subscriber, this.bySubscriber ? this.subscriber : '', this.byEmail ? this.email : '').then((success) => {
                this.showAlert(success.response, () => {
                    this.goLoginPage();
                });
                this.dismissProgress();
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
        }
    }
};
Step4Component.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
Step4Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'recover-step4',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./step4.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step4/step4.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./step4.component.scss */ "./src/app/pages/recover/step4/step4.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], Step4Component);



/***/ }),

/***/ "./src/app/pages/recover/step5/step5.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/recover/step5/step5.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY292ZXIvc3RlcDUvc3RlcDUuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/recover/step5/step5.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/recover/step5/step5.component.ts ***!
  \********************************************************/
/*! exports provided: Step5Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Step5Component", function() { return Step5Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");










let Step5Component = class Step5Component extends _base_page__WEBPACK_IMPORTED_MODULE_5__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.oldPassword = '';
        this.password = '';
        this.passwordRepeat = '';
        this.token = '';
    }
    ngOnInit() {
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.TOKEN).then(success => {
            this.token = success;
        });
    }
    nextStep() {
        if (this.password.length < 8 || this.password.length > 15
            || this.password !== this.passwordRepeat
            || !this.lowercaseValidator(this.password)
            || !this.uppercaseValidator(this.password)
            || !this.twoNumberValidator(this.password)
            || !this.specialCharacterValidator(this.password)) {
            return;
        }
        else if (this.oldPassword.length < 8) {
            this.showError('Hemos detectado que la clave temporal ingresada no es válida. Por favor intente nuevamente.');
        }
        else {
            this.showProgress();
            this.services.passwordUpdate(btoa(this.oldPassword), btoa(this.password), this.token).then((success) => {
                this.dismissProgress();
                this.showAlert(success.response, () => {
                    this.goLoginPage();
                });
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
        }
    }
    lowercaseValidator(c) {
        const regex = /[a-z]/g;
        return regex.test(c);
    }
    uppercaseValidator(c) {
        const regex = /[A-Z]/g;
        return regex.test(c);
    }
    twoNumberValidator(c) {
        const numbers = '0123456789';
        let count = 0;
        for (let i = 0; i < c.length; i++) {
            if (numbers.indexOf(c.charAt(i), 0) !== -1) {
                count++;
            }
        }
        return count >= 2;
    }
    specialCharacterValidator(c) {
        return c.match('^[A-z0-9]+$');
    }
};
Step5Component.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
Step5Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'recover-step5',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./step5.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step5/step5.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./step5.component.scss */ "./src/app/pages/recover/step5/step5.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], Step5Component);



/***/ }),

/***/ "./src/app/pages/recover/step6/step6.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/pages/recover/step6/step6.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlY292ZXIvc3RlcDYvc3RlcDYuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/pages/recover/step6/step6.component.ts":
/*!********************************************************!*\
  !*** ./src/app/pages/recover/step6/step6.component.ts ***!
  \********************************************************/
/*! exports provided: Step6Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Step6Component", function() { return Step6Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");










let Step6Component = class Step6Component extends _base_page__WEBPACK_IMPORTED_MODULE_5__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.oldPassword = '';
        this.password = '';
        this.passwordRepeat = '';
        this.token = '';
    }
    ngOnInit() {
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.TEMPORARY_PASSWORD).then(success => {
            this.oldPassword = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].RECOVER_PASSWORD.TOKEN).then(success => {
            this.token = success;
        });
    }
    nextStep() {
        if (this.password.length < 8 || this.password.length > 15
            || this.password !== this.passwordRepeat
            || !this.lowercaseValidator(this.password)
            || !this.uppercaseValidator(this.password)
            || !this.twoNumberValidator(this.password)
            || !this.specialCharacterValidator(this.password)) {
            return;
        }
        else {
            this.showProgress();
            this.services.passwordUpdate(this.oldPassword, btoa(this.password), this.token).then((success) => {
                this.dismissProgress();
                this.showAlert(success.response, () => {
                    this.goLoginPage();
                });
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
        }
    }
    lowercaseValidator(c) {
        const regex = /[a-z]/g;
        return regex.test(c);
    }
    uppercaseValidator(c) {
        const regex = /[A-Z]/g;
        return regex.test(c);
    }
    twoNumberValidator(c) {
        const numbers = '0123456789';
        let count = 0;
        for (let i = 0; i < c.length; i++) {
            if (numbers.indexOf(c.charAt(i), 0) !== -1) {
                count++;
            }
        }
        return count >= 2;
    }
    specialCharacterValidator(c) {
        return c.match('^[A-z0-9]+$');
    }
};
Step6Component.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
Step6Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'recover-step6',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./step6.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/recover/step6/step6.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./step6.component.scss */ "./src/app/pages/recover/step6/step6.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], Step6Component);



/***/ })

}]);
//# sourceMappingURL=pages-recover-recover-module-es2015.js.map