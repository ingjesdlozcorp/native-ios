(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-register-register-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/guest/guest.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/guest/guest.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-c-icon.png\">\n                                C&oacute;digo de seguridad\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                    Introduce el c&oacute;digo de seis d&iacute;gitos que hemos enviado a su n&uacute;mero de teléfono y/o correo electrónico asociado a su cuenta.\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"basicrow rel\">\n                        <input type=\"tel\"\n                               maxlength=\"6\"\n                               inputmode=\"numeric\"\n                               pattern=\"^[0-9]*$\"\n                               placeholder=\"Código de confirmación\"\n                               class=\"inp-f disc\"\n                               [(ngModel)]=\"code\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"btns red vcenter rippleR\" (click)=\"nextStep()\">\n                        <div class=\"tabcell\">\n                            Confirmar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top f-reg f-black roboto-r text-center\">\n                    &#191;No has recibido el c&oacute;digo de seguridad&#63; <a (click)=\"resend()\" class=\"linkdefs\">Reenviar</a>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <router-outlet></router-outlet>\n\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step1/step1.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step1/step1.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                Registro\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"basicrow\">\n                        <input type=\"number\"\n                               placeholder=\"N&uacute;mero de Tel&eacute;fono\"\n                               class=\"inp-f\"\n                               autocapitalize=\"off\"\n                               [(ngModel)]=\"number\"\n                               (keyup.enter)=\"nextStep()\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"btns red vcenter rippleR\" (click)=\"nextStep()\">\n                        <div class=\"tabcell\">\n                            Continuar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step2/step2.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step2/step2.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\" style=\"overflow-y: auto\">\n\n    <app-header-static></app-header-static>\n\n    <app-popup-general-terms\n            *ngIf='showTerms'\n            (close)=\"showTerms = false\">\n    </app-popup-general-terms>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon\" src=\"assets/images/reg-a-icon.png\">\n                                Registro\n                            </div>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"basicrow rel\">\n                        <img alt=\"\" [popper]=\"'PIN a tu correo electrónico.'\"\n                             [popperTrigger]=\"'click'\"\n                             [popperTimeoutAfterShow]=\"3000\"\n                             class=\"q-t-pay\" src=\"assets/images/tooltip1.png\">\n                        <input type=\"tel\"\n                               maxlength=\"6\"\n                               inputmode=\"numeric\"\n                               pattern=\"^[0-9]*$\"\n                               placeholder=\"Código de confirmación\"\n                               class=\"inp-f disc\"\n                               [(ngModel)]=\"code\">\n                        <div class=\"basicrow m-top-u f-reg f-black roboto-r text-center\">\n                            &#191;No has recibido el c&oacute;digo de seguridad&#63; <a (click)=\"resend()\" class=\"linkdefs\">Reenviar</a>\n                        </div>\n                    </div>\n                </div>\n\n                <div *ngIf=\"!isPrepaid\" class=\"basicrow m-top-i\">\n                    <div class=\"basicrow\">\n                        <input type=\"tel\"\n                               maxlength=\"4\"\n                               inputmode=\"numeric\"\n                               pattern=\"^[0-9]*$\"\n                               placeholder=\"4 &uacute;ltimos de SSN\"\n                               class=\"inp-f disc\"\n                               [(ngModel)]=\"ssn\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"basicrow\">\n                        <input type=\"email\"\n                               placeholder=\"Correo Electr&oacute;nico\"\n                               class=\"inp-f\"\n                               [(ngModel)]=\"email\">\n                    </div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a class=\"linkdefs\" (click)=\"showTerms = true\">T&eacute;rminos y Condiciones</a><br/>\n                    <div>\n                        <label class=\"label-terms\">\n                            <input type=\"checkbox\" [(ngModel)]=\"check\">\n                            &nbsp; He le&iacute;do, entiendo y acepto estos t&eacute;rminos y condiciones\n                        </label>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"btns red vcenter rippleR\" (click)=\"nextStep()\">\n                        <div class=\"tabcell\">\n                            Continuar\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"basicrow m-top-i\">\n                    <div class=\"logline full\"></div>\n                </div>\n\n                <div class=\"basicrow text-center m-top-i\">\n                    <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                </div>\n\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step3/step3.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step3/step3.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"allcont logsize\">\n\n    <app-header-static></app-header-static>\n\n    <div class=\"basicrow\">\n        <div class=\"container\">\n            <div class=\"basicrow mbott text-center\">\n                <div class=\"shortcont\">\n                    <div class=\"basicrow m-top\">\n                        <div class=\"basicrow f-btitle text-center\">\n                            <div class=\"basicrow\">\n                                <img alt=\"\" width=\"100%\" class=\"mc-icon last\" src=\"assets/images/reg-c-icon.png\">\n                                Escoja su contrase&ntilde;a de acceso\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top f-reg f-black roboto-r text-justify\">\n                        La contrase&ntilde;a de acceso es la clave con la que podr&aacute; acceder a manejar su cuenta de servicios en Claro\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"password\" placeholder=\"Contrase&ntilde;a\" class=\"inp-f\" maxlength=\"15\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"basicrow\">\n                            <input type=\"password\" [(ngModel)]=\"passwordRepeat\" placeholder=\"Confirmar Contrase&ntilde;a\" class=\"inp-f\" maxlength=\"15\" (keyup.enter)=\"nextStep()\">\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow f-black m-top roboto-r\">\n                        <div class=\"basicrow f-lmini roboto-b\">\n                            Requerimientos de contrase&ntilde;a:\n                        </div>\n\n                        <div class=\"basicrow\">\n                            <ul class=\"redb\">\n                                <li [ngClass]=\"{'done': (password.length >= 8 && password.length <= 16)}\">\n                                    Debe tener entre 8 y 15 caracteres\n                                </li>\n\n                                <li [ngClass]=\"{'done': lowercaseValidator(password)}\">\n                                    Al menos una letra min&uacute;scula\n                                </li>\n\n                                <li [ngClass]=\"{'done': uppercaseValidator(password)}\">\n                                    Al menos una letra may&uacute;scula\n                                </li>\n\n                                <li [ngClass]=\"{'done': twoNumberValidator(password)}\">\n                                    Al menos 2 n&uacute;meros\n                                </li>\n\n                                <li [ngClass]=\"{'done': specialCharacterValidator(password)}\">\n                                    No est&aacute; permitido ning&uacute;n car&aacute;cter especial o no alfanum&eacute;rico\n                                </li>\n\n                                <li [ngClass]=\"{'done': password === passwordRepeat && password.length > 0}\">\n                                    Las contrase&ntilde;as deben coincidir\n                                </li>\n                            </ul>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div (click)=\"nextStep()\" [className]=\"\n                        (password.length >= 8 && password.length <= 16)\n                         && lowercaseValidator(password)\n                         && uppercaseValidator(password)\n                         && twoNumberValidator(password)\n                         && specialCharacterValidator(password)\n                         && (password === passwordRepeat && password.length > 0)\n                         ? 'btns vcenter red rippleR' : 'btns vcenter gray'\">\n                            <div class=\"tabcell\">\n                                Continuar\n                            </div>\n                        </div>\n                    </div>\n\n                    <div class=\"basicrow m-top-i\">\n                        <div class=\"logline full\"></div>\n                    </div>\n\n                    <div class=\"basicrow text-center m-top-i\">\n                        <a (click)=\"goLoginPage()\" class=\"linkdefs\">Volver al inicio de sesi&oacute;n</a>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n</div>\n");

/***/ }),

/***/ "./src/app/pages/register/guest/guest.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/register/guest/guest.component.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZ2lzdGVyL2d1ZXN0L2d1ZXN0LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/register/guest/guest.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/register/guest/guest.component.ts ***!
  \*********************************************************/
/*! exports provided: GuestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuestComponent", function() { return GuestComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");










let GuestComponent = class GuestComponent extends _base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
    }
    ngOnInit() {
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER).then(success => {
            this.number = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_GUEST_UPDATE).then(success => {
            this.isUpdate = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.TOKEN).then(success => {
            this.token = success;
        });
    }
    nextStep() {
        if (this.code.length !== 6) {
            this.showError('Debe ingresar los datos solicitados.');
        }
        else {
            if (this.isUpdate) {
                this.updateGuest();
            }
            else {
                this.validateGuest();
            }
        }
    }
    validateGuest() {
        this.showProgress();
        this.services.validateGuest(this.number, this.code, this.token).then((success) => {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.SUBSCRIBER, String(this.number));
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.IS_LOGGED, true);
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.IS_GUEST, true);
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.BIOMETRIC, false);
            this.dismissProgress();
            this.showAlert('Su registro como usuario invitado fue realizado exitosamente.', () => {
                this.goPage('/guest');
            });
        }, error => {
            this.dismissProgress();
            this.showError(error.message);
        });
    }
    updateGuest() {
        this.showProgress();
        this.services.updateGuest(this.number, this.code).then((success) => {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.SUBSCRIBER, String(this.number));
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.IS_LOGGED, true);
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.IS_GUEST, true);
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].LOGIN.BIOMETRIC, false);
            this.dismissProgress();
            this.goPage('/guest');
        }, error => {
            this.dismissProgress();
            this.showError(error.message);
        });
    }
    resend() {
        this.showProgress();
        this.services.resendGuestCode(this.number).then((success) => {
            this.dismissProgress();
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER, String(this.number));
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.TOKEN, success.token);
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_GUEST_UPDATE, true);
            this.showAlert('Su código de verificación ha sido enviado nuevamente.');
        }, error => {
            this.dismissProgress();
            this.showError(error.message);
        });
    }
};
GuestComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
GuestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'register-guest',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./guest.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/guest/guest.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./guest.component.scss */ "./src/app/pages/register/guest/guest.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], GuestComponent);



/***/ }),

/***/ "./src/app/pages/register/register.component.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/register/register.component.ts ***!
  \******************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let RegisterComponent = class RegisterComponent {
    constructor() { }
    ngOnInit() {
    }
};
RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-register',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./register.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.component.html")).default
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], RegisterComponent);



/***/ }),

/***/ "./src/app/pages/register/register.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/register/register.module.ts ***!
  \***************************************************/
/*! exports provided: routes, RegisterModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterModule", function() { return RegisterModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_shared/shared.module */ "./src/app/pages/_shared/shared.module.ts");
/* harmony import */ var _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./step1/step1.component */ "./src/app/pages/register/step1/step1.component.ts");
/* harmony import */ var _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./step2/step2.component */ "./src/app/pages/register/step2/step2.component.ts");
/* harmony import */ var _step3_step3_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./step3/step3.component */ "./src/app/pages/register/step3/step3.component.ts");
/* harmony import */ var _register_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./register.component */ "./src/app/pages/register/register.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var ngx_popper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-popper */ "./node_modules/ngx-popper/fesm2015/ngx-popper.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _guest_guest_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./guest/guest.component */ "./src/app/pages/register/guest/guest.component.ts");












const routes = [
    { path: 'guest', component: _guest_guest_component__WEBPACK_IMPORTED_MODULE_11__["GuestComponent"] },
    { path: 'step1', component: _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__["Step1Component"] },
    { path: 'step2', component: _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__["Step2Component"] },
    { path: 'step3', component: _step3_step3_component__WEBPACK_IMPORTED_MODULE_6__["Step3Component"] }
];
let RegisterModule = class RegisterModule {
};
RegisterModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [
            _register_component__WEBPACK_IMPORTED_MODULE_7__["RegisterComponent"],
            _guest_guest_component__WEBPACK_IMPORTED_MODULE_11__["GuestComponent"],
            _step1_step1_component__WEBPACK_IMPORTED_MODULE_4__["Step1Component"],
            _step2_step2_component__WEBPACK_IMPORTED_MODULE_5__["Step2Component"],
            _step3_step3_component__WEBPACK_IMPORTED_MODULE_6__["Step3Component"]
        ],
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_10__["RouterModule"].forChild(routes),
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_3__["SharedModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
            ngx_popper__WEBPACK_IMPORTED_MODULE_9__["NgxPopperModule"].forRoot({ placement: 'top', styles: { 'background-color': 'white' } })
        ]
    })
], RegisterModule);



/***/ }),

/***/ "./src/app/pages/register/step1/step1.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/register/step1/step1.component.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZ2lzdGVyL3N0ZXAxL3N0ZXAxLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/register/step1/step1.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/register/step1/step1.component.ts ***!
  \*********************************************************/
/*! exports provided: Step1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Step1Component", function() { return Step1Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");










let Step1Component = class Step1Component extends _base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.utils.registerScreen('register');
    }
    ngOnInit() { }
    nextStep() {
        if (String(this.number).length !== 10) {
            this.showError('Debe ingresar un número de suscriptor válido.');
        }
        else {
            this.showProgress();
            this.services.validateSubscriber(String(this.number)).then((success) => {
                this.dismissProgress();
                this.processResponse(success);
            }, error => {
                this.dismissProgress();
                if (error.message.includes('cuenta ya existente')) {
                    error.message = 'Hemos detectado que está intentando registrar una cuenta ya existente en nuestro sistema. Por favor presione la opción ' +
                        '<b>Olvido su Contraseña</b> para recuperar su acceso al sistema';
                    this.showConfirmCustom('Aviso', error.message, 'Olvido su Contraseña', 'Cerrar', () => {
                        this.goPage('recover/step1');
                    });
                }
                else {
                    this.showError(error.message);
                }
            });
        }
    }
    processResponse(response) {
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER, String(this.number));
        if ((response.accountType === 'I' && response.accountSubType === 'P') ||
            (response.accountType === 'I3' && response.accountSubType === 'P')) {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_PREPAID, true);
        }
        else {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_PREPAID, false);
        }
        if ((response.accountType === 'I2' && response.accountSubType === '4') ||
            (response.accountType === 'I' && response.accountSubType === 'R') ||
            (response.accountType === 'I' && response.accountSubType === '4') ||
            (response.accountType === 'I' && response.accountSubType === 'E')) {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_POSTPAID, true);
        }
        else {
            this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_POSTPAID, false);
        }
        this.goPage('/register/step2');
    }
};
Step1Component.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
Step1Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'register-step1',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./step1.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step1/step1.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./step1.component.scss */ "./src/app/pages/register/step1/step1.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], Step1Component);



/***/ }),

/***/ "./src/app/pages/register/step2/step2.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/register/step2/step2.component.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZ2lzdGVyL3N0ZXAyL3N0ZXAyLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/register/step2/step2.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/register/step2/step2.component.ts ***!
  \*********************************************************/
/*! exports provided: Step2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Step2Component", function() { return Step2Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _utils_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../utils/utils */ "./src/app/utils/utils.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");











let Step2Component = class Step2Component extends _base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.code = '';
        this.ssn = '';
        this.email = '';
        this.subscriber = '';
        this.showTerms = false;
    }
    ngOnInit() {
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER).then(success => {
            this.subscriber = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_PREPAID).then(success => {
            this.isPrepaid = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.IS_POSTPAID).then(success => {
            this.isPostpaid = success;
        });
    }
    nextStep() {
        if (this.code.length !== 6) {
            this.showError('Debe ingresar los datos solicitados.');
        }
        else if (!this.isPrepaid && this.ssn.length !== 4) {
            this.showError('Debe ingresar los datos solicitados.');
        }
        else if (this.email.length === 0) {
            this.showError('Debe ingresar los datos solicitados.');
        }
        else if (!_utils_utils__WEBPACK_IMPORTED_MODULE_9__["Utils"].validateEmail(this.email)) {
            this.showError('Debe ingresar un correo electrónico válido.');
        }
        else if (!this.check) {
            this.showError('Debe seleccionar los términos y condiciones para poder continuar.');
        }
        else {
            if (this.isPrepaid) {
                this.ssn = this.code;
            }
            this.showProgress();
            this.services.validateSSNAndEmail(this.subscriber, this.code, this.ssn, this.email).then((success) => {
                this.dismissProgress();
                this.processResponse(success);
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
        }
    }
    processResponse(response) {
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER, this.subscriber);
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.EMAIL, this.email);
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SSN, this.ssn);
        this.store(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.CODE, this.code);
        this.goPage('/register/step3');
    }
    resend() {
        this.showProgress();
        this.services.validateSubscriber(this.subscriber).then((success) => {
            this.dismissProgress();
            this.showAlert(success.errorDisplay);
        }, error => {
            if (error.includes('href="http://miclaroreferals.claroinfo.com/forgotpassword')) {
                error = error.replace('id="forgotpassword" target="_blank" href="http://miclaroreferals.claroinfo.com/forgotpassword"', 'href="/recover/step1"');
            }
            if (error.includes('href="https://miclaro.claropr.com/forgotpassword')) {
                error = error.replace('id="forgotpassword" target="_blank" href="https://miclaro.claropr.com/forgotpassword"', 'href="/recover/step1"');
            }
            this.dismissProgress();
            this.showError(error.message);
        });
    }
};
Step2Component.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_10__["IntentProvider"] }
];
Step2Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'register-step2',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./step2.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step2/step2.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./step2.component.scss */ "./src/app/pages/register/step2/step2.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_10__["IntentProvider"]])
], Step2Component);



/***/ }),

/***/ "./src/app/pages/register/step3/step3.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/register/step3/step3.component.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZ2lzdGVyL3N0ZXAzL3N0ZXAzLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/pages/register/step3/step3.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/register/step3/step3.component.ts ***!
  \*********************************************************/
/*! exports provided: Step3Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Step3Component", function() { return Step3Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base.page */ "./src/app/pages/base.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_services_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/services.provider */ "./src/app/services/services.provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _utils_const_keys__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/const/keys */ "./src/app/utils/const/keys.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_utils_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/utils.service */ "./src/app/services/utils.service.ts");
/* harmony import */ var _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/intent.provider */ "./src/app/services/intent.provider.ts");










let Step3Component = class Step3Component extends _base_page__WEBPACK_IMPORTED_MODULE_2__["BasePage"] {
    constructor(router, storage, modelsServices, alertController, utilsService, userStorage) {
        super(router, storage, modelsServices, alertController, utilsService, userStorage);
        this.password = '';
        this.passwordRepeat = '';
        this.subscriber = '';
        this.email = '';
        this.code = '';
        this.ssn = '';
    }
    ngOnInit() {
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SUBSCRIBER).then(success => {
            this.subscriber = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.EMAIL).then(success => {
            this.email = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.CODE).then(success => {
            this.code = success;
        });
        this.fetch(_utils_const_keys__WEBPACK_IMPORTED_MODULE_6__["keys"].REGISTER.SSN).then(success => {
            this.ssn = success;
        });
    }
    nextStep() {
        if (this.password.length < 8 || this.password.length > 15
            || this.password !== this.passwordRepeat
            || !this.lowercaseValidator(this.password)
            || !this.uppercaseValidator(this.password)
            || !this.twoNumberValidator(this.password)
            || !this.specialCharacterValidator(this.password)) {
            return;
        }
        else {
            this.showProgress();
            this.services.validatePassword(this.subscriber, this.code, this.ssn, this.email, this.password).then((success) => {
                this.dismissProgress();
                this.clearStore();
                this.showAlert('Has sido registrado con Exito!', () => {
                    this.goLoginPage();
                });
            }, error => {
                this.dismissProgress();
                this.showError(error.message);
            });
        }
    }
    lowercaseValidator(c) {
        const regex = /[a-z]/g;
        return regex.test(c);
    }
    uppercaseValidator(c) {
        const regex = /[A-Z]/g;
        return regex.test(c);
    }
    twoNumberValidator(c) {
        const numbers = '0123456789';
        let count = 0;
        for (let i = 0; i < c.length; i++) {
            if (numbers.indexOf(c.charAt(i), 0) !== -1) {
                count++;
            }
        }
        return count >= 2;
    }
    specialCharacterValidator(c) {
        return c.match('^[A-z0-9]+$');
    }
};
Step3Component.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"] },
    { type: _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"] }
];
Step3Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'register-step3',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./step3.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/step3/step3.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./step3.component.scss */ "./src/app/pages/register/step3/step3.component.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _services_services_provider__WEBPACK_IMPORTED_MODULE_4__["ServicesProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_utils_service__WEBPACK_IMPORTED_MODULE_8__["UtilsService"],
        _services_intent_provider__WEBPACK_IMPORTED_MODULE_9__["IntentProvider"]])
], Step3Component);



/***/ })

}]);
//# sourceMappingURL=pages-register-register-module-es2015.js.map