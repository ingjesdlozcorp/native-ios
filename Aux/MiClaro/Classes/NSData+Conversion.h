//
//  NSData+Conversion.h
//  MiOpenMobile
//
//  Created by Daniel Thompson on 8/7/13.
//
//

#import <Foundation/Foundation.h>

@interface NSData (NSData_Conversion)

#pragma mark - String Conversion
- (NSString *)hexadecimalString;

@end
