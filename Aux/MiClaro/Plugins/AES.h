//
//  AES.h
//  Mi Claro
//
//  Created by Daniel Thompson on 8/7/13.
//
//

#import <Cordova/CDVPlugin.h>

@interface AES : CDVPlugin

- (void)encryp:(CDVInvokedUrlCommand*)command;

@end
